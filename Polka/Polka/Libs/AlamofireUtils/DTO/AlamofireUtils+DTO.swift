//
//  AlamofireUtils+DTO.swift
//  Vegcard
//
//  Created by Vitaly Trofimov on 19/10/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation

import Alamofire




protocol DTOField: Hashable, RawRepresentable where RawValue == String
{
}

protocol DTO: ParametersConvertible
{
	associatedtype Fields: DTOField
	
	var values: [Fields: Any] { get }
}



// MARK: - ParametersConvertible

extension DTO
{
	var asParameters: Parameters
	{
		return Dictionary(
			uniqueKeysWithValues: values.map { ($0.key.rawValue, $0.value) }
		)
	}
}
