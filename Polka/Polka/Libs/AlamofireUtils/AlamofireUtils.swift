//
//  AlamofireUtils.swift
//
//  Created by Vitaly Trofimov on 07/10/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation

import Alamofire
import PromiseKit




struct AlamofireUtils
{
	typealias ParseErrors = (_ data: Data?, _ response: HTTPURLResponse) -> Error?
	typealias Cancel = () -> Void
	
	static var loggingEnabled = true
}



// MARK: - Parameters

protocol ParametersConvertible
{
	var asParameters: Parameters { get }
}

extension AlamofireUtils
{
	enum EncodedParameters
	{
		case url(values: ParametersConvertible)
		case json(values: ParametersConvertible)
	}
}

extension Parameters: ParametersConvertible
{
	var asParameters: Parameters
	{
		return self
	}
}



// MARK: - Response parsing

extension AlamofireUtils
{
	static let defaultResponseParsingQueue: DispatchQueue = .global(qos: .default)
	
	class ParseResponse<ResultType>
	{
		let queue: DispatchQueue
		
		typealias Parse<ResultType> = (_ data: Data, _ response: HTTPURLResponse) -> ResultType?
		private let parse: Parse<ResultType>
		
		init(
			on queue: DispatchQueue = defaultResponseParsingQueue,
			_ parse: @escaping Parse<ResultType>
			)
		{
			self.queue = queue
			self.parse = parse
		}
		
		subscript(data: Data, response: HTTPURLResponse) -> ResultType?
		{
			return parse(data, response)
		}
	}
	
	class ParseResponseWithExpectedStatuses: ParseResponse<Void>
	{
		init(
			_ statuses: Int...,
			on queue: DispatchQueue = defaultResponseParsingQueue
			)
		{
			super.init(on: queue) { (_, response) -> Void? in
				//
				if statuses.contains(response.statusCode)
				{
					return ()
				}
				
				return nil
			}
		}
	}
}



// MARK: - Errors

extension AlamofireUtils
{
	struct Errors
	{
		static let domain = "AlamofireUtilsErrorDomain"
		
		static let authorizationHandlerRequired = NSError(
			domain: domain,
			code: 1,
			userInfo: [NSLocalizedDescriptionKey: "Authorization handler required"]
		)
		static let authorizationNotAvailable = NSError(
			domain: domain,
			code: 2,
			userInfo: [NSLocalizedDescriptionKey: "Authorization not available"]
		)
		static let authorizationRefreshNotAvailable = NSError(
			domain: domain,
			code: 3,
			userInfo: [NSLocalizedDescriptionKey: "Authorization refresh not available"]
		)
		static let invalidResponse = NSError(
			domain: domain,
			code: 4,
			userInfo: [NSLocalizedDescriptionKey: "Invalid response"]
		)
	}
}



// MARK: - Performing requests

extension SessionManager
{
	func performRequest<ResultType>(
		_ method: HTTPMethod,
		_ url: URLConvertible,
		parameters: AlamofireUtils.EncodedParameters? = nil,
		headers: HTTPHeaders? = nil,
		authorizationOptions: AlamofireUtils.AuthorizationOptions = .auto,
		parseResponse: AlamofireUtils.ParseResponse<ResultType>,
		parseErrors: AlamofireUtils.ParseErrors? = nil,
		onDownloadProgressUpdated: Request.ProgressHandler? = nil,
		cancel: UnsafeMutablePointer<AlamofireUtils.Cancel?>? = nil
		) -> Promise<ResultType>
	{
		return performDataRequest(
			prepareRequest: { () -> Promise<DataRequest> in
				//
				let (parametersDictionary, parametersEncoding) = AlamofireUtils.convertParameters(parameters)
				let dataRequest = request(
					url,
					method: method,
					parameters: parametersDictionary,
					encoding: parametersEncoding,
					headers: httpHeadersByAddingAuthorizationOptions(
						headers,
						authorizationOptions
					)
				)

				if let onDownloadProgressUpdated = onDownloadProgressUpdated
				{
					dataRequest.downloadProgress(
						closure: onDownloadProgressUpdated
					)
				}

				if let error = dataRequest.delegate.error
				{
					AlamofireUtils.logRequestFail(
						with: error,
						method: method,
						url: url
					)
				}
				else
				{
					AlamofireUtils.logDataRequest(dataRequest)
				}
				
				return .value(dataRequest)
			},
			authorizationOptions: authorizationOptions,
			parseResponse: parseResponse,
			parseErrors: parseErrors,
			cancel: cancel
		)
	}
	
	func performRequest<ResultType>(
		_ method: HTTPMethod,
		_ url: URLConvertible,
		parameters: Any,
		headers: HTTPHeaders? = nil,
		authorizationOptions: AlamofireUtils.AuthorizationOptions = .auto,
		parseResponse: AlamofireUtils.ParseResponse<ResultType>,
		parseErrors: AlamofireUtils.ParseErrors? = nil,
		onDownloadProgressUpdated: Request.ProgressHandler? = nil,
		cancel: UnsafeMutablePointer<AlamofireUtils.Cancel?>? = nil
		) -> Promise<ResultType>
	{
		return performDataRequest(
			prepareRequest: { () -> Promise<DataRequest> in
				
				let rawRequest  = try! URLRequest(url: url, method: method, headers: httpHeadersByAddingAuthorizationOptions(
					headers,
					   authorizationOptions
				   ))
					
				let requestWithEncodedParameters =  try! JSONEncoding().encode(rawRequest, withJSONObject: parameters)
				
				let dataRequest = request(requestWithEncodedParameters)

				if let onDownloadProgressUpdated = onDownloadProgressUpdated
				{
					dataRequest.downloadProgress(
						closure: onDownloadProgressUpdated
					)
				}

				if let error = dataRequest.delegate.error
				{
					AlamofireUtils.logRequestFail(
						with: error,
						method: method,
						url: url
					)
				}
				else
				{
					AlamofireUtils.logDataRequest(dataRequest)
				}
				
				return .value(dataRequest)
			},
			authorizationOptions: authorizationOptions,
			parseResponse: parseResponse,
			parseErrors: parseErrors,
			cancel: cancel
		)
	}
	
    func performUpload<ResultType>(
        _ method: HTTPMethod,
		_ url: URLConvertible,
        file: MultipartFile,
        headers: HTTPHeaders? = nil,
        authorizationOptions: AlamofireUtils.AuthorizationOptions = .auto,
		parseResponse: AlamofireUtils.ParseResponse<ResultType>,
        parseErrors: AlamofireUtils.ParseErrors? = nil,
		onUploadProgressUpdated: Request.ProgressHandler? = nil,
        cancel: UnsafeMutablePointer<AlamofireUtils.Cancel?>? = nil
        ) -> Promise<ResultType>
    {
		return performDataRequest(
			prepareRequest: { () -> Promise<DataRequest> in
				//
				return Promise(resolver: { resolver in
					//
					upload(
						multipartFormData: { multipartData in
							//
							multipartData.append(
								file.data,
								withName: file.key,
								fileName: file.filename,
								mimeType: file.mimeType
							)
						},
						to: url,
						method: method,
						headers: httpHeadersByAddingAuthorizationOptions(
							headers,
							authorizationOptions
						),
						encodingCompletion: { encodingResult in
							//
							switch encodingResult
							{
							case .failure(let error):
								//
								AlamofireUtils.logRequestFail(
									with: error,
									method: method,
									url: url
								)
								
								resolver.reject(error)
							
							case .success(let uploadRequest, _, _):
								//
								if let error = uploadRequest.delegate.error
								{
									AlamofireUtils.logRequestFail(
										with: error,
										method: method,
										url: url
									)
									
									resolver.reject(error)
								}
								else
								{
									if let onUploadProgressUpdated = onUploadProgressUpdated
									{
										uploadRequest.uploadProgress(
											closure: onUploadProgressUpdated
										)
									}
									
									AlamofireUtils.logUploadRequest(uploadRequest)
									
									resolver.fulfill(uploadRequest)
								}
							}
						}
					)
				})
			},
			authorizationOptions: authorizationOptions,
			parseResponse: parseResponse,
			parseErrors: parseErrors,
			cancel: cancel
		)
    }

	func performUpload<ResultType>(
		_ method: HTTPMethod,
		_ url: URLConvertible,
		file: MultipartUrlFile,
		headers: HTTPHeaders? = nil,
		authorizationOptions: AlamofireUtils.AuthorizationOptions = .auto,
		parseResponse: AlamofireUtils.ParseResponse<ResultType>,
		parseErrors: AlamofireUtils.ParseErrors? = nil,
		onUploadProgressUpdated: Request.ProgressHandler? = nil,
		cancel: UnsafeMutablePointer<AlamofireUtils.Cancel?>? = nil
		) -> Promise<ResultType>
	{
		return performDataRequest(
			prepareRequest: { () -> Promise<DataRequest> in
				//
				return Promise(resolver: { resolver in
					//
					upload(
						multipartFormData: { multipartData in
							//
							multipartData.append(
								file.url,
								withName: file.name
							)
						},
						to: url,
						method: method,
						headers: httpHeadersByAddingAuthorizationOptions(
							headers,
							authorizationOptions
						),
						encodingCompletion: { encodingResult in
							//
							switch encodingResult
							{
							case .failure(let error):
								//
								AlamofireUtils.logRequestFail(
									with: error,
									method: method,
									url: url
								)

								resolver.reject(error)

							case .success(let uploadRequest, _, _):
								//
								if let error = uploadRequest.delegate.error
								{
									AlamofireUtils.logRequestFail(
										with: error,
										method: method,
										url: url
									)

									resolver.reject(error)
								}
								else
								{
									if let onUploadProgressUpdated = onUploadProgressUpdated
									{
										uploadRequest.uploadProgress(
											closure: onUploadProgressUpdated
										)
									}

									AlamofireUtils.logUploadRequest(uploadRequest)

									resolver.fulfill(uploadRequest)
								}
							}
						}
					)
				})
			},
			authorizationOptions: authorizationOptions,
			parseResponse: parseResponse,
			parseErrors: parseErrors,
			cancel: cancel
		)
	}
	
	private func performDataRequest<ResultType>(
		prepareRequest: () -> Promise<DataRequest>,
        authorizationOptions: AlamofireUtils.AuthorizationOptions,
        parseResponse: AlamofireUtils.ParseResponse<ResultType>,
        parseErrors: AlamofireUtils.ParseErrors?,
        cancel: UnsafeMutablePointer<AlamofireUtils.Cancel?>?
        ) -> Promise<ResultType>
    {
        if authorizationOptions == .authorize,
            authorizationHandler == nil
        {
            return Promise(error: AlamofireUtils.Errors.authorizationHandlerRequired)
        }
		
		let cancellationToken = AlamofireUtils.CancellationToken()
		cancel?.pointee = cancellationToken.cancel
		
		let promise = prepareRequest().then { request -> Promise<ResultType> in
			//
			guard !cancellationToken.isCancelled
			else
			{
				throw PMKError.cancelled
			}
			
			cancellationToken.request = request
			
            return request.validate { (_, response, data) -> Request.ValidationResult in
				//
				return parseErrors?(data, response)
					.map { .failure($0) }
					?? .success
			}
			.responseData().tap { result in
				//
				if request.request != nil,
					request.response != nil
					// or else request failed and thus was already logged
				{
					AlamofireUtils.logResponse(
						to: request,
						with: result
					)
				}
			}
			.map(on: parseResponse.queue) { (data, response) -> ResultType in
				//
				guard !cancellationToken.isCancelled
				else
				{
					throw PMKError.cancelled
				}
				
				guard let response = response.response,
					let result = parseResponse[data, response]
				else
				{
					throw AlamofireUtils.Errors.invalidResponse
				}

				return result
			}
			.get { _ in
				//
				guard !cancellationToken.isCancelled
				else
				{
					throw PMKError.cancelled
				}
			}
        }

        return promise
    }
}



// MARK: - Converting parameters

private extension AlamofireUtils
{
	static func convertParameters(_ parameters: EncodedParameters?) -> (Parameters?, ParameterEncoding)
	{
		if let parameters = parameters
		{
			switch parameters
			{
			case let .json(values):
				return (
					values.asParameters,
					JSONEncoding.default
				)
			case let .url(values):
				return (
					values.asParameters,
					URLEncoding.default
				)
			}
		}
		else
		{
			return (nil, URLEncoding.default)
		}
	}
}



// MARK: - Cancellation

private extension AlamofireUtils
{
	class CancellationToken
	{
		var request: Request? = nil
		{
			didSet
			{
				if let request = request,
					isCancelled
				{
					AlamofireUtils.logCancellation(of: request)
				}
			}
		}
		
		private(set) var isCancelled: Bool = false
		
		func cancel()
		{
			isCancelled = true
			
			request?.cancel()
			
			if let request = request
			{
				AlamofireUtils.logCancellation(of: request)
			}
		}
	}
}



// MARK: - Logging

private extension AlamofireUtils
{
	static func logRequestFail(
		with error: Error,
		method: HTTPMethod,
		url: URLConvertible
		)
	{
		let message = """
			Request failed with error:
			\(error)
			"""
		log(
			message,
			withHeader: logHeader(
				withMethod: method.rawValue,
				url: "\(url)",
				type: .fail
			)
		)
	}
	
	static func logDataRequest(_ request: DataRequest)
	{
		let body = request.request!.httpBody
			.flatMap { String(data: $0, encoding: .utf8) }
			?? absentValue
		let message = """
			Body:
			\(body)
			"""
		log(
			request,
			message,
			type: .request
		)
	}
	
	static func logUploadRequest(_ request: UploadRequest)
	{
		log(
			request,
			"Upload",
			type: .request
		)
	}
	
	static func logResponse(
		to request: Request,
		with result: PromiseKit.Result<(data: Data, response: PMKAlamofireDataResponse)>
		)
	{
		switch result
		{
		case let .fulfilled((data, response)):
			logResponse(
				to: request,
				with: data,
				response: response
			)
		case let .rejected(error):
			logResponse(
				to: request,
				with: error
			)
		}
	}
	
	static func logResponse(
		to request: Request,
		with data: Data,
		response: PMKAlamofireDataResponse
		)
	{
		let body = data.count == 0
			? absentValue
			: String(data: data, encoding: .utf8)
				?? "<\(data.count) bytes>"
		var message = """
			Status \(response.response!.statusCode)
			
			Body:
			\(body)
			"""
		
		if let responseUrl = response.response?.url,
			responseUrl != request.request?.url
		{
			let redirectMessage = """
				Redirected to \(responseUrl.absoluteString)
				
				
				"""	// here must be one additional caret return at the end
			message = redirectMessage + message
		}
		
		log(
			request,
			message,
			type: .response
		)
	}
	
	static func logResponse(
		to request: Request,
		with error: Error
		)
	{
		let status = (request.response?.statusCode)
			.map { String($0) }
			?? absentValue
		let message = """
			Status \(status)
			
			Error:
			\(error)
			"""
		log(
			request,
			message,
			type: .error
		)
	}
	
	static func logCancellation(of request: Request)
	{
		log(
			request,
			"Cancelled",
			type: .cancel
		)
	}
}



// MARK: - Logging routines

private extension AlamofireUtils
{
	static let absentValue = "<null>"
	
	enum LogMessageType: String
	{
		case fail		= "→❌"
		case request	= "→🌐"
		case response	= "←🌐"
		case cancel		= "↩︎🌐"
		case error		= "⨯🌐"
		
		var indicator: String
		{
			return rawValue
		}
	}
	
	static func logHeader(
		for request: URLRequest,
		type: LogMessageType
		) -> String
	{
		return logHeader(
			withMethod: request.httpMethod!,
			url: request.url!.absoluteString,
			type: type
		)
	}
	
	static func logHeader(
		withMethod method: String,
		url: String,
		type: LogMessageType
		) -> String
	{
		return "\(type.indicator) \(method): \(url)"
	}
	
	static func log(
		_ request: Request,
		_ message: String,
		type: LogMessageType
		)
	{
		log(
			message,
			withHeader: logHeader(
				for: request.request!,
				type: type
			)
		)
	}
	
	static func log(
		_ message: String,
		withHeader header: String
		)
	{
		guard loggingEnabled
		else
		{
			return
		}
		
		let text = """
			
			-----
			
			\(header)
			
			\(message)
			
			-----
			
			"""
		print(text)
	}
}
