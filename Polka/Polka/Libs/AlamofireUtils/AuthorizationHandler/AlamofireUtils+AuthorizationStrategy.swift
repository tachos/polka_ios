//
//  AlamofireUtils+AuthorizationStrategy.swift
//
//  Created by Vitaly Trofimov on 01.04.2020.
//  Copyright © 2020 Tachos. All rights reserved.
//

import Foundation

import PromiseKit




// MARK: Single token authorization

extension AlamofireUtils
{
	/// Uses Token HTTP authentication scheme
	class TokenAuthorizationStrategy
	{
		let token: () -> String?
		
		init(token: @escaping @autoclosure () -> String?)
		{
			self.token = token
		}
	}
}

extension AlamofireUtils.TokenAuthorizationStrategy: AuthorizationHandlerStrategy
{
	func authorizeRequest(_ urlRequest: URLRequest) throws -> URLRequest?
	{
		guard let token = token()
		else
		{
			return nil
		}
		
		var urlRequest = urlRequest
		urlRequest.setValue(
			"Token \(token)",
			forHTTPHeaderField: "Authorization"
		)
		
		return urlRequest
	}
	
	func shouldRefreshAuthorization(on error: Error) -> Bool
	{
		return false
	}
	
	func refreshAuthorization() -> Promise<Void>?
	{
		return nil
	}
}
