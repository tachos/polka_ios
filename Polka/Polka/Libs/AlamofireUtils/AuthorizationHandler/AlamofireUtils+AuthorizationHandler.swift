//
//  AuthorizationHandler.swift
//
//  Created by Vitaly Trofimov on 01.11.2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation

import Alamofire
import PromiseKit




protocol AuthorizationHandlerStrategy
{
	func authorizeRequest(_ urlRequest: URLRequest) throws -> URLRequest?
	
	func shouldRefreshAuthorization(on error: Error) -> Bool
	func refreshAuthorization() -> Promise<Void>?
}




extension AlamofireUtils
{
	class AuthorizationHandler
	{
		private let strategy: AuthorizationHandlerStrategy
		
		private let authorizationRefreshLock = NSLock()
		private var authorizationRefreshPromise: Promise<Void>? = nil
		
		
		init(strategy: AuthorizationHandlerStrategy)
		{
			self.strategy = strategy
		}
	}
	
	enum AuthorizationOptions: String
	{
		/// Request authorization and authorization refresh will be disabled.
		case none
		
		/// If authorization handler is set and authorization is possible, request will be authorized. Authorization refresh will be enabled.
		case auto
		
		/// Request authorization will be forced, if authorization handler is not set or authorization is not possble, request will fail immediately. Authorization refresh will be disabled.
		case authorize
		
		/// Request authorization will be forced, if authorization handler is not set or authorization is not possble, request will fail immediately. Authorization refresh will be enabled.
		case authorizeAndEnableRefresh
	}
}



// MARK: - Accessing authorization handler

extension SessionManager
{
	func setAuthorizationHandler(withStrategy strategy: AuthorizationHandlerStrategy)
	{
		authorizationHandler = AlamofireUtils.AuthorizationHandler(strategy: strategy)
	}
	
	func removeAuthorizationHandler()
	{
		authorizationHandler = nil
	}
	
	private(set) var authorizationHandler: AlamofireUtils.AuthorizationHandler?
	{
		get
		{
			return adapter as? AlamofireUtils.AuthorizationHandler
		}
		set
		{
			adapter = newValue
			retrier = newValue
		}
	}
}



// MARK: - Accessing authorization options

private extension URLRequest
{
	static var AuthorizationOptionsHeader = "AlamofireUtils.AuthorizationOptions"
	
	var authorizationOptions: AlamofireUtils.AuthorizationOptions?
	{
		return value(forHTTPHeaderField: URLRequest.AuthorizationOptionsHeader)
			.flatMap { AlamofireUtils.AuthorizationOptions(rawValue: $0) }
	}
}

func httpHeadersByAddingAuthorizationOptions(
	_ headers: HTTPHeaders?,
	_ options: AlamofireUtils.AuthorizationOptions
	) -> HTTPHeaders
{
	var headers = headers ?? [:]
	headers[URLRequest.AuthorizationOptionsHeader] = options.rawValue
	
	return headers
}



// MARK: - Refreshing authorization

private extension Request
{
	var isAuthorizationRefreshEnabled: Bool
	{
		if let urlRequest = request,
			let authorizationOptions = urlRequest.authorizationOptions
		{
			switch authorizationOptions
			{
			case .none, .authorize:
				return false
			case .authorizeAndEnableRefresh, .auto:
				return true
			}
		}
		
		return false
	}
}

private extension AlamofireUtils.AuthorizationHandler
{
	func refreshAuthorization() -> Promise<Void>
	{
		authorizationRefreshLock.lock()
		defer {
			authorizationRefreshLock.unlock()
		}
		
		if let promise = authorizationRefreshPromise
		{
			return promise
		}
		
		guard let refreshPromise = strategy.refreshAuthorization()
		else
		{
			return Promise(error: AlamofireUtils.Errors.authorizationRefreshNotAvailable)
		}
		
		authorizationRefreshPromise = refreshPromise.ensure {
			//
			self.authorizationRefreshLock.lock()
			defer {
				self.authorizationRefreshLock.unlock()
			}
			
			self.authorizationRefreshPromise = nil
		}
		
		return authorizationRefreshPromise!
	}
}



// MARK: - RequestAdapter

extension AlamofireUtils.AuthorizationHandler: RequestAdapter
{
	func adapt(_ urlRequest: URLRequest) throws -> URLRequest
	{
		guard let authorizationOptions = urlRequest.authorizationOptions
		else
		{
			return urlRequest
		}
		
		switch authorizationOptions
		{
		case .none:
			//
			return urlRequest
			
		case .auto:
			//
			return (try? strategy.authorizeRequest(urlRequest)) ?? urlRequest
			
		case .authorize, .authorizeAndEnableRefresh:
			//
			guard let authorizedRequest = try strategy.authorizeRequest(urlRequest)
			else
			{
				throw AlamofireUtils.Errors.authorizationNotAvailable
			}
			
			return authorizedRequest
		}
	}
}



// MARK: - RequestRetrier

extension AlamofireUtils.AuthorizationHandler: RequestRetrier
{
	func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion)
	{
		guard request.isAuthorizationRefreshEnabled,
			strategy.shouldRefreshAuthorization(on: error)
		else
		{
			completion(false, 0)
			
			return
		}
		
		firstly {
			refreshAuthorization()
		}
		.done {
			completion(true, 0)
		}
		.catch { error in
			request.delegate.error = error
			completion(false, 0)
		}
	}
}
