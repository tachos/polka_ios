//
//  SwiftTypes+JSONDeserializable.swift
//
//  Created by Vitaly Trofimov on 07/10/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation

import SwiftyJSON




extension Bool: JSONDeserializable
{
	static func from(json: JSON) -> Bool?
	{
		return json.bool
	}
}



extension Int: JSONDeserializable
{
	static func from(json: JSON) -> Int?
	{
		return json.int
	}
}



extension Double: JSONDeserializable
{
	static func from(json: JSON) -> Double?
	{
		return json.double
	}
}



extension String: JSONDeserializable
{
	static func from(json: JSON) -> String?
	{
		return json.string
	}
}



extension URL: JSONDeserializable
{
	static func from(json: JSON) -> URL?
	{
		return json.string
			.flatMap { URL(string: $0) }
	}
}



extension AlamofireUtils
{
	static func parseArray<ElementType: JSONDeserializable>(
		from json: JSON,
		skipFailedElements: Bool,
		canBeEmpty: Bool = true
		) -> [ElementType]?
	{
		guard let array = json.array
		else
		{
			return nil
		}
		
		let parsedElements = try? array.compactMap { json -> ElementType? in
			//
			if let element = ElementType.from(json: json)
			{
				return element
			}
			else
			{
				if skipFailedElements
				{
					return nil
				}
				else
				{
					throw Errors.invalidResponse
				}
			}
		}
		
		guard let elements = parsedElements,
			canBeEmpty || elements.count > 0
		else
		{
			return nil
		}
		
		return elements
	}
}
