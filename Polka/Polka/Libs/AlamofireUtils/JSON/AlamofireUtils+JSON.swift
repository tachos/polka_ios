//
//  AlamofireUtils+JSON.swift
//
//  Created by Vitaly Trofimov on 07/10/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation

import Alamofire
import PromiseKit
import SwiftyJSON




protocol JSONDeserializable
{
	static func from(json: JSON) -> Self?
}



extension JSONDeserializable
{
	

    static func parse(_ node: String? = nil) -> AlamofireUtils.ParseResponse<Self>
    {
		return AlamofireUtils.ParseResponseJSONWithType(
			Self.self,
			node: node
		)
    }
	
	static func parseArray(
		_ node: String? = nil,
		skipFailedElements: Bool,
		canBeEmpty: Bool = true
	) -> AlamofireUtils.ParseResponse<[Self]>
    {
		return AlamofireUtils.ParseResponseJSONArrayWithElementType(
			Self.self,
			node: node,
			skipFailedElements: skipFailedElements,
			canBeEmpty: canBeEmpty
		)
    }

	static func from(dictionary: [AnyHashable : Any]) -> Self?
	{
		if let json = JSON(rawValue: dictionary)
		{
			return from(json: json)
		}

		return nil
	}
}



extension AlamofireUtils
{
	typealias ParseJSON<ResultType> = (_ json: JSON, _ response: HTTPURLResponse) -> ResultType?
	
	class ParseResponseJSON<ResultType>: ParseResponse<ResultType>
	{
		init(
			on queue: DispatchQueue = defaultResponseParsingQueue,
			_ parse: @escaping ParseJSON<ResultType>
			)
		{
			super.init(on: queue) { (data, response) -> ResultType? in
				//
				if let json = try? JSON(data: data)
				{
					return parse(json, response)
				}
				
				return nil
			}
		}
	}
	
	class ParseResponseJSONWithType<ResultType: JSONDeserializable>: ParseResponse<ResultType>
	{
		init(
			_ type: ResultType.Type,
			node: String? = nil,
			on queue: DispatchQueue = defaultResponseParsingQueue
			)
		{
			super.init(on: queue) { (data, _) -> ResultType? in
				//
				if let json = try? JSON(data: data)
				{
					return ResultType.from(
						json: node
							.map { json[$0] }
							?? json
					)
				}
				
				return nil
			}
		}
	}
	
	class ParseResponseJSONArrayWithElementType<ResultElementType: JSONDeserializable>: ParseResponse<[ResultElementType]>
	{
		init(
			_ elementType: ResultElementType.Type,
			node: String? = nil,
			on queue: DispatchQueue = defaultResponseParsingQueue,
			skipFailedElements: Bool,
			canBeEmpty: Bool = true
			)
		{
			super.init(on: queue) { (data, _) -> [ResultElementType]? in
				//
				if let json = try? JSON(data: data)
				{
					return AlamofireUtils.parseArray(
						from: node
							.map { json[$0] }
							?? json,
						skipFailedElements: skipFailedElements,
						canBeEmpty: canBeEmpty
					)
				}
				
				return nil
			}
		}
	}
	
	class ParseResponseFirstElementOfJSONArrayWithElementType<ResultElementType: JSONDeserializable>: ParseResponse<ResultElementType>
	{
		init(
			_ elementType: ResultElementType.Type,
			node: String? = nil,
			on queue: DispatchQueue = defaultResponseParsingQueue
			)
		{
			super.init(on: queue) { (data, _) -> ResultElementType? in
				//
				if let json = try? JSON(data: data)
				{
					return AlamofireUtils.parseArray(
						from: node
							.map { json[$0] }
							?? json,
						skipFailedElements: false,
						canBeEmpty: false
					)?.first
				}
				
				return nil
			}
		}
	}
}
