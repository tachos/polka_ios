//
//  AlamofireUtils+MultipartData.swift
//
//  Created by Dmitry Cherednikov on 18/11/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import UIKit




struct MultipartFile
{
    let key: String
    let data: Data
    let filename: String
    let mimeType: String
}

struct MultipartUrlFile
{
	let url: URL
	let name: String 
}


