//
//  UIImage+.swift
//  Polka
//
//  Created by Makson on 21.04.2021.
//

import UIKit

typealias ImageCallback = (UIImage) -> Void

extension UIImage
{
    static func from(color: UIColor) -> UIImage
    {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)

        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
	
	func tinted(with color: UIColor) -> UIImage
	{
		defer { UIGraphicsEndImageContext() }

		UIGraphicsBeginImageContextWithOptions(
				self.size, false, self.scale
		)
		color.set()
		self.withRenderingMode(.alwaysTemplate)
				.draw(in: CGRect(origin: .zero, size: self.size))

		return UIGraphicsGetImageFromCurrentImageContext() ?? self
	}
	
    enum JPEGQuality: CGFloat
    {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    func jpeg(_ quality: JPEGQuality) -> Data?
    {
        return self.jpegData(compressionQuality: quality.rawValue)
    }
    
    func save(imageName: String) -> URL?
    {
        let localPath = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imageName)
        
        let data = self.jpeg(.lowest)
        do
        {
            try data?.write(to: localPath)
        }
        catch
        {
            return nil
        }
        
        return localPath
    }
    
    var squared: UIImage
    {
        let imageRef = self.cgImage!.cropping(to: getSquareRect())
        
        let image = UIImage(cgImage: imageRef!, scale: scale, orientation: self.imageOrientation)
        
        return image
    }
    
    private func getSquareRect() -> CGRect
    {
        let breadth    = min(size.width, size.height)
        let squareSize = CGSize(width: breadth, height: breadth)
        
        let x = size.height > size.width ? (size.height - breadth) / 2 : (size.width - breadth) / 2
        
        return CGRect(origin: CGPoint(x: x, y: 0), size: squareSize)
    }
}


extension UIImage
{
    func resized(_ targetSize: CGSize) -> UIImage
    {
        let size = self.size
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        let newSize = widthRatio > heightRatio ?  CGSize(width: size.width * heightRatio, height: size.height * heightRatio) : CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}


extension UIImage
{
	var grayed: UIImage
	{
		guard let ciImage = CIImage(image: self)
			else { return self }
		let filterParameters = [ kCIInputColorKey: CIColor.white, kCIInputIntensityKey: 1.0 ] as [String: Any]
		let grayscale = ciImage.applyingFilter("CIColorMonochrome", parameters: filterParameters)
		return UIImage(ciImage: grayscale)
	}
}
