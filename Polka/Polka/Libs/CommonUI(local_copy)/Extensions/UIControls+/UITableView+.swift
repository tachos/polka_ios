//
//  UITableView+.swift
//  Polka
//
//  Created by Makson on 20.04.2021.
//

import UIKit

extension UITableView
{
    func getCellIfVisible(indexPath: IndexPath) -> UITableViewCell?
    {
        for visibleCell in visibleCells
        {
            let visibleCellIndexPath = self.indexPath(for: visibleCell)
            
            if visibleCellIndexPath == indexPath
            {
                return visibleCell
            }
        }
        
        return nil
    }
    
    func registerNibCell(_ cellIdentifier: String)
    {
        register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
    }
    
    func registerNibCell<TableViewCell: UITableViewCell>(_ tableViewCellType: TableViewCell.Type)
    {
        registerNibCell(String(describing: tableViewCellType))
    }
    
    func setSeparatorStyleNone()
    {
        self.separatorStyle = .none
    }
    
    func setContentInsetAndScrollIndicatorInsets(_ edgeInsets: UIEdgeInsets)
    {
        self.contentInset = edgeInsets
        self.scrollIndicatorInsets = edgeInsets
    }

    func dequeueReusableCell<CellType: UITableViewCell>(ofType cellType: CellType.Type) -> CellType?
    {
        let cell = self.dequeueReusableCell(withIdentifier: String(describing: cellType)) as? CellType
        return cell
    }
    
    func registerNibHeaderFooterView<ViewType: UITableViewHeaderFooterView>(_ viewType: ViewType.Type)
    {
        registerNibHeaderFooterView(ViewType.typeName)
    }
    
    private func registerNibHeaderFooterView(_ viewIdentifier: String)
    {
        register(UINib(nibName: viewIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: viewIdentifier)
    }
}

// copy-pasted form TableFlip Pod
extension UITableView
{
    func animateWithTransform(duration:      TimeInterval,
                              transform:     CGAffineTransform,
                              options:       UIView.AnimationOptions = .curveEaseInOut,
                              springDamping: CGFloat = 0.9,
                              completion:    (() -> Void)? = nil)
    {
        for (index, cell) in visibleCells.enumerated()
        {
            let delay: TimeInterval = duration / Double(visibleCells.count) * Double(index)

            cell.layer.setAffineTransform(transform)

            UIView.animate(withDuration: duration,
                           delay: delay,
                           usingSpringWithDamping: springDamping,
                           initialSpringVelocity: 0.0,
                           options: options,
                           animations:
            {
                cell.layer.setAffineTransform(.identity)
            },
                           completion: nil)

            let completionDelay: Int = Int((2 * duration) * 1000)

            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(completionDelay))
            {
                completion?()
            }
        }
    }
}

extension UITableView
{
    func reloadRows(at indexPaths: [IndexPath], with animation: RowAnimation, completion: @escaping VoidCallback)
    {
        if #available(iOS 11, *)
        {
            performBatchUpdates(
            {
                reloadRows(at: indexPaths, with: animation)
            },
            completion:
            {
                _ in

                completion()
            })
        }
        else
        {
            CATransaction.begin()

            CATransaction.setCompletionBlock(completion)

            beginUpdates()
            reloadRows(at: indexPaths, with: animation)
            endUpdates()

            CATransaction.commit()
        }
    }
}
