//
//  StoryboardInstantitable.swift
//  Broadway
//
//  Created by Ivan Goremykin on 11/07/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import UIKit

protocol StoryboardInstantitable: UIViewController
{
    static var storyboardName: StoryboardName { get }
}
