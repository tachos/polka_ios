//
//  Instantiation.swift
//  Sellel
//
//  Created by Ivan Goremykin on 07.09.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

func createViewController(storyBoardName: String, viewControllerId: String) -> UIViewController
{
    let storyboard = UIStoryboard(name: storyBoardName, bundle: nil)
    
    return storyboard.instantiateViewController(withIdentifier: viewControllerId)
}

func createViewController<ViewControllerType: UIViewController>(_ storyBoardName: StoryboardName, _ viewControllerType: ViewControllerType.Type) -> ViewControllerType
{
    return createViewController(storyBoardName: storyBoardName.rawValue.uppercasedFirstLetter(), viewControllerId: String(describing: ViewControllerType.self)) as! ViewControllerType
}

func createView(nibName: String, bundle: Bundle? = nil, owner: Any? = nil) -> UIView
{
    return UINib(nibName: nibName, bundle: bundle).instantiate(withOwner: owner, options: nil)[0] as! UIView
}

func createView<ViewType: UIView>(_ viewType: ViewType.Type, owner: Any? = nil) -> ViewType
{
    return createView(nibName: String(describing: ViewType.self), owner: owner) as! ViewType
}
