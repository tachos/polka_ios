//
//  +String.swift
//  Polka
//
//  Created by Michael Goremykin on 21.04.2021.
//

import UIKit

enum FixedDimension
{
    case width(CGFloat)
    case height(CGFloat)
}

extension String
{
    func getLabelSizeWithFixedDimension(_ fixedDimension: FixedDimension,
                                        _ font: UIFont,
                                        _ fontSize: CGFloat? = nil) -> CGSize
    {
        var height,width: CGFloat
        
        switch fixedDimension
        {
        case .height(let val):
            height = val
            width =  .greatestFiniteMagnitude
        case .width(let val):
            height = .greatestFiniteMagnitude
            width = val
            
        }
        
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: height))
        label.numberOfLines = 0
        label.text = self
        label.font = font
        if let fontSize = fontSize
        {
            label.font = label.font.withSize(fontSize)
        }
        label.sizeToFit()
        
        return label.frame.size
    }
}
