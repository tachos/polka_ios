//
//  GradientView.swift
//  Polka
//
//  Created by Makson on 20.04.2021.
//

import UIKit

enum GradientDirection {
    case none
    case horizontal(leftColor: UIColor, rightColor: UIColor)
    case vertical(bottomColor: UIColor, topColor: UIColor)
    case diagonal(topLeftColor: UIColor, bottomRightColor: UIColor)
}

class GradientView: UIView
{
    var startPoint: CGPoint?
    var endPoint: CGPoint?

    var gradientDirection: GradientDirection? {
        didSet {
            setNeedsDisplay()
        }
    }
}

extension GradientView {
    override func draw(_ rect: CGRect) {
        guard let gradientDirection = gradientDirection else { return }

        switch gradientDirection {
        case .none:
            clearContext(in: rect)

        case .horizontal(let leftColor, let rightColor):
            drawHorizontalGradient(in: rect, leftColor: leftColor, rightColor: rightColor)

        case .vertical(let bottomColor, let topColor):
            drawVerticalGradient(in: rect, bottomColor: bottomColor, topColor: topColor)

        case .diagonal(let topLeftColor, let bottomRightColor):
            drawDiagonalGradient(in: rect, topLeftColor: topLeftColor, bottomRightColor: bottomRightColor)
        }
    }

    private func clearContext(in rect: CGRect) {
        if let context = UIGraphicsGetCurrentContext() {
            context.clear(rect)
            context.setFillColor(backgroundColor?.cgColor ?? UIColor.clear.cgColor)
            context.fill(rect)
        }
    }

    private func drawHorizontalGradient(in rect: CGRect, leftColor: UIColor, rightColor: UIColor) {
        drawGradient(colors: [leftColor.cgColor, rightColor.cgColor],
                     startPoint: startPoint ?? CGPoint(x: 0, y: rect.height / 2),
                     endPoint: endPoint ?? CGPoint(x: rect.width, y: rect.height / 2))
    }

    private func drawVerticalGradient(in rect: CGRect, bottomColor: UIColor, topColor: UIColor) {
        drawGradient(colors: [bottomColor.cgColor, topColor.cgColor],
                     startPoint: startPoint ?? CGPoint(x: rect.width / 2, y: rect.height),
                     endPoint: endPoint ?? CGPoint(x: rect.width / 2, y: 0))
    }

    private func drawDiagonalGradient(in rect: CGRect, topLeftColor: UIColor, bottomRightColor: UIColor) {
        drawGradient(colors: [topLeftColor.cgColor, bottomRightColor.cgColor],
                     startPoint: startPoint ?? CGPoint(x: 0, y: 0),
                     endPoint: endPoint ?? CGPoint(x: rect.width, y: rect.height))
    }

    private func drawGradient(colors: [CGColor], startPoint: CGPoint, endPoint: CGPoint)
    {
        guard let context = UIGraphicsGetCurrentContext() else { return }

        let locations = [CGFloat(0), CGFloat(1)]
        let colorSpace = CGColorSpaceCreateDeviceRGB()

        if let gradient = CGGradient(colorsSpace: colorSpace, colors: colors as CFArray, locations: locations) {
            let options: CGGradientDrawingOptions = [.drawsBeforeStartLocation, .drawsAfterEndLocation]
            context.drawLinearGradient(gradient, start: startPoint, end: endPoint, options: options)
        }
    }
}
