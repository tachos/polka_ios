//
//  UITableViewUtils.swift
//  Hice
//
//  Created by Vitaly Trofimov on 29.09.2021.
//  Copyright © 2021 Hice. All rights reserved.
//

import UIKit



extension UITableView
{
	static func disableSectionPadding()
	{
		if #available(iOS 15.0, *)
		{
			appearance().sectionHeaderTopPadding = .zero
		}
	}
}
