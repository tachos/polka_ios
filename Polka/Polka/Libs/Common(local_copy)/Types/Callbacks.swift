//
//  Callbacks.swift
//  Polka
//
//  Created by Makson on 19.04.2021.
//

import Foundation

typealias VoidCallback   = ()       -> Void
typealias StringCallback = (String) -> Void
typealias IntCallback    = (Int)    -> Void
typealias BoolCallback   = (Bool)   -> Void
typealias UrlCallback    = (URL) -> Void
