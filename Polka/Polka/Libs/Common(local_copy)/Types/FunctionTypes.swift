//
//  FunctionTypes.swift
//  Polka
//
//  Created by Makson on 19.04.2021.
//

typealias Handler<Item> = (Item) -> Void
typealias AsyncHandler<Item> = Handler<Handler<Item>>
typealias Handler2<Item1, Item2> = (Item1, Item2) -> Void
typealias Getter<Item>  = () -> Item

class OneShotFunc
{
    private var function: VoidCallback?

    init(_ function: @escaping VoidCallback)
    {
        self.function = function
    }

    init() {}

    func performIfFirstTime()
    {
        if let function = function
        {
            function()

            self.function = nil
        }
    }
}
