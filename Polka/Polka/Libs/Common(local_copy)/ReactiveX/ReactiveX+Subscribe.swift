//
//  ReactiveX+Subscribe.swift
//  Polka
//
//  Created by Makson on 12.05.2021.
//
import RxSwift

func subscribeOnNext<ObservableType, ObservableParameterType>(_ observable: ObservableType,
															  _ callback: @escaping (ObservableParameterType) -> Void,
															  _ disposeBag: DisposeBag)
	where ObservableType: Observable<ObservableParameterType>
{
	observable.subscribe(onNext: callback).disposed(by: disposeBag)
}

func subscribeOnError<ObservableType, ObservableParameterType>(_ observable: ObservableType,
															   _ callback: @escaping (Error) -> Void,
															   _ disposeBag: DisposeBag)
	where ObservableType: Observable<ObservableParameterType>
{
	observable.subscribe(onError: callback).disposed(by: disposeBag)
}
