//
//  ReactiveX+Predicates.swift
//  Polka
//
//  Created by Makson on 12.05.2021.
//

import RxSwift

func ignoreNil<A>(x: A?) -> Observable<A>
{
	return x.map { Observable.just($0) } ?? Observable.empty()
}
