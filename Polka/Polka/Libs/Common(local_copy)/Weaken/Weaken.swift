//
//  Weaken.swift
//  Polka
//
//  Created by Makson on 12.05.2021.
//

import Foundation

// MARK:- 0 arguments
func weaken<T: AnyObject>(_ instance: T, _ classFunction: @escaping (T) -> ()->Void) -> ()->Void
{
	return {
		[weak instance] in
		
		if let instance = instance
		{
			let instanceFunction = classFunction(instance)
			
			instanceFunction()
		}
	}
}

// MARK:- 1 argument
func weaken<T: AnyObject, A>(_ instance: T, _ classFunction: @escaping (T) -> (A)->Void) -> (A)->Void
{
	return {
		[weak instance] args in
		
		if let instance = instance
		{
			let instanceFunction = classFunction(instance)
			
			instanceFunction(args)
		}
	}
}

// MARK:- 2 arguments
func weaken<T: AnyObject, A, B>(_ instance: T, _ classFunction: @escaping (T) -> (A, B)->Void) -> (A, B)->Void
{
	return {
		[weak instance] argA, argB in
		
		if let instance = instance
		{
			let instanceFunction = classFunction(instance)
			
			instanceFunction(argA, argB)
		}
	}
}

func weaken<T: AnyObject, A, R>(_ instance: T, _ classFunction: @escaping (T) -> (A)->R) -> (A)->R
{
	return {
		[weak instance] argA in
		
		if let instance = instance
		{
			let instanceFunction = classFunction(instance)
			
			return instanceFunction(argA)
		}
		
		fatalError()
	}
}

func weaken<T: AnyObject, A, B, R>(_ instance: T, _ classFunction: @escaping (T) -> (A, B)->R) -> (A, B)->R
{
	return {
		[weak instance] argA, argB in
		
		if let instance = instance
		{
			let instanceFunction = classFunction(instance)
			
			return instanceFunction(argA, argB)
		}
		
		fatalError()
	}
}

func weaken<T: AnyObject, A, B, C, R>(_ instance: T, _ classFunction: @escaping (T) -> (A, B, C)->R) -> (A, B, C)->R
{
	return {
		[weak instance] argA, argB, argC in
		
		if let instance = instance
		{
			let instanceFunction = classFunction(instance)
			
			return instanceFunction(argA, argB, argC)
		}
		
		fatalError()
	}
}

private func testWeaken()
{
	class A
	{
		func foo(param: Int) { print(param) }
	}
	
	var a: A! = A()
	
	let weakenFoo = weaken(a, A.foo)
	
	weakenFoo(42)
	
	a = nil
	
	weakenFoo(42) // No crash, no print!
}
