//
//  AppDelegate.swift
//  Polka
//
//  Created by Makson on 16.04.2021.
//

import UIKit
import GoogleMaps
import Firebase
import FirebaseMessaging
import AppsFlyerLib
import AVFoundation
import UserNotifications

@main
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
	
	let gcmMessageIDKey = "gcm.message_id"
	
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
		fetchCartIfNecessary()
		
        launchUI()
		setupGoogleMaps()
		setupFirebaseCrashlytics()
		setupAppsFlyer()
		setupAudioSession()
		managers.dispose()

        return true
    }
	
	func applicationDidBecomeActive(_ application: UIApplication)
	{
		managers.appEvents.onAppMovedToForeground()

		AppsFlyerLib.shared().start()
	}
	
	func applicationWillResignActive(_ application: UIApplication)
	{
		managers.appEvents.onAppWillMoveToBackground()
	}
	
	func application(
		_ application: UIApplication,
		continue userActivity: NSUserActivity,
		restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void
	) -> Bool
	{
		return managers.deepLinks.handleUserActivity(userActivity)
	}
	
	func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool
	{
		return managers.deepLinks.handleUrl(url, options: options)
	}
}

private extension AppDelegate
{
	@objc func clipboardChanged()
	{
		   let pasteboardString: String? = UIPasteboard.general.string
		   if let theString = pasteboardString
		{
			   print("String is \(theString)")
			   // Do cool things with the string
		   }
	   }
	
    func launchUI()
    {
		UITableView.disableSectionPadding()
		
        window = UIWindow()
        window!.makeKeyAndVisible()
		
		RootStory.start(in: window!)
    }
	func setupAppsFlyer()
	{
		AppsFlyerLib.shared().appsFlyerDevKey = "UoHsp5phXY2YgV2dsyno2j"
		AppsFlyerLib.shared().appleAppID = "1570152015"
		
		AppsFlyerLib.shared().delegate = self
		AppsFlyerLib.shared().deepLinkDelegate = managers.deepLinks
		
		#if DEBUG
		AppsFlyerLib.shared().isDebug = true
		#endif
	}
	func setupGoogleMaps()
	{
		GMSServices.provideAPIKey(GoogleApiKey.value)
	}
	
	func setupFirebaseCrashlytics()
	{
		FirebaseApp.configure()
	}

	func setupAudioSession()
	{
		try? AVAudioSession.sharedInstance().setCategory(
			.playback,
			mode: .moviePlayback
		)
	}
}

extension AppDelegate:AppsFlyerLibDelegate
{
func onConversionDataSuccess(_ conversionInfo: [AnyHashable : Any])
{
print("onConversionDataSuccess data:")
for (key, value) in conversionInfo
{
print(key, ":", value)
}
if let status = conversionInfo["af_status"] as? String
{
if (status == "Non-organic")
{
if let sourceID = conversionInfo["media_source"],
let campaign = conversionInfo["campaign"]
{
print("This is a Non-Organic install. Media source: \(sourceID)  Campaign: \(campaign)")
}
}
else
{
print("This is an organic install.")
}

if let is_first_launch = conversionInfo["is_first_launch"] as? Bool,
is_first_launch
{
print("First Launch")
}
else
{
print("Not First Launch")
}
}
}

func onConversionDataFail(_ error: Error)
{
print(error)
}
}



// MARK:- Push notifications
extension AppDelegate
{
	func registerForPushNotifications(_ application: UIApplication)
	{
		Messaging.messaging().delegate = self
		
		if #available(iOS 10.0, *)
		{
			UNUserNotificationCenter.current().delegate = self
			
			let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
			UNUserNotificationCenter.current().requestAuthorization(options: authOptions,
																	completionHandler: { _, _ in })
		}
		else
		{
			let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound],
																				  categories: nil)
			
			application.registerUserNotificationSettings(settings)
		}

		application.registerForRemoteNotifications()
	}
	
	func application(_ application: UIApplication,
					 didReceiveRemoteNotification userInfo: [AnyHashable: Any])
	{
		if let messageID = userInfo[gcmMessageIDKey]
		{
			print("Message ID: \(messageID)")
		}
		
		print(userInfo)
	}
	
	func application(_ application: UIApplication,
					 didReceiveRemoteNotification userInfo: [AnyHashable: Any],
					 fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult)
					   -> Void)
	{
		if let messageID = userInfo[gcmMessageIDKey]
		{
			print("Message ID: \(messageID)")
		}

		print(userInfo)

		completionHandler(UIBackgroundFetchResult.newData)
	}
	
	func application(_ application: UIApplication,
					 didFailToRegisterForRemoteNotificationsWithError error: Error)
	{
	  print("Unable to register for remote notifications: \(error.localizedDescription)")
	}

	// This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
	// If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
	// the FCM registration token.
//	func application(_ application: UIApplication,
//					 didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
//	{
//	  print("APNs token retrieved: \(deviceToken)")
//
//	  // With swizzling disabled you must set the APNs token here.
//	  // Messaging.messaging().apnsToken = deviceToken
//	}
}

@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate
{
	func userNotificationCenter(_ center: UNUserNotificationCenter,
								willPresent notification: UNNotification,
								withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions)
								-> Void)
	{
		let userInfo = notification.request.content.userInfo

		if let messageID = userInfo[gcmMessageIDKey]
		{
			print("Message ID: \(messageID)")
		}
		
		print(userInfo)
		
		completionHandler([[.alert, .sound]])
	}

	func userNotificationCenter(_ center: UNUserNotificationCenter,
								didReceive response: UNNotificationResponse,
								withCompletionHandler completionHandler: @escaping () -> Void)
	{
		let userInfo = response.notification.request.content.userInfo

		if let messageID = userInfo[gcmMessageIDKey]
		{
			print("Message ID: \(messageID)")
		}

		print(userInfo)

		completionHandler()
	}
}

extension AppDelegate: MessagingDelegate
{
	func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?)
	{
		if let token = fcmToken
		{
			PushNotificationController.registerDeviceIfAuthorized(deviceToken: token,
																  onFailureCallback: {[weak self] error in self?.onDeviceRegisterFailure (error)})
		}
	}
	
	private func onDeviceRegisterFailure(_ error: Error)
	{
		print("Error occurred while registering device for push notifications: \n\(error)")
	}
}

private extension AppDelegate
{
	private func fetchCartIfNecessary()
	{
		switch managers.webService.currentUserProfile?.profile?.role
		{
		case .consumer:
			CartController.shared.synchronizeWithBackend()
		default:
			return
		}
	}
}
