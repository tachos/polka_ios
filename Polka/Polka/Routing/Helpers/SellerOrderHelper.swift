//
//  SellerOrderHelper.swift
//  Polka
//
//  Created by Makson on 16.06.2021.
//

import Foundation

import PromiseKit

class SellerOrderHelper
{
	var subOrder:[SubOrderInfo] = []
}
extension SellerOrderHelper
{
	func getSubOrderInfo(ordersPerPageCount: Int, offset:Int) -> Promise<SubOrdersPaginationResponse>
	{
		return managers.webService.getSubOrdersWithPagination(ordersPerPageCount, offset)
			.get
			{ _ in
				
			}
			
	}
}
