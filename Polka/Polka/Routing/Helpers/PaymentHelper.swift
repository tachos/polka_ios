//
//  PaymentHelper.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 04.06.2021.
//

import Foundation
import PromiseKit
import WebKit



class PaymentHelper: NSObject
{
	var showPaymentFormSegue: Handler<URL>?
	var hidePaymentFormSegue: VoidCallback?
}



// MARK: - API

extension PaymentHelper
{
	func pay(_ viewController: CartViewController)
	{
		firstly
		{
			managers.webService.createOrder()
		}
		.done
		{
			orderInfo in

			viewController.onSuccessfullyCreatedOrder()
			
			// AAAAA TOREMOVE
			if let tabController = viewController.getTabBarController(),
			   let tabNavigationControler = tabController
				.getTabViewController(.orders) as? NavigationController,
			   let ordersViewController = tabNavigationControler.viewControllers.first(
				where: { $0  is OrdersViewController }
			   )
			{
				tabNavigationControler.viewControllers = [ordersViewController]

				tabController.switchToTab(.orders)
			}
			
			// AAAAA
			self.showPaymentFormSegue?(orderInfo.paymentTransactionInfo.paymentFormUrl)
		}
		.catch
		{
			_ in

			PopUps.showRetryQuery(
				from: viewController,
				queryRetryCallback:
			{
				[weak self, weak viewController] in

				guard let `self` = self, let viewController = viewController
				else
				{
					return
				}

				self.pay(viewController)
			},
				onCancelCallback: {}
			)
		}
	}
}



// MARK: - WKNavigationDelegate

extension PaymentHelper: WKNavigationDelegate
{
	func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void)
	{
		if navigationAction.navigationType == .other,
		   let url = navigationAction.request.url,
		   url.absoluteString.contains("polkaonline.ru")
		{
			hidePaymentFormSegue?()
			decisionHandler(.cancel)
		}
		else
		{
			decisionHandler(.allow)
		}
	}
}
