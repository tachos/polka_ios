//
//  ChatHelper+AttachmentsObserver.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 25.06.2021.
//

import Foundation



// MARK: - AttachmentStatus

extension ChatHelper
{
	enum AttachmentStatus
	{
		case uploading
		case uploaded(Attachment.Id)
		case failed
	}
}



// MARK: - AttachmentsObserver

extension ChatHelper
{
	class AttachmentsObserver
	{
		private var statuses: [Attachment.Id : AttachmentStatus] = [:]
		private let onAllUploaded: Handler<[Attachment.Id]>
		private let onFailure: Handler<Attachment.Id>

		init(
			attachmentsIds: [Attachment.Id],
			onAllUploaded: @escaping Handler<[Attachment.Id]>,
			onFailure: @escaping Handler<Attachment.Id>
		)
		{
			for id in attachmentsIds
			{
				statuses[id] = .uploading
			}

			self.onAllUploaded = onAllUploaded
			self.onFailure = onFailure
		}

		func setStatus(
			_ status: AttachmentStatus,
			for attachmentId: Attachment.Id
		)
		{
			statuses[attachmentId] = status

			switch status
			{
			case .uploading:
				break

			case .failed:
				onFailure(attachmentId)

			case .uploaded:
				checkAllUploaded()
			}
		}

		var newAttachmentsIds: [Attachment.Id]
		{
			var newIds: [Attachment.Id] = []

			for status in statuses.values
			{
				if case let .uploaded(newId) = status
				{
					newIds.append(newId)
				}
			}

			return newIds
		}

		private func checkAllUploaded()
		{
			let newAttachmentsIds = self.newAttachmentsIds

			if newAttachmentsIds.count == statuses.values.count
			{
				onAllUploaded(newAttachmentsIds)
			}
		}
	}
}
