//
//  ChatHelper.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 24.06.2021.
//

import Foundation
import PromiseKit
import RxSwift



class ChatHelper
{
	// MARK: Config
	let receiver: Chat.Receiver
	private weak var viewController: ChatViewController?

	// MARK: Messages
	private(set) var hasMoreOlderMessages = true
	private(set) var messagesByDate: [MessagesByDate] = []
	private var messagesPromise: Promise<MessagesList>?
	private var sendMessagePromise: Promise<Message>?
	private var oldestMessageDate: Date?

	// MARK: Messages Data Mapping
	private var outgoingMessagesStatuses: [Message.Id : OutgoingMessageStatus] = [:]
	private var messagesIdsMap: [Message.Id : Message.Id] = [:] // Local ids mapped by remote ids

	// MARK: Attachments
	private var attachmentsObservers: [Message.Id : AttachmentsObserver] = [:]

	// MARK: ReactiveX
	private let disposeBag = DisposeBag()

	// MARK: Initializer
	init(
		receiver: Chat.Receiver,
		viewController: ChatViewController
	)
	{
		self.receiver = receiver
		self.viewController = viewController

		subscribeForSocketUpdates()
	}
}



// MARK: - Data Fetching

extension ChatHelper
{
	func loadFirstPage()
	{
		guard let viewController = viewController else { return }

		let messagesPromise = managers.webService.getMessages(
			receiverId: receiver.id,
			oldestMessageDate: oldestMessageDate,
			resultsPerPage: Constants.resultsPerPage
		)

		self.messagesPromise = messagesPromise

		viewController.setTopLoaderVisible(false)
		
		firstly
		{
			messagesPromise
		}
		.done
		{
			list  in

			self.handleOlderMessages(
				list,
				isFirstPage: true,
				viewController: viewController
			)
		}
		.catch
		{
			_ in

			viewController.showRetryAlert(
				onRetry:
			{
				[weak self] in

				self?.loadMore()
			})
		}
	}

	func loadMore()
	{
		// check if loading possible
		guard let viewController = viewController else { return }

		if !hasMoreOlderMessages
		{
			return
		}

		if let messagesPromise = messagesPromise,
		   messagesPromise.isPending
		{
			return
		}

		// get messages
		let messagesPromise = managers.webService.getMessages(
			receiverId: receiver.id,
			oldestMessageDate: oldestMessageDate,
			resultsPerPage: Constants.resultsPerPage
		)

		self.messagesPromise = messagesPromise

		viewController.setTopLoaderVisible(true)

		firstly
		{
			messagesPromise
		}
		.ensure
		{
			viewController.setTopLoaderVisible(false)
		}
		.done
		{
			list in

			self.handleOlderMessages(
				list,
				isFirstPage: false,
				viewController: viewController
			)
		}
		.catch
		{
			_ in

			viewController.showRetryAlert(
				onRetry:
			{
				[weak self] in

				self?.loadMore()
			})
		}
	}
}



// MARK: - Socket Handling

private extension ChatHelper
{
	func subscribeForSocketUpdates()
	{
		managers.webService.socketMessageObservable
			.subscribe(onNext: { [weak self] in self?.onSocketMessage($0) })
			.disposed(by: disposeBag)
	}

	func onSocketMessage(_ message: Message)
	{
		if let messagesPromise = messagesPromise,
		   messagesPromise.isPending
		{
			return
		}

		let userId = managers.webService.currentUserProfile?.profile?.id

		let isIncoming = message.authorId == receiver.id
			&& message.receiverId == userId
		let isOutgoing = message.authorId == userId
			&& message.receiverId == receiver.id

		let handler =
		{
			if !self.hasRemoteMessage(message)
			{
				self.appendNewerMessage(message)
				self.viewController?.handleNewerMessage()
			}
		}

		if isIncoming
		{
			handler()

			markRead()
		}
		else if isOutgoing
		{
			// newly sent message is received via socket
			// so attampt to insert only after sending is fully complete
			if let sendMessagePromise = sendMessagePromise,
			   sendMessagePromise.isPending
			{
				sendMessagePromise
					.cauterize()
					.finally
				{
					handler()
				}
			}
			else
			{
				handler()
			}
		}
	}
}



// MARK: - Managing Messages

private extension ChatHelper
{
	func handleOlderMessages(
		_ list: MessagesList,
		isFirstPage: Bool,
		viewController: ChatViewController
	)
	{
		let lessMessagesThanNeeded = list.messages.count < Constants.resultsPerPage
		hasMoreOlderMessages = !(list.isListEnd || lessMessagesThanNeeded)

		appendOlderMessages(list.messages)

		if isFirstPage
		{
			markRead()
		}

		isFirstPage
			? viewController.handleFirstPage()
			: viewController.handleOlderMessages()
	}

	func appendOlderMessages(_ olderMessages: [Message])
	{
		self.messagesByDate = Self.groupByDate(
			olderMessages + Self.toMessages(self.messagesByDate)
		)

		if let oldestMessage = olderMessages.first
		{
			self.oldestMessageDate = oldestMessage.date
		}
	}

	func appendNewerMessage(_ newerMessage: Message)
	{
		self.messagesByDate = Self.groupByDate(
			Self.toMessages(self.messagesByDate) + [newerMessage]
		)
	}

	func hasRemoteMessage(_ remoteMessage: Message) -> Bool
	{
		let idInLocalData = messagesIdsMap[remoteMessage.id] ?? remoteMessage.id

		for message in Self.toMessages(messagesByDate).reversed()
		{
			if message.id == idInLocalData
			{
				return true
			}
		}

		return false
	}
}



// MARK: - Mark Read

private extension ChatHelper
{
	func markRead()
	{
		managers.chats.markAllRead(receiver.id)

		firstly
		{
			managers.webService.markChatRead(
				receiverId: receiver.id,
				upTo: Date()
			)
		}
		.cauterize()
	}
}



// MARK: - Send Message

extension ChatHelper
{
	func sendMessage(
		text: String
	)
	{
		guard let viewController = viewController
		else
		{
			return
		}

		guard let newMessage = createLocalOutgoingMessage(
			content: .text(text)
		)
		else
		{
			return
		}

		appendNewerMessage(newMessage)
		setOutgoingMessageStatus(
			message: newMessage,
			status: .sending,
			viewControllerToNotify: nil
		)
		viewController.handleNewerMessage()

		sendMessage(
			newMessage,
			viewController: viewController
		)
	}

	func sendMessage(
		localMedia: [LocalMedia]
	)
	{
		if localMedia.isEmpty
		{
			return
		}
		
		guard let viewController = viewController
		else
		{
			return
		}

		let attachments: [Attachment] = localMedia.map
		{
			switch $0
			{
			case .image(let image):
				return .init(
					id: UUID().uuidString,
					content: .localImage(image)
				)

			case .video(let video):
				return .init(
					id: UUID().uuidString,
					content: .localVideo(video)
				)
			}
		}

		guard let newMessage = createLocalOutgoingMessage(
			content: .media(attachments)
		)
		else
		{
			return
		}

		appendNewerMessage(newMessage)
		setOutgoingMessageStatus(
			message: newMessage,
			status: .sending,
			viewControllerToNotify: nil
		)
		viewController.handleNewerMessage()

		let observer = AttachmentsObserver(
			attachmentsIds: attachments.map { $0.id },
			onAllUploaded:
		{
			[weak self] newAttachmentsIds in

			guard let self = self else { return }

			self.sendMessage(
				newMessage,
				viewController: viewController
			)
		},
			onFailure:
		{
			[weak viewController] _ in

			guard let viewController = viewController else { return }

			self.setOutgoingMessageStatus(
				message: newMessage,
				status: .failed,
				viewControllerToNotify: viewController
			)
		})

		attachmentsObservers[newMessage.id] = observer

		for attachment in attachments
		{
			switch attachment.content
			{
			case .localImage(let image):
				self.uploadAttachment(
					with: attachment.id,
					image: image,
					observer: observer
				)

			case .localVideo(let video):
				self.uploadAttachment(
					with: attachment.id,
					video: video,
					observer: observer
				)

			case .remoteImage, .remoteVideo:
				break
			}
		}
	}

	func resendMessage(
		message: Message
	)
	{
		guard let viewController = viewController
		else
		{
			return
		}

		setOutgoingMessageStatus(
			message: message,
			status: .sending,
			viewControllerToNotify: viewController
		)

		sendMessage(message, viewController: viewController)
	}

	func deleteFailedMessage(
		message: Message
	)
	{
		guard let viewController = viewController
		else
		{
			return
		}

		if let indexPath = getIndexPath(messageId: message.id)
		{
			var messages = messagesByDate[indexPath.section].messages
			messages.remove(at: indexPath.item)

			if messages.isEmpty
			{
				messagesByDate.remove(at: indexPath.section)
			}
			else
			{
				messagesByDate[indexPath.section] = .init(
					date: messagesByDate[indexPath.section].date,
					messages: messages
				)
			}

			viewController.handleMessageDeletion()
		}

		self.attachmentsObservers.removeValue(
			forKey: message.id
		)
	}

	private func sendMessage(
		_ message: Message,
		viewController: ChatViewController
	)
	{
		let promise: Promise<Message>

		switch message.content
		{
		case .text(let text):
			promise = managers.webService.sendMessage(
				to: receiver.id,
				text: text
			)

		case .media:
			let attachmentsIds = attachmentsObservers[message.id]?
				.newAttachmentsIds

			promise = managers.webService.sendMessage(
				to: receiver.id,
				attachmentsIds: attachmentsIds
			)
		}

		self.sendMessagePromise = promise

		firstly
		{
			promise
		}
		.done
		{
			remoteMessage in

			self.attachmentsObservers.removeValue(
				forKey: message.id
			)

			self.setOutgoingMessageStatus(
				message: message,
				status: .delivered,
				viewControllerToNotify: nil
			)

			self.messagesIdsMap[remoteMessage.id] = message.id
		}
		.catch
		{
			_ in

			self.setOutgoingMessageStatus(
				message: message,
				status: .failed,
				viewControllerToNotify: viewController
			)
		}
	}

	private func createLocalOutgoingMessage(content: Message.Content) -> Message?
	{
		guard let authorId = managers.webService.currentUserProfile?.profile?.id
		else
		{
			return nil
		}

		return .init(
			id: "local-\(UUID().uuidString)",
			authorId: authorId,
			receiverId: receiver.id,
			date: Date(),
			content: content
		)
	}
}



// MARK: - Statuses

extension ChatHelper
{
	enum OutgoingMessageStatus
	{
		case sending
		case delivered
		case failed
	}

	func getOutgoingMessageStatus(messageId: Message.Id) -> OutgoingMessageStatus?
	{
		return outgoingMessagesStatuses[messageId]
	}

	private func setOutgoingMessageStatus(
		message: Message,
		status: OutgoingMessageStatus?,
		viewControllerToNotify: ChatViewController?
	)
	{
		outgoingMessagesStatuses[message.id] = status

		if let indexPath = getIndexPath(messageId: message.id)
		{
			viewControllerToNotify?.handleOutgoingMessageStatusUpdate(
				message,
				indexPath: indexPath
			)
		}
	}
}



// MARK: - Attachments

private extension ChatHelper
{
	func uploadAttachment(
		with id: Attachment.Id,
		image: UIImage,
		observer: AttachmentsObserver
	)
	{
		firstly
		{
			managers.webService.uploadImage(image)
		}
		.done
		{
			newId in

			observer.setStatus(.uploaded(newId), for: id)
		}
		.catch
		{
			_ in

			observer.setStatus(.failed, for: id)
		}
	}

	func uploadAttachment(
		with id: Attachment.Id,
		video: LocalVideo,
		observer: AttachmentsObserver
	)
	{
		firstly
		{
			managers.webService.uploadVideo(video.url)
		}
		.done
		{
			newId in

			observer.setStatus(.uploaded(newId), for: id)
		}
		.catch
		{
			_ in

			observer.setStatus(.failed, for: id)
		}
	}
}



// MARK: - Messages Utils

private extension ChatHelper
{
	static func toMessages(_ messagesByDate: [MessagesByDate]) -> [Message]
	{
		return messagesByDate.reduce([Message](), { $0 + $1.messages })
	}

	// it's assumed that messages are sorted from older to newer
	static func groupByDate(_ messages: [Message]) -> [MessagesByDate]
	{
		if messages.isEmpty
		{
			return []
		}

		var startOfDay = messages.first!.date.startOfDay
		var group: [Message] = []
		var messagesByDate: [MessagesByDate] = []

		for (index, message) in messages.enumerated()
		{
			if message.date.startOfDay > startOfDay // if message is newer
			{
				messagesByDate.append(
					.init(
						date: startOfDay,
						messages: group
					)
				)

				startOfDay = message.date.startOfDay
				group = [message]

				if index == messages.count - 1 // if last (newest) message
				{
					messagesByDate.append(
						.init(
							date: startOfDay,
							messages: group
						)
					)
				}
			}
			else if index == messages.count - 1 // if last (newest) message
			{
				group.append(message)

				messagesByDate.append(
					.init(
						date: startOfDay,
						messages: group
					)
				)
			}
			else
			{
				group.append(message)
			}
		}

		return messagesByDate
	}

	func getIndexPath(messageId: Message.Id) -> IndexPath?
	{
		for (section, messagesByDate) in messagesByDate.enumerated().reversed()
		{
			for (item, message) in messagesByDate.messages.enumerated().reversed()
			{
				if message.id == messageId
				{
					return IndexPath(
						item: item,
						section: section
					)
				}
			}
		}

		return nil
	}
}



// MARK: - Constants

private extension ChatHelper
{
	struct Constants
	{
		static let resultsPerPage = 10
	}
}
