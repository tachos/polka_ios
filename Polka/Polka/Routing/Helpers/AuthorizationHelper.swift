//
//  AuthorizationHelper.swift
//  Polka
//
//  Created by Makson on 27.04.2021.
//

import Foundation
import UIKit
import PromiseKit
import AppsFlyerLib
import FirebaseMessaging

class AuthorizationHelper
{
	var skipToCatalogScreen: (() -> Void)?
	var navigateToNextScreen:((String,Int)-> Void)?
	var navigationToWebView: ((String) -> Void)?
	var navigationToBackScreen:(()->Void)?
	var navigationToCatalogScreen:(()->Void)?
	var phone:String?
	var timeTimer:Int = 0
}

extension AuthorizationHelper
{
	
	func sendOTP(_ phone: String,_ isTransitionNextScreen: Bool) -> Promise<OTP>
	{
		managers.webService.sendOTP(phone: phone)
			.get
			{ otp  in
				self.phone = phone
				self.timeTimer = otp.timeout
				self.transitionToNextScreen(phone,otp.timeout,isTransitionNextScreen)
			}
	}
	
	func transitionToNextScreen(_ phone: String,_ timeTimer:Int,_ isTransitionNextScreen:Bool)
	{
		if isTransitionNextScreen
		{
			self.navigateToNextScreen?(phone,timeTimer)
		}
		else
		{
			
		}
	}
	func skipAuthorization()
	{
		skipToCatalogScreen?()
	}
	
	func navigateToWeb(url:String)
	{
		self.navigationToWebView?(url)
	}
	func back()
	{
		navigationToBackScreen?()
	}
	
	func authorization(_ otp: String) -> Promise<User>
	{
		guard let phone = phone else { return Promise(error: PMKError.cancelled)}
		return managers.webService.authorization(phone: phone, otp: otp)
			.get
			{[weak self] user  in
				guard let self = self else { return }
				managers.appsFlyer.sendLog(logText: "login_success", params: nil)
				self.navigationToCatalogScreen?()
				
				//AAAAA TOREMOVE
				self.registerForPushNotification()
			}
	}
	
	private func registerForPushNotification()
	{
		if let appDelegate =  UIApplication.shared.delegate as? AppDelegate
		{
			appDelegate.registerForPushNotifications(UIApplication.shared)
		}
		
		//AAAAA TOREMOVE
		DispatchQueue.main.asyncAfter(deadline: .now() + 3.0)
		{
			if let token = Messaging.messaging().fcmToken
			{
				PushNotificationController.registerDevice(deviceToken: token,
														  onFailureCallback: {[weak self] error in
															self?.onRegisterDeviceFailure(error)
														  })
			}
		}
	}
	
	private func onRegisterDeviceFailure(_ error: Error)
	{
		print("Error occurred while registering device for push notifications: \n\(error)")
	}
	
}
