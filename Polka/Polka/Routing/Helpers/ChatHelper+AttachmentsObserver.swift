//
//  ChatHelper+AttachmentsObserver.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 25.06.2021.
//

import Foundation



extension ChatHelper
{
	class AttachmentsObserver
	{
		// MARK: Data
		let messageId: Message.Id
		private var statuses: [Attachment.Id : AttachmentStatus] = [:]
		private let onAllUploaded: Handler<[Attachment.Id]>
		private let onFailure: Handler<Attachment.Id>

		// MARK: Init
		init(
			messageId: Message.Id,
			attachmentsIds: [Attachment.Id],
			onAllUploaded: @escaping Handler<[Attachment.Id]>,
			onFailure: @escaping Handler<Attachment.Id>
		)
		{
			self.messageId = messageId

			for id in attachmentsIds
			{
				statuses[id] = .uploading
			}

			self.onAllUploaded = onAllUploaded
			self.onFailure = onFailure
		}
	}
}



// MARK: - Hashable

extension ChatHelper.AttachmentsObserver: Hashable
{
	func hash(into hasher: inout Hasher)
	{
		hasher.combine(messageId)
	}

	static func == (lhs: ChatHelper.AttachmentsObserver, rhs: ChatHelper.AttachmentsObserver) -> Bool
	{
		lhs.messageId == rhs.messageId
	}
}



// MARK: - Managing Statuses

extension ChatHelper.AttachmentsObserver
{
	func setStatus(
		_ status: AttachmentStatus,
		for attachmentId: Attachment.Id
	)
	{
		statuses[attachmentId] = status

		switch status
		{
		case .uploading:
			break

		case .failed:
			onFailure(attachmentId)

		case .uploaded:
			checkAllUploaded()
		}
	}

	private func checkAllUploaded()
	{
		var newIds: [Attachment.Id] = []

		for status in statuses.values
		{
			if case let .uploaded(newId) = status
			{
				newIds.append(newId)
			}
			else
			{
				return
			}
		}

		onAllUploaded(newIds)
	}
}
