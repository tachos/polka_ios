//
//  SellerAccountHelper.swift
//  Polka
//
//  Created by Makson on 17.05.2021.
//

import Foundation
import PromiseKit

class SellerAccountHelper
{
	let onLogoutButton: VoidCallback
	let showErrorAlert: Handler<String>

	private(set) var sellerName: String?
	private(set) var isSellerWorking = false
	private(set) var productCategories: [SellerProductCategory] = []

	init(
		onLogoutButton: @escaping VoidCallback,
		showErrorAlert: @escaping Handler<String>
	)
	{
		self.onLogoutButton = onLogoutButton
		self.showErrorAlert = showErrorAlert

		let userRole = managers.webService.currentUserProfile?.profile?.role
		sellerName = userRole?.sellerName
		isSellerWorking = userRole?.isSellerOpen ?? false
	}
}

extension SellerAccountHelper
{
	func sendSellerWorkingStatus(
		isSellerWorking: Bool,
		on sellerAccountVC: SellerAccountViewController,
		onSuccess: @escaping VoidCallback
	)
	{
		firstly {
			managers.webService.sendSellerWorkingStatus(isOpen: isSellerWorking)
		}
		.indicateProgress(on: sellerAccountVC.view)
		.done { [weak self] in
			self?.isSellerWorking = isSellerWorking
			Self.updateUserProfile(isSellerWorking: isSellerWorking)
			onSuccess()
		}
		.catch { [weak self] in
			self?.showErrorAlert($0.localizedDescription)
		}
	}

	private static func updateUserProfile(isSellerWorking: Bool)
	{
		managers.webService.setSellerWorkingStatus(
			isSellerWorking: isSellerWorking
		)
	}

	func loadProductCategories(on sellerAccountVC: SellerAccountViewController)
	{
		firstly {
			managers.webService.getSellerProductCategories()
		}
		.indicateProgress(on: sellerAccountVC.view)
		.done { [weak self, weak sellerAccountVC] sellerProductCategories in
			self?.productCategories = sellerProductCategories
			sellerAccountVC?.onProductCategoriesLoaded()
		}
		.catch { [weak self] in
			self?.showErrorAlert($0.localizedDescription)
		}
	}
}

private extension User.Profile.Role
{
	var sellerName: String?
	{
		switch self {
		case .seller(let sellerInfo):
			return sellerInfo?.caption

		case .consumer:
			return nil
		}
	}

	var isSellerOpen: Bool
	{
		switch self {
		case .seller(let sellerInfo):
			return sellerInfo?.isOpen ?? false

		case .consumer:
			return false
		}
	}
}
