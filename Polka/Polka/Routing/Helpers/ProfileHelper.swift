//
//  ProfileHelper.swift
//  Polka
//
//  Created by Makson on 29.04.2021.
//

import Foundation

import PromiseKit


class ProfileHelper
{
	 var navigateToAuthorizationScreen:(()-> Void)?
}

extension ProfileHelper
{
	func logout()
	{
		CartController.shared.removeAllLocally()
		self.navigateToAuthorizationScreen?()
	}
}
