//
//  SplashHelper.swift
//  Polka
//
//  Created by Makson on 21.06.2021.
//

import Foundation
import PromiseKit

class SplashHelper
{
	var navigationToCatalogScreen:(()->Void)?
	var navigationToAuthorization:(()->Void)?
}

extension SplashHelper
{
	func getProfile()
	{
		managers.webService.getProfile()
			.get
			{ _ in
				self.navigationToCatalogScreen?()
			}
			.catch
			{ _ in
				self.navigationToAuthorization?()
			}
	}
	func navigationToScreen()
	{
		navigationToCatalogScreen?()
	}
}
