//
//  WhereBringHelper.swift
//  Polka
//
//  Created by Makson on 12.05.2021.
//

import Foundation
import PromiseKit

class WhereBringHelper
{
	var navigationToCatalogScreen:(()->Void)?
	var presentPopup:(()->Void)?
	var titleBtn = ""
	var address: DeliveryAddresses!
}

extension WhereBringHelper
{
	func setupAddress() -> DeliveryAddresses
	{
		if managers.webService.deliveryAddress != nil
		{
			return managers.webService.deliveryAddress!
		}
		else if managers.webService.currentUserProfile?.profile?.role != nil
		{
			switch managers.webService.currentUserProfile?.profile?.role
			{
			case .consumer(let consumer):
				return consumer?.deliveryAddresses?.last ?? DeliveryAddresses(id: nil, street: nil, house: nil, apartment: nil, floor: nil, entrance: nil, door_code: nil, location_point: LocationPoint(latitude: nil, longitude: nil), comment: nil, is_delivery_possible: nil)
			case .seller(_):
				print("")
			default:
				return DeliveryAddresses(id: nil, street: nil, house: nil, apartment: nil, floor: nil, entrance: nil, door_code: nil, location_point: LocationPoint(latitude: nil, longitude: nil), comment: nil, is_delivery_possible: nil)
			}
		}
		return DeliveryAddresses(id: nil, street: nil, house: nil, apartment: nil, floor: nil, entrance: nil, door_code: nil, location_point: LocationPoint(latitude: nil, longitude: nil), comment: nil, is_delivery_possible: nil)
	}
	
	func updateAddress(street:String, house:String, lat:Double, lng:Double)
	{
		address.house = house
		address.street = street
		address.location_point?.latitude = lat
		address.location_point?.longitude = lng
	}
	
	func createDeliveryAddress(street:String,
							   house:String,
							   apartment:String?,
							   floor:String?,
							   entrance:String?,
							   door_code:String?,
							   location_point: LocationPoint,
							   comment:String?) -> Promise<DeliveryAddresses>
	{
		return managers.webService.createDeliveryAddress(street,
												  house,
												  apartment,
												  floor,
												  entrance,
												  door_code,
												  location_point,
												  comment)
			.get
			{ deliveryAddress in
				self.navigationToScreen()
			}
	}
	
	func updateDeliveryAddress(id:String,
							   street:String,
							   house:String,
							   apartment:String?,
							   floor:String?,
							   entrance:String?,
							   door_code:String?,
							   location_point: LocationPoint,
							   comment:String?) -> Promise<DeliveryAddresses>
	{
		return managers.webService.updateDeliveryAddress(id,
														 street,
														 house,
														 apartment,
														 floor,
														 entrance,
														 door_code,
														 location_point,
														 comment)
			.get
			{ deliveryAddress in
				self.navigationToScreen()
			}
	}
	
	func presentPopupOverScreen()
	{
		presentPopup?()
	}
	
	func navigationToScreen()
	{
		navigationToCatalogScreen?()
	}
}
