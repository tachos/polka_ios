//
//  OnboardingStory.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 04.06.2021.
//

import UIKit


class OnboardingStory
{
	static func start(
		from window: UIWindow,
		startButtonSegue: @escaping VoidCallback
	)
	{
		let onboardingViewController = OnboardingViewController.create()

		onboardingViewController.config = .init(
			pageViewControllers: [
				OnboardingPage0ViewController.create(),
				OnboardingPage1ViewController.create(),
				OnboardingPage2ViewController.create(),
				OnboardingPage3ViewController.create(),
			],
			startButtonSegue: {
				//
				startButtonSegue()
			}
		)

		window.rootViewController = onboardingViewController
	}
	
	static var isInProgress: Bool
	{
		return RootStory.rootViewController is OnboardingViewController
	}
	
	static func skipOnboarding()
	{
		if let window = RootStory.window.warnIfNil()
		{
			TabBarStory.startForUnauthorizedUser(
				in: window
			)
		}
	}
}
