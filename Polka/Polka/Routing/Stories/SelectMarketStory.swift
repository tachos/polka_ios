//
//  SelectMarketStory.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 12.10.2021.
//

import UIKit
import FloatingPanel


class SelectMarketStory
{
	static func present(from catalogViewController: CatalogViewController)
	{
		let selectMarketViewController = SelectMarketViewController.create()
		
		//
		let fpc = FloatingPanelController()
		fpc.set(contentViewController: selectMarketViewController)
		fpc.track(scrollView: selectMarketViewController.marketsTableView)
		
		fpc.delegate = selectMarketViewController // Optional
		
		//
		let alreadyHadSelectedMarket = managers.localStore.currentMarket != nil
		fpc.isRemovalInteractionEnabled = alreadyHadSelectedMarket
		
		//
		fpc.surfaceView.grabberHandle.isHidden = true
		
		//
		let shadow = SurfaceAppearance.Shadow()
		shadow.color = UIColor.black.withAlphaComponent(0.9)
		shadow.offset = CGSize(width: 0, height: -10)
		shadow.radius = 50
		shadow.spread = 0
		fpc.surfaceView.appearance.shadows = [shadow]
		
		//
		fpc.surfaceView.appearance.cornerRadius = 18
		
		//
		fpc.layout = MyFloatingPanelLayout()
		fpc.behavior = MyPanelBehavior()

		//
		fpc.panGestureRecognizer.delegateProxy = selectMarketViewController
		
		// Block everything behind the popup.
		let inputBlockingView = UIView()
		inputBlockingView.backgroundColor = .clear
		inputBlockingView.isUserInteractionEnabled = true
		fpc.view.addSubview(inputBlockingView)
		fpc.view.sendSubviewToBack(inputBlockingView)
		inputBlockingView.edgesToSuperview()

		//
		let helper = LoadMarketsHelper(
			onMarketSelected: { [weak fpc, weak catalogViewController] selectedMarket, marketDidChange in
				//
				fpc?.dismiss(animated: true, completion: nil)
				
				//
				if marketDidChange
				{
					dbgout("Selected market changed, reloading categories...")
					catalogViewController?.onSelectedMarketChanged(selectedMarket)
				}
			}
		)
		
		//
		helper.onLoadingFailed = { [unowned selectMarketViewController] in
			//
			PopUps.showRetryQuery(
				from: selectMarketViewController,
				queryRetryCallback: { [unowned helper, unowned selectMarketViewController] in
					//
					helper.loadMarkets(on: selectMarketViewController)
				},
				cancelButtonTitle: "Выйти",
				onCancelCallback: { [unowned selectMarketViewController] in
					//
					LogoutStory.start(
						on: selectMarketViewController,
						onLogoutCancelled: { [unowned helper, unowned selectMarketViewController] in
							//
							helper.loadMarkets(on: selectMarketViewController)
						}
					)
				}
			)
		}
		
		//
		selectMarketViewController.config = .init(
			helper: helper
		)
		
		//
		catalogViewController.present(fpc, animated: true, completion: nil)
	}
}



private class MyFloatingPanelLayout: FloatingPanelLayout {
	let position: FloatingPanelPosition = .bottom
	let initialState: FloatingPanelState = .half
	var anchors: [FloatingPanelState: FloatingPanelLayoutAnchoring] {
		return [
			.full: FloatingPanelLayoutAnchor(absoluteInset: 55, edge: .top, referenceGuide: .safeArea),

			//.half: FloatingPanelLayoutAnchor(absoluteInset: 336, edge: .top, referenceGuide: .safeArea)
			.half: FloatingPanelLayoutAnchor(fractionalInset: 356.0/794.0, edge: .top, referenceGuide: .safeArea)
		]
	}
}

private class MyPanelBehavior: FloatingPanelBehavior
{
	func allowsRubberBanding(for edge: UIRectEdge) -> Bool {
		return false
	}
	
	func shouldProjectMomentum(_ fpc: FloatingPanelController, to proposedTargetPosition: FloatingPanelState) -> Bool
	{
		switch proposedTargetPosition {
		case .tip:
			return false
		default:
			return true
		}
	}
}
