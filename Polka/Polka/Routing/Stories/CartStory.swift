//
//  CartStory.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 04.06.2021.
//

import Foundation
import SafariServices



class CartStory
{
	static func start(tabBarController: TabBarController) -> NavigationController
	{
		let navigationController = NavigationController()

		let paymentHelper = createPaymentHelper(
			presentingViewController: tabBarController
		)

		let cartViewController = CartViewController.create(
			paymentHelper: paymentHelper
		)

		navigationController.initialize([cartViewController])

		return navigationController
	}
}



// MARK: - Implementation

private extension CartStory
{
	static func showWebPaymentForm(
		presentingViewController: UIViewController,
		url: URL,
		helper: PaymentHelper
	)
	{
		let webViewController = WebViewController()
		webViewController.config = .init(
			url: url,
			delegate: helper,
			dismissSegue:
		{
			[weak presentingViewController] in

			presentingViewController?.dismiss(animated: true)
		})

		presentingViewController.present(
			webViewController.embeddedInNavigation(),
			animated: true,
			completion:
		{
			showOrderOnly()
		})
	}

	static func showOrderOnly()
	{
		if let tabController = RootStory.tabBarController,
		   let tabNavigationControler = tabController
			.getTabViewController(.orders) as? NavigationController,
		   let orderViewController = tabNavigationControler.viewControllers.first(
			where: { $0  is OrdersViewController }
		   )
		{
			tabNavigationControler.viewControllers = [orderViewController]

			tabController.switchToTab(.orders)
		}
	}

	static func createPaymentHelper(
		presentingViewController: UIViewController
	) -> PaymentHelper
	{
		let paymentHelper = PaymentHelper()

		paymentHelper.showPaymentFormSegue =
		{
			[weak paymentHelper, weak presentingViewController] url in

			guard let paymentHelper = paymentHelper,
				  let presentingViewController = presentingViewController
			else
			{
				return
			}

			showWebPaymentForm(
				presentingViewController: presentingViewController,
				url: url,
				helper: paymentHelper
			)
		}

		paymentHelper.hidePaymentFormSegue =
		{
			[weak presentingViewController] in

			presentingViewController?.dismiss(animated: true)
			managers.appsFlyer.sendLog(logText: "payment_success", params: nil)
			
		}

		return paymentHelper
	}
}
