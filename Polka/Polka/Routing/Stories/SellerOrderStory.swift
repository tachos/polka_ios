//
//  SellerOrderStory.swift
//  Polka
//
//  Created by Makson on 31.05.2021.
//

import Foundation
import UIKit

class SellerOrderStory
{
	static func start()-> NavigationControllerA
	{
		let sellerScreenViewController = SellerOrderScreenViewController.create()
		
		let sellerOrderHelper = SellerOrderHelper()
		
		sellerScreenViewController.config = SellerOrderScreenViewController.Config(helper: sellerOrderHelper)
		
		let navigationController = NavigationControllerA.withHiddenBar(
			rootViewController: sellerScreenViewController
		)
		return navigationController
	}
}
