//
//  ProfileStory.swift
//  Polka
//
//  Created by Makson on 29.04.2021.
//

import Foundation
import UIKit


class ProfileStory
{
	static func start() -> NavigationControllerA
	{
		let profileViewController = ProfileViewController.create()
		
		let navigationController = NavigationControllerA.withHiddenBar(
			rootViewController: profileViewController
		)
		
		let profileHelper = ProfileHelper()
		
		profileViewController.config = ProfileViewController.Config(
			helper: profileHelper,
			phone: managers.webService.currentUserProfile?.profile?.phone ?? ""
		)

		profileHelper.navigateToAuthorizationScreen =
		{
			[weak profileViewController ] in

			guard let profileViewController = profileViewController else { return }

			LogoutStory.start(on: profileViewController)
		}
		
		return navigationController
	}
}
