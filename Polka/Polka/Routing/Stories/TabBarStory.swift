//
//  TabBarStory.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 16.09.2021.
//

import UIKit


struct TabBarStory
{
	static func startForUnauthorizedUser(in window: UIWindow)
	{
		showTabBar(
			window: window,
			consumer: nil,
			seller: nil
		)
	}
	
	static func showTabBar(window: UIWindow, consumer:ConsumerInfo?, seller:SellerInfo?)
	{
		var tabs:[Tab] = []
		
		if consumer != nil
		{
			tabs = [.catalog, .cart, .chats, .orders, .contacts]
		}
		else if seller != nil
		{
			tabs = [.statysWork, .sellerOrder, .chats, .contacts]
		}
		else
		{
			tabs = [.catalog, .orders, .contacts]
		}

		//
		let tabBarController = TabBarController.create(tabs)

		window.rootViewController = tabBarController
	}
}
