//
//  SearchResultsStory.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 08.09.2021.
//

import UIKit


struct SearchResultsStory
{
	static func start(
		on navigationController: NavigationController,
		tabBarController: TabBarController
	)
	{
		let searchResultsVC = CatalogSearchViewController.create()
		
		searchResultsVC.config = .init(
			helper: CatalogSearchHelper(),
			goToSellerAction: { [unowned navigationController] in
				//
				SellerStory.startFromSearch(
					on: navigationController,
					sellerShortInfo: $0
				)
			},
			goToChatAction: { [weak tabBarController] in
				//
				tabBarController?.switchToTab(.chats)
			},
			goToCatalogAction: { [weak navigationController] in
				//
				navigationController?.popToRootViewController(animated: true)
			}
		)
		
		navigationController.pushViewController(
			searchResultsVC,
			animated: true
		)
	}
}
