//
//  ChatsStory.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 07.06.2021.
//

import UIKit



class ChatsStory
{
	static func start() -> NavigationController
	{
		let navigationController = NavigationController()

		let chatsViewController = ChatsViewController.create()
		
		chatsViewController.config = .init(
			chatSegue:
			{
				[weak navigationController] chat in

				guard let navigationController = navigationController else { return }

				ChatStory.start(
					in: navigationController,
					receiver: chat.receiver
				)
			}
		)

		navigationController.initialize([chatsViewController])

		return navigationController
	}
}
