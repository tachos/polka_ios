//
//  AuthorizationStory.swift
//  Polka
//
//  Created by Makson on 19.04.2021.
//

import Foundation
import UIKit


class AuthorizationStory
{
    static func start(window: UIWindow)
    {
        let authorizationScreenViewController = AuthorizationViewController.create()
       
		let navigationController = UINavigationController(rootViewController: authorizationScreenViewController)
		navigationController.isNavigationBarHidden = true
        let authorizationHelper = AuthorizationHelper()
		
        window.rootViewController = navigationController
		
		authorizationScreenViewController.config = .init(
			helper: authorizationHelper
		)
		authorizationHelper.skipToCatalogScreen = { [weak window] in
			guard let window = window else
			{
				return
			}
			
			TabBarStory.startForUnauthorizedUser(
				in: window
			)
		}
		authorizationHelper.navigateToNextScreen = { [weak navigationController ,weak authorizationHelper, weak window] phone , timer in
			guard let navigationController = navigationController,
				  let authorizationHelper = authorizationHelper,
				  let window = window
			else
			{
				return
			}
			
			showAuthorizationOTP(on: navigationController, timer: timer, phone: phone, helper: authorizationHelper,window: window)
		}
		authorizationHelper.navigationToWebView = {[weak navigationController] url in
			guard let navigationController = navigationController
			else
			{
				return
			}
			showWebView(on: navigationController, url: url)
		}
    }
	
	static var isInProgress: Bool
	{
		if let navigationController = RootStory.rootViewController as? UINavigationController,
		   navigationController.topViewController is AuthorizationViewController
		{
			return true
		}
		return false
	}
}

private extension AuthorizationStory
{
	static func showWebView(on navigationController: UINavigationController, url:String)
	{
		let privacyViewController = PrivacyViewViewController.create()
		privacyViewController.url = url
		navigationController.pushViewController(privacyViewController, animated: true)
	}
}
private extension AuthorizationStory
{
	static func showAuthorizationOTP(on navigationController: UINavigationController, timer:Int, phone:String,helper:AuthorizationHelper, window: UIWindow)
    {
		let authorizationOtpViewController = AuthorizationOtpViewController.create()
		authorizationOtpViewController.config = AuthorizationOtpViewController.Config(helper: helper)
		helper.navigationToBackScreen = { [weak navigationController] in
			navigationController?.popViewController(animated: true)
		}
		helper.navigationToCatalogScreen = { [weak window] in
			guard let window = window else
			{
				return
			}
			
			checkRole(window: window)
		}
		navigationController.pushViewController(authorizationOtpViewController,animated: true)
    }
}

private extension AuthorizationStory
{
	static func checkRole(window: UIWindow)
	{
		
		
		guard let role = managers.webService.currentUserProfile?.profile?.role
		else
		{
			return
		}
		
		switch  role
		{
			case .consumer(let consumer):
				if consumer?.deliveryAddresses?.last?.street != nil && consumer?.deliveryAddresses?.last?.house != nil
				{
					TabBarStory.showTabBar(window: window,consumer: consumer,seller: nil)
				}
				else
				{
					WhereBringStory.start(window: window,viewController: nil, titleBtn: "Посмотреть каталог")
				}
			case .seller(let seller):
				TabBarStory.showTabBar(window: window,consumer: nil,seller: seller)
		}
	}
}

private extension AuthorizationStory
{
    struct Navigation
    {
        private(set) weak var controller: NavigationControllerA?
        private(set) weak var window: UIWindow?
    }
}
