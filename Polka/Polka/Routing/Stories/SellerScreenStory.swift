//
//  SellerScreenStory.swift
//  Polka
//
//  Created by Makson on 17.05.2021.
//

import Foundation
import UIKit

enum SellerScreenStory
{
	static func start() -> UINavigationController
	{
		let sellerScreenViewController = SellerAccountViewController.create()

		let navigationController = NavigationController.create(
			[sellerScreenViewController]
		)

		let navigateBackAction: VoidCallback = { [weak navigationController] in
			navigationController?.popViewController(animated: true)
		}

		//
		weak var sellerProductsHelper: SellerProductsHelper?
		//
		navigationController.navBarView.onBackButtonCallback = { [weak sellerScreenViewController] in
			guard let sellerScreenViewController = sellerScreenViewController else { return }

			if sellerProductsHelper?.hasUnsavedChanges ?? false {
				showDoYouReallyWantToGoBackDialog(
					from: sellerScreenViewController,
					navigateBack: navigateBackAction
				)
			} else {
				navigateBackAction()
			}
		}

		//
		let sellerScreenHelper = SellerAccountHelper(
			onLogoutButton: { [weak sellerScreenViewController] in
				guard let sellerScreenViewController = sellerScreenViewController else { return }

				LogoutStory.start(on: sellerScreenViewController)
			},
			showErrorAlert: { [weak sellerScreenViewController] errorMessage in
				guard let sellerScreenViewController = sellerScreenViewController else { return }

				showErrorPopup(from: sellerScreenViewController, message: errorMessage)
			}
		)

		sellerScreenViewController.config = SellerAccountViewController.Config(
			helper: sellerScreenHelper,
			onProductCategorySelected: { [weak navigationController] productCategory in
				guard let navigationController = navigationController else { return }

				sellerProductsHelper = pushSellerProductsScreen(
					productCategory: productCategory,
					onto: navigationController
				)
			}
		)

		return navigationController
	}
}

private extension SellerScreenStory
{
	static func pushSellerProductsScreen(
		productCategory: SellerProductCategory,
		onto navigationController: UINavigationController
	) -> SellerProductsHelper
	{
		let vc = SellerProductsViewController.create()

		let helper = SellerProductsHelper(
			productCategory: productCategory,
			showErrorAlert: { [weak vc] in
				guard let vc = vc else { return }

				showErrorPopup(from: vc, message: $0)
			}
		)

		vc.config = .init(
			helper: helper
		)
		navigationController.pushViewController(vc, animated: true)

		return helper
	}
}

private extension SellerScreenStory
{
	static func showLogoutAlert(
		from presentingViewController: UIViewController
	)
	{
		let alertViewController = CustomPopupViewController.create(
			text: "Вы уверены, что хотите выйти из аккаунта?",
			titleYesBtn: "Выйти",
			titleNoBtn: "Отмена",
			yesAction:
		{
			[unowned presentingViewController] in

			presentingViewController.dismiss(
				animated: false,
				completion:
			{
				managers.webService.logout()
					.done
					{
						let appDelegate = UIApplication.shared.delegate as! AppDelegate
						AuthorizationStory.start(window: appDelegate.window!)
					}
					.cauterize()
			})
		},
			noAction:
		{
			[unowned presentingViewController] in

			presentingViewController.dismiss(animated: false)
		})
		alertViewController.modalPresentationStyle = .overFullScreen
		presentingViewController.present(
			alertViewController,
			animated: false
		)
	}

	static func showDoYouReallyWantToGoBackDialog(
		from presentingViewController: UIViewController,
		navigateBack: @escaping VoidCallback
	)
	{
		let alertViewController = CustomPopupViewController.create(
			text: "Вы уверены, что хотите вернуться назад?\nВозможные изменения не сохранятся.",
			titleYesBtn: "Да",
			titleNoBtn: "Нет",
			yesAction: { [weak presentingViewController] in
				presentingViewController?.dismiss(
					animated: false,
					completion: navigateBack
				)
			},
			noAction: { [weak presentingViewController] in
			  presentingViewController?.dismiss(animated: false)
		  }
		)
		alertViewController.modalPresentationStyle = .overFullScreen
		presentingViewController.present(
			alertViewController,
			animated: false
		)
	}

	static func showErrorPopup(
		from presentingViewController: UIViewController,
		message: String
	)
	{
		let popupViewController = CustomPopupOneButtonViewController.create(
			text: message,
			textYesButton: "Ок",
			yesAction: {
				[unowned presentingViewController] in
				presentingViewController.dismiss(animated: false, completion: nil)
			})

		popupViewController.modalPresentationStyle = .overFullScreen
		presentingViewController.present(
			popupViewController,
			animated: false
		)
	}
}
