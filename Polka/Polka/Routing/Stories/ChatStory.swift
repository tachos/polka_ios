//
//  ChatStory.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 30.06.2021.
//

import UIKit
import AVKit



class ChatStory
{
	static func start(
		in navigationController: NavigationController,
		receiver: Chat.Receiver
	)
	{
		let chatViewController = ChatViewController.create()
		chatViewController.hidesBottomBarWhenPushed = true

		let helper = ChatHelper(
			receiver: receiver,
			viewController: chatViewController
		)

		chatViewController.config = .init(
			helper: helper,
			viewAttachmentSegue:
		{
			[weak chatViewController] attachment in

			guard let chatViewController = chatViewController else { return }

			showAttachmentViewer(
				for: attachment,
				presentingViewController: chatViewController
			)
		},
			pickMediaSegue:
		{
			[weak chatViewController] handler in

			guard let chatViewController = chatViewController else { return }

			PickMediaStory.start(
				from: chatViewController,
				maxItemsCount: Message.maxAttachmentsCount,
				completion: handler
			)
		},
			sellerSegue: getSellerSegue(
				receiver,
				navigationController: navigationController
			)
		)

		navigationController.pushViewController(
			chatViewController,
			animated: true
		)
	}

	static func startInTab(receiver: Chat.Receiver)
	{
		if let tabController = RootStory.tabBarController,
		   let tabNavigationControler = tabController.getTabViewController(.chats) as? NavigationController
		{
			tabController.selectTab(.chats)

			tabNavigationControler.popToRootViewController(
				animated: false
			)

			start(
				in: tabNavigationControler,
				receiver: receiver
			)
		}
	}
}



private extension ChatStory
{
	static func getSellerSegue(
		_ receiver: Chat.Receiver,
		navigationController: UINavigationController
	) -> VoidCallback?
	{
		switch receiver
		{
		case .consumer:
			return nil

		case .seller(let seller):
			let completion: VoidCallback =
			{
				[weak navigationController] in

				navigationController?.popViewController(animated: false)
				SellerStory.startInTab(seller: seller)
			}

			return completion
		}
	}
	static func showAttachmentViewer(
		for attachment: Attachment,
		presentingViewController: UIViewController
	)
	{
		switch attachment.content
		{
		case .localImage(let image):
			showImageViewer(
				for: .local(image),
				presentingViewController: presentingViewController
			)

		case .remoteImage(let image):
			showImageViewer(
				for: .remote(image),
				presentingViewController: presentingViewController
			)

		case .localVideo(let video):
			showVideoViewer(
				videoUrl: video.url,
				presentingViewController: presentingViewController
			)

		case .remoteVideo(let video):
			showVideoViewer(
				videoUrl: video.streamUrl,
				presentingViewController: presentingViewController
			)
		}
	}

	static func showImageViewer(
		for image: Image,
		presentingViewController: UIViewController
	)
	{
		let imageViewer = ImageViewerViewController()

		imageViewer.config = .init(
			image: image,
			dismissSegue:
		{
			[weak presentingViewController] in

			presentingViewController?.dismiss(animated: true)
		})

		presentingViewController.present(
			imageViewer.embeddedInNavigation(),
			animated: true
		)
	}

	static func showVideoViewer(
		videoUrl: URL,
		presentingViewController: UIViewController
	)
	{
		let player = AVPlayer(url: videoUrl)

		let playerViewController = AVPlayerViewController()
		playerViewController.player = player

		presentingViewController.present(
			playerViewController,
			animated: true,
			completion:
		{
			player.play()
		})
	}
}
