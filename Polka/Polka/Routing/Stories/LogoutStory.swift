//
//  LogoutStory.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 01.07.2021.
//

import UIKit



// use this story as single entry point for logout flow

class LogoutStory
{
	static func start(
		on presentingViewController: UIViewController,
		onLogoutCancelled: VoidCallback? = nil
	)
	{
		showLogoutAlert(
			on: presentingViewController,
			completion:
		{
			guard let window = RootStory.window else { return }

			managers.webService.logout()
			.done
			{
				// reset all the data here
				managers.chats.reset()
				CartController.shared.removeAllLocally()

				AuthorizationStory.start(window: window)
			}
			.cauterize() // do we need a retry popup on error?
		},
			onLogoutCancelled: onLogoutCancelled
		)
	}
}



private extension LogoutStory
{
	static func showLogoutAlert(
		on presentingViewController: UIViewController,
		completion: @escaping VoidCallback,
		onLogoutCancelled: VoidCallback?
	)
	{
		let alertViewController = CustomPopupViewController.create(
			text: "Вы уверены, что хотите выйти из аккаунта?",
			titleYesBtn: "Выйти",
			titleNoBtn: "Отмена",
			yesAction:
		{
			[weak presentingViewController] in

			presentingViewController?.dismiss(
				animated: false,
				completion: completion
			)
		},
			noAction:
		{
			[weak presentingViewController] in

			presentingViewController?.dismiss(
				animated: false,
				completion: onLogoutCancelled
			)
		})

		alertViewController.modalPresentationStyle = .overFullScreen

		presentingViewController.present(
			alertViewController,
			animated: false
		)
	}
}
