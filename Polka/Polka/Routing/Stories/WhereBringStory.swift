//
//  WhereBringStory.swift
//  Polka
//
//  Created by Makson on 29.04.2021.
//

import Foundation
import UIKit

class WhereBringStory
{
	static func start(window: UIWindow? , viewController: UIViewController? , titleBtn:String)
	{
		let whereBringViewController = WhereBringViewController.create()
	   
		
		let whereBringHelper = WhereBringHelper()
		whereBringViewController.config = WhereBringViewController.Config(
			helper: whereBringHelper)
		whereBringHelper.titleBtn = titleBtn
		
		if window != nil
		{
			let navigationController = NavigationControllerA.withHiddenBar(
				rootViewController: whereBringViewController
			)
			whereBringHelper.navigationToCatalogScreen = { [weak window]  in
				guard let window = window else
				{
					return
				}
				showTabBar(window: window)
			}
			window!.rootViewController = navigationController
		}
		else if viewController != nil
		{
			whereBringHelper.navigationToCatalogScreen = { [weak viewController]  in
				guard let viewController = viewController else
				{
					return
				}
				viewController.dismiss(animated: true, completion: { [weak viewController] in
					viewController?.viewWillAppear(true)
				})
			}
			whereBringHelper.presentPopup = {
				showGeoAlert(on: whereBringViewController)
			}
			whereBringViewController.modalPresentationStyle = .overFullScreen
			viewController?.present(whereBringViewController, animated: true, completion: nil)
		}
	}
	
	static func showTabBar(window: UIWindow)
	{
		let tabBarController = TabBarController.create([.catalog, .cart, .chats, .orders, .contacts])

		window.rootViewController = tabBarController
	}
	
	static func showGeoAlert(
		on presentingViewController: UIViewController
	)
	{
		let alertViewController = CustomPopupOneButtonViewController.create(
			text: "Необходимо предоставить доступ к гео-локации в настройках вашего телефона для доставки заказа по вашему адресу.",
			textYesButton: "Ок",
			yesAction:
			{ [unowned presentingViewController] in
				presentingViewController.dismiss(animated: false, completion: nil)
			}
		)
		
		alertViewController.modalPresentationStyle = .overFullScreen
		presentingViewController.present(
			alertViewController,
			animated: false
		)
	}
}

