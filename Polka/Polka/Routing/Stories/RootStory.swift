//
//  RootStory.swift
//  Polka
//
//  Created by Makson on 19.04.2021.
//

import Foundation
import UIKit

class RootStory
{
    static func start(in window: UIWindow)
    {
		if managers.webService.isAuthorized
		{
			SplashStory.start(window: window)
		}
		else
		{
						
#if DEBUG
			
			// skip onboarding in debug mode
			TabBarStory.startForUnauthorizedUser(
				in: window
			)
			
#else
			OnboardingStory.start(
				from: window,
				startButtonSegue: {
					//
					if managers.webService.isBring
					{
						WhereBringStory.start(
							window: window,
							viewController: nil,
							titleBtn: "Посмотреть каталог"
						)
					}
					else
					{
						managers.webService.resetAuthorization()
						AuthorizationStory.start(window: window)
					}
				}
			)
#endif
			
		}
    }
}



// MARK: UI State

extension RootStory
{
	static var window: UIWindow?
	{
		return UIApplication.shared.windows.first
	}

	static var rootViewController: UIViewController?
	{
		return window?.rootViewController
	}

	static var tabBarController: TabBarController?
	{
		return rootViewController as? TabBarController
	}
}



// MARK: - Redirect

extension RootStory
{
	typealias RedirectionCallback = Handler<TabBarController>
	
	private static var pendingRedirect: RedirectionCallback?


	static func redirectFromTabBar(
		skipOnboarding: Bool = false,
		requiresAuthorization: Bool = false,
		_ redirectionCallback: @escaping RedirectionCallback
	)
	{
		if let rootTabBarController = tabBarController,
		   rootTabBarController.isViewLoaded,
		   !rootTabBarController.children.isEmpty
		{
			redirectionCallback(rootTabBarController)
		}
		else
		{
			pendingRedirect = redirectionCallback
		}
		
		//
		if OnboardingStory.isInProgress && skipOnboarding
		{
			OnboardingStory.skipOnboarding()
		}
		// Skip "Auth by Phone Number" screen.
		else if !requiresAuthorization && AuthorizationStory.isInProgress
		{
			if let window = window.warnIfNil() {
				TabBarStory.startForUnauthorizedUser(
					in: window
				)
			}
		}
	}

	static func handlePendingRedirect(from tabBarController: TabBarController)
	{
		pendingRedirect?(tabBarController)
		pendingRedirect = nil
	}
}
