//
//  PickMediaStory.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 28.06.2021.
//

import UIKit
import YPImagePicker



class PickMediaStory
{
	static func start(
		from presentingViewController: UIViewController,
		maxItemsCount: Int,
		completion: @escaping Handler<[LocalMedia]> // may be extended for Videos
	)
	{
		var config = YPImagePickerConfiguration()
		config.startOnScreen = .library
		config.screens =  /*[.library, .photo]*/ [.library, .photo, .video]
		config.showsCrop = .none
		config.filters = []
		config.onlySquareImagesFromCamera = false
		config.showsPhotoFilters = false
		config.hidesStatusBar = false
		config.hidesBottomBar = false
		config.hidesCancelButton = false
		config.library.maxNumberOfItems = maxItemsCount
		config.library.mediaType = /*.photo*/ .photoAndVideo
		config.library.onlySquare = false
		config.library.isSquareByDefault = false
		config.video.compression = "AVAssetExportPresetMediumQuality"
		config.video.fileType = .mov
		config.video.trimmerMaxDuration = 60
		config.video.automaticTrimToTrimmerMaxDuration = true

		let picker = YPImagePicker(configuration: config)
		picker.didFinishPicking(
			completion:
		{
			[weak presentingViewController] items, _ in

			let images: [LocalMedia] = items.compactMap
			{
				switch $0
				{
				case .photo(let photo):
					return .image(photo.image)

				case .video(let video):
					let localVideo = LocalVideo(
						previewImage: video.thumbnail,
						url: video.url,
						duration: video.asset?.duration
					)
					return .video(localVideo)
				}
			}

			presentingViewController?.dismiss(
				animated: true,
				completion:
			{
				completion(images)
			})
		})

		presentingViewController.present(picker, animated: true)
	}
}
