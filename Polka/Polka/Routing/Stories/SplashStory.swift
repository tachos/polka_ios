//
//  SplashStory.swift
//  Polka
//
//  Created by Makson on 21.06.2021.
//

import Foundation
import UIKit

class SplashStory
{
	static func start(window: UIWindow?)
	{
		let splashViewController = SplashViewController.create()
		
		let splashHelper = SplashHelper()
		let navigationController = NavigationControllerA.withHiddenBar(
			rootViewController: splashViewController
		)
		splashViewController.config = SplashViewController.Config(
			helper: splashHelper)
		
		splashHelper.navigationToCatalogScreen = { [weak window] in
			checkRole(window: window!)
		}
		splashHelper.navigationToAuthorization = { [weak window] in
			
			guard let window = window else { return }
			
			AuthorizationStory.start(window: window)
		}
		
		window?.rootViewController = navigationController
	}
	
	static func checkRole(window: UIWindow)
	{
		
		
		guard let role = managers.webService.currentUserProfile?.profile?.role
		else
		{
			return
		}
		
		switch  role
		{
			case .consumer(let consumer):
				showTabBar(window: window,consumer: consumer,seller: nil)
				
			case .seller(let seller):
				showTabBar(window: window,consumer: nil,seller: seller)
		}
	}
	
	static func showTabBar(window: UIWindow, consumer:ConsumerInfo?, seller:SellerInfo?)
	{
		var tabBar:[Tab] = []
		
		if consumer != nil
		{
			tabBar = [.catalog, .cart, .chats, .orders, .contacts]
		}
		else if seller != nil
		{
			tabBar = [.statysWork, .sellerOrder, .chats, .contacts]
		}
		else
		{
			tabBar = [.catalog, .orders, .contacts]
		}
		let tabBarController = TabBarController.create(tabBar)

		window.rootViewController = tabBarController
	}
}
