//
//  SellerStory.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 30.06.2021.
//

import UIKit



class SellerStory
{
	static func start(
		in navigationController: UINavigationController,
		seller: Seller,
		categoryId: String?,
		categoryDescription:String?
	)
	{
		var categoryPartialInfo: CategoryPartialInfo? = nil
		if let id = categoryId
		{
			categoryPartialInfo = (id, categoryDescription ?? "")
		}

		//
		let sellerViewController = SellerViewController.create()

		sellerViewController.config = .init(
			mode: .withFullInfo(seller),
			cartController: CartController.shared,
			categoryPartialInfo: categoryPartialInfo,
			chatSegue: getChatSegue()
		)

		sellerViewController.hidesBottomBarWhenPushed = true
		navigationController.pushViewController(
			sellerViewController,
			animated: true
		)
	}

	static func startInTab(
		seller: Seller
	)
	{
		// Copy-pasted from CartViewController + Utils
		// probably needs refactoring
		if let tabController = RootStory.tabBarController,
		   let tabNavigationControler = (tabController.getTabViewController(.catalog) as? NavigationController).warnIfNil(),
		   let catalogViewController = tabNavigationControler
			.viewControllers
			.first(where: { $0  is CatalogViewController })
			.warnIfNil()
		{
			let sellerViewController = SellerViewController.create()
			
			sellerViewController.config = .init(
				mode: .withFullInfo(seller),
				cartController: CartController.shared,
				categoryPartialInfo: nil,
				chatSegue: getChatSegue()
			)

			sellerViewController.hidesBottomBarWhenPushed = true
			tabNavigationControler.viewControllers = [
				catalogViewController,
				sellerViewController
			]
			
			tabController.switchToTab(.catalog)
		}
	}
}



extension SellerStory
{
	static func startFromDeeplink(
		on navigationController: UINavigationController,
		sellerId: String,
		marketAlias: String
	)
	{
		let sellerViewController = SellerViewController.create()
		
		sellerViewController.config = .init(
			mode: .fromDeepLink(sellerId),
			cartController: CartController.shared,
			categoryPartialInfo: nil,
			chatSegue: getChatSegue()
		)
		
		sellerViewController.hidesBottomBarWhenPushed = true
		navigationController.pushViewController(
			sellerViewController,
			animated: true
		)
	}
}



extension SellerStory
{
	static func startFromSearch(
		on navigationController: UINavigationController,
		sellerShortInfo: SellerShortInfo
	)
	{
		let sellerViewController = SellerViewController.create()
		
		sellerViewController.config = .init(
			mode: .fromSearch(sellerShortInfo),
			cartController: CartController.shared,
			categoryPartialInfo: nil,
			chatSegue: getChatSegue()
		)
		
		sellerViewController.hidesBottomBarWhenPushed = true
		navigationController.pushViewController(
			sellerViewController,
			animated: true
		)
	}
}



private extension SellerStory
{
	static func getChatSegue() -> Handler<Seller>?
	{
		if managers.webService.isAuthorized
		{
			return {
				seller in

				ChatStory.startInTab(
					receiver: .seller(seller)
				)
			}
		}

		return nil
	}
}
