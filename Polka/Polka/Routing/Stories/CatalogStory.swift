//
//  CatalogStory.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 12.10.2021.
//

import UIKit


class CatalogStory
{
	static func start(tabBarController: TabBarController) -> NavigationController
	{
		let catalogVC = CatalogViewController.create()

		let navigationController = NavigationController.create(
			[catalogVC]
		)

		catalogVC.config = .init(
			onViewWillAppear: { [unowned catalogVC] in
				//
				if managers.localStore.currentMarket == nil
				{
					SelectMarketStory.present(from: catalogVC)
				}
			},
			
			showSellersByCategoryScreen: { [unowned navigationController] category in
				//
				let vc = SellersByCategoryViewController.create(
					navigationController,
					category.id,
					category.caption,
					category.sellers,
					category.description
				)
				
				navigationController.pushViewController(vc, animated: true)
			},

			searchButtonAction: { [unowned navigationController, unowned tabBarController] in
				//
				SearchResultsStory.start(
					on: navigationController,
					tabBarController: tabBarController
				)
			},
			
			showMarketsSegue: { [unowned catalogVC] in
				//
				SelectMarketStory.present(from: catalogVC)
			},
			
			deliveryAddressSegue: { [unowned catalogVC] in
				//
				if managers.webService.isAuthorized
				{
					WhereBringStory.start(
						window: nil,
						viewController: catalogVC,
						titleBtn: "Вернуться в каталог"
					)
				}
				else
				{
					dbgUnreachable("Address picker should be disabled!")
				}
			}
		)
		
		return navigationController
	}
}
