//
//  Chat.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 23.06.2021.
//

import Foundation
import SwiftyJSON



struct Chat
{
	let receiver: Receiver
	let lastMessage: Message
	let unreadCount: Int 
}



extension Chat
{
	enum Receiver
	{
		case seller(Seller)
		case consumer(Consumer)

		var id: String
		{
			switch self
			{
			case .seller(let seller):
				return seller.id

			case .consumer(let consumer):
				return consumer.id
			}
		}
	}
}



extension Chat
{
	func updated(unreadCount: Int) -> Chat
	{
		return Chat(
			receiver: receiver,
			lastMessage: lastMessage,
			unreadCount: unreadCount
		)
	}
}



// MARK: - JSONDeserializable

extension Chat: JSONDeserializable
{
	static func from(json: JSON) -> Chat?
	{
		if let receiver = Receiver.from(json: json["receiver"]),
		   let lastMessage = Message.from(json: json["last_message"]),
		   let unreadCount = json["unread_count"].int
		{
			return .init(
				receiver: receiver,
				lastMessage: lastMessage,
				unreadCount: unreadCount
			)
		}

		return nil
	}
}



extension Chat.Receiver: JSONDeserializable
{
	static func from(json: JSON) -> Chat.Receiver?
	{
		switch json["role"].string
		{
		case "consumer":
			return Consumer
				.from(json: json)
				.flatMap({ .consumer($0) })

		case "seller":
			return Seller
				.from(json: json)
				.flatMap({ .seller($0) })

		default:
			return nil
		}
	}
}
