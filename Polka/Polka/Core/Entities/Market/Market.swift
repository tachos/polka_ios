//
//  Market.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 12.10.2021.
//

import SwiftyJSON


typealias MarketAlias = String


struct Market: Codable
{
	let alias: MarketAlias
	
	let caption: String
	let description: String
	let address: String
	
	//let geolocation: ?
	
	let isOpenNow: Bool
	
	let workHoursStart: String
	let workHoursEnd: String
}


extension Market
{
	var workHoursString: String
	{
		"\(workHoursStart) - \(workHoursEnd)"
	}
}


extension Market: JSONDeserializable
{
	static func from(json: JSON) -> Market?
	{
		if let alias = json["alias"].string.warnIfNil(),
		   
		   let caption = json["caption"].string.warnIfNil(),
		   let description = json["description"].string.warnIfNil(),
		   let address = json["address"].string.warnIfNil(),
		   
		   let isOpenNow = json["is_open_now"].bool.warnIfNil(),
		   
		   let workHoursStart = json["start_work"].string.warnIfNil(),
		   let workHoursEnd = json["end_work"].string.warnIfNil()
		{
			return Market(
				alias: alias,
				
				caption: caption,
				description: description,
				address: address,
				
				isOpenNow: isOpenNow,
				
				workHoursStart: workHoursStart,
				workHoursEnd: workHoursEnd
			)
		}

		return nil
	}
}
