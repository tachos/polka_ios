//
//  FoundProduct.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 17.09.2021.
//

import SwiftyJSON



struct FoundProduct
{
	let cartProduct: CartProduct
	let seller: SellerShortInfo
}



extension FoundProduct: JSONDeserializable
{
	static func from(json: JSON) -> FoundProduct?
	{
		if let cartProduct = CartProduct.from(json: json).warnIfNil(),
		   let sellerShortInfo = SellerShortInfo.from(json: json["seller"]).warnIfNil()
		{
			return FoundProduct(
				cartProduct: cartProduct,
				seller: sellerShortInfo
			)
		}
		
		return nil
	}
}
