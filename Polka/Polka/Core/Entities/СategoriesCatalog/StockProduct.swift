//
//  StockProduct.swift
//  Polka
//
//  Created by Makson on 18.05.2021.
//

import Foundation

struct StockProduct
{
	let id: String
	let price_nominal: Int
	let balance: Float
}

struct AssembleProduct
{
	let id: String
	var count: Float
}
