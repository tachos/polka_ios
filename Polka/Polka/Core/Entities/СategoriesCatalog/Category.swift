//
//  Category.swift
//  Polka
//
//  Created by Michael Goremykin on 28.04.2021.
//

import SwiftyJSON

struct Category
{
	let id:				String
	let alias: 			String
	let imageUrl:		URL?
	let caption: 		String
	let description:	String
	let sellers:		[Seller]
}

extension Category: JSONDeserializable
{
	static func from(json: JSON) -> Category?
	{
		if let id = json["id"].string,
		   let alias = json["alias"].string,
		   let caption = json["caption"].string,
		   let description = json["description"].string,
		   let sellers: [Seller] = AlamofireUtils.parseArray(from: json["sellers"],
												   skipFailedElements: true,
												   canBeEmpty: false)
		{
			return Category(id: id,
							alias: alias,
							imageUrl: json["art"].url,
							caption: caption,
							description: description,
							sellers: sellers)
		}
		
		return nil
	}
}

extension Category: Equatable
{
	static func == (lhs: Category, rhs: Category) -> Bool
	{
		lhs.id == rhs.id &&
		lhs.imageUrl == rhs.imageUrl &&
		Set(lhs.sellers) == Set(rhs.sellers)
	}
}

extension Category: Hashable
{
	func hash(into hasher: inout Hasher)
	{
		hasher.combine(id)
	}
}
