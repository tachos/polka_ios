//
//  SearchCategory.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 10.09.2021.
//

import SwiftyJSON



struct CatalogSearchData
{
	let filters: [SearchCategory]
	let sellers: [SellerShortInfo]
}


struct SearchCategory
{
	let id: String
	let caption: String
}

struct SellerShortInfo
{
	let id: SellerId
	let caption: String
	let description: String
	let isOpen: Bool
}




extension CatalogSearchData: JSONDeserializable
{
	static func from(json: JSON) -> CatalogSearchData?
	{
		let filters: [SearchCategory]? = AlamofireUtils.parseArray(
			from: json["categories"],
			skipFailedElements: true,
			canBeEmpty: false
		)
		let sellers: [SellerShortInfo]? = AlamofireUtils.parseArray(
			from: json["sellers"],
			skipFailedElements: true,
			canBeEmpty: false
		)
		
		if let filters = filters,
		   let sellers = sellers
		{
			return CatalogSearchData(
				filters: filters,
				sellers: sellers
			)
		}
		
		return nil
	}
}



extension SearchCategory: JSONDeserializable
{
	static func from(json: JSON) -> SearchCategory?
	{
		if let id = json["id"].string,
		   let caption = json["caption"].string
		{
			return SearchCategory(
				id: id,
				caption: caption
			)
		}
		
		return nil
	}
}



extension SellerShortInfo: JSONDeserializable
{
	static func from(json: JSON) -> SellerShortInfo?
	{
		if let id = json["id"].string.warnIfNil(),
		   let caption = json["caption"].string.warnIfNil(),
		   let description = json["description"].string.warnIfNil(),
		   let isOpen = json["is_open_now"].bool.warnIfNil()
		{
			return SellerShortInfo(
				id: id,
				caption: caption,
				description: description,
				isOpen: isOpen
			)
		}
		
		return nil
	}
}
