//
//  SellerProduct.swift
//  Polka
//
//  Created by Makson on 21.04.2021.
//

import SwiftyJSON


typealias Price = Double
typealias ProductQuantity = Float


struct SellerCatalog: JSONDeserializable
{
	let sellerInfo: SellerInfo
	let sellerCatalogProducts: [SellerCatalogProduct]
}

struct SellerCatalogProduct
{
	let product: Product
	let stockQuantity: Float
	let holds: Float
}

struct ProductStock
{
	let product		: Product
	let quantity	: Float
	
}

extension SellerCatalog
{
	static func from(json: JSON) -> SellerCatalog?
	{
		if let sellerInfo: SellerInfo 	= SellerInfo.from(json: json["seller"]),
		   let products: [SellerCatalogProduct] = AlamofireUtils.parseArray(from: json["products"],
																	skipFailedElements: true,
																	canBeEmpty: false)
		{
			return SellerCatalog(sellerInfo: sellerInfo, sellerCatalogProducts: products)
		}
		
		return nil
	}
}

extension SellerCatalog
{
	func getStockQuantityForProduct(byId id: String) -> Float
	{
		return sellerCatalogProducts.first { $0.product.id == id }?.stockQuantity ?? 0
	}
}

extension SellerCatalogProduct: JSONDeserializable
{
	static func from(json: JSON) -> SellerCatalogProduct?
	{
		if let product = Product.from(json: json["product"]),
		   let stockAmount = json["balance"].float,
		   let holds = json["holds"].float
		{
			return .init(product: product, stockQuantity: stockAmount, holds: holds)
		}
		
		return nil
	}
}

extension ProductStock: JSONDeserializable
{
	static func from(json: JSON) -> ProductStock?
	{
		if let product: Product = Product.from(json: json["product"]),
		   let quantity 		= json["balance"].float
		{
			return ProductStock(product: product,
								quantity: quantity)
		}
		
		return nil
	}
}

struct Product
{
	let id: 					String
	let categoryId: 			String
	let name:					String
	let imageUrl:				URL?
	let description: 			String
	let price:					Int
	let nominalPrice:			Int
	let unitWeight:				Float
	let unit: 					MeasurementUnit
	let minCountProductInCart: 	Float
	let stepChangeInCart: 		Float
}

extension Product: JSONDeserializable
{
	static func from(json: JSON) -> Product?
	{
		if let id 						= json["id"].string.warnIfNil(),
		   let categoryId 				= json["category_id"].string.warnIfNil(),
		   let name 					= json["caption"].string.warnIfNil(),
		   let description 				= json["description"].string.warnIfNil(),
		   let price					= json["price"].int.warnIfNil(),
		   let nominalPrice				= json["price_nominal"].int.warnIfNil(),
		   let unitWeight 				= json["weight_for_unit"].float.warnIfNil(),
		   let unit 					= MeasurementUnit.from(json: json["unit_of_count"]).warnIfNil(),
		   let minCountProductInCart	= json["min_order_count"].float.warnIfNil(),
		   let stepChangeInCart 		= json["step_order_count"].float.warnIfNil()
		{
			return Product(id					: id,
						   categoryId			: categoryId,
						   name					: name,
						   imageUrl				: json["art"].url,
						   description			: description,
						   price				: price,
						   nominalPrice			: nominalPrice,
						   unitWeight			: unitWeight,
						   unit					: unit,
						   minCountProductInCart: minCountProductInCart,
						   stepChangeInCart		: stepChangeInCart)
		}
		
		return nil
	}
}

// MARK:- Purchase availability
enum PurchaseAvailability
{
	case available
	case notAvailable(Set<NotPurchasableReason>)
}

enum NotPurchasableReason
{
	case sellerNotAvailable
	case outOfStock
}

typealias CartA = [SellerCatalog]

extension Product
{
	func couldBePurchased(_ cart: CartA, _ sellerId: String) -> PurchaseAvailability
	{
		return .notAvailable([])
	}
}

// MARK:- Mesurement units
enum MeasurementUnit
{
	case unit
	case kg
	case l
}

// MARK:- JSONDeserializable
extension MeasurementUnit:JSONDeserializable
{
	static func from(json: JSON) -> MeasurementUnit?
	{
		if let rawValue = json.string?.lowercased()
		{
			switch rawValue
			{
			case "kg", "кг":
				return .kg
				
			case "l", "л":
				return .l
				
			case "qt", "шт":
				return .unit
			default:
				return nil
			}
		}
		
		return nil
	}
}

// MARK: - MeasurementUnit utils
extension MeasurementUnit
{
	func toString() -> String
	{
		switch self
		{
		case .unit:
			return "шт"
		case .kg:
			return "кг"
		case .l:
			return "л"
		}
	}
}
