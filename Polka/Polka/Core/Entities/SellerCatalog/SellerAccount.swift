//
//  SellerAccount.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 01.03.2022.
//

import SwiftyJSON

typealias SellerCaterogyId = String

struct SellerProductCategory
{
	let id: SellerCaterogyId
	let name: String
	let imageUrl: URL?
}

extension SellerProductCategory: JSONDeserializable
{
	static func from(json: JSON) -> SellerProductCategory?
	{
		if let id = json["id"].string,
		   let name = json["caption"].string
		{
			return SellerProductCategory(
				id: id,
				name: name,
				imageUrl: json["art"].url
			)
		}
		return nil
	}
}
