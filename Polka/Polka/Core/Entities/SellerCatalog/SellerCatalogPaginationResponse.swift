//
//  SellerCatalogPaginationResponse.swift
//  Polka
//
//  Created by Michael Goremykin on 19.07.2021.
//

import SwiftyJSON

struct SellerCatalogPaginationResponse
{
	let count: Int
	let next: URL?
	let previous: URL?
	let sellerInfo: SellerInfo
	let sellerProducts: [SellerCatalogProduct]
}

extension SellerCatalogPaginationResponse: JSONDeserializable
{
	static func from(json: JSON) -> SellerCatalogPaginationResponse?
	{
		if let count = json["count"].int,
		   let sellerInfo = SellerInfo.from(json: json["seller"]),
		   let products: [SellerCatalogProduct] = AlamofireUtils.parseArray(from: json["products"],
															   skipFailedElements: true,
															   canBeEmpty: true)
		{
			return .init(count: count,
						 next: json["next"].url,
						 previous: json["previous"].url,
						 sellerInfo: sellerInfo,
						 sellerProducts: products)
		}
		
		return nil
	}
}
