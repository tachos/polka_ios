//
//  Message.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 04.06.2021.
//

import Foundation
import SwiftyJSON



struct Message
{
	let id: Id
	
	let authorId: PartyId
	let receiverId: PartyId

	let date: Date
	let content: Content

	var direction: Direction
	{
		return authorId == managers.webService.currentUserProfile?.profile?.id
			? .outgoing
			: .incoming
	}
}



// MARK: - Nested Types And Consts

extension Message
{
	typealias Id = String
	typealias PartyId = String

	enum Direction
	{
		case incoming
		case outgoing
	}

	enum Content
	{
		case text(String)
		case media([Attachment])
		// TODO: add audio
	}

	static let maxAttachmentsCount = 10
}



// MARK: - Hashable

extension Message: Hashable
{
	func hash(into hasher: inout Hasher)
	{
		hasher.combine(id)
	}

	static func == (lhs: Message, rhs: Message) -> Bool
	{
		lhs.id == rhs.id
	}
}



// MARK: - Grouping

struct MessagesByDate
{
	let date: Date
	let messages: [Message]
}



struct MessagesList
{
	let count: Int
	let isListEnd: Bool
	let messages: [Message]
}



// MARK: - Message / JSONDeserializable

extension Message: JSONDeserializable
{
	static func from(json: JSON) -> Message?
	{
		if let id = json["id"].string,
		   let authorId = json["author_id"].string,
		   let receiverId = json["receiver_id"].string,
		   let date = json["date"].string.flatMap({ DatesFormatter.fromResponse($0) }),
		   let content = Message.Content.from(json: json)
		{
			return .init(
				id: id,
				authorId: authorId,
				receiverId: receiverId,
				date: date,
				content: content
			)
		}

		return nil
	}

	static func from(notification string: String) -> Message?
	{
		return string.data(using: .utf8)
			.flatMap { try? JSON(data: $0) }
			.flatMap { $0["message"] }
			.flatMap { Message.from(json: $0) }
	}
}



// MARK: - Message.Content / JSONDeserializable

extension Message.Content: JSONDeserializable
{
	static func from(json: JSON) -> Message.Content?
	{
		if let text = json["body"].string,
		   !text.isEmpty
		{
			return .text(text)
		}

		if let attachments = json["attachments"].array?
			.compactMap({ Attachment.from(json: $0) }),
		   !attachments.isEmpty
		{
			return .media(attachments)
		}

		return nil
	}
}



// MARK: - MessagesList / JSONDeserializable

extension MessagesList: JSONDeserializable
{
	static func from(json: JSON) -> MessagesList?
	{
		if let count = json["count"].int,
		   let isListEnd = json["is_list_end"].bool,
		   let messages: [Message] = AlamofireUtils.parseArray(
			from: json["results"],
			skipFailedElements: true
		   )
		{
			return .init(
				count: count,
				isListEnd: isListEnd,
				messages: messages
			)
		}

		return nil 
	}
}
