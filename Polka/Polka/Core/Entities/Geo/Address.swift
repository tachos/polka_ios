//
//  Address.swift
//  Polka
//
//  Created by Makson on 11.05.2021.
//

import Foundation
import SwiftyJSON

struct Address
{
	var id:String?
	var house: String?
	var street: String?
	var lat: Double?
	var lng: Double?
}

extension Address:Codable
{}
extension Address: JSONDeserializable
{
	static func from(json: JSON) -> Address?
	{
		
		if let dictionary = json.dictionaryObject,
			let results = dictionary["results"] as? [Any]
		{
			for result  in results
			{
				guard let result = result as? [String:Any] else
				{
					return nil
				}
				if let types = result["types"] as? [String]
				{
					if types.contains("street_address")
					{
						if let formattedAddress = result["address_components"] as? [Any],
						let id = result["place_id"] as? String,
						let addressHouse = formattedAddress.first as? [String: Any],
						let addressStreet = formattedAddress[1] as? [String: Any],
						let long_name_House = addressHouse["long_name"] as? String,
						let long_name_Street = addressStreet["short_name"] as? String,
						let geometry = result["geometry"] as? [String: Any],
						let location = geometry["location"] as? [String: Any],
						let lat = location["lat"] as? Double,
						let lng = location["lng"] as? Double
						{
							return Address(id: id,house: long_name_House, street: long_name_Street, lat: lat, lng: lng)
						}
					}
				}
			}
			return nil
		}
		else
		{
			return nil
		}
	}
}

extension Address
{
	static func getAddressConsumer() -> String
	{
		if managers.webService.deliveryAddress != nil
		{
			let street = managers.webService.deliveryAddress?.street
			let house = managers.webService.deliveryAddress?.house
			return "\(street ?? ""), \(house ?? "")"
		}
		else if managers.webService.currentUserProfile?.profile?.role != nil
		{
			switch managers.webService.currentUserProfile?.profile?.role
			{
			case .consumer(let consumer):
				if consumer?.deliveryAddresses?.last?.street != nil && consumer?.deliveryAddresses?.last?.house != nil
				{
					let street = consumer?.deliveryAddresses?.last?.street
					let house = consumer?.deliveryAddresses?.last?.house
					return "\(street ?? ""), \(house ?? "")"
				}
				else
				{
					return "Адрес"
				}
			case .seller(_):
				return "Адрес"
			default:
				return "Адрес"
			}
		}
		return "Адрес"
	}
	static func addressLbl() -> String
	{
		if managers.webService.deliveryAddress != nil
		{
			if  (managers.webService.deliveryAddress?.is_delivery_possible)!
			{
				return "Ура! Есть доставка"
			}
			else
			{
				return "Сюда пока не доставляем"
			}
		}
		else if managers.webService.currentUserProfile?.profile?.role != nil
		{
			switch managers.webService.currentUserProfile?.profile?.role
			{
			case .consumer(let consumer):
				guard let is_delivery_possible = consumer?.deliveryAddresses?.last?.is_delivery_possible else
				{
					return "Укажите адрес доставки"
				}
				if is_delivery_possible
				{
					return "Ура! Есть доставка"
				}
				else
				{
					return "Сюда пока не доставляем"
				}
			case .seller(_):
				return "Укажите адрес доставки"
			default:
				return "Укажите адрес доставки"
			}

		}
		else
		{
			return "Укажите адрес доставки"
		}
	}

}
