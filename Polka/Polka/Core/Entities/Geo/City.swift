//
//  City.swift
//  Polka
//
//  Created by Makson on 13.05.2021.
//

import Foundation
import CoreLocation

struct City
{
	let name: String
	let coordinates: CLLocationCoordinate2D
}

extension City
{
	static let defaultCity = City(
		name: "Москва",
		coordinates: CLLocationCoordinate2D(
			latitude: 55.7558,
			longitude: 37.6173
		)
	)
}
