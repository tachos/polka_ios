//
//  PlaceAutocompleteResult.swift
//  Polka
//
//  Created by Makson on 13.05.2021.
//

import Foundation
import SwiftyJSON
import CoreLocation

struct SearchAddress
{
	let address: [PlaceAutocompleteResult]
}
struct PlaceAutocompleteResult
{
	let name         : String?
	let googlePlaceId: String?
}

struct EventPlace
{
	let coordinates  : LocationPoint
	let name         : String?
	let house        : String?
	let googlePlaceId: String?
}

extension SearchAddress:Codable {}
extension PlaceAutocompleteResult:Codable {}

extension EventPlace:Codable {}

extension SearchAddress: JSONDeserializable
{
	static func from(json: JSON) -> SearchAddress?
	{
		guard let address:[PlaceAutocompleteResult] = AlamofireUtils.parseArray(from: json["predictions"], skipFailedElements: true)
		else
		{
			return nil
		}
		return SearchAddress(address: address)
	}
	
	
}

extension PlaceAutocompleteResult:JSONDeserializable
{
	static func from(json: JSON) -> PlaceAutocompleteResult?
	{
		
		guard let place_id = json["place_id"].string,
			  let structured_formatting = json["structured_formatting"].dictionary,
			  let main_text = structured_formatting["main_text"]?.string
		else
		{
			return nil
		}
				
		return PlaceAutocompleteResult(name:main_text,
											   googlePlaceId: place_id)
	}
}

extension EventPlace: JSONDeserializable
{
	static func from(json: JSON) -> EventPlace?
	{
		if let dictionary = json.dictionaryObject,
		   let result = dictionary["result"] as? [String:Any]
		{
			if let name = result["name"] as? String,
			   let place_id = result["place_id"] as? String,
			   let geometry = result["geometry"] as? [String:Any],
			   let location = geometry["location"] as? [String:Any],
			   let lat = location["lat"] as? Double,
			   let lng = location["lng"] as? Double
			   {
				var house = "-"
				
				if name.contains(",")
				{
					house = String(name.split(separator: ",")[1])
					if let index = name.range(of: ",")?.lowerBound
					{
						let transform_name = String(name[..<index])
						
						return EventPlace(coordinates:LocationPoint(latitude: lat, longitude: lng),name:transform_name, house: house, googlePlaceId: place_id)
					}
				}
				
				return EventPlace(coordinates:LocationPoint(latitude: lat, longitude: lng),name:name, house: house, googlePlaceId: place_id)
			   }
		}
		return nil
	}
}
