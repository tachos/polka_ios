//
//  OrderProduct.swift
//  Polka
//
//  Created by Michael Goremykin on 10.06.2021.
//

import SwiftyJSON

struct OrderProduct
{
	let product: Product
	let quantityPlacedInOrder: Float
	let finalQuantityInAssembledOrder: Float?
}

// MARK:- Parsing
extension OrderProduct: JSONDeserializable
{
	static func from(json: JSON) -> OrderProduct?
	{
		if let product = Product.from(json: json["product"]),
		   let quantityPlacedInOrder = json["ordered_count"].float
		{
			return .init(product: product,
						 quantityPlacedInOrder: quantityPlacedInOrder,
						 finalQuantityInAssembledOrder: json["assembled_count"].float)
		}
		
		return nil
	}
}
