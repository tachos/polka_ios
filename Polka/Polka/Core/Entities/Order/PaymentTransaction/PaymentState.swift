//
//  PaymentState.swift
//  Polka
//
//  Created by Michael Goremykin on 10.06.2021.
//

import SwiftyJSON

enum PaymentState
{
	case new
	case authorized
	case rejected
	case canceled
	case reversed
	case refunded
	case partialRefunded
	case confirmed
	
}

// MARK:- Parsing
extension PaymentState: JSONDeserializable
{
	static func from(json: JSON) -> PaymentState?
	{
		switch json.string?.lowercased()
		{
		case "new":
			return new
			
		case "authorized":
			return authorized
			
		case "rejected":
			return rejected
			
		case "canceled":
			return canceled
			
		case "reversed":
				return reversed
			
		case "refunded":
				return refunded
			
		case "partial_refunded":
				return partialRefunded
			
		case "confirmed":
				return confirmed
			
		default:
			return nil
		}
	}
}
