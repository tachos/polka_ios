//
//  PaymentTransactionInfo.swift
//  Polka
//
//  Created by Michael Goremykin on 10.06.2021.
//

import SwiftyJSON

struct PaymentTransactionInfo
{
	let state: PaymentState
	let paymentFormUrl: URL
}

// MARK:- Parsing
extension PaymentTransactionInfo: JSONDeserializable
{
	static func from(json: JSON) -> PaymentTransactionInfo?
	{
		if let state = PaymentState.from(json: json["status"]),
		   let paymentFormUrl = json["url"].url
		{
			return .init(state: state, paymentFormUrl: paymentFormUrl)
		}
		
		return nil
	}
}
