//
//  SubOrdersPaginationResponse.swift
//  Polka
//
//  Created by Michael Goremykin on 10.06.2021.
//

import SwiftyJSON

struct SubOrdersPaginationResponse
{
	let count: Int
	let next: URL?
	let previous: URL?
	let items: [SubOrderInfo]
}

// MARK:- Parsing
extension SubOrdersPaginationResponse: JSONDeserializable
{
	static func from(json: JSON) -> SubOrdersPaginationResponse?
	{
		if let count = json["count"].int,
		   let results: [SubOrderInfo] = AlamofireUtils.parseArray(from: json["results"], skipFailedElements: true, canBeEmpty: true)
		{
			return .init(count: count,
						 next: URL(string: json["next"].string ?? ""),
						 previous: URL(string: json["previous"].string ?? ""),
						 items: results)
		}
		
		return nil
	}
}
