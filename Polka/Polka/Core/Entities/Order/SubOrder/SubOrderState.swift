//
//  SubOrderState.swift
//  Polka
//
//  Created by Michael Goremykin on 10.06.2021.
//

import SwiftyJSON

enum SubOrderState
{
	case created
	case assembling
	case assembled
	case canceled(SubOrderCancelReason?)
}

enum SubOrderCancelReason
{
	case canceled
	case notAssembled
}

extension SubOrderState: JSONDeserializable
{
	static func from(json: JSON) -> SubOrderState?
	{
		switch json["status"].string?.lowercased()
		{
		case "created":
			return .created
			
		case "assemble":
			return .assembling
			
		case "assembled":
			return .assembled
			
		case "canceled":
			return .canceled(SubOrderCancelReason.from(json: json["cancel_reason"]))
			
		default:
			return nil
		}
	}
}

extension SubOrderCancelReason: JSONDeserializable
{
	static func from(json: JSON) -> SubOrderCancelReason?
	{
		switch json.string?.lowercased()
		{
		case "canceled":
			return .canceled
			
		case "not_assembled":
			return .notAssembled
			
		default:
			return nil
		}
	}
}

extension SubOrderState
{
	func getLabelColor() -> UIColor
	{
		switch self
		{
		case .created,
			 .assembling:
			return #colorLiteral(red: 1, green: 0.4784313725, blue: 0, alpha: 1)
		case .canceled:
			return #colorLiteral(red: 0.5607843137, green: 0.5607843137, blue: 0.5607843137, alpha: 1)
		case .assembled:
			return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		}
	}
	func toString() -> String
	{
		switch self
		{
		case .assembling:
			return "Заказ собирают"
			
		case .canceled:
			return "Отменён"
			
		case .created:
			return "Создан"
			
		case .assembled:
			return "Заказ собран"
			
		}
	}
}
