//
//  SubOrderInfo.swift
//  Polka
//
//  Created by Michael Goremykin on 08.06.2021.
//

import SwiftyJSON

struct SubOrderInfo
{
	let id: String
	let orderId: Int
	let state: SubOrderState
	let created: Date
	let modified: Date
	let seller: Seller
	let products: [OrderProduct]
}

// MARK:- API
extension SubOrderInfo
{
	func getTotalInUnits() -> Int
	{
		switch state
		{
		case .created, .assembling, .canceled:
			return products.count
			
		case .assembled:
			return products.filter(hasNotBeenRemovedFromSubOrder).count
		}
	}
	
	func getTotal() -> Float
	{
		let rawTotal = products.map{ getProductQuantityForState($0, forState: state) *  FormatterPrice.kopeksToRoubles($0.product.price) }
			                   .reduce(0, +)
		
		return Float(round(100*rawTotal)/100)
	}
	
	func getProductsImagesUrls() -> [URL?]
	{
		return products.map { $0.product.imageUrl }
	}
}

// MARK:- Utils
extension SubOrderInfo
{
	private func hasNotBeenRemovedFromSubOrder(_ orderProduct: OrderProduct) -> Bool
	{
		(orderProduct.finalQuantityInAssembledOrder ?? 0) > 0
	}
	
	private func getProductQuantityForState(_ orderProduct: OrderProduct, forState state: SubOrderState) -> Float
	{
		switch state
		{
		case .created, .assembling, .canceled:
			return orderProduct.quantityPlacedInOrder
			
		case .assembled:
			return orderProduct.finalQuantityInAssembledOrder ?? 0
		}
	}
	
}

// MARK:- Utils
extension SubOrderInfo: Hashable, Equatable
{
	static func == (lhs: SubOrderInfo, rhs: SubOrderInfo) -> Bool
	{
		return lhs.id == rhs.id
	}
	
	func hash(into hasher: inout Hasher)
	{
		hasher.combine(id)
	}
}

// MARK:- Parsing
extension SubOrderInfo: JSONDeserializable
{
	static func from(json: JSON) -> SubOrderInfo?
	{
		if let id = json["id"].string,
		   let orderId = json["number"].int,
		   let state = SubOrderState.from(json: json),
		   let seller = getSeller(json: json),
		   let products: [OrderProduct] = AlamofireUtils.parseArray(from: json["products"],
																	skipFailedElements: true,
																	canBeEmpty: false)
		{
			return .init(id: id,
						 orderId: orderId,
						 state: state,
						 created: DatesFormatter.fromResponse(json["created_at"].string),
						 modified: DatesFormatter.fromResponse(json["modified_at"].string),
						 seller: seller,
						 products: products)
		}
		
		return nil
	}
	
	private static func getSeller(json: JSON) -> Seller?
	{
		if let sellerId = json["seller_id"].string,
		   let sellerInfo = SellerInfo.from(json: json["seller_data"])
		{
			return .init(id: sellerId, info: sellerInfo)
		}
		
		return nil
	}
}
