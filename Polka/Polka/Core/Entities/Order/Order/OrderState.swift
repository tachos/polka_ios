//
//  OrderInfo+Utils.swift
//  Polka
//
//  Created by Michael Goremykin on 10.06.2021.
//

import SwiftyJSON

enum OrderState
{
	case verifyingDelivery
	case waitingForPayment
	case assembling
	case waitingForCourier
	case delivering
	case completed
	case canceled(reason: OrderCancelReason?, comment: String)
}

enum OrderCancelReason
{
	case canceledByAdmin
	case notVerifiedOrHasBeenPayed
	case canceledByAcquirer
	case notAssembled
}

// MARK:- Parsing OrderState
extension OrderState: JSONDeserializable
{
	static func from(json: JSON) -> OrderState?
	{
		switch json["status"].string?.lowercased()
		{
		case "delivery_verification":
			return .verifyingDelivery
			
		case "pay_wait":
			return .waitingForPayment
			
		case "assemble":
			return .assembling
			
		case "courier_wait":
			return .waitingForCourier
			
		case "on_delivery":
			return .delivering
			
		case "finished":
			return .completed
			
		case "canceled":
			return .canceled(reason: OrderCancelReason.from(json: json["cancel_reason"]),
							 comment: json["cancel_comment"].string ?? "")
			
		default:
			return nil
		}
	}
}

// MARK:- Parsing OrderCancelReason
extension OrderCancelReason: JSONDeserializable
{
	static func from(json: JSON) -> OrderCancelReason?
	{
		switch json.string?.lowercased()
		{
		case "cancel_by_admin":
			return .canceledByAdmin

		case "not_verified_or_payed":
			return .notVerifiedOrHasBeenPayed

		case "cancel_by_acquirer":
			return .canceledByAcquirer

		case "not_assembled":
			return .notAssembled

		default:
			return nil
		}
	}
}

// MARK:- Utils
extension OrderState
{
	func toString() -> String
	{
		switch self
		{
		case .verifyingDelivery:
			return "Доставка согласуется"
			
		case .waitingForPayment:
			return "Ожидание оплаты"
			
		case .assembling:
			return "Заказ собирают"
			
		case .waitingForCourier:
			return "Ожидание курьера"
			
		case .delivering:
			return "Доставляется"
			
		case .completed:
			return "Доставлен"
			
		case .canceled:
			return "Отменён"
			
		}
	}
	
	func getLabelColor() -> UIColor
	{
		switch self
		{
		case .verifyingDelivery, .waitingForPayment, .assembling, .waitingForCourier, .canceled:
			return #colorLiteral(red: 0.1333333333, green: 0.1333333333, blue: 0.1333333333, alpha: 1)
			
		case .delivering:
			return #colorLiteral(red: 1, green: 0.4784313725, blue: 0, alpha: 1)
			
		case .completed:
			return #colorLiteral(red: 0.5607843137, green: 0.5607843137, blue: 0.5607843137, alpha: 1)
		}
	}
}
