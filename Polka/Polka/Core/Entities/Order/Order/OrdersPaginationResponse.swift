//
//  OrdersPaginationResponse.swift
//  Polka
//
//  Created by Michael Goremykin on 10.06.2021.
//

import SwiftyJSON

struct OrdersPaginationResponse
{
	let count: Int
	let next: URL?
	let previous: URL?
	let items: [OrderInfo]
}

// MARK:- Parsing
extension OrdersPaginationResponse: JSONDeserializable
{
	static func from(json: JSON) -> OrdersPaginationResponse?
	{
		if let count = json["count"].int,
		   let results: [OrderInfo] = AlamofireUtils.parseArray(from: json["results"], skipFailedElements: true, canBeEmpty: true)
		{
			return .init(count: count,
						 next: URL(string: json["next"].string ?? ""),
						 previous: URL(string: json["previous"].string ?? ""),
						 items: results)
		}
		
		return nil
	}
}
