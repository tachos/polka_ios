//
//  OrderInfo.swift
//  Polka
//
//  Created by Michael Goremykin on 08.06.2021.
//

import SwiftyJSON

typealias OrderID = String

struct OrderInfo
{
	let id: OrderID
	let orderNumber: Int
	let state: OrderState
	let created: Date
	let modified: Date
	var subOrders: [SubOrderInfo]
	let deliveryInfo: DeliveryInfo
	let is_paid: Bool
	let paymentTransactionInfo: PaymentTransactionInfo
}


// MARK:- API
extension OrderInfo
{
	func getProductsCount() -> Int
	{
		return subOrders.map{ $0.getTotalInUnits() }.reduce(0, +)
	}
	
	func getTotal() -> Float
	{
		let rawTotal = subOrders.map { $0.getTotal() }.reduce(0, +) + getDeliveryCost()
		
		return Float(round(100*rawTotal)/100)
	}
	
	func getDeliveryCost() -> Float
	{
		FormatterPrice.kopeksToRoubles(self.deliveryInfo.cost ?? 0).round(decimalCount: 2)
	}
	
	func getProductsImagesUrls() -> [URL?]
	{
		return self.subOrders.map { subOrder in subOrder.products.map{ $0.product.imageUrl } }.reduce([], +)
	}
	
	func isInFinalisedState() -> Bool
	{
		if case .canceled = self.state
		{
			return true
		}
		else if case .completed = self.state
		{
			return true
		}
		else
		{
			return false
		}
	}
}

// MARK:- Utils
extension OrderInfo: Hashable, Equatable
{
	static func == (lhs: OrderInfo, rhs: OrderInfo) -> Bool
	{
		return lhs.id == rhs.id
	}
	
	func hash(into hasher: inout Hasher)
	{
		hasher.combine(id)
	}
}

// MARK:- Parsing
extension OrderInfo: JSONDeserializable
{
	static func from(json: JSON) -> OrderInfo?
	{
		if let id = json["id"].string,
		   let orderNumber = json["number"].int,
		   let state = OrderState.from(json: json),
		   let subOrdersInfo: [SubOrderInfo] = AlamofireUtils.parseArray(from: json["sub_orders"],
																	 skipFailedElements: true,
																	 canBeEmpty: false),
		   let deliveryInfo = DeliveryInfo.from(json: json["delivery"]),
		   let is_paid = json["is_paid"].bool,
		   let paymentInfo = PaymentTransactionInfo.from(json: json["payment_transaction"])
		{
			return .init(id: id,
						 orderNumber: orderNumber,
						 state: state,
						 created: DatesFormatter.fromResponse(json["created_at"].string),
						 modified: DatesFormatter.fromResponse(json["modified_at"].string),
						 subOrders: subOrdersInfo,
						 deliveryInfo: deliveryInfo,
						 is_paid: is_paid,
						 paymentTransactionInfo: paymentInfo)
		}
		
		return nil
	}
}
