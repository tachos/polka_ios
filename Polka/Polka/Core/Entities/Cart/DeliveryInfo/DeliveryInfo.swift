//
//  DeliveryInfo.swift
//  Polka
//
//  Created by Michael Goremykin on 12.06.2021.
//

import SwiftyJSON

struct DeliveryInfo
{
	let type: DeliveryKind
	let cost: Int?
}

// MARK:- Parsing
extension DeliveryInfo: JSONDeserializable
{
	static func from(json: JSON) -> DeliveryInfo?
	{
		if let type = DeliveryKind.from(json: json["type"])
		{
			return .init(type: type, cost: json["cost"].int)
		}
		
		return nil
	}
}
