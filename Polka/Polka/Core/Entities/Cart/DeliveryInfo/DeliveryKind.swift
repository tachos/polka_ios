//
//  DeliveryKind.swift
//  Polka
//
//  Created by Michael Goremykin on 12.06.2021.
//

import SwiftyJSON

enum DeliveryKind
{
	case auto
	case free
	case manual
}

extension DeliveryKind: JSONDeserializable
{
	static func from(json: JSON) -> DeliveryKind?
	{
		switch json
		{
		case "auto":
			return .auto
			
		case "free":
			return .free
			
		case "manual":
			return .manual
			
		default:
			return nil
		}
	}
}

