//
//  Cart+API.swift
//  Polka
//
//  Created by Michael Goremykin on 18.06.2021.
//

import Foundation

// MARK:- API
extension Cart
{
	static let empty: Cart = .init(marketIsOpen: false,
								   sellers: [],
								   deliveryInfo: nil)
	
	func getCartProductStock(forProductId productId: String, withSellerId sellerId: String? = nil) -> StockAvailability
	{
		if let sellerId = sellerId
		{
			return self.sellers.first { $0.seller.id == sellerId }?
				.cartProducts.first { $0.product.id == productId}?.stockAvailability ?? .notAvailable
		}
		
		return self.sellers.map{ $0.cartProducts }.reduce([], +).first{ $0.product.id ==  productId}?.stockAvailability ?? .notAvailable
	}
	
	func getTotal() -> Float
	{
		if !self.marketIsOpen { return 0 }
		
		let rawTotal = getTotalBySeller().map{ $0.1 }.reduce(0, +) + FormatterPrice.kopeksToRoubles((self.deliveryInfo?.cost ?? 0))
		
		return Float(round(100*rawTotal)/100)
	}
	
	func getDeliveryCost() -> Float
	{
		FormatterPrice.kopeksToRoubles(self.deliveryInfo?.cost ?? 0).round(decimalCount: 2)
	}
	
	func getTotalBySeller() -> [(cartSeller: CartSeller, total: Float)]
	{
		return self.sellers.filter{ $0.seller.info.isOpen }
			.map { ($0, getProductsTotal($0.cartProducts)) }
	}
	
	func getProductsTotal(_ cartProducts: [CartProduct]) -> Float
	{
		return  cartProducts.map { $0.quantity * FormatterPrice.kopeksToRoubles($0.product.price) }
							.reduce(0, +)
	}
	
	func getCartProductsCountInCart() -> Int
	{
		return self.sellers.map { $0.cartProducts.count }.reduce(0, +)
	}

}
