//
//  Cart+Utils.swift
//  Polka
//
//  Created by Michael Goremykin on 12.06.2021.
//

import Foundation

// MARK:- API
extension Cart
{
	func isEmpty() -> Bool
	{
		self.sellers.count == 0
	}
	
	func hasSameProducts(as otherCart: Cart) -> Bool
	{
		return Cart.getCartDiff(self, otherCart).count == 0
	}
	
	func getProductQuantity(_ productId: String) -> Float
	{
		return self.sellers.map{ $0.cartProducts }.reduce([], +).first{ $0.product.id == productId }?.quantity ?? 0
	}
	
	func getProductStockQuantity(_ productId: String) -> Float
	{
		return self.sellers.map{ $0.cartProducts }.reduce([], +).first{ $0.product.id == productId }?.stockQuantity ?? 0
	}
}
