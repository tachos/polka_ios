//
//  Cart+Diff.swift
//  Polka
//
//  Created by Michael Goremykin on 12.06.2021.
//

import Foundation

typealias CartDiff = [(id: String, quantity: Float)]

// MARK:- API
extension CartDiff
{
	func toQueryParameters() -> [[String: Any]]
	{
		return self.map{ ["id": $0.id, "count": String($0.quantity.roundToFourDecimal())] }
	}
}

// MARK:- Diff
extension Cart
{
	static func getCartDiff(_ currentCart: Cart, _ newCart: Cart) -> CartDiff
	{
		let currentCartProducts = currentCart.sellers.map { $0.cartProducts }.reduce([], +)
		let newCartAllProducts = newCart.sellers.map { $0.cartProducts }.reduce([], +)
		
		let currentCartProductsIds = currentCartProducts.map{ $0.product.id }
		let newCartAllProductsIds = newCartAllProducts.map{ $0.product.id }
		
		let addedIds = getNewAddedProductsIds(currentCartProductsIds, newCartAllProductsIds)
		let removedIds = getRemovedProductsIds(currentCartProductsIds, newCartAllProductsIds)
		let modifiedIds = getExistingProductsDiff(newCartAllProductsIds, addedIds)
		
		return getNewAddedProductsDiffs(addedIds, newCartAllProducts) +
			   getModifiedProductsDiffs(modifiedIds, currentCartProducts, newCartAllProducts) +
			   getRemovedProductsDiffs(removedIds)
	}
	
	static func getNewAddedProductsIds(_ currentCartAllProductsIds: [String], _ newCartAllProductsIds: [String]) -> [String]
	{
		return newCartAllProductsIds.filter{ !currentCartAllProductsIds.contains($0) }
	}
	
	static func getRemovedProductsIds(_ currentCartAllProductsIds: [String], _ newCartAllProductsIds: [String]) -> [String]
	{
		return currentCartAllProductsIds.filter{ !newCartAllProductsIds.contains($0) }
	}
	
	static func getExistingProductsDiff( _ newCartAllProductsIds: [String], _ newProductsIds: [String]) -> [String]
	{
		return newCartAllProductsIds.filter { !newProductsIds.contains($0) }
	}
	
	static func getNewAddedProductsDiffs(_ ids: [String], _ products: [CartProduct]) -> [(String, Float)]
	{
		return ids.map{id in products.first(where: { id == $0.product.id }) }.compactMap{ $0 }.map { ($0.product.id, $0.quantity) }
	}
	
	static func getRemovedProductsDiffs(_ ids: [String]) -> [(String, Float)]
	{
		return ids.map {($0, 0)}
	}
	
	static func getModifiedProductsDiffs(_ ids: [String],
										 _ currentCartProducts: [CartProduct],
										 _ newCartProducts:[CartProduct]) -> [(String, Float)]
	{
		return ids.map { productId in
			if  let currentQuantity = currentCartProducts.first(where: { $0.product.id == productId })?.quantity,
				let newQuantity = newCartProducts.first(where: { $0.product.id == productId })?.quantity,
				newQuantity != currentQuantity
			{
				return (productId, newQuantity)
			}
			else
			{
				return nil
			}
		}.compactMap { $0 }
	}
}
