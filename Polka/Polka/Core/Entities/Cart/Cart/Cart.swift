//
//  CartItem.swift
//  Polka
//
//  Created by Michael Goremykin on 06.05.2021.
//

import SwiftyJSON

struct Cart
{
	let marketIsOpen: Bool
	let sellers: [CartSeller]
	let deliveryInfo: DeliveryInfo?
}

extension Cart: JSONDeserializable
{
	static func from(json: JSON) -> Cart?
	{
		if let marketIsOpen = json["market_is_open"].bool,
		   let sellers: [CartSeller] = AlamofireUtils.parseArray(from: json["sellers"],
																   skipFailedElements: true,
																   canBeEmpty: true)
		{
			return .init(marketIsOpen: marketIsOpen,
						 sellers: sellers,
						 deliveryInfo: DeliveryInfo.from(json: json["delivery"]))
		}
		
		return nil
	}
}
