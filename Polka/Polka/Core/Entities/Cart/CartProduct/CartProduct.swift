//
//  CartProduct.swift
//  Polka
//
//  Created by Michael Goremykin on 12.06.2021.
//

import SwiftyJSON

struct CartProduct
{
	let product: Product
	var quantity: Float
	let stockQuantity: Float
	
	var stockAvailability: StockAvailability
	{
		get { StockAvailability.getStock(forProduct: self.product,
										 amountInCart: self.quantity,
										 stockAmount: stockQuantity) }
	}
}

// MARK:- Parsing
extension CartProduct: JSONDeserializable
{
	static func from(json: JSON) -> CartProduct?
	{
		//SVD: why float? these should be integers, right?
		let quantity 	  = json["count"].float ?? 0
		let stockQuantity = json["balance"].float ?? 0
		
		if let product = Product.from(json: json["product"])
		{
			return CartProduct(
				product: product,
				quantity: quantity,
				stockQuantity: stockQuantity
			)
		}
		
		return nil
	}
}

extension CartProduct
{
	func getProductQuantity(stepCount: Int) -> ProductQuantity
	{
		if stepCount == 0 { return 0 }

		return self.product.minCountProductInCart
			+ Float(stepCount - 1) * self.product.stepChangeInCart
	}
}
