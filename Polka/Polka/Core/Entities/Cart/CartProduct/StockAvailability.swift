//
//  StockAvailability.swift
//  Polka
//
//  Created by Michael Goremykin on 13.06.2021.
//

import Foundation

enum StockAvailability
{
	case available(amount: Float)
	case notAvailable
}


// MARK:- API
extension StockAvailability
{
	func raw() -> Float
	{
		switch self
		{
		case .available(let quantity):
			return quantity

		case .notAvailable:
			return 0
		}
	}
	
	func isAvailable() -> Bool
	{
		switch self
		{
		case .available:
			return true
			
		case .notAvailable:
			return false
		}
	}

	static func getStock(forProduct product: Product, amountInCart: Float, stockAmount: Float) ->  StockAvailability
	{
		if stockQuantityIsLessThenMinimalCartAddQuantity(product, stockAmount)
		   || stockQuantityIsLessThenCartStepChangeQuantity(product, amountInCart, stockAmount)
		{
			return .notAvailable
		}
		
		return .available(amount: stockAmount)
	}
	
}

// MARK:- Utils
extension StockAvailability
{
	private static func stockQuantityIsLessThenMinimalCartAddQuantity(_ product: Product,
																	  _ stockAmount: Float) -> Bool
	{
		return product.minCountProductInCart > stockAmount
	}
	
	private static func stockQuantityIsLessThenCartStepChangeQuantity(_ product: Product,
																	  _ amountInCart: Float,
																	  _ stockAmount: Float) -> Bool
	{
		if amountInCart == 0
		{
			return stockAmount - product.minCountProductInCart < 0
		}
		else
		{
			return stockAmount - amountInCart < product.stepChangeInCart
		}
	}
}
