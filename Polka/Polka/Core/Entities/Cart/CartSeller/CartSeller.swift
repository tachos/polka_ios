//
//  CartSeller.swift
//  Polka
//
//  Created by Michael Goremykin on 12.06.2021.
//

import SwiftyJSON

struct CartSeller
{
	let seller: Seller
	var cartProducts: [CartProduct]
}

// MARK:- Parsing
extension CartSeller: JSONDeserializable
{
	static func from(json: JSON) -> CartSeller?
	{
		if let id = json["id"].string,
		   let info = SellerInfo.from(json: json["seller_data"]),
		   let cartProducts: [CartProduct] = AlamofireUtils.parseArray(from: json["products"],
																	   skipFailedElements: true,
																	   canBeEmpty: false)
		{
			return .init(seller: .init(id: id, info: info),
						 cartProducts: cartProducts)
		}
		
		return nil
	}
}
