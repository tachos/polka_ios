//
//  OTP.swift
//  Polka
//
//  Created by Makson on 27.04.2021.
//

import Foundation

import SwiftyJSON

struct OTP
{
	let timeout:Int
}

extension OTP:Codable
{}

extension OTP: JSONDeserializable
{
	static func from(json: JSON) -> OTP?
	{
		if let timeout = json["timeout"].int
		{
			if timeout > 0
			{
				return OTP(timeout: timeout)
			}
		}
		
		return nil
	}
}
