//
//  Seller.swift
//  Polka
//
//  Created by Makson on 29.04.2021.
//

import Foundation
import SwiftyJSON


typealias SellerId = String


struct Seller
{
	let id		: SellerId
	let info	: SellerInfo
}

struct SellerInfo
{
	let caption: String
	let description:String
	let profileImageUrl: URL?
	let sellerGallery: SellerGallery
	let isOpen:Bool
	let isPriceActual:Bool
}

struct SellerGallery
{
	static let empty = SellerGallery(items: [])
	
	let items: [SellerGalleryItem]
}

extension SellerGallery
{
	func getProfileImagesUrls() -> [URL]
	{
		return items.map{ item in
			switch item.kind
			{
			case .image(let url):
				return url
				
			default:
				return nil
			}}.compactMap{ $0 }
	}
}

extension SellerGallery: Codable
{}

extension SellerGallery: JSONDeserializable
{
	static func from(json: JSON) -> SellerGallery?
	{
		return .init(items: AlamofireUtils.parseArray(from: json, skipFailedElements: true, canBeEmpty: true) ?? [])
	}
}

enum SellerGalleryItemKind
{
	case image(URL)
	case video(URL)
}

extension SellerGalleryItemKind: Codable
{
//	init(from decoder: Decoder) throws {
//		fatalError()
//	}
//	
//	func encode(to encoder: Encoder) throws {
//		fatalError()
//	}
}

extension SellerGalleryItemKind: JSONDeserializable
{
	static func from(json: JSON) -> SellerGalleryItemKind?
	{
		switch json["type"].string ??  ""
		{
		case "image":
			if let url = json["data"].url
			{
				return .image(url)
			}
			
			return nil
			
		case "video":
			return .video(URL(string: "https://www.apple.com/")!)
			
		default:
			return nil
		}
	}
}

struct SellerGalleryItem: JSONDeserializable
{
	let id: 	String
	let kind: 	SellerGalleryItemKind
}

extension SellerGalleryItem
{
	static func from(json: JSON) -> SellerGalleryItem?
	{
		if let id = json["id"].string,
		   let itemKind = SellerGalleryItemKind.from(json: json)
		{
			return .init(id: id, kind: itemKind)
		}
		
		return nil
	}
}

extension SellerGalleryItem: Codable
{}

extension Seller:Codable
{}

extension Seller: JSONDeserializable
{
	static func from(json: JSON) -> Seller?
	{
		guard let id 	= json["id"].string,
			  let info 	= SellerInfo.from(json: json["seller_data"]) else
		{
			return nil
		}
		
		return Seller(id	: id,
					  info	: info)
	}
}

extension SellerInfo: JSONDeserializable
{
	static func from(json: JSON) -> SellerInfo?
	{
		guard let caption = json["caption"].string,
			  let description = json["description"].string,
			  let sellerGallery = parseGalleryBasedOnUserRole(json: json["gallery"]),
			  let isOpen = json["is_open_now"].bool,
			  let is_price_actual = json["is_price_actual"].bool else
		{
			return nil
		}
		
		return SellerInfo(caption: caption,
						  description: description,
						  profileImageUrl: json["art"].url,
						  sellerGallery: sellerGallery,
						  isOpen: isOpen,
						  isPriceActual: is_price_actual)
	}
	
	private static func parseGalleryBasedOnUserRole(json: JSON) -> SellerGallery?
	{
		if let roleOfAuthorizedUser = managers.webService.currentUserProfile?.profile?.role
		{
			switch roleOfAuthorizedUser
			{
			case .consumer:
				return SellerGallery.from(json: json)
				
			case .seller:
				return .init(items: [])
			}
		}
		else
		{
			// the user is not logged in
			return SellerGallery.from(json: json)
		}
	}
}

extension SellerInfo: Codable
{}

extension Seller: Equatable
{
	static func == (lhs: Seller, rhs: Seller) -> Bool
	{
		lhs.id == rhs.id
	}
}

extension Seller: Hashable
{
	func hash(into hasher: inout Hasher)
	{
		hasher.combine(id)
	}
}
