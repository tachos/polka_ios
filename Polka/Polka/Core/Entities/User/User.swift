//
//  User.swift
//  Polka
//
//  Created by Makson on 27.04.2021.
//

import Foundation

import SwiftyJSON
import SwiftyUserDefaults

struct User
{
	struct Authorization
	{
		let token: String
	}
	
	struct Profile
	{
		enum Role
		{
			case consumer(_ consumer: ConsumerInfo?)
			case seller(_ seller: SellerInfo?)
		}
		
		let id: String
		let phone: String
		let role: Role?
	}
	let authorization: Authorization
	let profile: Profile?
}
extension User: Codable
{}
extension User.Authorization:Codable
{}
extension User.Profile:Codable
{}
extension User.Profile.Role
{
	private enum CodingKeys: CodingKey
	{
		case consumer_data
		case seller_data
	}
	enum PostTypeCodingError: Error
	{
		   case decoding(String)
	 }
}
extension User.Profile.Role:Codable
{
	init(from decoder: Decoder) throws
	{
		let values = try decoder.container(keyedBy: CodingKeys.self)
		if let value = try? values.decode(ConsumerInfo.self, forKey: .consumer_data)
		{
			self = .consumer(value)
			return
		}
		else if let value = try? values.decode(SellerInfo.self, forKey: .seller_data)
		{
			self = .seller(value)
			return
		}
		
		throw PostTypeCodingError.decoding("Whoops! \(dump(values))")
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
				switch self
				{
				case .consumer(let consumer):
					try container.encode(consumer, forKey: .consumer_data)
				case .seller(let seller):
					try container.encode(seller, forKey: .seller_data)
				}
	}
}

extension User:JSONDeserializable
{
	static func from(json: JSON) -> User?
	{
		if let authorization = Authorization.from(json: json["token"]),
		   let profile = Profile.from(json: json["profile"])
		{
			return User(authorization: authorization, profile: profile)
		}
		
		return nil
	}
}

extension User.Authorization:JSONDeserializable
{
	static func from(json: JSON) -> User.Authorization?
	{
		guard let token = json["token"].string else { return nil }
		
		return User.Authorization(token: token)
	}
}

extension User.Profile:JSONDeserializable
{
	static func from(json: JSON) -> User.Profile?
	{
		guard let id = json["id"].string,
			  let phone = json["phone"].string,
			  let role = json["role"].string
		else
		{
			return nil
			
		}
		
		if role == "consumer"
		{
			if let consumer_data = ConsumerInfo.from(json: json["consumer_data"])
			{
				let consumer: Role = .consumer(consumer_data)
				return User.Profile(id: id, phone: phone, role: consumer)
			}
		}
		else if role == "seller"
		{
			if let seller_data = SellerInfo.from(json: json["seller_data"])
			{
				let seller: Role = .seller(seller_data)
				return User.Profile(id: id, phone: phone, role: seller)
			}
		}
		return User.Profile(id: id, phone: phone , role: nil)
	}
}
extension User.Profile: DefaultsSerializable
{
	static var _defaults: DefaultsBridge<User.Profile>
	{
		return DefaultsCodableBridge()
	}
	static var _defaultsArray: DefaultsBridge<[User.Profile]>
	{
		return DefaultsCodableBridge()
	}
}
extension User.Profile.Role: DefaultsSerializable
{
	static var _defaults: DefaultsBridge<User.Profile.Role>
	{
		return DefaultsCodableBridge()
	}
	static var _defaultsArray: DefaultsBridge<[User.Profile.Role]>
	{
		return DefaultsCodableBridge()
	}
}
