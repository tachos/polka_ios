//
//  Consumer.swift
//  Polka
//
//  Created by Makson on 28.04.2021.
//

import Foundation

import SwiftyJSON
import SwiftyUserDefaults


struct Consumer
{
	typealias Id = String

	let id: Id
	let info: ConsumerInfo
}

struct ConsumerInfo
{
	let deliveryAddresses:    [DeliveryAddresses]?
	let gifts:                Int?
	let free_devilery:        Int?
}
struct DeliveryAddresses
{
	let id:                   String?
	var street:               String?
	var house:                String?
	var apartment:            String?
	var floor:                String?
	var entrance:			  String?
	var door_code:            String?
	var location_point:       LocationPoint?
	var comment:              String?
	let is_delivery_possible: Bool?
}
struct LocationPoint
{
	var latitude:             Double?
	var longitude:            Double?
}

extension ConsumerInfo: Codable
{}
extension LocationPoint: Codable
{}
extension DeliveryAddresses : Codable
{}

extension Consumer: JSONDeserializable
{
	static func from(json: JSON) -> Consumer?
	{
		if let id = json["id"].string,
		   let info = ConsumerInfo.from(json: json["consumer_data"])
		{
			return .init(id: id, info: info)
		}

		return nil
	}
}

extension ConsumerInfo: JSONDeserializable
{
	static func from(json: JSON) -> ConsumerInfo?
	{
		guard let delivery_addresses: [DeliveryAddresses] = AlamofireUtils.parseArray(from: json["delivery_addresses"], skipFailedElements: true),
			  let gifts = json["gifts_count"].int,
			  let free_devilery = json["free_delivery_count"].int
		else
		{
			return nil
		}
		
		return ConsumerInfo(
			deliveryAddresses: delivery_addresses,
			gifts: gifts,
			free_devilery: free_devilery)
		
	}
}

extension DeliveryAddresses:JSONDeserializable
{
	static func from(json: JSON) -> DeliveryAddresses?
	{
		guard let id = json["id"].string,
		 let street = json["street"].string,
		 let house = json["house"].string,
		 let location_point = LocationPoint.from(json: json["location_point"]),
		 let is_delivery_possible = json["is_delivery_possible"].bool
		 else
		{
			return nil
		}
		
		return DeliveryAddresses(id: id,
								 street: street,
								 house: house,
								 apartment: json["apartment"].string,
								 floor: json["floor"].string,
								 entrance: json["entrance"].string,
								 door_code: json["door_code"].string,
								 location_point: location_point,
								 comment: json["comment"].string,
								 is_delivery_possible: is_delivery_possible)
	}
	
	
}
extension DeliveryAddresses: DefaultsSerializable
{
	static var _defaults: DefaultsBridge<DeliveryAddresses>
	{
		return DefaultsCodableBridge()
	}
	static var _defaultsArray: DefaultsBridge<[DeliveryAddresses]>
	{
		return DefaultsCodableBridge()
	}
}
extension LocationPoint:JSONDeserializable
{
	static func from(json: JSON) -> LocationPoint?
	{
		guard let latitude = json["latitude"].double,
			  let longitude = json["longitude"].double
		else
		{
			return nil
		}
		
		return LocationPoint(latitude: latitude, longitude: longitude)
	}
	
	
}
