//
//  Media.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 30.06.2021.
//

import UIKit


enum LocalMedia
{
	case image(UIImage)
	case video(LocalVideo)
}

enum Image
{
	case local(UIImage)
	case remote(RemoteImage)
}

struct RemoteImage
{
	let preview: URL
	let original: URL
}

struct LocalVideo
{
	let previewImage: UIImage
	let url: URL
	let duration: TimeInterval?
}

struct RemoteVideo
{
	let previewUrl: URL
	let streamUrl: URL
	let duration: TimeInterval
}
