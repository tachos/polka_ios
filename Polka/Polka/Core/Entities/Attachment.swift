//
//  Attachment.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 23.06.2021.
//

import Foundation
import SwiftyJSON



struct Attachment
{
	typealias Id = String

	let id: Id
	let content: Content
}



// MARK: - Nested Types

extension Attachment
{
	enum Content
	{
		case remoteImage(RemoteImage)
		case localImage(UIImage)
		case remoteVideo(RemoteVideo)
		case localVideo(LocalVideo)
	}
}



// MARK: - JSONDeserializable

extension Attachment: JSONDeserializable
{
	static func from(json: JSON) -> Attachment?
	{
		if let id = json["id"].string,
		   let data = json["data"].string.flatMap({ URL(string: $0) })
		{
			if let preview = json["image_part"]["preview"]
				.string
				.flatMap({ URL(string: $0) })
			{
				let remoteImage = RemoteImage(
					preview: preview,
					original: data
				)

				return .init(
					id: id,
					content: .remoteImage(remoteImage)
				)
			}

			let videoPart = json["video_part"]
			if let previewUrl = videoPart["preview"]
				.string
				.flatMap({ URL(string: $0) }),
			   let streamUrl = videoPart["stream"]
				.string
				.flatMap({ URL(string: $0) }),
			   let duration = videoPart["duration"].double
			{
				let remoteVideo = RemoteVideo(
					previewUrl: previewUrl,
					streamUrl: streamUrl,
					duration: duration
				)

				return .init(
					id: id,
					content: .remoteVideo(remoteVideo)
				)
			}
		}

		return nil
	}
}

