//
//  GeocodingHelper.swift
//  Polka
//
//  Created by Makson on 11.05.2021.
//

import Foundation
import Alamofire
import PromiseKit
import CoreLocation

struct GoogleApiKey
{
	static let value = "AIzaSyCbF9ogM62PW1wgX4-WLwV8savS_XNpaNc"
}

class GeocodingManager
{
	private let apiKey = GoogleApiKey.value
}


// MARK:- GeocodingService
extension GeocodingManager
{
	func getAddress(at coordinate: CLLocationCoordinate2D) -> Promise<Address>
	{
		let url = "https://maps.googleapis.com/maps/api/geocode/json"
		let promise = SessionManager.default.performRequest(
			.get,
			url,
			parameters: .url(values: ["key": apiKey,
							 "latlng": GeocodingManager.getString(for: coordinate),
							 "language":"ru"])
,
			authorizationOptions: .none,
			parseResponse: Address.parse())
		
		return promise
	}
	
	func getPlaces(for query: String,
				   nearestTo coordinate: CLLocationCoordinate2D?,
				   language: String?) -> Promise<SearchAddress>
	{
		let url = "https://maps.googleapis.com/maps/api/place/autocomplete/json"

		var params = ["key":       	  apiKey,
					  "input":     	  query,
					  "type":         "address"] as [String: Any]

		if let coordinate = coordinate
		{
			params["location"] = GeocodingManager.getString(for: coordinate)
			params["radius"] = 50000 /* m */
		}

		if let language = language
		{
			params["language"] = language
		}

		let promise = SessionManager.default.performRequest(
			.get,
			url,
			parameters: .url(values: params),
			authorizationOptions: .none,
			parseResponse: SearchAddress.parse())

		return promise
	}
	func getPlace(with id: String,language: String?) -> Promise<EventPlace>
	{
		let url = "https://maps.googleapis.com/maps/api/place/details/json"
		
		var params = ["key": apiKey,
					  "placeid": id,
		"fields": "place_id,name,formatted_address,geometry"] as [String:Any]
		
		if let language = language
		{
			params["language"] = language
		}
		
		let promise = SessionManager.default.performRequest(
			.get,
			url,
			parameters: .url(values: params),
			authorizationOptions: .none,
			parseResponse: EventPlace.parse())
		return promise
	}
}

// MARK:- Utils
private extension GeocodingManager
{
	typealias JSONDictionary = [String: Any]

	static func getString(for coordinate: CLLocationCoordinate2D) -> String
	{
		return "\(coordinate.latitude),\(coordinate.longitude)"
	}
	
}
