//
//  LocationManagerHelper.swift
//  Polka
//
//  Created by Makson on 12.05.2021.
//

import Foundation
import PromiseKit
import RxSwift
import CoreLocation

class LocationManager: NSObject
{
	// MARK: Managers
	private let locationManager = CLLocationManager()
	
	// MARK: Data
	var lastUpdatedLocation: CLLocation?
	var currentLocation:CLLocationCoordinate2D?

	private var locationObtainingPromise: Promise<CLLocation>?
	
	// MARK: ReactiveX
	private let coordinateUpdatedSubject = BehaviorSubject<CLLocationCoordinate2D?>(value: nil)
	private lazy var privateCoordinateUpdatedObservable = {
		//
		return coordinateUpdatedSubject.flatMap(ignoreNil).share(replay: 1, scope: .whileConnected)
	}()
	
	private let coordinateFailureSubject = PublishSubject<Void>()

	private let authorizationStatusChangedSubject = PublishSubject<CLAuthorizationStatus>()

	// MARK: Lifecycle
	deinit
	{
		locationManager.stopUpdatingLocation()
	}
	
	func obtainLocation() -> Promise<CLLocation>
	{
		if let promise = locationObtainingPromise
		{
			return promise
		}

		locationObtainingPromise = firstly
		{
			CLLocationManager.requestLocation().lastValue
		}.get {  location in
			self.temporarilyStoreLocation(location)
		}.ensure
		{
			self.locationObtainingPromise = nil
		}
		
		return locationObtainingPromise!
	}
}

// MARK:- API
extension LocationManager
{
	var userLocation: CLLocationCoordinate2D?
	{
		return lastUpdatedLocation?.coordinate
	}
	var coordinateUpdatedObservable: Observable<CLLocationCoordinate2D>
	{
		return privateCoordinateUpdatedObservable
	}

	var coordinateFailureObservable: Observable<Void>
	{
		return coordinateFailureSubject.asObservable()
	}
	
	var authorizationStatusChangedObservable: Observable<CLAuthorizationStatus>
	{
		return authorizationStatusChangedSubject.asObservable()
	}

	func startUpdatingLocation()
	{
		locationManager.delegate = self
		locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters

		switch CLLocationManager.authorizationStatus()
		{
		case .notDetermined:
			locationManager.requestWhenInUseAuthorization()

		case .restricted,
			 .denied:
			 coordinateFailureSubject.onNext(())
			currentLocation = nil
			lastUpdatedLocation = nil
			
		case .authorizedWhenInUse:
			locationManager.startUpdatingLocation()

		case .authorizedAlways:
			locationManager.startUpdatingLocation()
		@unknown default:
			fatalError()
		}
	}

	func stopUpdatingLocation()
	{
		locationManager.stopUpdatingLocation()
	}
	
	func requestWhenInUseAuthorization()
	{
		locationManager.delegate = self
		locationManager.requestWhenInUseAuthorization()
	}
}
extension LocationManager: CLLocationManagerDelegate
{
	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
	{
		manager.stopUpdatingLocation()
	}

	func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
	{
		authorizationStatusChangedSubject.onNext(status)

		switch status
		{
		case .authorizedWhenInUse,
			 .authorizedAlways,
			 .notDetermined:
			manager.startUpdatingLocation()
		case
			 .restricted,
			 .denied:
			currentLocation = nil
			lastUpdatedLocation = nil
			locationObtainingPromise = nil 
			break

		@unknown default:
			currentLocation = nil
			lastUpdatedLocation = nil
		}
	}

	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
	{
		switch CLLocationManager.authorizationStatus()
		{
		case .authorizedWhenInUse,
			 .authorizedAlways,
			 .notDetermined:
			guard let location = locations.last else { return }
			self.currentLocation = location.coordinate
			stopUpdatingLocation()
		case
			 .restricted,
			.denied:
			break
		@unknown default:
			break
		}
	}
}

// MARK:- Utils
private extension LocationManager
{
	func temporarilyStoreLocation(_ location: CLLocation)
	{
		lastUpdatedLocation = location
		
	}
}
