//
//  WebServiceManager.swift
//  Polka
//
//  Created by Makson on 27.04.2021.
//

import Foundation

import Alamofire
import PromiseKit
import RxSwift
import SwiftyJSON
import Starscream



protocol UserStore: AnyObject
{
	var userStore: User? { get set }
	var isBring: Bool { get set}
	var deliveryAddress: DeliveryAddresses? { get set}
}


typealias PromiseTuple<T> = (promise: Promise<T>, resolver: Resolver<T>)


class WebServiceManager
{
	private lazy var sessionManager = createSessionManager()
	
	var store: UserStore!

	private var socketConnectionPromiseTuple: PromiseTuple<WebSocket>?
	private var socketReconnectionTimer: Timer?
	private var socket: WebSocket?
	private var socketMessageSubject = PublishSubject<Message>()
	
	init(_ store: UserStore)
	{
		self.store = store

		AlamofireUtils.loggingEnabled = true

		if store.userStore != nil
		{
			establishSocket()
		}
	}
}


private extension WebServiceManager
{
	static let sessionManagerRequestTimeout: TimeInterval = 60
	
	func createSessionManager() -> SessionManager
	{
		let configuration = URLSessionConfiguration.default
		configuration.timeoutIntervalForRequest = WebServiceManager.sessionManagerRequestTimeout
		let sessionManager = SessionManager(configuration: configuration)
		
		sessionManager.setAuthorizationHandler(
			withStrategy: CustomTokenAuthorizationStrategy(
				token: self.currentUserProfile?.authorization.token
			)
		)
		
		return sessionManager
	}
}
// MARK: - Managing authorization

private extension WebServiceManager
{
	var user: User?
	{
		return store.userStore
	}
	
	func updateUserStore(_ userStore: User?)
	{
		store.userStore = userStore

		if userStore != nil, socket == nil
		{
			establishSocket()
		}
		else if userStore == nil
		{
			terminateSocket()
		}
		
		// AAAAA TOREMOVE
		if let userStore = userStore,
		   let role = userStore.profile?.role,
		   case User.Profile.Role.consumer(let rawConsumer) = role,
		   let consumer = rawConsumer,
		   let deliveryAddresses  = consumer.deliveryAddresses,
		   deliveryAddresses.count > 0
		{
			UserController.shared.onNewDeliveryAddressObtained(deliveryAddresses[deliveryAddresses.count - 1])
		}
	}
}


// MARK: - AuthorizationHandlerDelegate

extension WebServiceManager
{
	var isAuthorized: Bool
	{
		return currentUserProfile != nil
	}
	
	var currentUserProfile: User?
	{
		return store.userStore
	}
	
	var isBring: Bool
	{
		return store.isBring
	}
	var deliveryAddress: DeliveryAddresses?
	{
		return store.deliveryAddress
	}
	func resetAuthorization()
	{
		updateUserStore(nil)
	}
	
	func updateBring(isBring:Bool)
	{
		store.isBring = isBring
	}
	func updateDelivery(address:DeliveryAddresses?)
	{
		store.deliveryAddress = address
		
		// AAAAA TOREMOVE
		if let newDeliveryAddress = address
		{
			UserController.shared.onNewDeliveryAddressObtained(newDeliveryAddress)
		}
	}

	func setSellerWorkingStatus(
		isSellerWorking: Bool
	)
	{
		if let oldUser = store.userStore
		{
			func patchUserProfile(_ oldProfile: User.Profile?, isSellerWorking: Bool) -> User.Profile?
			{
				oldProfile.map {
					User.Profile(
						id: $0.id,
						phone: $0.phone,
						role: patchUserProfileRole($0.role, isSellerWorking: isSellerWorking)
					)
				}
			}

			func patchUserProfileRole(_ oldRole: User.Profile.Role?, isSellerWorking: Bool) -> User.Profile.Role?
			{
				switch oldRole {
				case .consumer(let consumerInfo):
					return .consumer(consumerInfo)

				case .seller(let sellerInfo):
					if let oldSellerInfo = sellerInfo {
						return .seller(SellerInfo(
							caption: oldSellerInfo.caption,
							description: oldSellerInfo.description,
							profileImageUrl: oldSellerInfo.profileImageUrl,
							sellerGallery: oldSellerInfo.sellerGallery,
							isOpen: isSellerWorking,
							isPriceActual: oldSellerInfo.isPriceActual
						))
					} else {
						return nil
					}

				case .none:
					return nil
				}
			}

			//
			let newUser = User(
				authorization: oldUser.authorization,
				profile: patchUserProfile(oldUser.profile, isSellerWorking: isSellerWorking)
			)

			store.userStore = newUser
		}
	}
}

//MARK: - Authorization
extension WebServiceManager
{
	func sendOTP(phone: String) -> Promise<OTP>
	{
		return firstly {
			sessionManager.performRequest(
				.post,
				Endpoints.requestAuthorizationCode,
				parameters: .json(
					values: [
						"phone": phone,
						"app_type": "ios"
					]
				),
				authorizationOptions: .none,
				//если кусок нуже из объекта то OTP.parse("key")
				parseResponse: OTP.parse(),
				parseErrors: Errors.createParser()
			)
		}
	}
	func authorization(phone:String, otp:String) -> Promise<User>
	{
		return firstly {
			sessionManager.performRequest(
				.post,
				Endpoints.loginWithPhone,
				parameters: .json(
					values: [
						"phone": phone,
						"app_type": "ios",
						"otp":otp
					]
				),
				authorizationOptions: .none,
				parseResponse: User.parse(),
				parseErrors: Errors.createParser()
			)
		}
		.get{ user in
			self.updateUserStore(user)
			
			switch user.profile?.role
			{
			case .consumer(let consumer):
				self.updateDelivery(address: consumer?.deliveryAddresses?.last)
			case .seller(_):
				print("Продавец")
			default:
				print("")
			}
		}
	}

}

//MARK: - Profile
extension WebServiceManager
{
	func getProfile() -> Promise<User.Profile>
	{
		return firstly {
			sessionManager.performRequest(
				.get,
				Endpoints.requestGetProfile,
				authorizationOptions: .authorize,
				parseResponse: User.Profile.parse(),
				parseErrors: Errors.createParser()
			)
		}
		.get{ profile in
			switch profile.role
			{
			case .consumer(let consumer):
				self.updateDelivery(address: consumer?.deliveryAddresses?.last)
			case .seller(_):
				print("Продавец")
			default:
				print("")
			}
		}
	}
	
	func logout() -> Promise<Void>
	{
		return firstly {
			sessionManager.performRequest(
				.delete,
				Endpoints.logout,
				authorizationOptions: .authorize,
				parseResponse: AlamofireUtils.ParseResponseWithExpectedStatuses(204)
			)
		}
		.done
		{
			self.updateUserStore(nil)
			self.updateDelivery(address: nil)
		}
	}
}



// MARK: - Markets

extension WebServiceManager
{
	func getMarkets() -> Promise<[Market]>
	{
		return sessionManager.performRequest(
			.get,
			Endpoints.getMarkets(),
			authorizationOptions: .none,
			parseResponse: Market.parseArray(
				skipFailedElements: true,
				canBeEmpty: false
			),
			parseErrors: Errors.createParser()
		)
	}
}


//MARK: - Categories
extension WebServiceManager
{
	func getCategories(
		marketAlias: MarketAlias,
		cancel: UnsafeMutablePointer<AlamofireUtils.Cancel?>?
	) -> Promise<[Category]>
	{
		return sessionManager.performRequest(
			.get,
			Endpoints.categories(marketAlias),
			authorizationOptions: .none,
			parseResponse: Category.parseArray(
				skipFailedElements: true,
				canBeEmpty: false
			),
			parseErrors: Errors.createParser(),
			cancel: cancel
		)
	}
}


//MARK: - Seller catalog
extension WebServiceManager
{
	func getSellerCatalog(sellerId: SellerId, categoryId: SellerCaterogyId?) -> Promise<SellerCatalog>
	{
		return sessionManager.performRequest(
			.get,
			Endpoints.sellersCatalog(sellerId),
			parameters: categoryId == nil ? nil: .url(values: ["category_id": (categoryId ?? "")]),
			authorizationOptions: .none ,
			parseResponse: SellerCatalog.parse(),
			parseErrors: Errors.createParser()
		)
	}
	
	func getSellerCatalogWithPagination(_ sellerId: String,
										_ categoryId: String?,
										_ itemsPerPageCount: Int,
										_ offset: Int) -> Promise<SellerCatalogPaginationResponse>
	{
		return sessionManager.performRequest(
			.get,
			Endpoints.sellersCatalog(sellerId),
			parameters: .url(values: getSellerCatalogPaginationParameters(categoryId,
																		  itemsPerPageCount,
																		  offset)),
			authorizationOptions: .none ,
			parseResponse: SellerCatalogPaginationResponse.parse(),
			parseErrors: Errors.createParser()
		)
	}
	
	private func getSellerCatalogPaginationParameters(_ categoryId: String?,
													  _ itemsPerPageCount: Int,
													  _ offset: Int) -> [String: Any]
	{
		let rawParameters: [(String, String)] = [("category_id", categoryId),
												 ("limit", String(itemsPerPageCount)),
												 ("offset", String(offset))].map{
													if let value = $0.1 { return ($0.0, value) } else { return nil }
												}.compactMap{$0}
		
		return rawParameters.reduce(into: [:], { (dict, raw) in
			dict[raw.0] = raw.1
		})
	}
}

//MARK: - Seller Account
extension WebServiceManager
{
	func getSellerProductCategories() -> Promise<[SellerProductCategory]>
	{
		sessionManager.performRequest(
			.get,
			Endpoints.getSellerProductCategories(),
			authorizationOptions: .authorize,
			parseResponse: SellerProductCategory.parseArray(
				skipFailedElements: true,
				canBeEmpty: true
			),
			parseErrors: Errors.createParser()
		)
	}

	func getSellerProducts(
		categoryId: SellerCaterogyId,
		page: Page
	) -> Promise<[SellerCatalogProduct]>
	{
		let params: [String : Any] = [
			//"q": query,
			"cids": "\(categoryId)",
			"offset": page.offset,
			"limit": page.limit,
		]

		return sessionManager.performRequest(
			.get,
			Endpoints.getSellerProducts(),
			parameters: .url(values: params),
			authorizationOptions: .authorize,
			parseResponse: SellerCatalogProduct.parseArray(
				"results",
				skipFailedElements: true,
				canBeEmpty: true
			),
			parseErrors: Errors.createParser()
		)
	}
}

//MARK: - Market
extension WebServiceManager
{
	func sendSellerWorkingStatus(isOpen:Bool) -> Promise<Void>
	{
		sessionManager.performRequest(
			.patch,
			Endpoints.requestSeller,
			parameters: .json(values: ["is_open_now":isOpen]),
			authorizationOptions: .authorize,
			parseResponse: AlamofireUtils.ParseResponseWithExpectedStatuses(200),
			parseErrors: Errors.createParser()
		)
	}
	
	func updateStock(products:[StockProduct]) -> Promise<[ProductStock]>
	{
		return firstly {
			sessionManager.performRequest(
				.patch,
				Endpoints.requestStock,
				parameters: .json(
					values: [
						"products": products.map{["id":$0.id,"price_nominal":$0.price_nominal,"balance": $0.balance]}
					]
				),
				authorizationOptions: .authorize,
				parseResponse: ProductStock.parseArray(skipFailedElements: true,
												   canBeEmpty: false),
				parseErrors: Errors.createParser()
			)
		}
	}
}

//MARK: - Address
extension WebServiceManager
{
	func createDeliveryAddress(_ street:String,
							   _ house:String,
							   _ apartment:String?,
							   _ floor:String?,
							   _ entrance:String?,
							   _ door_code:String?,
							   _ location_point: LocationPoint,
							   _ comment: String?) -> Promise<DeliveryAddresses>
	{
		var params: [String: Any] = [
			"street":street,
			"house":house,

			"location_point": ["latitude":location_point.latitude,
							   "longitude":location_point.longitude
			]
		]
		
		params.writeIfNotNil("apartment", apartment)
		params.writeIfNotNil("floor", floor)
		params.writeIfNotNil("entrance", entrance)
		params.writeIfNotNil("door_code", door_code)
		params.writeIfNotNil("comment", comment)
		
		return firstly{
			sessionManager.performRequest(.put,
										  Endpoints.createDeliveryAddress,
										  parameters: .json(values: params),
										   authorizationOptions: .authorize,
										   parseResponse: DeliveryAddresses.parse(),
										   parseErrors: Errors.createParser())
		}
		.get
		{ address in
			self.updateDelivery(address: address)
		}
	}
	
	func updateDeliveryAddress(_ id:String,
							   _ street:String,
							   _ house:String,
							   _ apartment:String?,
							   _ floor:String?,
							   _ entrance:String?,
							   _ door_code:String?,
							   _ location_point: LocationPoint,
							   _ comment: String?) -> Promise<DeliveryAddresses>
	{
		var params: [String: Any] = [
			"street":street,
			"house":house,

			"location_point": ["latitude":location_point.latitude,
							   "longitude":location_point.longitude
			]
		]
		
		params.writeIfNotNil("apartment", apartment)
		params.writeIfNotNil("floor", floor)
		params.writeIfNotNil("entrance", entrance)
		params.writeIfNotNil("door_code", door_code)
		params.writeIfNotNil("comment", comment)
		
		return firstly{
			sessionManager.performRequest(.patch,
										  Endpoints.updateDeliveryAddress(id),
										  parameters: .json(values: params),
										   authorizationOptions: .authorize,
										   parseResponse: DeliveryAddresses.parse(),
										   parseErrors: Errors.createParser())
		}
		.get
		{ address in
			self.updateDelivery(address: address)
		}
	}
}

// MARK:- Cart
extension WebServiceManager: CartWebServiceProtocol
{
	func getCartContents() -> Promise<Cart>
	{
		sessionManager.performRequest(
			.get,
			Endpoints.cart,
			parameters: nil,
			authorizationOptions: .authorize,
			parseResponse: Cart.parse(),
			parseErrors: Errors.createParser()
		)
	}
	
	
	enum CartUpdateError: Error
	{
		case cannotPutProductsFromDifferentMarkets
	}
	
	func updateCart(_ cartDiff: CartDiff) -> Promise<Cart>
	{
		return sessionManager.performRequest(
			.put,
			Endpoints.cart,
			parameters: cartDiff.toQueryParameters(),
			authorizationOptions: .authorize,
			parseResponse: Cart.parse(),
			parseErrors: Self.parseCartUpdateError
		)
	}
	
	private static func parseCartUpdateError(_ data: Data?, _ response: HTTPURLResponse) -> Error?
	{
		switch response.statusCode {
		case 200:
			return nil
			
		case 406:
			return CartUpdateError.cannotPutProductsFromDifferentMarkets
		
		default:
			return WebServiceManager.Errors.with(
				code: "unknown",
				statusCode: response.statusCode
			)
		}
	}
	
	
	func removeAllProductsFromCart() -> Promise<Void>
	{
		sessionManager.performRequest(
			.delete,
			Endpoints.cart,
			authorizationOptions: .authorize,
			parseResponse: AlamofireUtils.ParseResponseWithExpectedStatuses(200),
			parseErrors: Errors.createParser()
		)
	}
}

// MARK:- Order
extension WebServiceManager
{
	func createOrder() -> Promise<OrderInfo>
	{
		sessionManager.performRequest(.post,
									  Endpoints.order,
									  parameters: nil,
									  authorizationOptions: .authorize,
									  parseResponse: OrderInfo.parse(),
									  parseErrors: Errors.createParser())
	}
}

// MARK:- Orders with pagination
extension WebServiceManager
{
	func getOrdersWithPagination(_ itemsPerPageCount: Int, _ offset: Int) -> Promise<OrdersPaginationResponse>
	{
		sessionManager.performRequest(.get,
									  Endpoints.order,
									  parameters: .url(values: ["limit": itemsPerPageCount, "offset": offset]),
									  authorizationOptions: .authorize,
									  parseResponse: OrdersPaginationResponse.parse(),
									  parseErrors: Errors.createParser())
	}
}

// MARK:- Cancelling Orders
extension WebServiceManager
{
	func cancelOrder(orderId: OrderID) -> Promise<Void>
	{
		sessionManager.performRequest(
			.delete,
			Endpoints.cancelOrder(orderId: orderId),
			authorizationOptions: .authorize,
			parseResponse: AlamofireUtils.ParseResponseWithExpectedStatuses(200),
			parseErrors: Errors.createParser()
		)
	}
}

// MARK:- SubOrders with pagination
extension WebServiceManager
{
	func getSubOrdersWithPagination(_ itemsPerPageCount: Int, _ offset: Int) -> Promise<SubOrdersPaginationResponse>
	{
		sessionManager.performRequest(.get,
									  Endpoints.subOrder,
									  parameters: .url(values: ["limit": itemsPerPageCount, "offset": offset]),
									  authorizationOptions: .authorize,
									  parseResponse: SubOrdersPaginationResponse.parse(),
									  parseErrors: Errors.createParser())
	}
	
	func subOrderAssemble(subOderId:String,products:[AssembleProduct]) -> Promise<SubOrderInfo>
	{
		sessionManager.performRequest(.patch,
									  Endpoints.assembleSubOrder(subOderId),
									  parameters: products.map{ ["id":$0.id,
																 "count": String($0.count.roundToThreeDecimal())]},
									  authorizationOptions: .authorize,
									  parseResponse: SubOrderInfo.parse(),
									  parseErrors: Errors.createParser())
	}
}

// MARK:- Push notifications
extension WebServiceManager
{
	func registerDevice(_ deviceToken: String) -> Promise<Void>
	{
		sessionManager.performRequest(.put,
									  Endpoints.registerDevice,
									  parameters: .json(values: getRegisterDeviceParameters(deviceToken)),
									  parseResponse: AlamofireUtils.ParseResponseWithExpectedStatuses(201),
									  parseErrors: Errors.createParser())
	}
	
	private func getRegisterDeviceParameters(_ deviceToken: String) -> [String: Any]
	{
		return ["device_type": "ios", "device_token": deviceToken]
	}
}

// MARK:- Chats
extension WebServiceManager
{
	func getChats(page: Page) -> Promise<[Chat]>
	{
		sessionManager.performRequest(
			.get,
			Endpoints.chatsList,
			parameters: .url(
				values: [
					"limit": page.limit,
					"offset": page.offset
				]
			),
			authorizationOptions: .authorize,
			parseResponse: Chat.parseArray(
				"results",
				skipFailedElements: true,
				canBeEmpty: true
			),
			parseErrors: Errors.createParser()
		)
	}

	func markChatRead(
		receiverId: Message.PartyId,
		upTo newestReadMessageDate: Date
	) -> Promise<Void>
	{
		sessionManager.performRequest(
			.patch,
			Endpoints.markChatRead,
			parameters: .json(
				values: [
					"receiver_id": receiverId,
					"read_to": DatesFormatter.toRequestString(newestReadMessageDate)
				]
			),
			authorizationOptions: .authorize,
			parseResponse: AlamofireUtils.ParseResponseWithExpectedStatuses(200),
			parseErrors: Errors.createParser()
		)
	}

	func sendMessage(
		to receiverId: Message.PartyId,
		text: String? = nil ,
		attachmentsIds: [Attachment.Id]? = nil
	) -> Promise<Message>
	{
		var params: [String : Any] = [
			"receiver_id": receiverId
		]

		if let text = text
		{
			params["message"] = text
		}

		if let attachmentsIds = attachmentsIds
		{
			params["attachments"] = attachmentsIds
		}

		return sessionManager.performRequest(
			.post,
			Endpoints.sendMessage,
			parameters: .json(values: params),
			authorizationOptions: .authorize,
			parseResponse: Message.parse(),
			parseErrors: Errors.createParser()
		)
	}

	func getMessages(
		receiverId: Message.PartyId,
		oldestMessageDate: Date?,
		resultsPerPage: Int
	) -> Promise<MessagesList>
	{
		var params: [String : Any] = [
			"limit" : resultsPerPage
		]

		if let oldestMessageDate = oldestMessageDate
		{
			params["date"] = DatesFormatter.toRequestString(oldestMessageDate)
		}

		return sessionManager.performRequest(
			.get,
			Endpoints.messagesList(receiverId: receiverId),
			parameters: .url(values: params),
			authorizationOptions: .authorize,
			parseResponse: MessagesList.parse(),
			parseErrors: Errors.createParser()
		)
	}
}

// MARK:- Files
extension WebServiceManager
{
	func uploadImage(_ image: UIImage) -> Promise<Attachment.Id>
	{
		guard let jpegData = image.jpegData(compressionQuality: 1)
		else
		{
			return .init(error: Errors.custom("Couldn't get JPEG data from image"))
		}

		let file = MultipartFile(
			key: "file",
			data: jpegData,
			filename: "image.jpg",
			mimeType: "image/jpeg"
		)

		return sessionManager.performUpload(
			.post,
			Endpoints.uploadFile(.image),
			file: file,
			authorizationOptions: .authorize,
			parseResponse: Attachment.Id.parse("id"),
			parseErrors: Errors.createParser()
		)
	}

	func uploadVideo(_ localUrl: URL) -> Promise<Attachment.Id>
	{
//		let file = MultipartUrlFile(
//			url: localUrl,
//			name: localUrl.lastPathComponent
//		)

		guard let data = try? Data(contentsOf: localUrl)
		else
		{
			return .init(error: NSError(domain: "", code: -1, userInfo: [:]))
		}
		
		let file = MultipartFile(
			key: "file",
			data: data,
			filename: localUrl.lastPathComponent, // TODO:
			mimeType: "video/quicktime" // TODO:
		)

		return sessionManager.performUpload(
			.post,
			Endpoints.uploadFile(.video),
			file: file,
			authorizationOptions: .authorize,
			parseResponse: Attachment.Id.parse("id"),
			parseErrors: Errors.createParser()
		)
	}
}



// MARK: - Searching


extension WebServiceManager
{
	func getCatalogSearchData(marketAlias: MarketAlias) -> Promise<CatalogSearchData>
	{
		sessionManager.performRequest(
			.get,
			Endpoints.getCatalogSearchData(alias: marketAlias),
			authorizationOptions: .none,
			parseResponse: CatalogSearchData.parse(),
			parseErrors: Errors.createParser()
		)
	}
	
	func searchProducts(
		marketAlias: MarketAlias,
		query: String,
		filters: [SearchCategory],
		page: Page,
		cancel: UnsafeMutablePointer<AlamofireUtils.Cancel?>
	) -> Promise<[FoundProduct]>
	{
		let params: [String : Any] = [
			"q": query,
			"category_id": filters.map { $0.id },
			"offset": page.offset,
			"limit": page.limit,
		]
		
		return self.sessionManager.performRequest(
			.get,
			Endpoints.searchProducts(alias: marketAlias),
			parameters: .url(values: params),
			authorizationOptions: .none,
			parseResponse: FoundProduct.parseArray(
				"results",
				skipFailedElements: false,
				canBeEmpty: true
			),
			cancel: cancel
		)
	}
}



// MARK:- Page
extension WebServiceManager
{
	struct Page
	{
		let offset: Int
		let limit: Int

		init(
			offset: Int,
			limit: Int = defaultLimit
		)
		{
			self.offset = offset
			self.limit = limit
		}

		static func first() -> Page
		{
			return .init(offset: 0)
		}

		func next() -> Page
		{
			return .init(
				offset: offset + limit,
				limit: limit
			)
		}

		static let defaultLimit = 10
	}
}



// MARK: - Socket / Observable

extension WebServiceManager
{
	var socketMessageObservable: Observable<Message>
	{
		return socketMessageSubject
	}
}

// MARK: - Socket / Implementation

private extension WebServiceManager
{
	enum SocketErrors: Error
	{
		case connectionFailed
	}

	static let socketEstablishingRetries = 2

	func establishSocket()
	{
		cancelSocketReconnection()

		firstly
		{
			establishSocket(
				retriesLeft: WebServiceManager.socketEstablishingRetries
			)
		}
		.done
		{
			socket in

			self.socket = socket
		}
		.catch
		{
			_ in

			self.scheduleSocketReconnection()
		}
	}

	func establishSocket(retriesLeft: Int) -> Promise<WebSocket>
	{
		guard let authorization = store.userStore?.authorization
		else
		{
			return Promise(error: PMKError.cancelled)
		}

		return firstly
		{
			openSocket(with: authorization)
		}
		.recover
		{
			error -> Promise<WebSocket> in

			guard retriesLeft > 0
			else
			{
				throw error
			}

			return self.establishSocket(
				retriesLeft: retriesLeft - 1
			)
		}
	}

	func openSocket(with authorization: User.Authorization) -> Promise<WebSocket>
	{
		let tuple = Promise<WebSocket>.pending()
		socketConnectionPromiseTuple = tuple

		let socket = createSocket(with: authorization)

		socket.onEvent =
		{
			event in

			switch event
			{
			case .connected:
				tuple.resolver.fulfill(socket)

			case .disconnected, .error:
				tuple.resolver.reject(SocketErrors.connectionFailed)

			case .cancelled:
				tuple.resolver.reject(PMKError.cancelled)

			default:
				break
			}
		}

		socket.connect()

		return firstly
		{
			tuple.promise
		}
		.ensure
		{
			socket.onEvent = nil

			self.socketConnectionPromiseTuple = nil
		}
	}

	func createSocket(with authorization: User.Authorization) -> WebSocket
	{
		var request = URLRequest(
			url: URL(string: Endpoints.chatSocket)!
		)

		request.setValue(
			"Bearer \(authorization.token)",
			forHTTPHeaderField: "Authorization"
		)

		request.setValue(
			"permessage-deflate; client_max_window_bits",
			forHTTPHeaderField: "Sec-WebSocket-Extensions"
		)

		let socket = WebSocket(
			request: request
		)

		socket.delegate = self

		return socket
	}

	static let socketReconnectionInterval: TimeInterval = 60

	func scheduleSocketReconnection()
	{
		socketReconnectionTimer = Timer.scheduledTimer(
			withTimeInterval: WebServiceManager.socketReconnectionInterval,
			repeats: false,
			block: { _ in
				self.establishSocket()
			}
		)
	}

	func cancelSocketReconnection()
	{
		socketReconnectionTimer?.invalidate()
		socketReconnectionTimer = nil
	}

	func terminateSocket()
	{
		cancelSocketReconnection()
		cancelSocketConnection()
		closeSocket()
	}

	func cancelSocketConnection()
	{
		socketConnectionPromiseTuple?.resolver.reject(PMKError.cancelled)
		socketConnectionPromiseTuple = nil
	}

	func closeSocket()
	{
		socket?.delegate = nil
		socket?.disconnect()
		socket = nil
	}

	func handleSocketNotification(_ notification: String)
	{
		if let message = Message.from(notification: notification)
		{
			socketMessageSubject.onNext(message)
		}
	}
}

// MARK:- WebSocketDelegate
extension WebServiceManager: WebSocketDelegate
{
	func didReceive(event: WebSocketEvent, client: WebSocket)
	{
		guard client === socket
		else
		{
			return
		}

		switch event
		{
		case .disconnected,
			 .cancelled:
			establishSocket()

		case .text(let notification):
			handleSocketNotification(notification)

		default:
			break
		}
	}
}


private extension Dictionary where Key == String, Value == Any
{
	mutating func writeIfNotNil<T>(_ key: String, _ value: T?)
	{
		if let value = value
		{
			self[key] = value
		}
	}
}
