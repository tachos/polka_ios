//
//  WebServiceManager+Errors.swift
//  Polka
//
//  Created by Makson on 27.04.2021.
//

import Foundation

import SwiftyJSON




extension WebServiceManager
{
	struct Errors: Error
	{
		typealias StatusCode = Int
		typealias Code = String
		typealias Message = String
		typealias Field = String
		typealias ErrorInfo = (code: Code, message: Message)
		typealias ErrorsByFields = [Field: ErrorInfo]

		let statusCode: StatusCode
		var byFields: ErrorsByFields
	}
}

extension WebServiceManager.Errors
{
	static func with(code: Code, statusCode: StatusCode) -> WebServiceManager.Errors
	{
		return WebServiceManager.Errors(
			statusCode: statusCode,
			byFields:
			[
				"": ErrorInfo(
					code: code,
					message: WebServiceManager.Errors.messagesMapping[code] ?? ""
				)
			]
		)
	}
}



// MARK: - Parsing

extension WebServiceManager.Errors
{
	static func createParser() -> (_ data: Data?, _ response: HTTPURLResponse) -> Error?
	{
		return { (data, response) -> Error? in
			//
			return data.flatMap
			{
				parseError(
					from: $0,
					statusCode: response.statusCode
				)
			}
		}
	}
	
	static func parseError(from data: Data, statusCode: Int) -> Error?
	{
		func errorFrom(_ json: JSON) -> (field: String?, errorInfo: ErrorInfo)?
		{
			guard let code = json["code"].string
			else
			{
				return nil
			}
			
			guard let field = json["field"].string,
				  let message = messagesMapping[field]
			else
			{
				 return (code, (code, messagesMapping[code] ?? "Unrecognized error"))
			}
			
			return (
				field.count > 0 ? field : nil,
				(code, message)
			)
		}
		var error:WebServiceManager.Errors?
		
		guard let json = try? JSON(data: data),
			let errors = json.array?.compactMap({ errorFrom($0)}),
			errors.count > 0
		else
		{
			if statusCode >= 400 && statusCode < 500
			{
				return WebServiceManager.Errors(
					statusCode: statusCode,
					byFields: ["":("","\(statusCode)")]
				)
			}
			else
			{
				return nil
			}
		}
		
		let fieldsErrors = errors.filter { $0.field != nil }
		let errorsByFields = Dictionary(
			fieldsErrors.map { ($0.field!, $0.errorInfo) },
			uniquingKeysWith: { (first, _) in first }
		)
		error = WebServiceManager.Errors(
			statusCode: statusCode,
			byFields: errorsByFields
		)
		
		return error
	}
}



// MARK: - Check for authorization error

extension WebServiceManager.Errors
{
	static let authorizationErrorCode = "0x401"
}

extension Error
{
	var isAuthorizationError: Bool
	{
		guard let error = self as? WebServiceManager.Errors
		else
		{
			return false
		}
		
		return error.byFields.contains { (_, error) in
			error.code == WebServiceManager.Errors.authorizationErrorCode
		}
	}
}


// MARK: - Check for Card Does Not Exist error

extension WebServiceManager.Errors
{
	static let invalidParameterErrorCode = "0x400"
}

extension Error
{
	var isCardDoesNotExistsError: Bool
	{
		guard let error = self as? WebServiceManager.Errors
			else
		{
			return false
		}

		return error.byFields["card"]?.code == WebServiceManager.Errors.invalidParameterErrorCode
			&& error.byFields["code"]?.code == WebServiceManager.Errors.invalidParameterErrorCode
	}
}



// MARK: - Check for Review error

extension Error
{
	func getReviewTextError() -> String?
	{
		guard let error = self as? WebServiceManager.Errors
			else
		{
			return nil
		}

		return error.byFields["text"]?.message
	}
}


// MARK: - Custom

extension WebServiceManager.Errors
{
	static func custom(_ message: String) -> Error
	{
		return NSError(
			domain: "Web Service",
			code: -13,
			userInfo: [NSLocalizedDescriptionKey: message]
		)
	}
}

// MARK: - LocalizedError

extension WebServiceManager.Errors: LocalizedError
{
	var errorDescription: String?
	{
		return byFields.values
			.map { $0.message }
			.joined(separator: "\n")
	}
}

// MARK: - Localization

extension WebServiceManager.Errors
{
	static let messagesMapping = [
		"phone": NSLocalizedString(
			"Некорректный телефон",
			tableName: "WebServiceManager.Errors",
			comment: "0x400 error message"),
		"otp": NSLocalizedString(
			"Неверный код",
			tableName: "WebServiceManager.Errors",
			comment: "0x400 error message"),
		"authentication_failed": NSLocalizedString(
			"Авторизация несостоялась",
			tableName: "WebServiceManager.Errors",
			comment: "0x400 error message"),
		"parse_error": NSLocalizedString(
			"Ошибка парсинга тела запроса",
			tableName: "WebServiceManager.Errors",
			comment: "0x400 error message"),
		"not_authenticated": NSLocalizedString(
			"Неавторизованный запрос",
			tableName: "WebServiceManager.Errors",
			comment: "0x400 error message"),
		"permission_denied": NSLocalizedString(
			"Доступ запрещен из-за нехватки прав",
			tableName: "WebServiceManager.Errors",
			comment: "0x400 error message"),
		"not_found": NSLocalizedString(
			"Объект не найден",
			tableName: "WebServiceManager.Errors",
			comment: "0x400 error message"),
		"method_not_allowed": NSLocalizedString(
			"Метод HTTP неподдерживается",
			tableName: "WebServiceManager.Errors",
			comment: "0x400 error message"),
		"not_acceptable": NSLocalizedString(
			"Неприменимо здесь",
			tableName: "WebServiceManager.Errors",
			comment: "0x400 error message"),
		"unsupported_media_type": NSLocalizedString(
			"Неподдерживаемый формат медиа",
			tableName: "WebServiceManager.Errors",
			comment: "0x400 error message"),
		"throttled": NSLocalizedString(
			"Слигком частые запросы",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"required": NSLocalizedString(
			"Обязательное поле",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"invalid": NSLocalizedString(
			"Поле содержит неверные данные",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"null": NSLocalizedString(
			"Поле не может содержать значение null",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"blank": NSLocalizedString(
			"Поле не может сожержать пустые данные",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"empty": NSLocalizedString(
			"Значение в поле должно быть указано из доступных вариантов",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"not_a_list": NSLocalizedString(
			"Значение в поле не является списком",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"not_a_dict": NSLocalizedString(
			"Значение в поле не является словарем",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"max_length": NSLocalizedString(
			"Длинна значения в поле больше максимальной длинны",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"min_length": NSLocalizedString(
			"Длинна значения в поле меньше минимальной длинны",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"max_value": NSLocalizedString(
			"Значение в поле превышает максимально возможное значение",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"min_value": NSLocalizedString(
			"Значение в поле меньше минимально возможного значения",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"max_string_length": NSLocalizedString(
			"Текстовое представление числа слишком длинное",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"max_digits": NSLocalizedString(
			"Значение в поле содержит большее количество чисел чем допустимо",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"max_decimal_places": NSLocalizedString(
			"Значение в поле содержит большее количество чисел в дробной части чем допустимо",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"date": NSLocalizedString(
			"Значение в поле имеет формат date вместо datetime",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"datetime": NSLocalizedString(
			"Значение в поле имеет формат datetime вместо date",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"invalid_choice": NSLocalizedString(
			"Значение в поле недопустимо",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"no_name": NSLocalizedString(
			"Название файла неверное",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
		"invalid_image": NSLocalizedString(
			"Изображение неверное или повреждено",
			tableName: "WebServiceManager.Errors",
			comment: "Error message"),
	]
}



