//
//  CustomTokenAuthorizationStrategy.swift
//  Polka
//
//  Created by Makson on 29.04.2021.
//

import Foundation

import PromiseKit

/// Uses Token HTTP authentication scheme
class CustomTokenAuthorizationStrategy
{
	let token: () -> String?
		
	init(token: @escaping @autoclosure () -> String?)
	{
		self.token = token
	}
}

extension CustomTokenAuthorizationStrategy: AuthorizationHandlerStrategy
{
	func authorizeRequest(_ urlRequest: URLRequest) throws -> URLRequest?
	{
		guard let token = token()
		else
		{
			return nil
		}
		
		var urlRequest = urlRequest
		urlRequest.setValue(
			"Bearer \(token)",
			forHTTPHeaderField: "authorization"
		)
		
		return urlRequest
	}
	
	func shouldRefreshAuthorization(on error: Error) -> Bool
	{
		return false
	}
	
	func refreshAuthorization() -> Promise<Void>?
	{
		return nil
	}
}
