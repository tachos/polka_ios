//
//  WebService+Endpoints.swift
//  Polka
//
//  Created by Makson on 27.04.2021.
//

import Foundation

struct Endpoints
{

	#if DEBUG

	// dev:
	private static let host = "dev.polkaonline.ru/api"
	private static let socketHost = "dev.polkaonline.ru"
	
	#else

	// prod:
	private static let host = "api.polkaonline.ru"
	private static let socketHost = "api.polkaonline.ru"

	#endif
	
	
	private static let scheme = "https"

	private static let version = "v2"
	
	private static let baseEndpoint = scheme + "://" + host + "/" + version + "/"
	private static let socketBaseEndpoint = "wss://" + socketHost + "/"
	private static func endpoint(
		baseEndpoint: String = baseEndpoint,
		_ path: String
	) -> String
	{
		return baseEndpoint + path
	}

	private static let versionlessBaseEndpoint = scheme + "://" + host + "/"
	private static func versionlessEndpoint(_ path: String) -> String
	{
		return versionlessBaseEndpoint + path
	}
}

// MARK: - Authorization

extension Endpoints
{
	static let requestAuthorizationCode	= endpoint("account/auth/otp/")
	static let loginWithPhone 			= endpoint("account/auth/")
}


// MARK: - Profile

extension Endpoints
{
	static func getMarkets() -> String
	{
		return endpoint("catalog/market/")
	}
	
	static func categories(_ alias: MarketAlias) -> String
	{
		return endpoint("catalog/market/\(alias)/")
	}
	
	static func sellersCatalog(_ sellerId: String) -> String
	{
		return endpoint("catalog/seller/\(sellerId)/")
	}

	static func getCatalogSearchData(alias: MarketAlias) -> String
	{
		return endpoint("catalog/market/\(alias)/search/filter/")
	}
	
	static func searchProducts(alias: MarketAlias) -> String
	{
		return endpoint("catalog/market/\(alias)/search/")
	}

	static func getSellerProductCategories() -> String
	{
		return endpoint("market/seller/search/filter/")
	}

	static func getSellerProducts() -> String
	{
		return endpoint("market/seller/search/")
	}
}


// MARK: - Cart
extension Endpoints
{
	static let cart = endpoint("order/cart/")
}


// MARK: - Order
extension Endpoints
{
	static let order = endpoint("order/")

	static func cancelOrder(orderId: OrderID) -> String
	{
		return endpoint("order/cancel/\(orderId)/")
	}

	static let subOrder = endpoint("order/sub/")
	
	static func assembleSubOrder(_ subOrderId: String) -> String
	{
		return endpoint("order/sub/\(subOrderId)/")
	}
}

//MARK: - Market
extension Endpoints
{
	static let requestSeller = endpoint("market/seller/")
	static let requestStock = endpoint("market/stock/")
}

//MARK: - Profile
extension Endpoints
{
	static let requestGetProfile = endpoint("profile/")
	static let logout = endpoint("account/auth/logout/")
}

// MARK: - Delivery address
extension Endpoints
{
	static let createDeliveryAddress = endpoint("profile/address/")
	static func updateDeliveryAddress(_ addressId: String) -> String
	{
		return endpoint("profile/address/\(addressId)/")
	}
}

// MARK: - Chats
extension Endpoints
{
	static let chatsList = endpoint("chat/")
	static let markChatRead = endpoint("chat/mark_read/")
	static let sendMessage = endpoint("chat/send_message/")
	static func messagesList(receiverId: Message.PartyId) -> String
	{
		return endpoint("chat/\(receiverId)/")
	}
	static let chatSocket = endpoint(
		baseEndpoint: socketBaseEndpoint,
		"ws/chat-server/"
	)
}

// MARK: - Files
extension Endpoints
{	
	enum UploadContentType
	{
		case image
		case video
	}
	
	static func uploadFile(_ contentType: UploadContentType) -> String
	{
		return endpoint("file/upload/\(getUploadFileUrlParameterForContentType(contentType))/")
	}
	
	private static func getUploadFileUrlParameterForContentType(_ contentType: UploadContentType) -> String
	{
		switch contentType
		{
		case .image:
			return "image"
			
		case .video:
			return "video"
		}
	}
}

// MARK:- Push
extension Endpoints
{
	static let registerDevice = endpoint("account/device/")
}
