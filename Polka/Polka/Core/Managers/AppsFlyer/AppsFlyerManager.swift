//
//  AppsFlyerManager.swift
//  Polka
//
//  Created by Makson on 29.06.2021.
//

import Foundation
import AppsFlyerLib


class AppsFlyerManager
{}

extension AppsFlyerManager
{
	func sendLog(logText:String, params: [AnyHashable:Any]?)
	{
		AppsFlyerLib.shared().logEvent(logText, withValues: params)
	}
}
