//
//  AppEventsManager.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 28.10.2021.
//

import RxSwift



class AppEventsManager
{
	private var appMovedToForegroundSubject = PublishSubject<Void>()
	private var appWillMoveToBackgroundSubject = PublishSubject<Void>()
}



// MARK: - API / Observables

extension AppEventsManager
{
	var appMovedToForegroundObservable: Observable<Void>
	{
		return appMovedToForegroundSubject
	}

	var appWillMoveToBackgroundObservable: Observable<Void>
	{
		return appWillMoveToBackgroundSubject
	}
}



// MARK: - API / Internal

extension AppEventsManager
{
	func onAppMovedToForeground()
	{
		appMovedToForegroundSubject.onNext(())
	}

	func onAppWillMoveToBackground()
	{
		appWillMoveToBackgroundSubject.onNext(())
	}
}
