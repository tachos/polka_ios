//
//  LocalStoreManager.swift
//  Polka
//
//  Created by Makson on 27.04.2021.
//

import Foundation
import SwiftyUserDefaults
import SwiftyJSON
import KeychainAccess



class LocalStoreManager
{
	private let jsonDecoder = JSONDecoder()
	private let jsonEncoder = JSONEncoder()
	
	
	init()
	{
		if firstLaunch
		{
			firstLaunch = false
			resetKeychain()
		}
	}
}



// MARK: - Resetting on install

private extension DefaultsKeys
{
	static let firstLaunch = DefaultsKey<Bool>("firstLaunch", defaultValue: true)
	static let isBring = DefaultsKey<Bool>("isBring", defaultValue: false )
}

private extension LocalStoreManager
{
	var firstLaunch: Bool
	{
		get
		{
			return Defaults[.firstLaunch]
		}
		set
		{
			Defaults[.firstLaunch] = newValue
		}
	}
	
	func resetKeychain()
	{
		try? Keychain().removeAll()
	}
}

// MARK: - AuthorizationStore

private extension LocalStoreManager
{
	private var token: String { return "token" }
}
private extension DefaultsKeys
{
	static let profile = DefaultsKey<User.Profile?>("profile")
	static let deliveryAddress = DefaultsKey<DeliveryAddresses?>("deliveryAddress")
}
extension LocalStoreManager
{
	var isBring: Bool
	{
		get
		{
			return Defaults[.isBring]
		}
		set
		{
			Defaults[.isBring] = newValue
		}
	}
}
extension LocalStoreManager: UserStore
{	
	var userStore: User?
	{
		get
		{
			let keychain = Keychain()

			if let token = keychain[token]
			{
				return User(
					authorization: User.Authorization(
						token: token
					),
					profile: Defaults[.profile]
				)
			}

			return nil
		}
		set
		{
			let keychain = Keychain()
			
			keychain[token] = newValue?.authorization.token
			Defaults[.profile] = newValue?.profile
		}
	}
	
	var deliveryAddress: DeliveryAddresses?
	{
		get
		{
			return Defaults[.deliveryAddress]
		}
		set
		{
			Defaults[.deliveryAddress] = newValue
		}
	}
}




// MARK: - Markets

extension LocalStoreManager
{
	var currentMarket: Market?
	{
		get
		{
			if let jsonData = Defaults[.currentMarket],
			   let dict = try? jsonDecoder.decode(Market.self,from: jsonData)
			{
				return dict
			}
			
			return nil
		}
		
		set
		{
			let jsonData = try? jsonEncoder.encode(newValue)
			Defaults[.currentMarket] = jsonData
		}
	}
}

private extension DefaultsKeys
{
	static let currentMarket = DefaultsKey<Data?>("currentMarket")
}
