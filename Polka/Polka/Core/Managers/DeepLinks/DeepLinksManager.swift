//
//  DeepLinksManager.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 14.09.2021.
//

import AppsFlyerLib
import RxSwift



class DeepLinksManager: NSObject
{
	enum DeepLink
	{
		case goToSeller(sellerId: String, marketAlias: String)
	}
	
	private var deepLinkOpenedSubject = PublishSubject<DeepLink>()
}



// MARK: - API / Observables

extension DeepLinksManager
{
	var deepLinkOpenedObservable: Observable<DeepLink>
	{
		return deepLinkOpenedSubject
	}
}



// MARK: - API / Deep Links

extension DeepLinksManager
{
	func handleUserActivity(_ userActivity: NSUserActivity) -> Bool
	{
		if userActivity.activityType == NSUserActivityTypeBrowsingWeb,
		   let incomingURL = userActivity.webpageURL,
		   let deepLink = parseDeepLink(from: incomingURL)
		{
			handleDeepLink(deepLink)
		}
		else
		{
			return AppsFlyerLib.shared().continue(
				userActivity,
				restorationHandler: nil
			)
		}
		
		return true
	}
	
	func handleUrl(
		_ url: URL,
		options: [UIApplication.OpenURLOptionsKey : Any]
	) -> Bool
	{
		if url.scheme == customAppUrlScheme,
		   let deepLink = parseDeepLink(from: url)
		{
			handleDeepLink(deepLink)
		}
		else
		{
			AppsFlyerLib.shared().handleOpen(
				url,
				options: options
			)
		}
		
		return true
	}
}


extension DeepLinksManager: DeepLinkDelegate
{
	func didResolveDeepLink(_ result: DeepLinkResult)
	{
		dbgtrace("\(result)")
		
		switch result.status {
		case .notFound:
			NSLog("[AFSDK] Deep link not found")
			return
			
		case .failure:
			print("Error %@", result.error!)
			return
			
		case .found:
			NSLog("[AFSDK] Deep link found")
		}
		
		guard let deepLinkObj = result.deepLink else {
			NSLog("[AFSDK] Could not extract deep link object")
			return
		}
		
		let deepLinkStr:String = deepLinkObj.toString()
		NSLog("[AFSDK] DeepLink data is: \(deepLinkStr)")
		if( deepLinkObj.isDeferred == true) {
			NSLog("[AFSDK] This is a deferred deep link")
		} else {
			NSLog("[AFSDK] This is a direct deep link")
		}
		
		guard let deeplinkValue = deepLinkObj.deeplinkValue,
			  let deeplinkUrl = URL(string: deeplinkValue),
			  let deepLink = parseDeepLink(from: deeplinkUrl)
		else
		{
			debugPrint("Could not extract deep_link_value from deep link object")
			return
		}
		
		debugPrint("deeplinkUrl: \(deeplinkUrl)")
		handleDeepLink(deepLink)
	}
}






// MARK: - Handling deeplinks

private extension DeepLinksManager
{
	func parseDeepLink(from url: URL) -> DeepLink?
	{
		guard
			let components = NSURLComponents(
				url: url,
				resolvingAgainstBaseURL: true
			),
			let params = components.queryItems
		else
		{
			return nil
		}
		
		dbgtrace("Parsing deep link: \(url)")
		
		//
		if let marketAlias = params
			.first(where: { $0.name == "alias" })?
			.value.warnIfNil(),
		   let sellerId = params
			.first(where: { $0.name == "seller" })?
			.value.warnIfNil()
		{
			return DeepLink.goToSeller(
				sellerId: sellerId,
				marketAlias: marketAlias
			)
		}
		
		return nil
	}
	
	func handleDeepLink(_ deepLink: DeepLink)
	{
		dbgout("\(deepLink)")
		
		deepLinkOpenedSubject.onNext(deepLink)
		
		switch deepLink
		{
		case .goToSeller(let sellerId, let marketAlias):
			RootStory.redirectFromTabBar(
				skipOnboarding: true
			)
			{
				tabBarCtrl in
				
				if let catalogTabVC = tabBarCtrl.switchToTab(.catalog).warnIfNil(),
				   let navigationController = (catalogTabVC as? UINavigationController).warnIfNil()
				{
					SellerStory.startFromDeeplink(
						on: navigationController,
						sellerId: sellerId,
						marketAlias: marketAlias
					)
				}
			}
		}
	}
}


// must be the same one as in Info.plist
private let customAppUrlScheme = "polkaonlineapp"
