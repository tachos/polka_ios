//
//  Managers.swift
//  Polka
//
//  Created by Makson on 27.04.2021.
//

import Foundation


private let localStoreManager = LocalStoreManager()


let managers = Managers(
	localStore: localStoreManager,
	webService:	WebServiceManager(localStoreManager),
	locationManager: LocationManager(),
	geoCodingManager: GeocodingManager(),
	chats: ChatsManager(),
	appEvents: AppEventsManager(),
	appsFlyer: AppsFlyerManager(),
	deepLinks: DeepLinksManager()
)

struct Managers
{
	let localStore: LocalStoreManager
	let webService: WebServiceManager
	let locationManager: LocationManager
	let geoCodingManager: GeocodingManager
	let chats: ChatsManager
	let appEvents: AppEventsManager
	let appsFlyer: AppsFlyerManager
	let deepLinks: DeepLinksManager
	
	func dispose()
	{
		chats.onManagersDisposed()
	}
}
