//
//  ChatsManager.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 23.06.2021.
//

import UIKit
import PromiseKit
import RxSwift



class ChatsManager
{
	// MARK: Data Fetching
	private var currentPage: WebServiceManager.Page?
	private var hasMoreChats = true
	private var chatsPromise: Promise<[Chat]>?
	private(set) var chats: [Chat] = []
	private(set) var unreadChatsCount = 0

	// MARK: Reactive X
	private let unreadChatsCountSubject = PublishSubject<Int>()
	private let disposeBag = DisposeBag()
}



// MARK: - Fetch Data

extension ChatsManager
{
	func loadMore(
		_ viewController: ChatsViewController,
		forceStartFromFirstPage: Bool = false
	)
	{
		if forceStartFromFirstPage
		{
			clearData()
		}
		
		// check if loading possible
		if !hasMoreChats
		{
			return
		}

		if let chatsPromise = chatsPromise,
		   chatsPromise.isPending
		{
			return
		}

		// get chats
		let nextPage = currentPage?.next() ?? .first()

		self.chatsPromise = managers.webService.getChats(page: nextPage)

		chatsPromise?
		.done
		{
			newChats in

			self.currentPage = nextPage

			if newChats.count < nextPage.limit
			{
				self.hasMoreChats = false
			}

			if !newChats.isEmpty
			{
				let indices = newChats.enumerated().map { self.chats.count + $0.offset }

				self.chats.append(contentsOf: newChats)
				viewController.insertChats(at: indices)
			}
		}
		.catch
		{
			error in

			viewController.showRetryAlert(onRetry:
			{
				[weak self, weak viewController] in

				guard let viewController = viewController else { return }

				self?.loadMore(viewController)
			})
		}
		.finally
		{
			self.updateUnreadChatsCount()
		}
	}

	func reset()
	{
		clearData()
		updateUnreadChatsCount()
	}

	private func clearData()
	{
		currentPage = nil
		chatsPromise = nil
		hasMoreChats = true
		chats.removeAll()
	}
}



// MARK: - On Disposed

extension ChatsManager
{
	func onManagersDisposed()
	{
		subscribeForSocketMessages()
	}
}



// MARK: - Socket Handling

private extension ChatsManager
{
	func subscribeForSocketMessages()
	{
		managers.webService.socketMessageObservable
			.subscribe(onNext: { [weak self] in self?.onSocketMessage($0) })
			.disposed(by: disposeBag)
	}

	func onSocketMessage(_ message: Message)
	{
		if !managers.webService.isAuthorized
		{
			return
		}

		if message.direction == .incoming
		{
			addUnreadMessage(message.authorId)
		}
	}
}



// MARK: - Unread Count

extension ChatsManager
{
	var unreadChatsCountObservable: Observable<Int>
	{
		return unreadChatsCountSubject
	}

	func unreadCount(_ receiverId: String) -> Int
	{
		if let chat = chats.first(where: { $0.receiver.id == receiverId })
		{
			return chat.unreadCount
		}

		return 0
	}

	func markAllRead(_ receiverId: String)
	{
		if let index = chats.firstIndex(where: { $0.receiver.id == receiverId })
		{
			chats[index] = chats[index].updated(unreadCount: 0)
			updateUnreadChatsCount()
		}
	}

	private func addUnreadMessage(_ receiverId: String)
	{
		if let index = chats.firstIndex(where: { $0.receiver.id == receiverId })
		{
			let chat = chats[index]

			chats[index] = chat.updated(unreadCount: chat.unreadCount + 1)
			updateUnreadChatsCount()
		}
	}

	private func updateUnreadChatsCount()
	{
		let oldCount = unreadChatsCount
		let newCount = Self.getUnreadChatsCount(chats)

		if newCount != oldCount
		{
			unreadChatsCount = newCount
			unreadChatsCountSubject.onNext(newCount)
		}
	}

	private static func getUnreadChatsCount(_ chats: [Chat]) -> Int
	{
		return chats.filter { $0.unreadCount > 0 }.count
	}
}
