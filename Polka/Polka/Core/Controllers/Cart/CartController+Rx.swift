//
//  CartController+RX.swift
//  Polka
//
//  Created by Michael Goremykin on 18.06.2021.
//

import RxSwift

extension CartController
{
	func subscribeToLocalCartEvents()
	{
		localChangesSubject.throttle(.seconds(Constants.throttleInterval),
									  scheduler: MainScheduler.instance)
			.subscribe({[weak self] _ in
			guard let self = self else { return }
			
			self.webService.updateCart(Cart.getCartDiff(self.lastSyncedCart, self.cart)).done{[weak self] cart in
				guard let self = self else { return }
				
				self.handleBackendResponse(sentCart: self.cart, result: .success(cart))
			}
			.catch
			{[weak self]  error in
				guard let self = self else { return }
				
				self.handleBackendResponse(sentCart: self.cart, result: .failure(error))
			}
		})
		.disposed(by: disposeBag)
	}
}
