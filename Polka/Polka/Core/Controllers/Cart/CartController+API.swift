//
//  CartController+API.swift
//  Polka
//
//  Created by Michael Goremykin on 25.05.2021.
//

import RxSwift
import PromiseKit


protocol CartControllerProtocol
{
	var cart: Cart { get }
	
	var cartStateObservable: Observable<CartState> { get }
	var cartObtainingObservable: Observable<Void> { get }
	
	func removeAllLocally()
	func removeAll()
	func removeAll(withSellerId sellerId: String)
	func remove(productWithId productId: String, fromSellerWithId sellerId: String)
	
	func set(quantity: Float,
			 forProduct product: Product,
			 stockQuantity: Float,
			 fromSeller seller: Seller)
	
	func synchronizeWithBackend()
}

// MARK:- CartControllerProtocol
extension CartController: CartControllerProtocol
{
	var cart: Cart
	{
		(try? cartStateSubject.value())?.cart ?? .empty
	}
	
	func removeAllLocally()
	{
		cartStateSubject.onNext(.local(.empty))
	}
	
	func removeAll()
	{		
		let emptyCart = Cart.empty
		
		firstly {
			managers.webService.removeAllProductsFromCart()
		}
		.done
		{
			self.cartStateSubject.onNext(.synced(emptyCart))
			
			self.handleBackendResponse(
				sentCart: emptyCart,
				result: .success(emptyCart)
			)
		}
		.catch { error in
			//
			self.handleBackendResponse(
				sentCart: emptyCart,
				result: .failure(error)
			)
		}
		
		
	}
	
	func removeAll(withSellerId sellerId: String)
	{
		update(cart.removingAll(withSellerId: sellerId))
	}
	
	func remove(productWithId productId: String, fromSellerWithId sellerId: String)
	{
		update(cart.removing(productWithId: productId, fromSellerWithId: sellerId))
	}
	
	func set(quantity: Float,
			 forProduct product: Product,
			 stockQuantity: Float,
			 fromSeller seller: Seller)
	{
		update(cart.setting(quantity: quantity,
							forProduct: product,
							stockQuantity: stockQuantity,
							fromSeller: seller))
	}
	
	func synchronizeWithBackend()
	{
		getCartContents()
	}
}


extension CartController
{
	func onProductQuantityChanged(
		newQuantity: Float,
		cartProduct: CartProduct,
		seller: Seller
	)
	{
		self.set(
			quantity: newQuantity,
			forProduct: cartProduct.product,
			stockQuantity: cartProduct.stockQuantity,
			fromSeller: seller
		)
	}
}


private extension CartController
{
	func update(_ cart: Cart)
	{
		cartStateSubject.onNext(.local(cart))
		
		localChangesSubject.onNext(())
	}
}
