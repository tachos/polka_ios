//
//  CartController+Utils.swift
//  Polka
//
//  Created by Michael Goremykin on 25.05.2021.
//

import Foundation

// MARK:- Cart utils
extension Cart
{
	func removingAll(withSellerId sellerId: String) -> Cart
	{
		//print("Removing seller from cart")
		
		return .init(marketIsOpen: self.marketIsOpen,
					 sellers: self.sellers.filter { $0.seller.id != sellerId },
					 deliveryInfo: self.deliveryInfo)
	}
	
	func removing(productWithId productId: String, fromSellerWithId sellerId: String) -> Cart
	{
		/*
		1 Remove productId for sellerId
		2 if seller has no product
			removingAll(withSellerId: sellerId)
			
		*/
		//print("Removing product from cart")
		
		if let cartSeller = getCartSeller(withId: sellerId)
		{
			if cartSeller.cartProducts.count == 1
			{
				return removingAll(withSellerId: sellerId)
			}
			else
			{
				let updatedSeller: CartSeller = .init(seller: cartSeller.seller,
													  cartProducts: cartSeller.cartProducts.filter{ $0.product.id != productId })
				
				return .init(marketIsOpen: self.marketIsOpen,
							 sellers: self.sellers.map{ cartSeller in
								if cartSeller.seller.id == updatedSeller.seller.id
								{
									return updatedSeller
								}
								else
								{
									return cartSeller
								}
							 },
							 deliveryInfo: self.deliveryInfo)
			}
		}
		
		return self
	}

	func setting(quantity: Float,
				 forProduct product: Product,
				 stockQuantity: Float,
				 fromSeller seller: Seller) -> Cart
	{
		if quantity == 0.0
		{
			return removing(productWithId: product.id, fromSellerWithId: seller.id)
		}
		else
		{
			return .init(marketIsOpen: self.marketIsOpen,
						 sellers: getPatchedCartSellers(currentCartSellers: self.sellers,
														withSeller: seller,
														withProduct: product,
														quantity: quantity,
														stockQuantity: stockQuantity),
						 deliveryInfo: self.deliveryInfo)
		}
	}
	
	private func getPatchedCartSellers(currentCartSellers: [CartSeller],
									   withSeller seller: Seller,
									   withProduct product: Product,
									   quantity: Float,
									   stockQuantity: Float) -> [CartSeller]

	{
		if currentCartSellers.map({ $0.seller.id }).contains(seller.id)
		{
			return currentCartSellers.map{
			   if seller.id == $0.seller.id
			   {
								return getCartSellerWithUpdatedProducts(
									forSeller: $0,
									quantity: quantity,
									forProduct: product,
									stockQuantity: stockQuantity
								)
			   }
			   else
			   {
				   return $0
			   }
			}
		}
		else
		{
			var ret = currentCartSellers
			
			ret.append(.init(seller: seller, cartProducts: [.init(product: product,
																  quantity: quantity,
																  stockQuantity: stockQuantity)]))
			
			return ret
		}
	}
	
	private func getCartSellerWithUpdatedProducts(forSeller cartSeller: CartSeller,
												  quantity: Float,
												  forProduct product: Product,
												  stockQuantity: Float) -> CartSeller
	{
		return .init(seller: cartSeller.seller,
					 cartProducts: getUpdatedCartProducts(cartSeller.cartProducts,
														  quantity: quantity,
														  forProduct: product,
														  stockQuantity: stockQuantity))
	}
	
	private func getUpdatedCartProducts(_ currentCartProducts: [CartProduct],
										quantity: Float,
										forProduct product: Product,
										stockQuantity: Float) -> [CartProduct]
	{
		if currentCartProducts.isEmpty
		{
			return [.init(product: product,
						  quantity: quantity,
						  stockQuantity: stockQuantity)]
		}
		else
		{
			if currentCartProducts.map({ $0.product.id }).contains(product.id)
			{
				return currentCartProducts.map { cartProduct in
					if cartProduct.product.id == product.id
					{
					   var ret = cartProduct
					   ret.quantity = quantity
					   
					   return ret
					}
					else
					{
						return cartProduct
					}
				}
			}
			else
			{
				var ret = currentCartProducts
				
				ret.append(.init(product: product, quantity: quantity, stockQuantity: stockQuantity))
				
				return ret
			}
		}
	}
	
	private func getCartSeller(withId sellerId: String) -> CartSeller?
	{
		return self.sellers.first{ $0.seller.id == sellerId }
	}
	
	private func getCartProduct(withId productId: String, fromSellerWithId sellerId: String? = nil) -> CartProduct?
	{
		if let sellerId = sellerId
		{
			return getCartSeller(withId: sellerId)?.cartProducts.first { $0.product.id == productId}
		}
		
		return self.sellers.map{ $0.cartProducts }.reduce([], +).first { $0.product.id == productId }
	}
}

enum CartState
{
	case local(Cart)
	case synced(Cart)
	case corrected(Cart, Error)
}

enum CartAction
{
	case initialization(Cart)
	case modifyingWithRemoteUpdate(Cart)
	case modifyingWithoutRemoteUpdate(Cart)
}

// MARK:- CartState API
extension CartState
{
	var cart: Cart
	{
		switch self
		{
		case let .local(cart), let .synced(cart), let .corrected(cart, _):
			   return cart
		}
	}
	
	func isInCorrectedState() -> Bool
	{
		switch self
		{
		case .corrected:
			return true
		
		case .local, .synced:
			return false
		}
	}
}

enum CartControllerError
{
	case quantityOutOffRange
}

// MARK:- Constants
extension CartController
{
	enum Constants
	{
		static let throttleInterval: Int = 2
	}
}
