//
//  CartController+Network.swift
//  Polka
//
//  Created by Michael Goremykin on 18.06.2021.
//

import PromiseKit

protocol CartWebServiceProtocol
{
	func getCartContents() -> Promise<Cart>
	func updateCart(_ cartDiff: CartDiff) -> Promise<Cart> // Отмапится в cartcontroller error
}

extension CartController
{
	func getCartContents()
	{
		// Cart snapshot instead sentCart
		//let sentCart = cart
		/*
		1 Запросить корзину: webService.getCart()
		2 handleBackendResponse(localCart: currentCart, backendCart, error)
		*/
		
		webService.getCartContents()
			.done
			{[weak self] cart in
				guard let self = self else { return }
				
				self.cartStateSubject.onNext(.synced(cart))
				
				self.lastSyncedCart = cart
			}
			.catch
			{[weak self] error in
				guard let self = self else { return }
				
				self.handleBackendResponse(sentCart: self.cart, result: .failure(error))
			}
	}
	
	func handleBackendResponse(sentCart: Cart, result: Swift.Result<Cart, Error>)
	{
		/*
		1 Если сервер вернул равный cart отправленному (или нет ошибок), то cartStateSubject.onNext(.synced(cart))
			1.1 Закэшировать удачный респонс в lastSyncedCart
		2 В противном случае
			2.1 Map promiseError -> cartControllerError
			2.2 cartStateSubject.onNext(.corrected(???, cartControllerErrors))
				2.2.1 Если сервер вернул неравный cart отправленному -- backendCart
				2.2.2 Если сервер вернул не 200 -- lastSyncedCart
		*/
		
		cartObtainingSubject.onNext(())
		
		switch result
		{
		case .success(let cart):
			lastSyncedCart = cart
			cartStateSubject.onNext(.synced(cart))
			
		case .failure(let error):
			dbgwarn("Failed to update cart: \(error)")
			cartStateSubject.onNext(.corrected(lastSyncedCart, error))
		}
	}
}
