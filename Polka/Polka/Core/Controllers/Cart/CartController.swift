//
//  CartController.swift
//  Polka
//
//  Created by Michael Goremykin on 10.05.2021.
//

import PromiseKit
import RxSwift

final class CartController
{	
	static private(set) var shared: CartController = CartController(webService: managers.webService,
													   throttleInterval: Constants.throttleInterval)
	
	private init(webService: CartWebServiceProtocol, throttleInterval: Int)
	{
		self.webService = webService
		
		subscribeToLocalCartEvents()
	}
	
	lazy var cartStateObservable: Observable<CartState> = { cartStateSubject.share() } ()
	lazy var cartObtainingObservable: Observable<Void> = { cartObtainingSubject.share() } ()
	
	let cartStateSubject: BehaviorSubject<CartState> = BehaviorSubject(value: .local(.empty))
	let cartObtainingSubject: PublishSubject<Void> = PublishSubject()
	let localChangesSubject: PublishSubject<Void> = PublishSubject()
	let webService: CartWebServiceProtocol
	
	let disposeBag = DisposeBag()
	
	// MARK:- Stateful
	var lastSyncedCart: Cart = .empty
}
