//
//  UserController.swift
//  Polka
//
//  Created by Michael Goremykin on 13.06.2021.
//

import RxSwift

protocol UserControllerProtocol
{
	var userLocalEventsObservable: Observable<UserEvent> { get }
	var currentDeliveryAddress: DeliveryAddresses? { get }
}

final class UserController: UserControllerProtocol
{
	static private(set) var shared: UserController = UserController()
	
	var currentDeliveryAddress: DeliveryAddresses?
	{
		managers.webService.deliveryAddress
	}
	
	lazy var userLocalEventsObservable: Observable<UserEvent> = { userLocalEventsSubject.share() } ()
	
	private let userLocalEventsSubject: PublishSubject<UserEvent> = PublishSubject()
}

// MARK:- AAAAA TOREMOVE
extension UserController
{
	func onNewDeliveryAddressObtained(_ newAddress: DeliveryAddresses)
	{
		userLocalEventsSubject.onNext(.newDeliveryAddressHasBeenSet(newAddress))
	}
}

enum UserEvent
{
	case newDeliveryAddressHasBeenSet(DeliveryAddresses)
}
