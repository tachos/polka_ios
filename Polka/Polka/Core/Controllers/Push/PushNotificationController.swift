//
//  PushNotificationController.swift
//  Polka
//
//  Created by Michael Goremykin on 08.07.2021.
//

import Foundation
import FirebaseMessaging

protocol PushNotificationClient
{
	static func registerDevice(deviceToken: String, onFailureCallback: @escaping (Error) -> Void)
	static func registerDeviceIfAuthorized(deviceToken: String, onFailureCallback: @escaping (Error) -> Void)
}

class PushNotificationController
{
	
}

extension PushNotificationController: PushNotificationClient
{
	static func registerDevice(deviceToken: String, onFailureCallback: @escaping (Error) -> Void)
	{
		managers.webService.registerDevice(deviceToken)
				.catch{ error in
					onFailureCallback(error)
				}
	}
	
	static func registerDeviceIfAuthorized(deviceToken: String, onFailureCallback: @escaping (Error) -> Void)
	{
		if managers.webService.currentUserProfile != nil
		{
			registerDevice(deviceToken: deviceToken, onFailureCallback: onFailureCallback)
		}
	}
}
