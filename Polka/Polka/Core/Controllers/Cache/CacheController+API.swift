//
//  CacheController+API.swift
//  Polka
//
//  Created by Michael Goremykin on 24.06.2021.
//

import RealmSwift

protocol CacheControllerProtocol
{
	func getCachedCategories() -> [Category]
	func removeAllCachedCategories()
	func setCachedCategories(_ categories: [Category], _ onSuccess: (() -> Void)?)
}

extension CacheController: CacheControllerProtocol
{
	func getCachedCategories() -> [Category]
	{
		do
		{
			let realm = try Realm()
			
			if let list = realm.objects(CachedCategories.self).first?.list
			{
				return Array(list).map{ $0.category }
			}
		}
		catch
		{
			let error = error as NSError
			
			print("Error occurs while obtaining categories \n\(error)")
		}
		
		return []
	}
	
	func setCachedCategories(_ categories: [Category], _ onSuccess: (() -> Void)? = nil)
	{
		removeAllCachedCategories()
		
		do
		{
			let realm = try Realm()
			
			try realm.write {
				realm.add(CachedCategories(categories))
			}
		}
		catch
		{
			let error = error as NSError
			
			print("Error occurs while saving categories \n\(error)")
		}

	}
	
	func removeAllCachedCategories()
	{
		do
		{
			let realm = try Realm()
			
			try realm.write
			{
				realm.delete(realm.objects(CachedCategories.self))
			}
		}
		catch
		{
			let error = error as NSError
			
			print("Error occurs while removing all categories \n\(error)")
		}
	}
}
