//
//  +SellerGalleryItemKind.swift
//  Polka
//
//  Created by Michael Goremykin on 30.06.2021.
//

import RealmSwift

extension SellerGalleryItemKind
{
	var cachedSellerGalleryItemKind: CachedSellerGalleryItemKind
	{
		switch self
		{
		case .image(let url), .video(let url):
			return CachedSellerGalleryItemKind(kind: self.toString(), contentUrl: url)
		}
	}
}
	
extension CachedSellerGalleryItemKind
{
	var sellerGalleryItemKind: SellerGalleryItemKind?
	{
		guard let contentUrl = URL(string: self.rawContentUrl ?? "") else { return nil }
		
		switch self.kind.lowercased()
		{
		case "image":
			return .image(contentUrl)
			
		case "video":
			return .video(contentUrl)
			
		default:
			return nil
		}
	}
}

extension SellerGalleryItemKind
{
	func toString() -> String
	{
		switch self
		{
		case .image:
			return "image"
			
		case .video:
			return "video"
		}
	}
}

