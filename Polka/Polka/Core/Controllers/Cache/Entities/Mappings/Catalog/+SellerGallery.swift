//
//  +SellerGallery.swift
//  Polka
//
//  Created by Michael Goremykin on 29.06.2021.
//

import Foundation

extension SellerGallery
{
	var cachedSellerGallery: CachedSellerGallery
	{
		CachedSellerGallery(items: self.items.map {
			CachedSellerGalleryItem(id: $0.id, kind: $0.kind.cachedSellerGalleryItemKind)
		})
	}
}

extension CachedSellerGallery
{
	var sellerGallery: SellerGallery
	{
		let raw: [(String, SellerGalleryItemKind)] = list.map{ ($0.id, $0.kind.sellerGalleryItemKind) }.compactMap{
			guard let id = $0.0, let sellerGalleryItemKind: SellerGalleryItemKind = $0.1 else { return nil}
			
			return (id, sellerGalleryItemKind)
		}
		
		return .init(items: raw.map{ .init(id: $0.0, kind: $0.1) })
	}
}
