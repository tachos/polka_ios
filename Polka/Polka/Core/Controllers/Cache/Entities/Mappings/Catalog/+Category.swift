//
//  +Category.swift
//  Polka
//
//  Created by Michael Goremykin on 29.06.2021.
//

import Foundation

extension Category
{
	var cachedCategory: CachedCategory
	{
		.init(id: self.id,
			  alias: self.alias,
			  imageUrl: self.imageUrl,
			  caption: self.caption,
			  description: self.description,
			  cachedSellers: self.sellers.map{ $0.cachedSeller } )
	}
}

extension CachedCategory
{
	var category: Category
	{
		.init(id: self.id,
			  alias: self.alias,
			  imageUrl: URL(string: self.rawImageUrl ?? ""),
			  caption: self.caption,
			  description: self.categoryDescription,
			  sellers: self.cachedSellersList.map { $0.seller })
	}
}
