//
//  +Seller.swift
//  Polka
//
//  Created by Michael Goremykin on 29.06.2021.
//

import Foundation

extension Seller
{
	var cachedSeller: CachedSeller
	{
		.init(id: self.id, cachedSellerInfo: self.info.cachedSellerInfo)
	}
}

extension CachedSeller
{
	var seller: Seller
	{
		.init(id: self.id, info: self.cachedSellerInfo.sellerInfo)
	}
}
