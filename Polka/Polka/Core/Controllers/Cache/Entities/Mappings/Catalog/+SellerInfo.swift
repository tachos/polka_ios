//
//  +SellerInfo.swift
//  Polka
//
//  Created by Michael Goremykin on 29.06.2021.
//

import Foundation

extension SellerInfo
{
	var cachedSellerInfo: CachedSellerInfo
	{
		.init(caption: self.caption,
			  description: self.description,
			  profileImageUrl: self.profileImageUrl,
			  cachedSellerGallery: self.sellerGallery.cachedSellerGallery,
			  isOpen: self.isOpen,
			  isPriceActual: self.isPriceActual)
	}
}

extension CachedSellerInfo
{
	var sellerInfo: SellerInfo
	{
		.init(caption: self.caption,
			  description: self.sellerDescription,
			  profileImageUrl: URL(string: self.rawProfileImageUrl ?? ""),
			  sellerGallery: self.sellerGallery.sellerGallery,
			  isOpen: self.isOpen,
			  isPriceActual: self.isPriceActual)
	}
}
