//
//  CachedCategories.swift
//  Polka
//
//  Created by Michael Goremykin on 29.06.2021.
//

import RealmSwift

class CachedCategories: Object
{
	var list = List<CachedCategory>()
	
	convenience init(_ categories: [Category])
	{
		self.init()
		
		categories.map{ $0.cachedCategory }.forEach{ list.append($0) }
	}
}
