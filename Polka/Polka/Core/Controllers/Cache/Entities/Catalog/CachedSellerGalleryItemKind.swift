//
//  CachedSellerGalleryItemKind.swift
//  Polka
//
//  Created by Michael Goremykin on 30.06.2021.
//

import RealmSwift

class CachedSellerGalleryItemKind: Object
{
	@objc dynamic var kind: String!
	@objc dynamic var rawContentUrl: String!
	
	convenience init(kind: String, contentUrl: URL)
	{
		self.init()
		
		self.kind 			= kind
		self.rawContentUrl 	= contentUrl.absoluteString
	}
}
