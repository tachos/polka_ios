//
//  CachedCategory.swift
//  Polka
//
//  Created by Michael Goremykin on 29.06.2021.
//

import RealmSwift

class CachedCategory: Object
{
	@objc dynamic var id: 					String!
	@objc dynamic var alias: 				String!
	@objc dynamic var rawImageUrl:			String!
	@objc dynamic var caption: 				String!
	@objc dynamic var categoryDescription:	String!
	var cachedSellersList = List<CachedSeller>()
	
	convenience init(id: String,
		 alias: MarketAlias,
		 imageUrl: URL?,
		 caption: String,
		 description: String,
		 cachedSellers: [CachedSeller])
	{
		self.init()
		
		self.id = id
		self.alias = alias
		self.rawImageUrl = imageUrl?.absoluteString ?? ""
		self.caption = caption
		self.categoryDescription = description
		cachedSellers.forEach{ cachedSellersList.append($0) }
	}
}
