//
//  CachedSeller.swift
//  Polka
//
//  Created by Michael Goremykin on 29.06.2021.
//

import RealmSwift

class CachedSeller: Object
{
	@objc dynamic var id:			String!
	@objc dynamic var cachedSellerInfo:	CachedSellerInfo!
	
	convenience init(id: String, cachedSellerInfo: CachedSellerInfo)
	{
		self.init()
		
		self.id = id
		self.cachedSellerInfo = cachedSellerInfo
	}
}
