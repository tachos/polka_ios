//
//  CachedSellerGallery.swift
//  Polka
//
//  Created by Michael Goremykin on 29.06.2021.
//

import RealmSwift

class CachedSellerGallery: Object
{
	static let empty = CachedSellerGallery(items: [])
	
	var list = List<CachedSellerGalleryItem>()
	
	convenience init(items: [CachedSellerGalleryItem])
	{
		self.init()
		
		items.forEach { list.append($0) }
	}
}
