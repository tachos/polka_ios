//
//  CachedSellerInfo.swift
//  Polka
//
//  Created by Michael Goremykin on 29.06.2021.
//

import RealmSwift

class CachedSellerInfo: Object
{
	@objc dynamic var caption: 				String!
	@objc dynamic var sellerDescription:	String!
	@objc dynamic var rawProfileImageUrl:	String!
	@objc dynamic var sellerGallery: 		CachedSellerGallery! = .empty
	@objc dynamic var isOpen:				Bool = false
	@objc dynamic var isPriceActual:		Bool = false
	
	convenience init(caption: String,
		 description: String,
		 profileImageUrl: URL?,
		 cachedSellerGallery: CachedSellerGallery,
		 isOpen: Bool,
		 isPriceActual: Bool)
	{
		self.init()
		
		self.caption = caption
		self.sellerDescription = description
		self.rawProfileImageUrl = profileImageUrl?.absoluteString ?? ""
		self.sellerGallery = cachedSellerGallery
		self.isOpen = isOpen
		self.isPriceActual = isPriceActual
	}
}
