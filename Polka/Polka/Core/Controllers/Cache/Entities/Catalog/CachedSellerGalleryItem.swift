//
//  CachedSellerGalleryItem.swift
//  Polka
//
//  Created by Michael Goremykin on 30.06.2021.
//

import RealmSwift

class CachedSellerGalleryItem: Object
{
	@objc dynamic var id:	String!
	@objc dynamic var kind: CachedSellerGalleryItemKind!
	
	convenience init(id: String, kind: CachedSellerGalleryItemKind)
	{
		self.init()
		
		self.id 	= id
		self.kind 	= kind
	}
}
