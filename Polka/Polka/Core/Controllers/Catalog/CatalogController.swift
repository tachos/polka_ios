//
//  CatalogController.swift
//  Polka
//
//  Created by Michael Goremykin on 30.06.2021.
//

import RxSwift
import PromiseKit

enum CategoriesState
{
	case local([Category])
	case synced([Category])
	case notObtained
}

extension CategoriesState
{
	func isNotSynced() -> Bool
	{
		switch self
		{
		case .notObtained, .local:
			return true
		default:
			return false
		}
	}
}


final class CatalogController
{
	static private(set) var shared: CatalogController = CatalogController(CacheController.shared)
	
	private init(_ cacheController: CacheControllerProtocol)
	{
		self.cacheController = cacheController
	}
	
	lazy var categoriesStateObservable: Observable<CategoriesState> = { categoriesStateSubject.share() } ()
	
	lazy var categoriesStateSubject: BehaviorSubject<CategoriesState> = { BehaviorSubject(value: .local(cacheController.getCachedCategories())) }()
	
	var cacheController: CacheControllerProtocol!
}

extension CatalogController
{
	var categoriesState: CategoriesState
	{
		(try? categoriesStateSubject.value()) ?? .notObtained
	}
	
	var cachedCategories: [Category]
	{
		cacheController.getCachedCategories()
	}
	
	func loadProductCategories(
		onFailure: (() -> Void)? = nil,
		completion: VoidCallback? = nil,
		cancel: UnsafeMutablePointer<AlamofireUtils.Cancel?>? = nil
	)
	{
		guard let currentMarket = managers.localStore.currentMarket
		else {
			dbgUnreachable()
			completion?()
			return
		}
		
		firstly {
			managers.webService.getCategories(
				marketAlias: currentMarket.alias,
				cancel: cancel
			)
		}
		.done
		{[weak self] categories  in
			//
			dbgout("Market alias=\(currentMarket): loaded \(categories.count) categories.")

			self?.handleResponse(categories)
		}
		.catch
		{ error in
			onFailure?()
		}
		.finally {
			//
			completion?()
		}
	}
}

extension CatalogController
{
	private func handleResponse(_ categories: [Category])
	{
		dbgtrace()
		cacheController.setCachedCategories(categories, nil)
		
		categoriesStateSubject.onNext(.synced(categories))
	}
}

extension CategoriesState
{
	var value: [Category]
	{
		switch self
		{
		case .synced(let categories), .local(let categories):
			return categories
		
		case .notObtained:
			return []
		}
	}
}

fileprivate extension Promise
{
	func onResolved(_ onResolvedCallback: @escaping () -> Void) -> Promise
	{
		return ensure {
			onResolvedCallback()
		}
	}
}
