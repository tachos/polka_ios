//
//  SellerMoreDetailsOrderViewController+UI.swift
//  Polka
//
//  Created by Makson on 17.06.2021.
//

import UIKit


extension SellerMoreDetailsOrderViewController
{
	func setupUI()
	{
		setupBackgroundColor()
		setupCloseButton()
		createOrdersContentsTableView()
		hideKeyboardWhenTappedAround()
		subscribeToKeyboardEvents()
		checkStatusOrder()
	}
}

// MARK:- UI utils
extension SellerMoreDetailsOrderViewController
{
	func setupBackgroundColor()
	{
		self.view.backgroundColor = .white
	}
	
	func hideKeyboardWhenTappedAround()
	{
		let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
		tap.cancelsTouchesInView = false
		view.addGestureRecognizer(tap)
	}
	
	@objc func dismissKeyboard()
	{
		view.endEditing(true)
		//subOrderContentsTableView.resignFirstResponder()
	}
}
// MARK:- UI close button utils
extension SellerMoreDetailsOrderViewController
{
	func setupCloseButton()
	{
		embedCloseButton(createCloseButton())
	}
	
	func createCloseButton() -> UIButton
	{
		let closeButton = UIButton()
		
		closeButton.addTarget(self, action: #selector(onCloseTapped), for: .touchUpInside)
		
		closeButton.setImage(UIImage(named: "close") ?? UIImage(), for: [])
		closeButton.imageView?.contentMode = .center
		
		return closeButton
	}
	
	func checkStatusOrder()
	{
		switch subOrderInfo.state
		{
			case .assembling:
				confirmBtn.isHidden = false
				
			case .canceled, .assembled,.created:
				confirmBtn.isHidden = true
		}
	}
	
	func embedCloseButton(_ buttonToEmbed: UIButton)
	{
		self.view.addSubview(buttonToEmbed)
		
		buttonToEmbed.translatesAutoresizingMaskIntoConstraints = false
		
		NSLayoutConstraint.activate([buttonToEmbed.heightAnchor.constraint(equalToConstant: Constants.closeButtonDimension),
									 buttonToEmbed.widthAnchor.constraint(equalToConstant: Constants.closeButtonDimension),
									 buttonToEmbed.topAnchor.constraint(equalTo: self.view.topAnchor,
																		constant: Constants.closeButtonPadding),
									 buttonToEmbed.trailingAnchor.constraint(equalTo: self.view.trailingAnchor,
																			 constant: -Constants.closeButtonPadding)])
	}
}

extension SellerMoreDetailsOrderViewController
{
	func createOrdersContentsTableView()
	{
		subOrderContentsTableView.registerNibCell(SubOrderHeaderView.self)
		subOrderContentsTableView.registerNibCell(SellerMoreDetailsOrderTableViewCell.self)
	}
}
//MARK: - UITableViewDelegate,UITableViewDataSource

extension SellerMoreDetailsOrderViewController:UITableViewDelegate,
											   UITableViewDataSource
{
	
	func numberOfSections(in tableView: UITableView) -> Int
	{
		return Constants.countSection
	}
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		if section == 0
		{
			return Constants.countItemInOneSection
		}
		else
		{
			return subOrderInfo.products.count
		}
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
	{
		if indexPath.section == 0
		{
			return SubOrderHeaderView.Constants.height
		}
		else
		{
			return Constants.heightCell
		}
	}
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		if indexPath.section == 0
		{
			guard let subOrderHeaderCell = tableView.dequeueReusableCell(ofType: SubOrderHeaderView.self) else {
				return UITableViewCell()
			}
			subOrderHeaderCell.initialize(subOrderInfo)
			return subOrderHeaderCell
		}
		else
		{
		guard let subOrderProductCell = tableView.dequeueReusableCell(ofType: SellerMoreDetailsOrderTableViewCell.self) else {
			return UITableViewCell()
		}
			subOrderProductCell.setup(state: subOrderInfo.state,
									  sellerDefinedCount: assembleProducts[indexPath.row].count,
									  subOrderProduct:subOrderInfo.products[indexPath.row],
									  onCount: {[weak self] count in
										self?.updateCountProduct(index: indexPath.row, count: count)
									  })
		return subOrderProductCell
		}
	}
}


// MARK:- Close button handler
extension SellerMoreDetailsOrderViewController
{
	@objc func onCloseTapped()
	{
		self.dismiss(animated: true, completion: { [weak sellerMoreDetails] in
						sellerMoreDetails?.viewWillAppear(true)})
	}

}

// MARK:- Constants
extension SellerMoreDetailsOrderViewController
{
	enum Constants
	{
		static let tableViewTopPadding = CGFloat(32)
		static let closeButtonPadding = CGFloat(11)
		static let closeButtonDimension = CGFloat(24)
		static let heightCell = CGFloat(78)
		static let countItemInOneSection = 1
		static let countSection = 2
	}
}
