//
//  SellerMoreDetailsOrderViewController.swift
//  Polka
//
//  Created by Makson on 17.06.2021.
//

import UIKit

class SellerMoreDetailsOrderViewController: UIViewController,StoryboardCreatable
{
	static var storyboardName: StoryboardName = .MoreDetailsOrder
	
	// MARK:- Outlets
    
    @IBOutlet weak var confirmBtn: CustomButton!
    @IBOutlet weak var containerToSafeAreaBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var subOrderContentsTableView: UITableView!
    //MARK: - Variable
	var subOrderInfo:SubOrderInfo!
	var assembleProducts:[AssembleProduct] = []
	var sellerMoreDetails:UIViewController!
	deinit
	{
		unsubscribeFromKeyboardEvents()
	}
}
extension SellerMoreDetailsOrderViewController
{
	
	func subscribeToKeyboardEvents()
	{
		NotificationCenter.default.addObserver(self,
											   selector: #selector(keyboardWillShow),
											   name: UIResponder.keyboardWillShowNotification,
											   object: nil)
		
		NotificationCenter.default.addObserver(self,
											   selector: #selector(keyboardWillHide),
											   name: UIResponder.keyboardWillHideNotification,
											   object: nil)
	}
	
	func unsubscribeFromKeyboardEvents()
	{
		NotificationCenter.default.removeObserver(self,
												  name: UIResponder.keyboardWillShowNotification,
												  object: nil)
		
		NotificationCenter.default.removeObserver(self,
												  name: UIResponder.keyboardWillHideNotification,
												  object: nil)
	}
	
	@objc func keyboardWillShow(notification: NSNotification)
	{
		guard let userInfo = notification.userInfo else {return}
		guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
		let keyboardFrame = keyboardSize.cgRectValue
		
		let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardFrame.height, right: 0.0);
		subOrderContentsTableView.contentInset = contentInsets
		
		
		//view.animateLayoutChanges()
	}
	
	@objc func keyboardWillHide(notification: NSNotification)
	{
		let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom:0.0, right: 0.0);
		subOrderContentsTableView.contentInset = contentInsets

		//view.animateLayoutChanges()
	}
	
	@IBAction func confirmBtnAction()
	{
		managers.webService.subOrderAssemble(subOderId: subOrderInfo.id, products: assembleProducts)
			.get
			{   subOrderInfo in
				self.subOrderInfo = subOrderInfo
				self.subOrderContentsTableView.reloadData()
				self.checkStatusOrder()
			}
			.catch
			{ _ in
				print("Ошибка")
			}
	}
}

extension SellerMoreDetailsOrderViewController
{
	func updateCountProduct(index:Int, count:Float)
	{
		assembleProducts[index].count = count
	}
	
	func updateAssembleProduct(products:[OrderProduct])
	{
		assembleProducts = []
		
		for i in 0 ..< products.count
		{
			let stockProduct = AssembleProduct(id: products[i].product.id,
											  count: products[i].quantityPlacedInOrder)
			assembleProducts.append(stockProduct)
		}
		
	}
}
// MARK:- Initialization
extension SellerMoreDetailsOrderViewController
{
	func initialize(_ subOrderInfo: SubOrderInfo, viewController: UIViewController)
	{
		self.subOrderInfo = subOrderInfo
		self.sellerMoreDetails = viewController
		updateAssembleProduct(products: subOrderInfo.products)
	}
}

//MARK: - Lifecycle
extension SellerMoreDetailsOrderViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		setupUI()
	}
}
