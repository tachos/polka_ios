//
//  SellerMoreDetailsOrderTableViewCell.swift
//  Polka
//
//  Created by Makson on 17.06.2021.
//

import UIKit
import SDWebImage


class SellerMoreDetailsOrderTableViewCell: UITableViewCell
{
    //MARK: - Outlet
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var nameProductLbl: UILabel!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var countLbl: UILabel!
    @IBOutlet weak var unitLbl: UILabel!
    
    @IBOutlet weak var editableCountView: UIView!
    @IBOutlet weak var editableCountTextField: TachosTextField!
    @IBOutlet weak var editableUnitLabel: UILabel!
    @IBOutlet weak var editableCountErrorLabel: UILabel!
    
	private var containsWrongCount: Bool = false
	
    //MARK: - Variable
	private var onCount: CountCallback!
	private var count:Float = 0.0
	private var subOrderProduct:OrderProduct!
	private var stockAvailability: StockAvailability!
	
}

extension SellerMoreDetailsOrderTableViewCell
{
	override func awakeFromNib()
	{
		super.awakeFromNib()
		
		setupGestureRecognizerForCountEditContainer()
	}
}

extension SellerMoreDetailsOrderTableViewCell
{
	func setup(state:SubOrderState,
			   sellerDefinedCount: Float,
			   subOrderProduct:OrderProduct,
			   onCount:@escaping CountCallback)
	{
		productImage.sd_setImage(with: subOrderProduct.product.imageUrl, placeholderImage: UIImage(named: "smile"), options: [])
		nameProductLbl.text = subOrderProduct.product.name
		self.count = subOrderProduct.quantityPlacedInOrder
		self.subOrderProduct = subOrderProduct
		self.onCount = onCount
		self.stockAvailability = StockAvailability.getStock(forProduct: subOrderProduct.product,
															amountInCart: subOrderProduct.quantityPlacedInOrder,
															stockAmount: subOrderProduct.quantityPlacedInOrder)
		checkStatus(state: state)
		setCostumerDefinedQuantity(state, sellerDefinedCount)
		setupCountTextField()
		setupUnitMeasurementLabels()
	}
	
	func checkStatus(state:SubOrderState)
	{
		switch state
		{
		case .assembling:
            editableCountView.isHidden = false
			countView.isHidden = true
            
		case .canceled, .assembled,.created:
            editableCountErrorLabel.isHidden = true
            editableCountView.isHidden = true
			countView.isHidden = false
			countLbl.text = QuantityFormatter.cartItemQuantityWithoutUnit(count, subOrderProduct.product.unit)
			unitLbl.text = subOrderProduct.product.unit.toString()
		}
	}
}

extension SellerMoreDetailsOrderTableViewCell
{
	func setCostumerDefinedQuantity(_ state:SubOrderState, _ sellerDefinedCount: Float)
	{
		switch state
		{
		case .assembling:
			editableCountTextField.text = QuantityFormatter.cartItemQuantityWithoutUnit(sellerDefinedCount, subOrderProduct.product.unit)
			
		case .assembled,.created:
			countLbl.text = QuantityFormatter.cartItemQuantityWithoutUnit(subOrderProduct.finalQuantityInAssembledOrder ?? 0, subOrderProduct.product.unit)
		case .canceled:
			countLbl.text = QuantityFormatter.cartItemQuantityWithoutUnit(subOrderProduct.quantityPlacedInOrder, subOrderProduct.product.unit)
		}
	}
	
	func setupUnitMeasurementLabels()
	{
		[unitLbl, editableUnitLabel].forEach{ $0?.text = subOrderProduct.product.unit.toString() }
	}
}

private extension SellerMoreDetailsOrderTableViewCell
{
	private func setupGestureRecognizerForCountEditContainer()
	{
		let tap = UITapGestureRecognizer(target: self, action: #selector(onContainerTapped))
		editableCountView.addGestureRecognizer(tap)
	}
	
	@objc private func onContainerTapped(_ sender: UITapGestureRecognizer? = nil)
	{
		editableCountTextField.becomeFirstResponder()
		
		editableCountTextField.setCursorLocation(editableCountTextField.text?.count ?? 0)
	}
}

extension SellerMoreDetailsOrderTableViewCell: UITextFieldDelegate
{
	func setupCountTextField()
	{
		editableCountTextField.delegate = self
		editableCountTextField.addTarget(self, action: #selector(onCountEditingChanged), for: .editingChanged)
		editableCountTextField.addTarget(self, action: #selector(onEditableCountTextFieldTouchDown), for: .touchDown)
		editableCountTextField.addTarget(self, action: #selector(onCountFinishedEditing), for: .editingDidEnd)
	}
	
	@objc private func onEditableCountTextFieldTouchDown()
	{
		editableCountTextField.setCursorLocation(editableCountTextField.text?.count ?? 0)
	}
	
	@objc private func onCountFinishedEditing()
	{
		if let rawNewVal = editableCountTextField.text
		{
			let newVal = (rawNewVal.replacingOccurrences(of: ",", with: ".") as NSString).floatValue
			if newVal == 0 && !rawNewVal.isEmpty
			{
				editableCountTextField.text = "0"
				onCount(0)
			}
			else if newVal < subOrderProduct.quantityPlacedInOrder && !rawNewVal.isEmpty
			{
				onCount(newVal)
			}
			else
			{
				setToCustomerDefinedQuantity()
			}
		}
	}
	
	@objc private func onCountEditingChanged()
	{
		if let rawNewVal = editableCountTextField.text
		{
			if !rawNewVal.contains(",")
			{
				sanitiseForIntegerInput(rawNewVal)
			}
			else
			{
				sanitiseForFloatInput(rawNewVal)
			}
		}
	}
	
	func sanitiseForIntegerInput(_ rawNewVal: String)
	{
		let newValue = (rawNewVal as NSString).floatValue
		
		if newValue == 0 && !rawNewVal.isEmpty
		{
			editableCountTextField.text = "0"
		}
		else if newValue < subOrderProduct.quantityPlacedInOrder
		{
			editableCountTextField.text = rawNewVal
		}
		else
		{
			setToCustomerDefinedQuantity()
		}
	}
		
	func sanitiseForFloatInput(_ rawNewVal: String)
	{
		let withoutExtraCommas = rawNewVal.removeAllOccurrencesExceptFirst(",")
		let newVal = (withoutExtraCommas.replacingOccurrences(of: ",", with: ".") as NSString).floatValue
		
		if newVal < subOrderProduct.quantityPlacedInOrder && !rawNewVal.isEmpty
		{
			editableCountTextField.text = QuantityFormatter.forOrderQuantityConfirmation(withoutExtraCommas)
		}
		else
		{
			setToCustomerDefinedQuantity()
		}
	}
	
	private func setToCustomerDefinedQuantity()
	{
		editableCountTextField.text = QuantityFormatter.orderItemQuantity(subOrderProduct.quantityPlacedInOrder, subOrderProduct.product.unit)
	}
}

extension UITextField {
	
	func setCursorLocation(_ location: Int) {
		guard let cursorLocation = position(from: beginningOfDocument, offset: location) else { return }
		DispatchQueue.main.async { [weak self] in
			guard let strongSelf = self else { return }
			strongSelf.selectedTextRange = strongSelf.textRange(from: cursorLocation, to: cursorLocation)
		}
	}
	
}


extension String
{
	func removeAllOccurrencesExceptFirst(_ separator: String) -> String
	{
		let groups = self.components(separatedBy: separator)
		
		if groups.count > 1 {
			return groups[0] + separator + groups.dropFirst().joined()
		}
		else
		{
			return ""
		}
	}
}

class TachosTextField: UITextField
{
	override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool
	{
		if action == #selector(UIResponderStandardEditActions.paste(_:))
		{
			return false
		}
		
		return super.canPerformAction(action, withSender: sender)
	}
}

func valueCheck(_ d: Double) -> Double {
	var result = 0.0
	do {
		let regex = try NSRegularExpression(pattern: "^(?:|0|[1-9]\\d*)(?:\\.\\d*)?$", options: [])
		let results = regex.matches(in: String(d), options: [], range: NSMakeRange(0, String(d).count))
		if results.count > 0 {result = d}
	} catch let error as NSError {
		print("invalid regex: \(error.localizedDescription)")
	}
	return result
}
