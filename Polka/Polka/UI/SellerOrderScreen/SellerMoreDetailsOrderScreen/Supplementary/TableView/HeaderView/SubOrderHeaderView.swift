//
//  SubOrderHeaderView.swift
//  Polka
//
//  Created by Makson on 17.06.2021.
//

import UIKit

class SubOrderHeaderView: UITableViewCell
{
	//MARK: - 
    @IBOutlet weak var numberLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
}

extension SubOrderHeaderView
{
	func initialize(_ subOrderState: SubOrderInfo)
	{
		numberLbl.text = "№ \(subOrderState.orderId)"
		
		statusLbl.textColor = subOrderState.state.getLabelColor()
		statusLbl.text = subOrderState.state.toString()
	}
}


// MARK:- Constants
extension SubOrderHeaderView
{
	enum Constants
	{
		static let height = CGFloat(142)
	}
}
