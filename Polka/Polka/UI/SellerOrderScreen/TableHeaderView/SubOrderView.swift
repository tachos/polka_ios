//
//  SubOrderView.swift
//  Polka
//
//  Created by Makson on 16.06.2021.
//

import UIKit

class SubOrderView: UIView
{
	override func awakeFromNib()
	{
		super.awakeFromNib()
	}
}
extension SubOrderView
{
	static func create() -> SubOrderView
	{
		let subOrdersHeaderView = createView(SubOrderView.self)
		
	
		return subOrdersHeaderView
	}
}

extension SubOrderView
{
	enum Constants
	{
		static let height = CGFloat(112)
		static let horizontalSpacing = CGFloat(16)
		static let addressViewHeight = CGFloat(56)
	}
}
