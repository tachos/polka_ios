//
//  SellerOrderScreenViewController.swift
//  Polka
//
//  Created by Makson on 31.05.2021.
//

import UIKit
import PromiseKit

class SellerOrderScreenViewController: UIViewController,StoryboardCreatable
{
	//MARK: - StoryboardCreatable
	static var storyboardName: StoryboardName = .sellerOrderScreen
	
	// MARK:- Outlets
	weak var subOrdersTableView: UITableView!
	weak var emptyOrdersListView: EmptyOrdersListView!

	
    struct Config
	{
		let helper: SellerOrderHelper
	}
	var config:Config!
	
	// MARK:- Parameters
	let ordersPerPageCount: Int = 6
	let fetchThreshold: Double = 2.0
	
	// MARK:- Stateful
	var subOrderArray: [SubOrderInfo] = []
	var backendOrdersCount = 0
	var navBarVisibilityToggled = false
	var initialOffset: CGFloat = 0
	var lastContentOffset = CGFloat(0)
	var isAlreadyFetching = false
	
}

//MARK: - Lifecycle
extension SellerOrderScreenViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		setupUI()
	}
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(true)
		setupNavBarAppearance()
		showEmptyOrdersViewIfNeeded()
		
		getSubOrderInfo(ordersPerPageCount: ordersPerPageCount,offset:0)
	}
}

extension SellerOrderScreenViewController
{
	func setupUI()
	{
		self.view.backgroundColor = .white
		
		setupOrdersTableView()
		createAmEmbedEmptyOrdersView()
		
		self.view.bringSubviewToFront(subOrdersTableView)
	}
	
	func setupOrdersTableView()
	{
		let ordersTableView = createOrdersTabelView()
		
		embedOrdersTabelView(ordersTableView)
		
		self.subOrdersTableView =  ordersTableView
	}
	
	func createAmEmbedEmptyOrdersView()
	{
		let emptyListView = createView(EmptyOrdersListView.self)
		
		self.view.addSubview(emptyListView)
		
		emptyListView.translatesAutoresizingMaskIntoConstraints = false
		
		NSLayoutConstraint.activate([emptyListView.topAnchor.constraint(equalTo: self.view.topAnchor),
				 emptyListView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
				 emptyListView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
				 emptyListView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)])
		
		self.emptyOrdersListView = emptyListView
	}
	
	func embedOrdersTabelView(_ tableViewToEmbed: UITableView)
	{
		self.view.addSubview(tableViewToEmbed)
		
		tableViewToEmbed.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [tableViewToEmbed.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
						   tableViewToEmbed.topAnchor.constraint(equalTo: self.view.topAnchor),
						   tableViewToEmbed.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
						   tableViewToEmbed.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
		
		NSLayoutConstraint.activate(constraints)
	}
	
	func createOrdersTabelView() -> UITableView
	{
		let tv = UITableView()
		
		tv.backgroundColor = .clear
		tv.separatorStyle = .none
		tv.showsVerticalScrollIndicator = false
		
		tv.contentInset = UIEdgeInsets(top: -(getStatusBarHeight() ?? 0),
									   left: 0,
									   bottom: 0,
									   right: 0)
		
		tv.registerNibCell(OrderInfoTableViewCell.self)
		
		setupTableHeader(tv)
		
		tv.delegate = self
		tv.dataSource = self
		
		return tv
	}
	func setupTableHeader(_ tableView: UITableView)
	{
		let headerView = SubOrderView.create()
		
		let headerContainerView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: self.view.frame.width,
																				   height: SubOrderView.Constants.height)))
		
		headerContainerView.addSubview(headerView)
		
		headerView.translatesAutoresizingMaskIntoConstraints = false
		
		NSLayoutConstraint.activate ([headerView.centerXAnchor.constraint(equalTo: headerContainerView.centerXAnchor),
		 headerView.centerYAnchor.constraint(equalTo: headerContainerView.centerYAnchor),
		 headerView.widthAnchor.constraint(equalToConstant: self.view.frame.width),
		 headerView.heightAnchor.constraint(equalToConstant: SubOrderView.Constants.height)])
		
		tableView.tableHeaderView = headerContainerView
	}
	
	func setupNavBarAppearance()
	{
		(self.navigationController as? NavBarCustomizable)?.setAppearance(navBarBackground: .blurred,
																		hasBackButton: false,
																		titleViewType: .tabTitleHeader("Заказы"),
																		initiallyNotVisible: lastContentOffset < (getNavBarHeight() ?? 0))
	}
	
	func showEmptyOrdersViewIfNeeded()
	{
		emptyOrdersListView.isHidden =  !(subOrderArray.count <= 1)
	}
	
	func getSubOrderInfo(ordersPerPageCount:Int, offset:Int)
	{
		config.helper.getSubOrderInfo(ordersPerPageCount: ordersPerPageCount,offset: offset)
			.indicateProgress(on: self.view)
			.get
			{
				response in
				self.handleOrdersBatchSuccess(response)
			}
			.catch
			{  _ in
				print("Error")
			}
	}
	
	func registredCell()
	{
		
	}
	private func handleOrdersBatchSuccess(_ response: SubOrdersPaginationResponse)
	{
		isAlreadyFetching = false
		
		self.subOrderArray = response.items.sorted{ $0.orderId > $1.orderId }
		subOrdersTableView.reloadData()
	}
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension SellerOrderScreenViewController: UITableViewDelegate, UITableViewDataSource
{
	func scrollViewDidScroll(_ scrollView: UIScrollView)
	{
		setNavBarAppearance(scrollView)
		
		fetchDataIfNeeded(scrollView)
	}
	
	func fetchDataIfNeeded(_ scrollView: UIScrollView)
	{
		if isSomeDataNotObtainedFromBackend() &&
		   hasBeenScrolledToAdditionalDataFetchState(scrollView) &&
		   !isAlreadyFetching
		{
			//print("Should fetch!")
			
			isAlreadyFetching = true
			getSubOrderInfo(ordersPerPageCount:ordersPerPageCount, offset: subOrderArray.count - 1)
		}
		else
		{
			print("No fetch!")
		}
	}
	private func hasBeenScrolledToAdditionalDataFetchState(_ scrollView: UIScrollView) -> Bool
	{
		return getNumberOffCellsFromCurrentScrollPositionToBottom(scrollView) <= fetchThreshold
	}
	private func getNumberOffCellsFromCurrentScrollPositionToBottom(_ scrollView: UIScrollView) -> Double
	{
		Double((getObtainedContentHeight() - (initialOffset + scrollView.contentOffset.y))/OrderInfoTableViewCell.Constants.Dimensions.cellHeight)
	}
	
	private func getObtainedContentHeight() -> CGFloat
	{
		return subOrderArray.enumerated().map{ getCellHeight($0.offset) }.reduce(0, +)
	}
	
	private func isSomeDataNotObtainedFromBackend() -> Bool
	{
		return subOrderArray.count < backendOrdersCount
	}
	func setNavBarAppearance(_ scrollView: UIScrollView)
	{
		if scrollView.contentOffset.y > (getNavBarHeight() ?? 0) && !navBarVisibilityToggled
		{
			(self.navigationController as? NavigationController)?.toggleVisibility()
			
			navBarVisibilityToggled = true
		}
		else if lastContentOffset > scrollView.contentOffset.y &&
					scrollView.contentOffset.y < (getNavBarHeight() ?? 0)  && navBarVisibilityToggled
		{
			(self.navigationController as? NavigationController)?.toggleVisibility()
			
			navBarVisibilityToggled = false
		}

		lastContentOffset = scrollView.contentOffset.y
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		onOrderTapped(subOrderArray[indexPath.row])
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return subOrderArray.count
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
	{
		return getCellHeight(indexPath.row)
	}
	
	func getCellHeight(_ currentIndex: Int) -> CGFloat
	{
		return getOrderInfoCellHeight(currentIndex)
	}
	
	private func getOrderInfoCellHeight(_ currentIndex: Int) -> CGFloat
	{
		if let locationKind = getOrderCellLocationKind(currentIndex)
		{
			switch locationKind
			{
			case .top, .bottom:
				return OrderInfoTableViewCell.Constants.Dimensions.cornerCellHeight
				
			case .middle:
				return OrderInfoTableViewCell.Constants.Dimensions.cellHeight
			}
		}
		
		return 0
	}
	
	private func getOrderCellLocationKind(_ currentIndex: Int) -> OrderInfoTableViewCell.CellLocationKind?
	{
		return OrderInfoTableViewCell.CellLocationKind.get(0, currentIndex, subOrderArray.count - 1)
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		return createOrderInfoTableViewCell(tableView, indexPath, subOrderArray[indexPath.row])
	}
	
	func onOrderTapped(_ subOrderInfo: SubOrderInfo)
	{
		let sellerMoreDetailsOrderViewController = SellerMoreDetailsOrderViewController.create()
		
		sellerMoreDetailsOrderViewController.initialize(subOrderInfo,viewController: self)
		
		getTabBarController()?.present(sellerMoreDetailsOrderViewController, animated: true, completion: nil)
	}
	private func createOrderInfoTableViewCell(_ tableView: UITableView, _ indexPath: IndexPath, _ subOrderInfo: SubOrderInfo) -> UITableViewCell
	{
		if let cell = tableView.dequeueReusableCell(ofType: OrderInfoTableViewCell.self),
		   let cellLocationKind = getOrderCellLocationKind(indexPath.row)
		{
			cell.initialize(parentContainerWidth: self.view.frame.width,
							cellLocationKind: cellLocationKind,
							kind: .seller(subOrderInfo),
							config: .init()
			)
			
			cell.selectionStyle = .none
			cell.clipsToBounds = false
			
			return cell
		}
		
		return UITableViewCell()
	}
}


