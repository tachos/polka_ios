//
//  SellerAccountViewController.swift
//  Polka
//
//  Created by Makson on 17.05.2021.
//

import UIKit
import VisualEffectView

class SellerAccountViewController: UIViewController, StoryboardCreatable
{
	//MARK: - StoryboardCreatable
	static let storyboardName: StoryboardName = .sellerScreen
	
    @IBOutlet private var collectionView: UICollectionView!
    @IBOutlet private var blurView: UIVisualEffectView!
    @IBOutlet private var editStatusView: UIView!


    struct Config
	{
		let helper: SellerAccountHelper
		let onProductCategorySelected: Handler<SellerProductCategory>
	}
    var config: Config!

	private var viewWillAppearWasCalled = false
}

//MARK: - Lifecycle
extension SellerAccountViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		setupUI()

		onWorkingStatusChanged(
			isSellerWorking: config.helper.isSellerWorking
		)
	}

	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)

		setupNavBar()

		if !viewWillAppearWasCalled {
			viewWillAppearWasCalled = true
			config.helper.loadProductCategories(on: self)
		}
	}

	private func setupNavBar()
	{
		if let navBarCustomizable = (self.navigationController as? NavBarCustomizable).warnIfNil()
		{
			navBarCustomizable.setAppearance(
				navBarBackground: .blurred,
				hasBackButton: false,
				titleViewType: .tabTitleHeader("Каталог"),
				initiallyNotVisible: true
			)
			navBarCustomizable.setRightButtonTitle(nil)
		}

		updateNavBarVisibility(collectionView)
	}
}

//MARK: - Setup
private extension SellerAccountViewController
{
	func setupUI()
	{
		setupBlur()
		collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 75, right: 0)
	}

	private func setupBlur()
	{
		let visualEffectView = VisualEffectView()
		visualEffectView.frame = blurView.contentView.frame
		visualEffectView.colorTint = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2)
		visualEffectView.blurRadius = 33.59
		visualEffectView.scale = 1
		
		blurView.contentView.addSubview(visualEffectView)
		blurView.contentView.addSubview(editStatusView)
	}
}

//MARK: - API for Helper
extension SellerAccountViewController
{
	func onProductCategoriesLoaded()
	{
		collectionView.reloadData()
	}
}

//MARK: - Utils
private extension SellerAccountViewController
{
	func updateWorkingStatus(isSellerWorking: Bool)
	{
		config.helper.sendSellerWorkingStatus(
			isSellerWorking: isSellerWorking,
			on: self,
			onSuccess: { [weak self] in
				self?.onWorkingStatusChanged(isSellerWorking: isSellerWorking)
			}
		)
	}

	func onWorkingStatusChanged(isSellerWorking: Bool)
	{
		blurView.isHidden = isSellerWorking
		collectionView.isScrollEnabled = isSellerWorking

		if !isSellerWorking {
			// scroll to top
			collectionView.contentOffset.y = 0
		}
	}

	func updateNavBarVisibility(_ scrollView: UIScrollView)
	{
		let shouldShowHeader = scrollView.contentOffset.y > (getNavBarHeight() ?? 0)

		if let navigationController = (self.navigationController as? NavigationController).warnIfNil()
		{
			navigationController.setNavBarVisibleAnimated(shouldShowHeader)
		}
	}
}

//MARK: - UICollectionViewDataSource
extension SellerAccountViewController: UICollectionViewDataSource
{
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		config.helper.productCategories.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{
		guard let cell = collectionView.dequeueReusableCell(
			ofType: CatalogItemCollectionViewCell.self,
			for: indexPath
		).warnIfNil() else { return UICollectionViewCell() }

		let productCategory = config.helper.productCategories[indexPath.row]
		cell.initialize(productCategory.imageUrl, productCategory.name)
		return cell
	}

	func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
	{
		switch kind
		{
		case UICollectionView.elementKindSectionHeader:
			guard let headerView = collectionView.dequeueReusableSupplementaryView(
				ofKind: kind,
				withReuseIdentifier: SellerCatalogHeaderView.typeName,
				for: indexPath
			) as? SellerCatalogHeaderView
			else {
				dbgUnreachable()
				return UICollectionReusableView()
			}

			headerView.configure(
				.init(
					sellerName: config.helper.sellerName,
					isSellerWorking: config.helper.isSellerWorking,
					onLogOutButton: { [weak self] in
						self?.config.helper.onLogoutButton()
					},
					onWorkingStatusChanged: { [weak self] isSellerWorking in
						self?.updateWorkingStatus(isSellerWorking: isSellerWorking)
					}
				)
			)
			return headerView

		default:
			dbgUnreachable("Unexpected element kind: \(kind)")
			return UICollectionReusableView()
		}
	}
}

//MARK: - UICollectionViewDelegate
extension SellerAccountViewController: UICollectionViewDelegate
{
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
	{
		collectionView.deselectItem(at: indexPath, animated: true)
		
		let productCategory = config.helper.productCategories[indexPath.row]
		config.onProductCategorySelected(productCategory)
	}
}

//MARK: - UICollectionViewDelegate
extension SellerAccountViewController: UICollectionViewDelegateFlowLayout
{
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
	{
		CGSize(
			width: self.view.bounds.width,
			height: 226
		)
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
	{
		let cellWidth = ceil(
			(collectionView.frame.width
			- Constants.collectionViewSideMargin*2
			- Constants.collectionViewintercellSpacing
			) * 0.5
		)

		let cellHeight = ceil(
			cellWidth * Constants.categoryCellHeightToWidthRatio
		)

		return CGSize(
			width: cellWidth,
			height: cellHeight
		)
	}

	func scrollViewDidScroll(_ scrollView: UIScrollView)
	{
		updateNavBarVisibility(scrollView)
	}
}

//MARK: - Constant
extension SellerAccountViewController
{
	struct Constants
	{
		static let collectionViewSideMargin: CGFloat = 16
		static let collectionViewintercellSpacing: CGFloat = 10
		static let categoryCellHeightToWidthRatio = CGFloat(232)/CGFloat(167)
	}
}
