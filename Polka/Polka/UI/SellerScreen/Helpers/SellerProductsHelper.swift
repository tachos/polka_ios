//
//  SellerProductsHelper.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 01.03.2022.
//

import PromiseKit

class SellerProductsHelper
{
	let productCategory: SellerProductCategory
	let showErrorAlert: Handler<String>

	private(set) var loadedProducts: [SellerCatalogProduct] = []
	private var canLoadMoreProducts = true


	struct EditedProduct
	{
		let productId: String

		var stockCount: Float
		var holds: Float
		var nominalPrice: Int

		var isStale: Bool
	}
	private(set) var editedProducts: [EditedProduct] = []


	init(
		productCategory: SellerProductCategory,
		showErrorAlert: @escaping Handler<String>
	)
	{
		self.productCategory = productCategory
		self.showErrorAlert = showErrorAlert
	}

	var hasUnsavedChanges: Bool {
		editedProducts.contains(where: { $0.isStale })
	}

	func resetDirtyFlags()
	{
		editedProducts.enumerated().forEach
		{
			editedProducts[$0.offset].isStale = false
		}
	}
}

extension SellerProductsHelper
{
	func loadProductCategories(on sellerProductsVC: SellerProductsViewController)
	{
		let nextPage = WebServiceManager.Page(offset: loadedProducts.count)

		firstly {
			managers.webService.getSellerProducts(
				categoryId: productCategory.id,
				page: nextPage
			)
		}
		.indicateProgress(on: sellerProductsVC.view)
		.done { [weak self, weak sellerProductsVC] newLoadedProducts in
			//
			let hasLoadedMore = !newLoadedProducts.isEmpty
			self?.canLoadMoreProducts = hasLoadedMore

			if hasLoadedMore
			{
				self?.appendLoadedProducts(
					newLoadedProducts,
					on: sellerProductsVC
				)
			}
		}
		.catch {
			dbgwarn("\($0.localizedDescription)")
		}
	}

	private func appendLoadedProducts(
		_ newLoadedProducts: [SellerCatalogProduct],
		on vc: SellerProductsViewController?
	)
	{
		let oldCount = loadedProducts.count
		let newCount = oldCount + newLoadedProducts.count

		loadedProducts.append(contentsOf: newLoadedProducts)

		editedProducts.append(contentsOf: newLoadedProducts.map {
			EditedProduct(
				productId: $0.product.id,
				stockCount: $0.stockQuantity,
				holds: $0.holds,
				nominalPrice: $0.product.nominalPrice,
				isStale: false
			)
		})

		//
		let newIndexPaths: [IndexPath] = Array(oldCount...(newCount - 1))
			.map { IndexPath(row: $0, section: 0) }

		//
		if let collectionView = vc?.collectionView
		{
			let contentOffset = collectionView.contentOffset

			collectionView.performBatchUpdates(
				{
					collectionView.insertItems(
						at: newIndexPaths
					)

					collectionView.setContentOffset(contentOffset, animated: false)
				},
				completion: nil
			)
		}
	}

	func setProductNominalPrice(_ newNominalPrice: Float, at index: Int)
	{
		editedProducts[index].nominalPrice = FormatterPrice.RoublesToKopeks(newNominalPrice)
		editedProducts[index].isStale = true
	}

	func setProductStockCount(_ newStockCount: Float, at index: Int)
	{
		editedProducts[index].stockCount = newStockCount
		editedProducts[index].isStale = true
	}

	func sendUpdatedProductsToBackend(
		on sellerProductsVC: SellerProductsViewController,
		onSuccess: @escaping Handler<[Int]>	// indices of updated products
	)
	{
		let changedProductsIndices: [Int] = editedProducts.enumerated()
			.filter({ $0.element.isStale })
			.map { $0.offset }

		if changedProductsIndices.isEmpty {
			onSuccess([])
			return
		}

		let changedProducts: [StockProduct] = changedProductsIndices
			.map { editedProducts[$0] }
			.map {
				StockProduct(
					id: $0.productId,
					price_nominal: $0.nominalPrice,
					balance: $0.stockCount
				)
			}

		//
		firstly {
			managers.webService.updateStock(
				products: changedProducts
			)
		}
		.indicateProgress(on: sellerProductsVC.view)
		.done { [weak self] productsInStock in
			//
			self?.patchUpdatedProducts(
				changedProductsIndices: changedProductsIndices,
				productsInStock: productsInStock
			)
			onSuccess(changedProductsIndices)
		}
		.catch { [weak self] error in
			dbgwarn("\(error.localizedDescription)")
			if error.localizedDescription == Constants.error406 {
				self?.showErrorAlert(
					"Ошибка\n Проверьте кол-во товаров с значением hold."
				)
			}
		}
	}

	private func patchUpdatedProducts(
		changedProductsIndices: [Int],
		productsInStock: [ProductStock]
	)
	{
		dbgcheck(changedProductsIndices.count == productsInStock.count)

		productsInStock.enumerated().forEach
		{
			let changedProductIndex = changedProductsIndices[$0.offset]

			let oldProduct = loadedProducts[changedProductIndex]

			let updatedProduct = SellerCatalogProduct(
				product: $0.element.product,
				stockQuantity: $0.element.quantity,
				holds: oldProduct.holds
			)

			loadedProducts[changedProductIndex] = updatedProduct
		}

		changedProductsIndices.forEach
		{
			editedProducts[$0].isStale = false
		}
	}
}

//MARK: - Constant
extension SellerProductsHelper
{
	struct Constants
	{
		static let error406 = "406"
	}
}
