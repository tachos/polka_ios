//
//  SellerProductsViewController.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 01.03.2022.
//

import UIKit

class SellerProductsViewController: UIViewController, StoryboardCreatable
{
	//MARK: - StoryboardCreatable
	static let storyboardName: StoryboardName = .sellerScreen

	@IBOutlet private(set) var collectionView: UICollectionView!
	@IBOutlet private var applyChangesButton: CustomButton!
	@IBOutlet private var containerToSafeAreaBottomConstraint: NSLayoutConstraint!

	//
	struct Config
	{
		let helper: SellerProductsHelper
	}
	var config: Config!

	// State
	var isEditingPrices: Bool = false {
		didSet {
			self.applyChangesButton.isHidden = !isEditingPrices
		}
	}
}

//MARK: - Lifecycle
extension SellerProductsViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		setupUI()
	}

	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)

		setupNavBar()

		collectionView.contentInset = .init(
			top: getNavBarHeight() ?? 0,	// nav bar
			left: 0,
			bottom: 75,	// tab bar
			right: 0
		)

		config.helper.loadProductCategories(on: self)

		subscribeToKeyboardEvents()
	}

	private func setupNavBar()
	{
		if let navBarCustomizable = (self.navigationController as? NavBarCustomizable).warnIfNil()
		{
			navBarCustomizable.setAppearance(
				navBarBackground: .blurred,
				hasBackButton: true,
				titleViewType: .category(config.helper.productCategory.name),
				initiallyNotVisible: false
			)
		}

		//
		updateNavBarRightButtonTitle(isEditingOn: false)

		if let navCtrl = (self.navigationController as? NavigationController).warnIfNil()
		{
			navCtrl.navBarView.onRightButtonCallback = { [weak self] in
				guard let self = self else { return }
				self.setEditingModeEnabled(isEditingOn: !self.isEditingPrices)
			}
		}
	}

	override func viewDidDisappear(_ animated: Bool)
	{
		super.viewDidDisappear(animated)
		unsubscribeFromKeyboardEvents()
	}

	private func subscribeToKeyboardEvents()
	{
		NotificationCenter.default.addObserver(self,
											   selector: #selector(keyboardWillShow),
											   name: UIResponder.keyboardWillShowNotification,
											   object: nil)

		NotificationCenter.default.addObserver(self,
											   selector: #selector(keyboardWillHide),
											   name: UIResponder.keyboardWillHideNotification,
											   object: nil)
	}

	private func unsubscribeFromKeyboardEvents()
	{
		NotificationCenter.default.removeObserver(self,
												  name: UIResponder.keyboardWillShowNotification,
												  object: nil)

		NotificationCenter.default.removeObserver(self,
												  name: UIResponder.keyboardWillHideNotification,
												  object: nil)
	}
}

//MARK: - Setup
private extension SellerProductsViewController
{
	func setupUI()
	{
		registerCells()
		hideKeyboardWhenTappedAround()
		setupAgreeButton()
	}

	private func hideKeyboardWhenTappedAround()
	{
		let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
		tap.cancelsTouchesInView = false
		view.addGestureRecognizer(tap)
	}

	private func setupAgreeButton()
	{
		applyChangesButton.setTitle("Согласен с ценами и наличием", for: .normal)
		applyChangesButton.isHidden = true
	}

	private func registerCells()
	{
		collectionView.registerNibCell(ProductCollectionViewCell.self)
		collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 75, right: 0)
	}
}

//MARK: - API
extension SellerProductsViewController
{
	func setEditingModeEnabled(isEditingOn: Bool)
	{
		self.isEditingPrices = isEditingOn

		updateNavBarRightButtonTitle(isEditingOn: isEditingOn)

		let isCancelling = !isEditingOn
		if isCancelling {
			config.helper.resetDirtyFlags()
		}

		self.collectionView.reloadData()
	}

	private func updateNavBarRightButtonTitle(isEditingOn: Bool)
	{
		if let navBarCustomizable = (self.navigationController as? NavBarCustomizable).warnIfNil()
		{
			navBarCustomizable.setRightButtonTitle(
				isEditingOn ? "Отменить" : "Редактировать"
			)
		}
	}
}

//MARK: - Action
extension SellerProductsViewController
{
	@objc func keyboardWillShow(notification: NSNotification)
	{
		guard let userInfo = notification.userInfo else {return}
		guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
		let keyboardFrame = keyboardSize.cgRectValue

		containerToSafeAreaBottomConstraint.constant = keyboardFrame.height

		collectionView.animateLayoutChanges()
	}

	@objc func keyboardWillHide(notification: NSNotification)
	{
		containerToSafeAreaBottomConstraint.constant = 0

		collectionView.animateLayoutChanges()
	}

	@objc func dismissKeyboard()
	{
		view.endEditing(true)
	}

	@IBAction func onAgreeButton()
	{
		config.helper.sendUpdatedProductsToBackend(
			on: self,
			onSuccess: { [weak self] changedProductsIndices in
				//
				self?.setEditingModeEnabled(isEditingOn: false)

				if !changedProductsIndices.isEmpty {
					self?.collectionView.reloadItems(
						at: changedProductsIndices.map {
							IndexPath(item: $0, section: 0)
						},
						animated: false
					)
				}
			}
		)
	}
}

//MARK: - UICollectionViewDataSource
extension SellerProductsViewController: UICollectionViewDataSource
{
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		config.helper.loadedProducts.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{
		guard let cell = collectionView.dequeueReusableCell(
			ofType: ProductCollectionViewCell.self,
			for: indexPath
		) else { return UICollectionViewCell() }

		let sellerCatalogProduct = config.helper.loadedProducts[indexPath.row]

		cell.setup(
			isEdit: isEditingPrices,
			product: sellerCatalogProduct.product,
			holds: config.helper.editedProducts[indexPath.row].holds,
			count: config.helper.editedProducts[indexPath.row].stockCount,
			onPrice: { [weak self] price in
				self?.config.helper.setProductNominalPrice(price, at: indexPath.row)
			},
			onCount: { [weak self] count in
				self?.config.helper.setProductStockCount(count, at: indexPath.row)
			},
			onPressTextField: {}
		)
		return cell
	}
}

//MARK: - UICollectionViewDelegate
extension SellerProductsViewController: UICollectionViewDelegate
{
}

//MARK: - UICollectionViewDelegateFlowLayout
extension SellerProductsViewController: UICollectionViewDelegateFlowLayout
{
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
	{
		CGSize(width: UIScreen.main.bounds.width, height: 102)
	}
}
