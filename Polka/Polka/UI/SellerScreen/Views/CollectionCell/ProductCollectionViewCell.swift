//
//  ProductCollectionViewCell.swift
//  Polka
//
//  Created by Makson on 17.05.2021.
//

import UIKit



typealias PriceCallback = Handler<Float>
typealias CountCallback = Handler<Float>

class ProductCollectionViewCell: UICollectionViewCell
{
	//MARK: - Outlet and variable
    @IBOutlet weak var costLbl: UILabel!
    @IBOutlet weak var residueLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var countLbl: UILabel!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var priceField: UITextField!
    @IBOutlet weak var countField: UITextField!
    
    @IBOutlet weak var pictureView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var unitCountLbl: UILabel!
    @IBOutlet weak var unitPriceLbl: UILabel!
    @IBOutlet weak var holdLbl: UILabel!
    
    var count:Float!
	var price:Float = 0.0
	
	private var onPrice: PriceCallback!
	private var onCount: CountCallback!
	private var onTextField: VoidCallback!
	private var isPermissible:Bool = true
	private var unit: MeasurementUnit!
	private var holds:Float!
	
}

//MARK: - Lifecycle
extension ProductCollectionViewCell
{
	override func awakeFromNib()
	{
		super.awakeFromNib()
		setupUI()
	}
	
	func setupUI()
	{
		setupTextField()
		setupBorderColorAllView(active: false)
	}
	
	func setupTextField()
	{
		priceField.delegate = self
		countField.delegate = self
		priceField.addTarget(self, action: #selector(priceTextFieldDidChange), for: .editingChanged)
		countField.addTarget(self, action: #selector(countTextFieldDidChange), for: .editingChanged)
	}
	
	func setupBorderColorAllView(active:Bool)
	{
		priceView.layer.borderColor = getBorderColor(active: active).cgColor
		countView.layer.borderColor = getBorderColor(active: active).cgColor
	}
	
	func getBorderColor(active:Bool) -> UIColor
	{
		if active
		{
			return UIColor(red: 69/255, green: 178/255, blue: 107/255, alpha: 1)
		}
		else
		{
			return UIColor.clear
		}
	}
	
	func setupBorderView(view:UIView,active:Bool)
	{
		view.layer.borderColor = getBorderColor(active: active).cgColor
	}
	
	override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {

		if priceField.isFirstResponder  || countField.isFirstResponder{
			DispatchQueue.main.async(execute: {
				if #available(iOS 13.0, *) {
					(sender as? UIMenuController)?.hideMenu()
				} else {
					(sender as? UIMenuController)?.setMenuVisible(false, animated: false)
				}
			})
			return false
		}

		return super.canPerformAction(action, withSender: sender)
	}
	
}

extension ProductCollectionViewCell
{
	
	func setup(isEdit:Bool,
			   product: Product,
			   holds:Float,
			   count:Float,
			   onPrice:@escaping PriceCallback,
			   onCount:@escaping CountCallback,
			   onPressTextField: @escaping VoidCallback
						)
	{
		stateCell(isEdit: isEdit)
		self.count = count
		self.holds = holds
		self.unit = product.unit
		setupCell(isEdit: isEdit, product: product)
		self.onPrice = onPrice
		self.onCount = onCount
		self.onTextField = onPressTextField
		setupTextFields()
		setupHold(hold: holds)
		setupColorHold(count: count, hold: holds)
	}
	
	func setupTextFields()
	{
		priceField.textColor = UIColor(red: 34/255, green: 34/255, blue: 34/255, alpha: 1)
		priceView.isUserInteractionEnabled = true
	}
	
	func setupCell(isEdit:Bool,product: Product)
	{
		nameLbl.text =  product.name
		pictureView.sd_setImage(with: nil, placeholderImage: UIImage(named: "smile"), options: [])
		
		if isEdit
		{
			priceField.text = "\(FormatterPrice.kopeksToRoubles(product.nominalPrice))"
			
			countField.text = QuantityFormatter.cartItemQuantityWithoutUnit(count, product.unit)
			unitPriceLbl.text = "₽/\(product.unit.toString())"
			unitCountLbl.text = "\(product.unit.toString())"
		}
		else
		{

			priceLbl.text = PriceFormatter.catalogItemWithUnits(price: FormatterPrice.kopeksToRoubles(product.nominalPrice),
																measurementUnit: product.unit,
																displayedOn: .aboutProductScreen)
			
			countLbl.text = QuantityFormatter.cartItemQuantity(count, product.unit)
		}
	}
	
	func stateCell(isEdit:Bool)
	{
		if isEdit
		{
			costLbl.isHidden = true
			residueLbl.isHidden = true
			priceLbl.isHidden = true
			countLbl.isHidden = true
			priceView.isHidden = false
			countView.isHidden = false
			holdLbl.isHidden = false
		}
		else
		{
			costLbl.isHidden = false
			residueLbl.isHidden = false
			priceLbl.isHidden = false
			countLbl.isHidden = false
			priceView.isHidden = true
			countView.isHidden = true
			holdLbl.isHidden = true 
		}
	}
	
	func setupHold(hold:Float)
	{
		holdLbl.text = "hold: \(hold)"
	}
	
	func setupColorHold(count:Float, hold: Float)
	{
		if count >= hold
		{
			holdLbl.textColor = #colorLiteral(red: 0.5607843137, green: 0.5607843137, blue: 0.5607843137, alpha: 1)
		}
		else if count < hold
		{
			holdLbl.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
		}
	}
}

//MARK: - UITextFieldDelegate
extension ProductCollectionViewCell:UITextFieldDelegate
{
	@objc func priceTextFieldDidChange()
	{
//		if !is_price_actual
//		{
//			
			priceField.text = priceField.text?.replacingOccurrences(of: ",", with: ".")
			priceField.text = transformationPriceOrCount(field: priceField.text!, variable: price).0
			price = transformationPriceOrCount(field: priceField.text!, variable: price).1
			updatePriceField(price: price)
//
//		}
//		else
//		{
//			print("\(String(describing: is_price_actual))")
//		}
	}
	
	@objc func countTextFieldDidChange()
	{
		countField.text = verificationCountField(string: countField.text!, unit: unit)
		countField.text = transformationPriceOrCount(field: countField.text!, variable: count).0
		count = transformationPriceOrCount(field: countField.text!, variable: count).1
		updateCountField(count: count)
	}
	
	func transformationPriceOrCount(field:String, variable:Float) -> (String,Float)
	{
		var field = field
		var variable = variable
		field = removeFirstCharacterIsZero(string: field)
		if checkCount(string: field)
		{
			if checkNumber(numeric:field) || checkPoint(str: field)
			{
				let removeLastSymbol = field.dropLast()
				field = String(removeLastSymbol)
				variable = Float(field) ?? 0
			}
			else
			{
				variable = Float(field) ?? 0
			}
		}
		else
		{
			if field.contains(".")
			{
				let fullStringArr = field.split(separator: ".")
				if fullStringArr.count > 1
				{
					field = String(fullStringArr[0].dropLast()) + "." + String(fullStringArr[1])
				}
				else
				{
					field = String(fullStringArr[0].dropLast()) + "."
				}

			}
			else
			{
				field = String(field.dropLast())
			}
		}
		
		if field.last == "." && field.split(separator: ".").count < 1
		{
			field = String(field.dropLast())
		}
		return (field,variable)
	}
	
	func removeFirstCharacterIsZero (string:String) -> String
	{
		if string.count == 2 && string.first == "0"
		{
			if !string.contains(".")
			{
				return String(string.dropFirst())
			}
			else
			{
				return string
			}
		}
		else
		{
			return string
		}
	}
	func verificationCountField(string:String,unit:MeasurementUnit) -> String
	{
		let removal:[Character] = [",","."]
		switch unit
		{
		case .unit:
			return string.filter{!removal.contains($0)}
		case .kg,.l:
			return string.replacingOccurrences(of: ",", with: ".")
		}
	}
	
	func checkCount(string:String) -> Bool
	{
		if string.contains(".")
		{
			let fullStringArr = string.split(separator: ".")
			
			return countingNumber(string: String(fullStringArr[0]))
		}
		else
		{
			return countingNumber(string: string)
		}
	}
	
	func countingNumber(string:String) -> Bool
	{
		if string.count < 5
		{
			return true
		}
		else
		{
			return false
		}
	}
	
	func checkPoint(str:String) -> Bool
	{
		var countPointInString = 0
		
		let countTemporary = str.components(separatedBy: ".")
		
		countPointInString = countTemporary.count - 1
		
		if countPointInString > 1
		{
			return true
		}
		else
		{
			return false
		}
	}
	
	func checkNumber(numeric:String) -> Bool
	{
		var countAfterThatPoint = 0
		
		if numeric.contains(".")
		{
			if numeric.split(separator: ".").count > 1
			{
				countAfterThatPoint = String(numeric.split(separator: ".")[1]).count
			}
		}
		
		if countAfterThatPoint > 2
		{
			return true
		}
		else
		{
			return false
		}
	}
	
	func textFieldDidBeginEditing(_ textField: UITextField)
	{
		onTextField?()
		
		if textField == priceField
		{
			setupBorderView(view: priceView, active: true)
		}
		else if textField == countField
		{
			textField.text = QuantityFormatter.cartItemQuantityWithoutUnit(count,unit)
			setupBorderView(view: countView, active: true)
		}
		
	}
	
	func textFieldDidEndEditing(_ textField: UITextField)
	{
		if textField.text?.last == "."
		{
			textField.text = String((textField.text?.dropLast())!)
		}
	
		if textField == priceField
		{
			setupBorderView(view: priceView, active: false)
		}
		else if textField == countField
		{
			setupBorderView(view: countView, active: false)
			textField.text = QuantityFormatter.cartItemQuantityWithoutUnit(count,unit)
			setupColorHold(count: count, hold: holds)
		}
		textField.resignFirstResponder()
	}
	
	func updateCountField(count:Float)
	{
		
		if count <= holds && !(countField.text?.contains(".") ?? true )
		{
			countField.text =  QuantityFormatter.cartItemQuantityWithoutUnit(holds,unit)
			self.count = holds
		}
		else
		{
			//countField.text =  QuantityFormatter.cartItemQuantityWithoutUnit(count,unit)
			self.count = count
		}
		print(count)
		onCount?(self.count)
	}
	
	func updatePriceField(price:Float)
	{
		if price >= Constant.minPrice
		{
			self.price = price
		}
		else
		{
			priceField.text = "\(Constant.minPrice)".replacingOccurrences(of: ",", with: ".")
			self.price = Constant.minPrice
		}
		onPrice?(self.price)
	}
}

extension ProductCollectionViewCell
{
	struct Constant
	{
		static let minPrice:Float = 1.0
	}
}
