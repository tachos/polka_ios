//
//  SellerCatalogHeaderView.swift
//  Polka
//
//  Created by Makson on 17.05.2021.
//

import UIKit

extension SellerCatalogHeaderView
{
	struct Config
	{
		let sellerName: String?
		let isSellerWorking: Bool

		let onLogOutButton: VoidCallback
		let onWorkingStatusChanged: Handler<Bool>
	}
}

class SellerCatalogHeaderView: UICollectionReusableView
{
	@IBOutlet private var sellerNameLabel: UILabel!
	@IBOutlet private var logOutButton: UIButton!

	@IBOutlet private var segmentControl: UISegmentedControl!

	//
	private var config: Config!
}

extension SellerCatalogHeaderView
{
	override func awakeFromNib()
	{
		super.awakeFromNib()

		setupSegmentedSelector()
	}

	private func setupSegmentedSelector()
	{
		let font = Fonts.ObjectSans.heavy(24)
		let lightgray = UIColor(red: 143/255, green: 143/255, blue: 143/255, alpha: 1)
		segmentControl.setTitleTextAttributes([NSAttributedString.Key.font: font,NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)
		segmentControl.layer.masksToBounds = true;
		segmentControl.setTitleTextAttributes([NSAttributedString.Key.font: font,NSAttributedString.Key.foregroundColor: lightgray], for: .normal)
	}
}

extension SellerCatalogHeaderView
{
	func configure(_ config: Config)
	{
		self.config = config

		sellerNameLabel.text = config.sellerName ?? "Продавец"
		segmentControl.selectedSegmentIndex = config.isSellerWorking ? 0 : 1
	}
}

private extension SellerCatalogHeaderView
{
	@IBAction func onLogOutButton()
	{
		config.onLogOutButton()
	}

	@IBAction func onSegmentedSelector(_ sender: Any)
	{
		guard let segmentedControl = sender as? UISegmentedControl else {
			dbgUnreachable()
			return
		}

		switch segmentedControl.selectedSegmentIndex
		{
		case 0:
			config.onWorkingStatusChanged(true)
		case 1:
			config.onWorkingStatusChanged(false)
		default:
			dbgUnreachable()
		}
	}
}
