//
//  DeliveryAddress+UI.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 01.07.2021.
//

import Foundation


extension DeliveryAddresses
{
	func toString() -> String
	{
		guard let street = street,
			  let house = house
		else
		{
			return ""
		}

		return [street, house, apartment]
			.compactMap { $0 }
			.joined(separator: ", ")
	}
}
