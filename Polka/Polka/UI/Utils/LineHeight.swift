//
//  LineHeight.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 04.06.2021.
//

import UIKit



extension NSParagraphStyle
{
	static func with(lineHeight: CGFloat) -> NSMutableParagraphStyle
	{
		let paragraphStyle = NSMutableParagraphStyle()

		paragraphStyle.minimumLineHeight = lineHeight
		paragraphStyle.maximumLineHeight = lineHeight

		return paragraphStyle
	}
}



extension String
{
	func with(lineHeight: CGFloat) -> NSAttributedString
	{
		let paragraphStyle = NSMutableParagraphStyle()

		paragraphStyle.minimumLineHeight = lineHeight
		paragraphStyle.maximumLineHeight = lineHeight

		return .init(
			string: self,
			attributes: [.paragraphStyle: paragraphStyle]
		)
	}
}
