//
//  CartViewController.swift
//  Polka
//
//  Created by Michael Goremykin on 05.05.2021.
//

import RxSwift
import MBProgressHUD

class CartViewController: UIViewController
{
	// MARK:- Outlets
	weak var emptyCartView: EmptyCartView!
	weak var cartTableView: UITableView!
	weak var storkView: StorkView!
	
	// MARK:- RX
	let disposeBag = DisposeBag()
	
	// MARK:- Stateful
	var progressIndicator:MBProgressHUD!
	var navBarVisibilityToggled = false
	var lastContentOffset: CGFloat = 0
	var sectionsHeights: [CGFloat] = []
	var productsPurchasability: [[Purchasability]] = []
	
	// MARK:- Parameters
	var cartController: CartControllerProtocol!
	var paymentHelper: PaymentHelper!
}

// MARK:- Life cycle
extension CartViewController
{
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
		
		setNavBarAppearance()
		
		getCartContents()
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		setupUI()
		
		subscribeToCartEvents()
	}
}

// MARK:- Initialization
extension CartViewController
{
	func initialize(
		cartController: CartControllerProtocol,
		paymentHelper: PaymentHelper
	)
	{
		self.cartController = cartController
		self.paymentHelper = paymentHelper
	}
}

// MARK:- Instantiation
extension CartViewController
{
	static func create(
		cartController: CartControllerProtocol = CartController.shared,
		paymentHelper: PaymentHelper
	) -> CartViewController
	{
		let vc = CartViewController()
		
		vc.initialize(
			cartController: cartController,
			paymentHelper: paymentHelper
		)
		
		return vc
	}
}
