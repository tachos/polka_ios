//
//  CartSellerSectionView.swift
//  Polka
//
//  Created by Michael Goremykin on 07.05.2021.
//

import UIKit

class CartSellerSectionView: UITableViewHeaderFooterView
{
	// MARK:- Outlets
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var notWorkingLabel: UILabel!
	@IBOutlet weak var fakeRoundedCornersViewHeightConstraint: NSLayoutConstraint!
	
	// MARK:- Stateful
	var catalogButtonCallback: (() -> Void)!
	var onChatWithBtnTappedCallback: (() -> Void)!
	var onClearBtnTappedCallback: (() -> Void)!
}

// MARK:- UI handlers
extension CartSellerSectionView
{
	@IBAction func onCatalogBtnTapped(_ sender: Any)
	{
		catalogButtonCallback()
	}
	
	@IBAction func onChatWithBtnTapped(_ sender: Any)
	{
		onChatWithBtnTappedCallback()
	}
	
	@IBAction func onClearBtnTapped(_ sender: Any)
	{
		onClearBtnTappedCallback()
	}
}

// MARK:- Initialization
extension CartSellerSectionView
{
	func initialize(displayCellTopCornersRounded: Bool,
                    isWorking: Bool,
					title: String,
					description: String,
					onCatalogButton catalogButtonCallback: @escaping () -> Void,
					onChatWithBtnTappedCallback: @escaping () -> Void,
					onClearBtnTappedCallback: @escaping () -> Void)
	{
		self.catalogButtonCallback		= catalogButtonCallback
		self.onChatWithBtnTappedCallback 	= onChatWithBtnTappedCallback
		self.onClearBtnTappedCallback		= onClearBtnTappedCallback
		
		displayFakeRoundedCornersIfNeeded(displayCellTopCornersRounded)
		displayOutOfStockLabelIfNeeded(false)
		
		titleLabel.text 		= title
        notWorkingLabel.isHidden = isWorking
	}
}

// MARK:- API
extension CartSellerSectionView
{
	static func getHeight(_ currentElementIndex: Int, _ showNotWorkingLabel: Bool) -> CGFloat
	{
		return (currentElementIndex == 0 ? Constants.firstHeight: Constants.otherHeight) +
			   (showNotWorkingLabel ? Constants.outOfStockDelta: 0)
	}
}

// MARK:- Utils
extension CartSellerSectionView
{
	func displayFakeRoundedCornersIfNeeded(_ displayCellTopCornersRounded: Bool)
	{
		fakeRoundedCornersViewHeightConstraint.constant = displayCellTopCornersRounded ? Constants.fakeRoundedCornersViewHeight : 0
	}

	func displayOutOfStockLabelIfNeeded(_ show: Bool)
	{
		notWorkingLabel.isHidden = !show
	}
}

// MARK:- Constants
extension CartSellerSectionView
{
	struct Constants
	{
		static let firstHeight 					= CGFloat(134)
		static let otherHeight 					= CGFloat(188)
		static let outOfStockDelta 				= CGFloat(26)
		static let fakeRoundedCornersViewHeight = CGFloat(54)
	}
}
