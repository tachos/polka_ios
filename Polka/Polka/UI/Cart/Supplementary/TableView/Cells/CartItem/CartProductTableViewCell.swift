//
//  CartItemTableViewCell.swift
//  Polka
//
//  Created by Michael Goremykin on 06.05.2021.
//

import SDWebImage
import TinyConstraints

enum Purchasability
{
	case purchasable
	case notPurchasable(Set<Reason>)
	
	enum Reason
	{
		case outOfStock
		case sellerClosed
	}
}

class CartProductTableViewCell: UITableViewCell
{
	// MARK:- Outlets
    @IBOutlet weak var itemImageView: CustomImage!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var greyStorkView: UIView!
    @IBOutlet weak var stepperView: StepperView!
    
	
	// MARK:- Parameters
	var purchasability: Purchasability!
	var cartProduct: CartProduct!
	var onItemQuantityChanged: ((Float) -> Void)!
	var onSetToZeroAdditionalActionCallback: (() -> Void)!
	var onItemClicked: (() -> Void)!
}

// MARK:- Initialization
extension CartProductTableViewCell
{
	func initialize(purchasability: Purchasability,
					cartProduct: CartProduct,
					onItemClicked: @escaping () -> Void,
					onItemQuantityChanged: @escaping (Float) -> Void,
					onSetToZeroAdditionalAction: @escaping () -> Void)
	{
		self.purchasability = purchasability
		self.cartProduct 	= cartProduct
		self.onSetToZeroAdditionalActionCallback = onSetToZeroAdditionalAction
		self.onItemClicked 			= onItemClicked
		self.onItemQuantityChanged 	=  onItemQuantityChanged
		
		setupUI()
	}
}

// MARK:- Setup UI
extension CartProductTableViewCell
{
	func setupUI()
	{
		itemImageView.sd_setImage(with: cartProduct.product.imageUrl,
								  placeholderImage: nil,
								  options: [])
		
		titleLabel.text = cartProduct.product.name
		
		priceLabel.attributedText = PriceFormatter.catalogItemWithUnits(price: FormatterPrice.kopeksToRoubles(cartProduct.product.price),
																		measurementUnit: cartProduct.product.unit)
		
		greyStorkView.isHidden = shouldStorkBeHidden(purchasability)
		
		setupStepper()
	}
}

// MARK:- UI handlers
extension CartProductTableViewCell
{
    @IBAction func onItemTapped(_ sender: Any)
    {
        onItemClicked()
    }
}

// MARK:- Utils
extension CartProductTableViewCell
{
    func shouldStorkBeHidden(_ purchasability: Purchasability) -> Bool
    {
        switch purchasability
        {
		case .purchasable:
			return  true
			
		case .notPurchasable(_):
			return false
		}
    }
    
    func setupStepper()
    {
		stepperView.setup(
			.grey,
			cartProduct.toStepperCount(),
			cartProduct.toAvailableStepCount(),
			weightFormatter: {[weak self] (val) in self?.stepperLabelFormatter(val) ?? ""},
			priceFormatter: { [unowned self] in
				//
				formatPrice(cartProduct: self.cartProduct, count: $0)
			},
			{[weak self] (val) in self?.onQuantityChanged(val) }
		)
	}
	
	private func onQuantityChanged(_ val: Int)
	{
		val == 0 ? self.onItemQuantityChanged(0) :
		self.onItemQuantityChanged(
			cartProduct.getProductQuantity(stepCount: val)
		)
	}
	
	private func stepperLabelFormatter(_ stepCount: Int) -> String
	{
		return QuantityFormatter.cartItemQuantity(
			cartProduct.getProductQuantity(stepCount: stepCount),
			cartProduct.product.unit
		)
	}
}

// MARK:- Constants
extension CartProductTableViewCell
{
	struct Constants
    {
        static let height           = CGFloat(60)
        static let heightWithError  = CGFloat(78)
    }
}
