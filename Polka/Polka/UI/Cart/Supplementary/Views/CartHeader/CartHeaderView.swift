//
//  CartHeaderView.swift
//  Polka
//
//  Created by Michael Goremykin on 07.05.2021.
//

import UIKit


class CartHeaderView: UIView
{
	@IBOutlet private var marketNameLabel: UILabel!
	
	// MARK:- Stateful
	private var onClearButtonTapped: (() -> Void)!
}


// MARK:- UI handlers
extension CartHeaderView
{
	@IBAction func onClearTapped(_ sender: Any)
	{
		onClearButtonTapped()
	}
}

// MARK:- Initialization
extension CartHeaderView
{
	func initialize(
		market: Market?,
		_ onClearButtonTapped: @escaping () -> Void
	)
	{
		marketNameLabel.text = market?.caption
		
		self.onClearButtonTapped = onClearButtonTapped
	}
}

// MARK:- Instantiation
extension CartHeaderView
{
	static func create(
		market: Market?,
		_ parentScreenWidth: CGFloat
		, _ onClearButtonTapped: @escaping () -> Void
	) -> CartHeaderView
	{
		let view = createView(CartHeaderView.self)
		
		view.frame = CGRect(origin: .zero, size: CGSize(width: parentScreenWidth,
														height: Constants.height))
		
		view.initialize(
			market: market,
			onClearButtonTapped
		)
		
		return view
	}
}

// MARK:- Constants
extension CartHeaderView
{
	struct Constants
	{
		static let height = CGFloat(136)
	}
}
