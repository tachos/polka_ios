//
//  EmptyCartView.swift
//  Polka
//
//  Created by Michael Goremykin on 05.05.2021.
//

import UIKit

class EmptyCartView: UIView
{
	// MARK:- Stateful
	var onButttonTapped: (() -> Void)!
}

// MARK:- UI handlers
extension EmptyCartView
{
	@IBAction func onButtonTapped(_ sender: Any)
	{
		onButttonTapped()
	}
}

// MARK:- Initialization
extension EmptyCartView
{
	func initialize(_ onButtonTappedCallback: @escaping () -> Void)
	{
		self.onButttonTapped = onButtonTappedCallback
	}
}

// MARK:- Instantiation
extension EmptyCartView
{
	static func create(_ onButtonTapped: @escaping () -> Void) -> EmptyCartView
	{
		let view = createView(EmptyCartView.self)
		
		view.initialize(onButtonTapped)
		
		return view
	}
}
