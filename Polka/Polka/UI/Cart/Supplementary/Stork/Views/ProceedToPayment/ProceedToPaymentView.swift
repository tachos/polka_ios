//
//  ProceedToPaymentView.swift
//  Polka
//
//  Created by Michael Goremykin on 26.05.2021.
//

import UIKit
import RxSwift

enum PaymentOptionState
{
    case available
    case notAvailable
}

class ProceedToPaymentView: UIView
{
    // MARK:- Outlets
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var proceedToPaymentButton: CustomButton!
	
    // MARK:- Parameters
    var paymentOptionState: PaymentOptionState!
    var total: Float = 0
    var onPaymentTappedCallback: (() -> Void)!
	
	// MARK:- Rx
	let disposeBag = DisposeBag()
}

// MARK:- API
extension ProceedToPaymentView
{
    func setState(_ state: PaymentOptionState)
    {
        self.paymentOptionState = state
        
        setupUI()
    }
}

// MARK:- Initialize
extension ProceedToPaymentView
{
	func initialize(paymentOptionState: PaymentOptionState,
					total: Float,
					onPaymentTapped onPaymentTappedCallback: @escaping () -> Void)
	{
		self.paymentOptionState = paymentOptionState
		self.total = total
		self.onPaymentTappedCallback = onPaymentTappedCallback
        
        setupUI()
	}
}

// MARK:- Instantiation
extension ProceedToPaymentView
{
	static func create(paymentOptionState: PaymentOptionState,
					   total: Float,
					   onPaymentTapped onPaymentTappedCallback: @escaping () -> Void) -> ProceedToPaymentView
	{
		let proceedToPaymentView = createView(ProceedToPaymentView.self)
		
		proceedToPaymentView.initialize(paymentOptionState: paymentOptionState, total: total, onPaymentTapped: onPaymentTappedCallback)
		
		return proceedToPaymentView
	}
}

// MARK:- Life cycle
extension ProceedToPaymentView
{
	override func awakeFromNib()
	{
		super.awakeFromNib()
		
//		subscribeToUserAnCartEvents()
		subscribeToCartEvents()
		subscribeToUserEvents()
	}
}

// MARK:- Life cycle
extension ProceedToPaymentView
{
	func setForState(_ paymentOptionState: PaymentOptionState)
	{
		self.paymentOptionState = paymentOptionState
		
		setupProceedToPaymentButton()
	}
}

// MARK:- Setup UI
extension ProceedToPaymentView
{
    func setupUI()
    {
		totalLabel.text = PriceFormatter.orderItem(self.total)
        
        setupProceedToPaymentButton()
    }
    
    func setupProceedToPaymentButton()
    {
        let buttonSettings = getProceedToPaymentButtonSettings()
        
        proceedToPaymentButton.style = buttonSettings.style
        proceedToPaymentButton.isEnabled = buttonSettings.isEnabled
    }
    
    func getProceedToPaymentButtonSettings() -> (style: String,
                                                 isEnabled: Bool)
    {
        switch paymentOptionState
        {
        case .available:
            return (style: "orange", isEnabled: true)
            
        case .notAvailable:
            return (style: "gray", isEnabled: false)
            
        case .none:
            return (style: "gray", isEnabled: false)
        }
    }
}

// MARK:- UI handlers
extension ProceedToPaymentView
{
    @IBAction func onPaymentTapped(_ sender: Any)
    {
        onPaymentTappedCallback()
		
		proceedToPaymentButton.isEnabled = false
    }
}

// MARK:- UI handlers
extension ProceedToPaymentView
{
//	func subscribeToUserAnCartEvents()
//	{
//		_  = Observable.combineLatest(CartController.shared.cartStateObservable,
//									  UserController.shared.userLocalEventsObservable).subscribe(onNext: {[weak self] cartState, userEvent in
//										self?.onPaymentOptionsStateChanged(cartState, userEvent)
//									}).disposed(by: disposeBag)
//
//	}
	
	private func onPaymentOptionsStateChanged(_ cart: Cart)
	{
		setTotalLabel(cart)
		
		self.setForState(ProceedToPaymentView.getPaymentOptionState())
	}
	
	private func setTotalLabel(_ cart: Cart)
	{
		self.totalLabel.text = PriceFormatter.orderItem(cart.getTotal())
	}
	
	func subscribeToCartEvents()
	{
		CartController.shared.cartStateObservable.subscribe(onNext: {[weak self] cartState in
			guard let self = self else { return }

			self.onPaymentOptionsStateChanged(cartState.cart)
		}).disposed(by: disposeBag)
	}

	func subscribeToUserEvents()
	{
		UserController.shared.userLocalEventsObservable.subscribe(onNext: {[weak self] userEvent in
			guard let self = self else { return }
			
			self.onPaymentOptionsStateChanged(CartController.shared.cart)
		}).disposed(by: disposeBag)
	}
}

// MARK:- Constants
extension ProceedToPaymentView
{
	enum Constants
	{
		static let height = CGFloat(152)
	}
}

extension ProceedToPaymentView
{
	static func isPaymentAvailable() -> Bool
	{
		CartController.shared.cart.marketIsOpen &&
		UserController.shared.currentDeliveryAddress?.is_delivery_possible ?? false &&
		CartController.shared.cart.getTotal() > 0
	}
	
	static func getPaymentOptionState() -> PaymentOptionState
	{
		isPaymentAvailable() ? .available : .notAvailable
	}
}
