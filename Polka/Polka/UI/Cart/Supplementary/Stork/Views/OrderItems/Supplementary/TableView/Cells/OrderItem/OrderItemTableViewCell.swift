//
//  OrderItemTableViewCell.swift
//  Polka
//
//  Created by Michael Goremykin on 26.05.2021.
//

import UIKit

// MARK:- OrderItemTableViewCell
class OrderItemTableViewCell: UITableViewCell
{
	weak var orderItemView: OrderItemView!
}

// MARK:- Setup UI
extension OrderItemTableViewCell
{
	func createAndEmbedOurderItemView(_ type: OrderItemView.OrderItemType) -> OrderItemView
	{
		let orderItemView = OrderItemView.create(type)
		
		self.addSubview(orderItemView)
		
		orderItemView.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [orderItemView.topAnchor.constraint(equalTo: self.topAnchor),
						   orderItemView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
						   orderItemView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
						   orderItemView.trailingAnchor.constraint(equalTo: self.trailingAnchor)]
		
		NSLayoutConstraint.activate(constraints)
		
		return orderItemView
	}
}

// MARK:- Initialization
extension OrderItemTableViewCell
{
	func initialize(_ type: OrderItemView.OrderItemType)
	{
		if orderItemView == nil
		{
			orderItemView = createAndEmbedOurderItemView(type)
		}
		else
		{
			orderItemView.setForType(type)
		}
	}
}
