//
//  OrderItemsView.swift
//  Polka
//
//  Created by Michael Goremykin on 26.05.2021.
//

import UIKit
import RxSwift

class OrderItemsView: UIView
{
	// MARK:- Outlets
	weak var orderItemsTableView: UITableView!
	
	// MARK:- Parameters
	var cartController: CartControllerProtocol!
	
	// MARK:- Stateful
	var items: [OrderItemView.OrderItemType] = []
	
	// MARK:- Rx
	let disposeBag = DisposeBag()
	
	init()
	{
		super.init(frame: .zero)
		
		self.subscribeToCartEvents()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

// MARK:- Life cycle
extension OrderItemsView
{
	override func layoutSubviews()
	{
		
	}
}

// MARK:- Initialization
extension OrderItemsView
{
	func initialize()
	{
		self.items = getOrderItems()
		
		setupUI()
	}
}

// MARK:- Instantiation
extension OrderItemsView
{
	static func create() -> OrderItemsView
	{
		let orderItemsView = OrderItemsView()
		
		orderItemsView.initialize()
		
		return orderItemsView
	}
}

// MARK:- Utils
extension OrderItemsView
{
	func setupUI()
	{
		self.backgroundColor = .white
		
		self.orderItemsTableView = createTableViewIfNeeded()
	}
}


// MARK:- Utils
extension OrderItemsView
{
	func createTableViewIfNeeded() -> UITableView
	{
		let tv = UITableView()
		
		tv.backgroundColor = .clear
		tv.separatorStyle 				= .none
		tv.showsVerticalScrollIndicator = false
		
		tv.registerNibCell(OrderItemTableViewCell.self)
		
		tv.delegate = self
		tv.dataSource = self
		
		self.addSubview(tv)
		
		tv.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [tv.topAnchor.constraint(equalTo: self.topAnchor, constant: Constants.topSpacing),
						   tv.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: Constants.bottomSpacing),
						   tv.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Constants.horizontalSpacing),
						   tv.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Constants.horizontalSpacing)]
		
		NSLayoutConstraint.activate(constraints)
		
		return tv
	}
	
	func getOrderItems() -> [OrderItemView.OrderItemType]
	{
		var ret: [OrderItemView.OrderItemType] = cartController.cart.getTotalBySeller().map {
			OrderItemView.OrderItemType.SellerInfo(sellerName: $0.cartSeller.seller.info.caption,
												   positionsCount: $0.cartSeller.cartProducts.count,
												   total: $0.total)
		}
		
		ret.append(OrderItemView.OrderItemType.DeliveryInfo(time: "", price: cartController.cart.getDeliveryCost()))
		
		return ret
	}
	
	func subscribeToCartEvents()
	{
		if cartController  == nil
		{
			self.cartController = CartController.shared
		}
		
		cartController.cartStateObservable.subscribe(onNext: {[weak self] cartState in
			self?.items = self?.getOrderItems() ?? []
			self?.orderItemsTableView?.reloadData()
		}).disposed(by: disposeBag)
	}
}

// MARK:- Utils
extension OrderItemsView: UITableViewDelegate, UITableViewDataSource
{
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return items.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		guard let cell = tableView.dequeueReusableCell(ofType: OrderItemTableViewCell.self) else
		{
			return UITableViewCell()
		}
		
		cell.initialize(items[indexPath.row])
		
		return cell
	}
}

// MARK:- Constants
extension OrderItemsView
{
	enum Constants
	{
		static let height = CGFloat(158)
		static let topSpacing = CGFloat(24)
		static let bottomSpacing = CGFloat(10)
		static let horizontalSpacing = CGFloat(16)
	}
}


