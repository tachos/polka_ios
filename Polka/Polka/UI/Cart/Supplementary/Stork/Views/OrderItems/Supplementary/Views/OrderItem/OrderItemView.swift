//
//  PurchasableItemView.swift
//  Polka
//
//  Created by Michael Goremykin on 24.05.2021.
//

import UIKit

extension OrderItemView
{
	enum OrderItemType
	{
		case SellerInfo(sellerName: String, positionsCount: Int, total: Float)
		case DeliveryInfo(time: String, price: Float)
        case TotalInfo(total: Float)
	}
}

class OrderItemView: UIView
{
    // MARK:- Outlets
    @IBOutlet weak var leftTopLabel: UILabel!
    @IBOutlet weak var leftBottomLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var stickLeftLabelToTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var stickCenterVerticallyLeftLabelConstraint: NSLayoutConstraint!
    
    // MARK:- Parameters
    var orderItemType: OrderItemType!
}

// MARK:- Initialization
extension OrderItemView
{
    func initialize(_ type: OrderItemType)
    {
        self.orderItemType = type
		
		setupUI()
    }
}

// MARK:- Instantiation
extension OrderItemView
{
    static func create(_ type: OrderItemType) -> OrderItemView
    {
        let orderItemView = createView(OrderItemView.self)
        
        orderItemView.initialize(type)
        
        return orderItemView
    }
}

// MARK:- API
extension OrderItemView
{
    func setForType(_ orderItemType: OrderItemType)
    {
        self.orderItemType = orderItemType
        
        setupUI()
    }
}

// MARK:- Constants
extension OrderItemView
{
    func setupUI()
    {
        leftTopLabel.text = getLeftTopTitle()
        leftBottomLabel.text = getLeftBottomTitle()
		priceLabel.text = PriceFormatter.orderItem(getPrice())
        
        setLeftLabelVerticalConstraint()
    }
    
    func setLeftLabelVerticalConstraint()
    {
        let settings = getConstraintSettings()
        
        stickLeftLabelToTopConstraint.isActive = settings.stickToTop
        stickCenterVerticallyLeftLabelConstraint.isActive = settings.stickToCenter
    }
    
    func getConstraintSettings() -> (stickToTop: Bool, stickToCenter: Bool)
    {
        switch orderItemType
        {
        case .DeliveryInfo, .TotalInfo:
            return (stickToTop: false, stickToCenter: true)
            
        case .SellerInfo:
            return (stickToTop: true, stickToCenter: false)
            
        case .none:
            return (stickToTop: false, stickToCenter: false)
        }
    }
    
    func getLeftTopTitle() -> String
    {
        switch orderItemType
        {
        case .DeliveryInfo:
            return "Доставка"
            
        case .SellerInfo(let sellerName, _, _):
            return sellerName
            
        case .TotalInfo:
            return "Стоимость заказа"
            
        case .none:
            return ""
        }
    }
    
    func getLeftBottomTitle() -> String
    {
        switch orderItemType
        {
        case .DeliveryInfo(let time, _):
            return time
            
        case .SellerInfo(_ , let count, _):
            return "\(count) \(count.productString())"
            
        case .TotalInfo:
            return ""
            
        case .none:
            return ""
        }
    }
    
    func getPrice() -> Float
    {
        switch orderItemType
		{
		case .DeliveryInfo(_ , let price), .SellerInfo(_, _, let price), .TotalInfo(let price):
            return price
			
        default:
            return 0
        }
    }
}

// MARK:- Utils
extension Int
{
    func productString() -> String
    {
        var ending = ""
        switch self
        {
        case 1:
            ending = ""
            
        case 2...4:
            ending = "а"
        
        default:
            ending = "ов"
        }
        
        return "товар\(ending)"
    }
}
    
// MARK:- Constants
extension OrderItemView
{
    enum Constants
    {
        static let  height = CGFloat(34)
    }
}
