//
//  OrderHeaderView.swift
//  Polka
//
//  Created by Michael Goremykin on 25.05.2021.
//

import RxSwift

class OrderHeaderViewForCollapsedState: UIView
{
	// MARK:- Outlets
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var collapseControlButton: UIButton!
	
	// MARK:- RX
	let disposeBag = DisposeBag()
}

// MARK:- UI handlers
extension OrderHeaderViewForCollapsedState
{
    @IBAction func onRightButtonTapped(_ sender: Any)
    {
		(self.superview?.superview?.superview as? BiDirectionalScrollable)?.scrollToBottom()
    }
}

// MARK:- Life cycle
extension OrderHeaderViewForCollapsedState
{
	override func awakeFromNib()
	{
		super.awakeFromNib()
		
		subscribeToCartEvents()
		
		setRoundedCorners()
		setShadow()
	}
	
	override func layoutSubviews()
	{
		super.layoutSubviews()
		
		titleLabel.text = PriceFormatter.cartTotal(CartController.shared.cart.getTotal())
	}
}

// MARK:- Initialization
extension OrderHeaderViewForCollapsedState
{
	func initilize(_ total: Float)
	{
		titleLabel.text = PriceFormatter.cartTotal(total)
	}
}

// MARK:- Instantiation
extension OrderHeaderViewForCollapsedState
{
	static func create(_ total: Float) -> OrderHeaderViewForCollapsedState
	{
		let orderHeaderView = createView(OrderHeaderViewForCollapsedState.self)
		
		orderHeaderView.initilize(total)
		
		return orderHeaderView
	}
}

// MARK:- Utils
extension OrderHeaderViewForCollapsedState
{
	func subscribeToCartEvents()
	{
		CartController.shared.cartStateObservable.subscribe(onNext: {[weak self] cartState in
			guard let self = self else { return }
			
			self.titleLabel.text = PriceFormatter.cartTotal(cartState.cart.getTotal())
		}).disposed(by: disposeBag)
	}
	
	func setShadow()
	{
		self.clipsToBounds = false
		self.layer.shadowColor = UIColor.gray.cgColor
		self.layer.shadowOffset = CGSize(width: 0, height: -40)
		self.layer.shadowOpacity = 0.1
		self.layer.shadowRadius = 40.0
	}
	
	func setRoundedCorners()
	{
		self.clipsToBounds 			= true
		self.layer.cornerRadius 	= Constants.cornderRadius
		self.layer.maskedCorners	= [.layerMinXMinYCorner, .layerMaxXMinYCorner]
	}
}

// MARK:- Constants
extension OrderHeaderViewForCollapsedState
{
	enum Constants
	{
		static let height 			= CGFloat(80)
		static let cornderRadius 	= CGFloat(20)
	}
}

