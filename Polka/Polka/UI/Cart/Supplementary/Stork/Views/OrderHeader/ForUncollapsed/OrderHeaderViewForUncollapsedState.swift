//
//  OrderHeaderViewForUncollapsedState.swift
//  Polka
//
//  Created by Michael Goremykin on 28.05.2021.
//

import UIKit

class OrderHeaderViewForUncollapsedState: UIView
{
	
}

// MARK:- UI handlers
extension OrderHeaderViewForUncollapsedState
{
    @IBAction func onRightButtonTapped(_ sender: Any)
    {
		(self.superview?.superview?.superview as? BiDirectionalScrollable)?.scrollToTop()
    }
}

// MARK:- Life cycle
extension OrderHeaderViewForUncollapsedState
{
	override func awakeFromNib()
	{
		super.awakeFromNib()
		
		setRoundedCorners()
		setShadow()
	}
}

// MARK:- Instantiation
extension OrderHeaderViewForUncollapsedState
{
	static func create() -> OrderHeaderViewForUncollapsedState
	{
        let ret = createView(OrderHeaderViewForUncollapsedState.self)
        
		return ret
	}
}

// MARK:- Utils
extension OrderHeaderViewForUncollapsedState
{
	func setShadow()
	{
		self.clipsToBounds = false
		self.layer.shadowColor = UIColor.gray.cgColor
		self.layer.shadowOffset = CGSize(width: 0, height: -40)
		self.layer.shadowOpacity = 0.1
		self.layer.shadowRadius = 40.0
	}
	
	func setRoundedCorners()
	{
		self.clipsToBounds 			= true
		self.layer.cornerRadius 	= Constants.cornderRadius
		self.layer.maskedCorners	= [.layerMinXMinYCorner, .layerMaxXMinYCorner]
	}
}

// MARK:- Constants
extension OrderHeaderViewForUncollapsedState
{
	enum Constants
	{
		static let height 			= CGFloat(80)
		static let cornderRadius 	= CGFloat(20)
	}
}
