//
//  AddressPicker.swift
//  Polka
//
//  Created by Michael Goremykin on 24.05.2021.
//

import RxSwift

enum AddressSelectionState
{
	case selected(Address)
	case notSelected
}

class AddressPickerView: UIView
{
	// MARK:- Outlets
	weak var addressView: AddressView!
	
	// MARK:- Parameters
	var selectionState: AddressSelectionState!
	var onWhereBring: VoidCallback!
	var parentViewController: UIViewController!
	
	init()
	{
		super.init(frame: .zero)
		
		subscribeToUserEvents()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	// MARK:- Rx
	let disposeBag = DisposeBag()
}

// MARK:- Setup UI
extension AddressPickerView
{
	func setupAddresspicker()
	{
		if addressView == nil
		{
			addressView = createAndEmbedAdressView(Address.addressLbl(),
													"edit",
													Address.getAddressConsumer(),
													.availableTapGesture,
													{ self.onWhereBring?() })
		}
	}
	
	func createAndEmbedAdressView(_ tipText: String,
									_ iconName: String,
									_ placeholderText: String,
									_ type: AddressViewLogic,
									_ onTapped:@escaping () -> Void) -> AddressView
	{
		self.backgroundColor = .white
		
		let ret: AddressView = createView(AddressView.self)
		
		ret.initialize(nameLbl: tipText, image: iconName, text: placeholderText , type: type, onPress: onTapped)
		
		self.addSubview(ret)
		
		ret.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [ret.topAnchor.constraint(equalTo: self.topAnchor),
						   ret.bottomAnchor.constraint(equalTo: self.bottomAnchor),
						   ret.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Constants.horizontalSpacing),
						   ret.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Constants.horizontalSpacing)]
		
		NSLayoutConstraint.activate(constraints)
		
		return ret
	}
	
	func setupAdressViewAppearnce(_ addressView: AddressView)
	{
		addressView.setupAppearance(nameLbl: "Укажите адрес доставки",
									image: "edit",
									text: "Адрес",
									type: .availableTapGesture,
									onPress:  { print("Picking address") } )
	}
}
// MARK:- Instantiation
extension AddressPickerView
{
	func initialize(_ parentViewController: UIViewController, _ selectionState: AddressSelectionState,onWhereBring: @escaping VoidCallback)
	{
		self.selectionState = selectionState
		self.onWhereBring = onWhereBring
		self.parentViewController = parentViewController
		setupAddresspicker()
	}
}

// MARK:- Instantiation
extension AddressPickerView
{
	static func create(_ parentViewController: UIViewController, _ selectionState: AddressSelectionState, onWhereBring: @escaping VoidCallback) -> AddressPickerView
	{
		let addressPickerView = AddressPickerView()
		
		addressPickerView.initialize(parentViewController, selectionState,onWhereBring: onWhereBring)
		
		return addressPickerView
	}
}

// MARK:- Constants
extension AddressPickerView
{
	override func layoutSubviews() {
		addressView.streetAndHouseLbl.text = Address.addressLbl()
		addressView.streetAndHouseField.text = Address.getAddressConsumer()
	}
}

// MARK:- AAAAA TOREMOVE
extension AddressPickerView
{
	// AAAAA TOREMOVE
	func setupAddressView()
	{
		addressView?.initialize(nameLbl: Address.addressLbl(),
							   image: "edit",
							   text: Address.getAddressConsumer(),
							   type: .availableTapGesture,
							   onPress: {[weak self] in
								WhereBringStory.start(window: nil, viewController: self?.parentViewController, titleBtn: "Вернуться на экран заказов")
							   })
	}
	
	func subscribeToUserEvents()
	{
		UserController.shared.userLocalEventsObservable.subscribe(onNext: {[weak self] userEvent in
			guard let self = self else { return }
			switch userEvent
			{
			case .newDeliveryAddressHasBeenSet(_):
				self.setupAddressView()
			}
		}).disposed(by: disposeBag)
	}
}

// MARK:- Constants
extension AddressPickerView
{
	enum Constants
	{
		static let height = CGFloat(56)
		static let horizontalSpacing = CGFloat(16)
	}
}
