//
//  TopViewTableViewCell.swift
//  Polka
//
//  Created by Michael Goremykin on 24.05.2021.
//

import UIKit

class TopViewTableViewCell: UITableViewCell
{
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collapseControlButton: UIButton!
    
}

// MARK:- UI handlers
extension TopViewTableViewCell
{
    @IBAction func onCollapseButtonTapped(_ sender: Any)
    {
        
    }
}
