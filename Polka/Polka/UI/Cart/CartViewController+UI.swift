//
//  CartViewController+UI.swift
//  Polka
//
//  Created by Michael Goremykin on 05.05.2021.
//

import UIKit

// MARK:- Setup UI
extension CartViewController
{
	func setupUI()
	{
		self.view.backgroundColor = .white
		
		embedEmptyView(false)
		
		embedTableView(createTableView())
		createAndEmbedStork()
	}
	
	func showCartTableViewWithStork()
	{
		emptyCartView.isHidden = true
		cartTableView.isHidden = false
		storkView.isHidden = false
	}
	
	func showEmptyCartView()
	{
		emptyCartView.isHidden = false
		cartTableView.isHidden = true
		storkView.isHidden = true
	}
}

// MARK:- Cart stork
extension CartViewController
{
	func createAndEmbedStork()
	{
		let storkView = createStork(getStorkSize())
		
		self.view.addSubview(storkView)
		
		storkView.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [storkView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
						   storkView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
						   storkView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
						   storkView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor)]
		
		NSLayoutConstraint.activate(constraints)
		
		self.storkView = storkView
		
		self.storkView.isHidden = true
	}
	
	private func createStork(_ size: CGSize) -> StorkView
	{
		return StorkView.create(availableSpaceForStork: size,
								headerViewForCollapsedState: (createOrderHeaderViewForCollapsedState(), OrderHeaderViewForCollapsedState.Constants.height),
								headerViewForUncollapsedState: (createOrderHeaderViewForUncollapsedState(), OrderHeaderViewForUncollapsedState.Constants.height),
								viewsToDisplayWithHeights: [(creatAddressPickerView(onWhereBring: {self.navigationToWhereBringContoller()}), AddressPickerView.Constants.height),
															(createOrderItemsView(), OrderItemsView.Constants.height),
															(createProceedToPaymentView(), ProceedToPaymentView.Constants.height)])
	}
	private func  navigationToWhereBringContoller()
	{
		WhereBringStory.start(window: nil, viewController: self, titleBtn: "Вернуться в корзину")
	}
	private func getStorkSize() -> CGSize
	{
		let delta = (getStatusBarHeight() ?? 0) + (getTabBarHeight() ?? 0) + (getBottomSafeAreaHeight() ?? 0)
		
		return CGSize(width: self.view.frame.width,
					  height: UIScreen.main.bounds.height - delta)
	}
	
	private func createOrderHeaderViewForCollapsedState() -> OrderHeaderViewForCollapsedState
	{
		return OrderHeaderViewForCollapsedState.create(cartController.cart.getTotal())
	}
	
	private func createOrderHeaderViewForUncollapsedState() -> OrderHeaderViewForUncollapsedState
	{
		return OrderHeaderViewForUncollapsedState.create()
	}
	
	private func creatAddressPickerView(onWhereBring: @escaping VoidCallback) -> AddressPickerView
	{
		return AddressPickerView.create(self, .notSelected , onWhereBring: onWhereBring)
	}
	
	private func createOrderItemsView() -> OrderItemsView
	{
		return OrderItemsView.create()
	}
	
	private func createProceedToPaymentView() -> ProceedToPaymentView
	{
		return ProceedToPaymentView.create(
			paymentOptionState: ProceedToPaymentView.getPaymentOptionState(),
			total: cartController.cart.getTotal(),
			onPaymentTapped: { [weak self] in self?.pay() }
		)
	}
	
	private func pay()
	{
		paymentHelper.pay(self)
	}
	
	func onSuccessfullyCreatedOrder()
	{
		cartController.synchronizeWithBackend()
	}
}

// MARK:- Empty cart view
extension CartViewController
{
	func embedEmptyView(_ isHidden: Bool = true)
	{
		let viewToEmbed = EmptyCartView.create({[weak self] in
			if let tabBarController = self?.getTabBarController()
			{
				tabBarController.switchToTab(.catalog)
			}
		})
		
		self.view.addSubview(viewToEmbed)
		
		viewToEmbed.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [viewToEmbed.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
						   viewToEmbed.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
						   viewToEmbed.topAnchor.constraint(equalTo: self.view.topAnchor),
						   viewToEmbed.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
		
		NSLayoutConstraint.activate(constraints)
		
		viewToEmbed.isHidden = isHidden
		
		self.emptyCartView  = viewToEmbed
	}
}

// MARK:- UITableView
extension CartViewController
{
	func createTableView() -> UITableView
	{
		let tableView = UITableView(frame: .zero, style: .grouped)
		
		tableView.delegate   = self
		tableView.dataSource = self
		
		tableView.showsVerticalScrollIndicator = false
		
		tableView.backgroundColor = .white
		
		tableView.separatorStyle = .none
		
		tableView.sectionFooterHeight = 0
		
		tableView.contentInset = UIEdgeInsets(top: 0,
											  left: 0,
											  bottom: TabBarView.Constants.tabBarHeight + 40,
											  right: 0)
		
		tableView.registerNibCell(CartProductTableViewCell.self)
		tableView.registerNibHeaderFooterView(CartSellerSectionView.self)
		
		tableView.tableHeaderView = CartHeaderView.create(
			market: nil,//TODO: store market id for each product
			self.view.frame.width,
			{ [weak self] in self?.clearCart()
				
			})
		
		return tableView
	}
	
	func embedTableView(_ tableView: UITableView,  _ isHidden: Bool = true)
	{
		tableView.isHidden = isHidden
		
		self.view.addSubview(tableView)
		
		tableView.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
						   tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
						   tableView.topAnchor.constraint(equalTo: self.view.topAnchor),
						   tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
		
		NSLayoutConstraint.activate(constraints)
		
		self.cartTableView = tableView
	}
}

// MAR:- UITableViewDelegate, UITableViewDataSource
extension CartViewController: UITableViewDelegate, UITableViewDataSource
{
	func scrollViewDidScroll(_ scrollView: UIScrollView)
	{
		if scrollView.contentOffset.y > (getNavBarHeight() ?? 0) && !navBarVisibilityToggled
		{
			(self.navigationController as? NavigationController)?.toggleVisibility()
			
			navBarVisibilityToggled = true
		}
		else if lastContentOffset > scrollView.contentOffset.y &&
					scrollView.contentOffset.y < (getNavBarHeight() ?? 0)  && navBarVisibilityToggled
		{
			(self.navigationController as? NavigationController)?.toggleVisibility()
			
			navBarVisibilityToggled = false
		}

		lastContentOffset = scrollView.contentOffset.y
	}

	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return cartController.cart.sellers[section].cartProducts.count
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
	{
		// AAAAA TOREMOVE
		if sectionsHeights.count == 0
		{
			sectionsHeights = getSectionsHeights()
		}
		
		return sectionsHeights[section]
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
	{
		guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CartSellerSectionView.typeName) as? CartSellerSectionView else
		{
			return nil
		}
		
		guard let seller = getSellerForSection_safe(section: section).warnIfNil()
		else {
			return nil
		}
		
		headerView.initialize(
			displayCellTopCornersRounded: section != 0,
			isWorking: seller.info.isOpen,
			title: seller.info.caption,
			description: seller.info.description.isEmpty ? " " : seller.info.description,
			onCatalogButton: { [weak self] in self?.showCatalog(ofSeller: seller) },
			onChatWithBtnTappedCallback: { [weak self] in self?.showChat(withSeller: seller) },
			onClearBtnTappedCallback: { [weak self] in self?.removeAllSellersProducts(forSellerWithId: seller.id)}
		)
		
		return headerView
	}
	
	func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat
	{
		return .leastNormalMagnitude
	}
	
	func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
	{
		return UIView(frame: CGRect(origin: .zero, size: CGSize(width: 0, height: 0)))
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
	{
		return CartProductTableViewCell.Constants.height
	}
	
	func numberOfSections(in tableView: UITableView) -> Int
	{
		return cartController.cart.sellers.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		guard let cell = tableView.dequeueReusableCell(ofType: CartProductTableViewCell.self) else
		{
			return UITableViewCell()
		}
		
		/// if the cart is inactive, the table view might attempt to deque a cell for section that doesn't exist (w/o calling numberOfSections())
		guard let cartProduct = getCartProductForCell_safe(at: indexPath).warnIfNil()
		else
		{
			return UITableViewCell()
		}
		
		let seller = getSellerForSection(section: indexPath.section)
		
		cell.initialize(purchasability: getProductPurchasabilityForCell(at: indexPath),
						cartProduct: cartProduct,
						onItemClicked: { },
						onItemQuantityChanged: {[weak self] (value) in
							guard let self = self else { return }
							
							self.onProductQuantityChanged(value,
														  fromSeller: seller,
														  forProduct: cartProduct.product,
														  stockQuantity: cartProduct.stockQuantity)
						},
						onSetToZeroAdditionalAction: {[weak self] in
							self?.cartController.remove(productWithId: cartProduct.product.id,
														fromSellerWithId: seller.id)
						})
		
		return cell
	}
}

// MARK:- Constants
extension CartViewController
{
	enum Constants
	{
		static let headerViewHeight		= CGFloat(120)
		static let horizontalSpacing	= CGFloat(16)
		static let collapsedCartHeight	= CGFloat(84)
	}
}
