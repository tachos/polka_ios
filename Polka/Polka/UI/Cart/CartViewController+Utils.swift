//
//  CartViewController+Utils.swift
//  Polka
//
//  Created by Michael Goremykin on 05.05.2021.
//

import UIKit


// MARK:- Data obtaining
extension CartViewController
{
	func getCartContents()
	{
		cartController.synchronizeWithBackend()
	}
}

// MARK:- Utils navigation
extension CartViewController
{
	func setNavBarAppearance()
	{
		(self.navigationController as? NavBarCustomizable)?.setAppearance(navBarBackground: .blurred,
																		hasBackButton: false,
																		titleViewType: .tabTitleHeader("Корзина"),
																		initiallyNotVisible: lastContentOffset < (getNavBarHeight() ?? 0))
	}
}

// MARK:- Utils rx
extension CartViewController
{
	func subscribeToCartObtainingEvents()
	{
		cartController.cartObtainingObservable.subscribe(onNext: {[weak self] _ in
			self?.progressIndicator?.hide(animated: true)
		}).disposed(by: disposeBag)
	}
	
	func subscribeToCartEvents()
	{
		cartController.cartStateObservable.subscribe(onNext: {[weak self] cart in self?.onCartStateChanged(cart)})
										  .disposed(by: disposeBag)
	}
	
	func onCartStateChanged(_ cartState: CartState)
	{
		switch cartState
		{
		case .local, .synced:
			handleSuccess(cartState)
			
		case .corrected(_, let error):
			handleFailure(error)
		}
	}
	
	func handleSuccess(_ cartState: CartState)
	{
		if cartState.cart.isEmpty()
		{
			showEmptyCartView()
			cartTableView.reloadData()
		}
		else
		{
			showCartTableViewWithStork()

			sectionsHeights = getSectionsHeights()

			productsPurchasability = getProductsPurchasability(cartController.cart)

			cartTableView.reloadData()
		}
	}
	
	func handleFailure(_ error: Error)
	{
		cartTableView.reloadData()
		
		if let cartUpdateError = error as? WebServiceManager.CartUpdateError
		{
			switch cartUpdateError
			{
			case .cannotPutProductsFromDifferentMarkets:
				PopUps.showCannotAddToCartProductsFromDifferentMarketsError(
					from: self,
					clearCartAndAddProduct: { [weak self] in
						//
						self?.cartController.removeAll()
					}
				)
			}
		}
		else
		{
			handleUnknownCartUpdateError()
		}
	}
	
	private func handleUnknownCartUpdateError()
	{
		PopUps.showTwoButtonPopUp(
			from: self,
			title: "Ошибка при получении данных",
			message: "Попробуйте повторить попытку через некоторое время.",
			firstButtonTitle: "Повторить",
			onFirstButton: {[weak self] in self?.getCartContents() },
			onSecondButton: {[weak self] in self?.showEmptyCartView() }
		)
	}
}

// MARK:- Utils cells
extension CartViewController
{
	func getSectionsHeights() -> [CGFloat]
	{
		return cartController.cart.sellers.enumerated().map { (index, cartSeller) in
			return CartSellerSectionView.getHeight(index, !cartSeller.seller.info.isOpen)
		}
	}

	func getCartProductForCell_safe(at indexPath: IndexPath) -> CartProduct?
	{
		if indexPath.section >= cartController.cart.sellers.count {
			return nil
		}

		let cartSeller = cartController.cart.sellers[indexPath.section]
		if indexPath.row >= cartSeller.cartProducts.count {
			return nil
		}

		return cartSeller.cartProducts[indexPath.row]
	}
	
	func getCartProductForCell(at indexPath: IndexPath) -> CartProduct
	{
		dbgcheck(indexPath.section < cartController.cart.sellers.count)
		let cartSeller = cartController.cart.sellers[indexPath.section]
		
		dbgcheck(indexPath.row < cartSeller.cartProducts.count)
		return cartSeller.cartProducts[indexPath.row]
	}
	
	func getSellerForSection_safe(section sectionIndex: Int) -> Seller?
	{
		if sectionIndex < cartController.cart.sellers.count {
			return cartController.cart.sellers[sectionIndex].seller
		}
		
		return nil
	}
	
	func getSellerForSection(section sectionIndex: Int) -> Seller
	{
		return cartController.cart.sellers[sectionIndex].seller
	}
	
	func getProductPurchasabilityForCell(at indexPath: IndexPath) -> Purchasability
	{
		// AAAAA TOREMOVE
		if productsPurchasability.count == 0
		{
			productsPurchasability = getProductsPurchasability(cartController.cart)
		}
		
		return productsPurchasability[indexPath.section][indexPath.row]
	}
	
	func getProductsPurchasability(_ cart: Cart) -> [[Purchasability]]
	{
		return cart.sellers.map { seller in
			return seller.cartProducts.map { product in
				getCartProductPurchasability(seller, product)
			}
		}
	}
	
	private func getCartProductPurchasability(_ cartSeller: CartSeller, _ cartProduct: CartProduct) -> Purchasability
	{
		var reasons: Set<Purchasability.Reason> = []
		
		if !isPurchasingDuringWorkingHours(cartSeller) { reasons.insert(.sellerClosed) }
		
		if reasons.isEmpty
		{
			return .purchasable
		}
		else
		{
			return .notPurchasable(reasons)
		}
	}
	
	private func isPurchasingDuringWorkingHours(_ cartSeller: CartSeller) -> Bool
	{
		return cartController.cart.marketIsOpen && cartSeller.seller.info.isOpen
	}
	
	private func isLeftInStock(_ cartProduct: CartProduct) -> Bool
	{
		if case .available(_) = cartProduct.stockAvailability
		{
			return true
		}
		else
		{
			return false
		}
	}
}

// MARK:- Utils Product UI handlers
extension CartViewController
{	
	func showCatalog(ofSeller seller: Seller)
	{
		// TODO: refactor. Perform segue passed from parent Story
		SellerStory.startInTab(seller: seller)
	}
	
	func showChat(withSeller seller: Seller)
	{
		ChatStory.startInTab(
			receiver: .seller(seller)
		)
	}
	
	func removeAllSellersProducts(forSellerWithId sellerId: String)
	{
		//print("Remove all products for seller  with id - \(sellerId)")
		self.cartController.removeAll(withSellerId: sellerId)
	}
	
	func clearCart()
	{
		cartController.removeAll()
		
		showEmptyCartView()
	}
}

// MARK:- Utils Product UI handlers
extension CartViewController
{
	func onProductQuantityChanged(_ value: Float,
								  fromSeller seller: Seller,
								  forProduct product: Product,
								  stockQuantity: Float)
	{
		self.cartController.set(quantity: value,
								forProduct: product,
								stockQuantity: stockQuantity,
								fromSeller: seller)
	}
}
