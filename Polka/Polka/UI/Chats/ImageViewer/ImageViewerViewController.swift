//
//  ImageViewerViewController.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 28.06.2021.
//

import UIKit
import TinyConstraints
import SDWebImage



class ImageViewerViewController: UIViewController
{
	private var imageView: UIImageView!

	var config: Config!

	struct Config
	{
		let image: Image
		let dismissSegue: VoidCallback
	}
}



// MARK: - Embed in Navigation

extension ImageViewerViewController
{
	func embeddedInNavigation() -> UINavigationController
	{
		let navigationController = UINavigationController(rootViewController: self)

		navigationController.navigationBar.isTranslucent = true
		navigationController.navigationBar.backgroundColor = .white
		navigationController.modalPresentationStyle = .fullScreen

		return navigationController
	}
}



// MARK: - UIViewController

extension ImageViewerViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()

		view.backgroundColor = .black

		let imageView = UIImageView()
		imageView.contentMode = .scaleAspectFit
		imageView.backgroundColor = .black

		view.addSubview(imageView)
		imageView.edgesToSuperview(usingSafeArea: true)

		self.imageView = imageView
	}

	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)

		navigationItem.leftBarButtonItem = .init(
			title: "Закрыть",
			style: .done,
			target: self,
			action: #selector(onDoneButton)
		)

		switch config.image
		{
		case .local(let image):
			imageView.image = image

		case .remote(let image):
			imageView.sd_setImage(with: image.original)
		}
	}
}



// MARK: - Handle UI

private extension ImageViewerViewController
{
	@objc func onDoneButton()
	{
		config.dismissSegue()
	}
}
