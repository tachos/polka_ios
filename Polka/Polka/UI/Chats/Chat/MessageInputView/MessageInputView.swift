//
//  MessageInputView.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 03.06.2021.
//

import UIKit


class MessageInputView: UIView
{
	@IBOutlet var mainView: UIView!
	@IBOutlet var leftButton: UIButton!
	@IBOutlet var textViewWrapper: InputTextViewWrapper!
	@IBOutlet var rightButton: UIButton!

	var config: Config?

	struct Config
	{
		let onDidBeginEditing: VoidCallback
		let onTextEntered: Handler<String>
		let onAttachButton: VoidCallback
	}

	required init?(coder: NSCoder)
	{
		super.init(coder: coder)

		setup()
	}
}



// MARK: - Setup

private extension MessageInputView
{
	func setup()
	{
		addNibView()
		setupShadow()
		setupInputTextView()
	}

	func addNibView()
	{
		let nibView = createView(
			nibName: Self.typeName,
			bundle: nil,
			owner: self
		)

		nibView.frame = self.bounds

		nibView.translatesAutoresizingMaskIntoConstraints = true
		nibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

		addSubview(nibView)
	}

	func setupShadow()
	{
		clipsToBounds = false
		backgroundColor = .clear

		layer.shadowColor = UIColor.black.cgColor
		layer.shadowRadius = 24
		layer.shadowOpacity = 0.1
		layer.shadowOffset = .init(width: 0, height: 4)
	}

	func setupInputTextView()
	{
		textViewWrapper.setup()
		textViewWrapper.textView.delegate = self
	}
}



// MARK: - Manage UI

extension MessageInputView
{
	func updatePlaceholderVisibility()
	{
		textViewWrapper.placeholderLabel.isHidden = !textViewWrapper.textView.text.isEmpty
	}
}



// MARK: - Handle UI

private extension MessageInputView
{
	@IBAction func onAttachButton()
	{
		config?.onAttachButton()
	}

	@IBAction func onSendButton()
	{
		let text: String = textViewWrapper.textView.text
			.trimmingCharacters(in: .whitespacesAndNewlines)

		if text.isEmpty
		{
			return
		}

		config?.onTextEntered(text)

		textViewWrapper.textView.text = ""
		textViewWrapper.textView.resignFirstResponder()
		textViewWrapper.onTextChanged()
	}
}



// MARK: - UITextViewDelegate

extension MessageInputView: UITextViewDelegate
{
	func textViewDidBeginEditing(_ textView: UITextView)
	{
		config?.onDidBeginEditing()
	}

	func textViewDidChange(_ textView: UITextView)
	{
		textViewWrapper.onTextChanged()

		updatePlaceholderVisibility()
	}

	func textViewDidEndEditing(_ textView: UITextView)
	{
		updatePlaceholderVisibility()
	}

	func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
	{
		let resultingText = (textView.text as NSString)
			.replacingCharacters(
				in: range,
				with: text
			)

		return resultingText.count <= Constants.maxCharactersCount
	}

	func textViewDidChangeSelection(_ textView: UITextView)
	{
		textViewWrapper.onSelectionChanged()
	}
}



// MARK: - Constants

private extension MessageInputView
{
	struct Constants
	{
		static let inputTextViewMinHeight = CGFloat(18)
		static let inputTextViewVerticalInset = CGFloat(19)
		static let maxCharactersCount = 10_000
	}
}



// MARK: - InputTextViewWrapper

class InputTextViewWrapper: UIScrollView
{
	// MARK: Text Selection
	private var lastSelectionStart: CGRect?
	private var lastSelectionEnd: CGRect?

	// MARK: Content Size
	private var textViewSizeObserver: NSKeyValueObservation?

	// MARK: Outlets
	@IBOutlet var textView: UITextView!
	@IBOutlet var placeholderLabel: UILabel!

	override var canBecomeFirstResponder: Bool
	{
		return true
	}

	override func becomeFirstResponder() -> Bool
	{
		return textView?.becomeFirstResponder() ?? false
	}

	override var intrinsicContentSize: CGSize
	{
		guard let textView = textView
		else
		{
			return .zero
		}

		var size = textView.sizeThatFits(
			CGSize(
				width: frame.width,
				height: .greatestFiniteMagnitude
			)
		)

		size.height = max(
			MessageInputView.Constants.inputTextViewMinHeight,
			size.height
		)

		size.height += 2 * MessageInputView.Constants.inputTextViewVerticalInset

		return size
	}

	func setup()
	{
		setupTextView()

		invalidateIntrinsicContentSize()

		textViewSizeObserver = textView?.observe(
			\.contentSize,
			options: .new,
			changeHandler:
		{
			[weak self] _, _ in

			self?.invalidateIntrinsicContentSize()
		})
	}

	private func setupTextView()
	{
		textView.textContainerInset = .zero
		textView.textContainer.lineFragmentPadding = 0
		textView.textContainer.maximumNumberOfLines = 0
	}

	func onTextChanged()
	{
		invalidateIntrinsicContentSize()

		if let range = textView.selectedTextRange
		{
			var rect = textView.caretRect(
				for: range.start
			)

			rect.size.height += MessageInputView
				.Constants
				.inputTextViewVerticalInset
			rect.origin.x = 0
			rect.size.width = bounds.width

			if rect.origin.y.isInfinite
			{
				DispatchQueue.main.async
				{
					rect.origin.y = self.contentSize.height
						- rect.size.height

					self.scrollRectToVisible(rect, animated: false)
				}
			}
			else
			{
				scrollRectToVisible(rect, animated: false)
			}
		}
	}

	func onSelectionChanged()
	{
		if let range = textView.selectedTextRange
		{
			var startRect = textView.caretRect(
				for: range.start
			)

			startRect.origin.y -= MessageInputView
				.Constants
				.inputTextViewVerticalInset
			startRect.origin.x = 0
			startRect.size.height += 3 * MessageInputView
				.Constants
				.inputTextViewVerticalInset
			startRect.size.width = bounds.width

			var endRect = textView.caretRect(
				for: range.end
			)

			endRect.origin.y -= MessageInputView
				.Constants
				.inputTextViewVerticalInset
			endRect.origin.x = 0
			endRect.size.height += 3 * MessageInputView
				.Constants
				.inputTextViewVerticalInset
			endRect.size.width = bounds.width

			if self.lastSelectionStart != startRect
			{
				scrollRectToVisible(
					startRect,
					animated: true
				)
			}
			else if self.lastSelectionEnd != endRect
			{
				scrollRectToVisible(
					endRect,
					animated: true
				)
			}

			self.lastSelectionStart = startRect
			self.lastSelectionEnd = endRect
		}
		else
		{
			lastSelectionStart = nil
			lastSelectionEnd = nil
		}
	}
}
