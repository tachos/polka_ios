//
//  OutgoingMessageCell.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 04.06.2021.
//

import UIKit



class OutgoingMessageCell: UICollectionViewCell, MessageIdentifiable
{
	// MARK: Outlets
	@IBOutlet var bubbleView: UIView!
	@IBOutlet var bubbleTrailing: NSLayoutConstraint!
	@IBOutlet var bubbleBottom: NSLayoutConstraint!
	@IBOutlet var mainTextView: UITextView!
	@IBOutlet var textViewHeight: NSLayoutConstraint!
	@IBOutlet var textViewWidth: NSLayoutConstraint!
	@IBOutlet var timeLabel: UILabel!
	@IBOutlet var notDeliveredLabel: UILabel!
	@IBOutlet var notDeliveredAlert: UIButton!
	@IBOutlet var retryButton: UIButton!

	// MARK: Callbacks
	private var handleRetryButton: VoidCallback?

	// MARK: MessageIdentifiable
	private(set) var messageId: Message.Id = ""

	// MARK: AwakeFromNib
	override func awakeFromNib()
	{
		super.awakeFromNib()

		MessageCell.setupTextView(mainTextView)
	}
}



// MARK: - Setup

extension OutgoingMessageCell
{
	func setup(
		_ message: Message,
		text: String,
		status: ChatHelper.OutgoingMessageStatus?,
		handleRetryButton: VoidCallback?
	)
	{
		// store
		self.handleRetryButton = handleRetryButton
		self.messageId = message.id

		// setup UI
		setupUI(
			message,
			text: text,
			failedToDeliver: status == .failed
		)
	}

	private func setupUI(
		_ message: Message,
		text: String,
		failedToDeliver: Bool
	)
	{
		// text container
		let trailing = failedToDeliver
			? Constants.largeBubbleTrailing
			: Constants.normalBubbleTrailing

		MessageCell.setText(
			text,
			color: .white,
			in: mainTextView,
			heightConstraint: textViewHeight,
			widthConstraint: textViewWidth,
			maxWidth: UIScreen.main.bounds.width
				- Constants.maxLeftTextViewInset
				- trailing
				- Constants.rightTextVeiwInset
		)

		// other constraints
		bubbleTrailing.constant = trailing
		bubbleBottom.constant = failedToDeliver
			? Constants.largeBubbleBottom
			: 0

		notDeliveredAlert.isHidden = !failedToDeliver
		notDeliveredLabel.isHidden = !failedToDeliver
		retryButton.isHidden = !failedToDeliver

		// time
		timeLabel.text = DatesFormatter.toMessage(message.date)
	}
}



// MARK: - Handle UI

private extension OutgoingMessageCell
{
	@IBAction func onRetryButton()
	{
		handleRetryButton?()
	}
}



// MARK: - Constants

private extension OutgoingMessageCell
{
	struct Constants
	{
		static let largeBubbleBottom = CGFloat(24)
		static let normalBubbleTrailing = CGFloat(32)
		static let largeBubbleTrailing = CGFloat(48)
		static let rightTextVeiwInset = CGFloat(16)
		static let maxLeftTextViewInset = CGFloat(74)
	}
}
