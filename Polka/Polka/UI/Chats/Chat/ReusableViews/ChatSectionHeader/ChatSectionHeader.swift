//
//  ChatSectionHeader.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 04.06.2021.
//

import UIKit


class ChatSectionHeader: UICollectionReusableView
{
	@IBOutlet var dateLabel: UILabel!
}



extension ChatSectionHeader
{
	func setup(_ date: Date)
	{
		dateLabel.text = DatesFormatter.toMessageGroup(date)
	}
}
