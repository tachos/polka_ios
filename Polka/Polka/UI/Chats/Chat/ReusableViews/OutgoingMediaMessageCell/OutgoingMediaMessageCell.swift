//
//  OutgoingMediaMessageCell.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 07.06.2021.
//

import UIKit



class OutgoingMediaMessageCell: UICollectionViewCell, MessageIdentifiable
{
	// MARK: Outlets
	@IBOutlet var attachmentView: AttachmentsView!
	@IBOutlet var attachmentViewTrailing: NSLayoutConstraint!
	@IBOutlet var attachmentViewBottom: NSLayoutConstraint!
	@IBOutlet var timeLabel: UILabel!
	@IBOutlet var notDeliveredLabel: UILabel!
	@IBOutlet var notDeliveredAlert: UIButton!
	@IBOutlet var retryButton: UIButton!

	// MARK: Callbacks
	private var handleRetryButton: VoidCallback?

	// MARK: MessageIdentifiable
	private(set) var messageId: Message.Id = ""
}



// MARK: - Setup

extension OutgoingMediaMessageCell
{
	func setup(
		_ message: Message,
		attachments: [Attachment],
		status: ChatHelper.OutgoingMessageStatus?,
		expanded: Bool,
		handleAttachmentTapped: Handler<Attachment>?,
		handleRetryButton: VoidCallback?,
		handleMoreButton: VoidCallback?
	)
	{
		// store
		self.messageId = message.id
		self.handleRetryButton = handleRetryButton

		// setup
		let failedToDeliver = status == .failed

		attachmentView.setup(
			attachments,
			expanded: expanded,
			handleAttachmentTapped: handleAttachmentTapped ?? { _ in },
			handleMoreButton: handleMoreButton ?? {}
		)

		let rightInset = failedToDeliver
			? Constants.largeRightAttachmentsViewInset
			: Constants.normalRightAttachmentsViewInset

		attachmentViewTrailing.constant = rightInset
		attachmentViewBottom.constant = failedToDeliver
			? Constants.largeAttachmentsViewBottom
			: 0

		notDeliveredAlert.isHidden = !failedToDeliver
		notDeliveredLabel.isHidden = !failedToDeliver
		retryButton.isHidden = !failedToDeliver

		timeLabel.text = DatesFormatter.toMessage(message.date)
	}
}



// MARK: - Handle UI

private extension OutgoingMediaMessageCell
{
	@IBAction func onRetryButton()
	{
		handleRetryButton?()
	}
}



// MARK: - Constants

private extension OutgoingMediaMessageCell
{
	struct Constants
	{
		static let largeAttachmentsViewBottom = CGFloat(24)
		static let normalRightAttachmentsViewInset = CGFloat(16)
		static let largeRightAttachmentsViewInset = CGFloat(48)
	}
}
