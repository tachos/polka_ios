//
//  IncomingMessageCell.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 04.06.2021.
//

import UIKit



class IncomingMessageCell: UICollectionViewCell, MessageIdentifiable
{
	// MARK: Outlets
	@IBOutlet var bubbleView: UIView!
	@IBOutlet var mainTextView: UITextView!
	@IBOutlet var textViewHeight: NSLayoutConstraint!
	@IBOutlet var textViewWidth: NSLayoutConstraint!
	@IBOutlet var timeLabel: UILabel!

	// MARK: MessageIdentifiable
	private(set) var messageId: Message.Id = ""

	// MARK: AwakeFromNib
	override func awakeFromNib()
	{
		super.awakeFromNib()

		MessageCell.setupTextView(mainTextView)
	}
}



// MARK: - Setup

extension IncomingMessageCell
{
	func setup(_ message: Message, text: String)
	{
		self.messageId = message.id
		
		MessageCell.setText(
			text,
			color: #colorLiteral(red: 0.1333333333, green: 0.1333333333, blue: 0.1333333333, alpha: 1),
			in: mainTextView,
			heightConstraint: textViewHeight,
			widthConstraint: textViewWidth,
			maxWidth: UIScreen.main.bounds.width
				- Constants.leftTextViewInset
				- Constants.maxRightTextViewInset
		)

		timeLabel.text = DatesFormatter.toMessage(message.date)
	}
}



// MARK: - Constants

private extension IncomingMessageCell
{
	struct Constants
	{
		static let leftTextViewInset = CGFloat(32)
		static let maxRightTextViewInset = CGFloat(74)
	}
}
