//
//  IncomingMediaMessageCell.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 07.06.2021.
//

import UIKit



class IncomingMediaMessageCell: UICollectionViewCell, MessageIdentifiable
{
	// MARK: Outlets
	@IBOutlet var attachmentsView: AttachmentsView!
	@IBOutlet var timeLabel: UILabel!

	// MARK: MessageIdentifiable
	private(set) var messageId: Message.Id = ""
}



// MARK: - Setup

extension IncomingMediaMessageCell
{
	func setup(
		_ message: Message,
		attachments: [Attachment],
		expanded: Bool,
		handleAttachmentTapped: Handler<Attachment>?,
		handleMoreButton: VoidCallback?
	)
	{
		self.messageId = message.id

		attachmentsView.setup(
			attachments,
			expanded: expanded,
			handleAttachmentTapped: handleAttachmentTapped ?? { _ in },
			handleMoreButton: handleMoreButton ?? {}
		)
		timeLabel.text = DatesFormatter.toMessage(message.date)
	}
}
