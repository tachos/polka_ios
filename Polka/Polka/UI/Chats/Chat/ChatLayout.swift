//
//  ChatLayout.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 04.06.2021.
//

import UIKit



class ChatLayout: UICollectionViewLayout
{
	// MARK: Layout Attributes
	private var layoutAttributesCache = [UICollectionViewLayoutAttributes]()
	private var itemsFramesByIndexPaths: [IndexPath : CGRect] = [:]
	private(set) var contentHeight = CGFloat(0)
	private var cellSizesByMessagesIds: [Message.Id : CGSize] = [:]

	// MARK: Sizing Cell
	private let incomingMessageSizingCell = createView(IncomingMessageCell.self)
	private let outgoingMessageSizingCell = createView(OutgoingMessageCell.self)
	private let incomingMediaMessageSizingCell = createView(IncomingMediaMessageCell.self)
	private let outgoingMediaMessageSizingCell = createView(OutgoingMediaMessageCell.self)

	// MARK: Data
	weak var snapshotProvider: ChatDataSnapshotProvider?
}



// MARK: - Computed Vars

private extension ChatLayout
{
	var contentWidth: CGFloat
	{
		return UIScreen.main.bounds.width
	}

	var visibleAreaHeight: CGFloat
	{
		return (collectionView?.frame.height ?? 0) - bottomContentInset
	}

	var bottomContentInset: CGFloat
	{
		return collectionView?.contentInset.bottom ?? 0
	}
}



// MARK: - UICollectionViewLayout

extension ChatLayout
{
	override var collectionViewContentSize: CGSize
	{
		return CGSize(
			width: UIScreen.main.bounds.width,
			height: contentHeight
		)
	}

	override func prepare()
	{
		layoutAttributesCache.removeAll()
		itemsFramesByIndexPaths.removeAll()
		contentHeight = 0

		if collectionView != nil
		{
			cacheLayoutAttributes()
		}

		super.prepare()
	}

	override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]?
	{
		return layoutAttributesCache.compactMap
		{
			return $0.frame.intersects(rect) ? $0 : nil
		}
	}
}



// MARK: - Cache Attributes

private extension ChatLayout
{
	func cacheLayoutAttributes()
	{
		var yOffset: CGFloat = hasMoreOlderMessages
			? Constants.topLoaderInset
			: 0

		let dataSnapshot = getDataSnapshot()

		for section in 0 ..< dataSnapshot.count
		{
			let headerFrame = CGRect(
				x: 0,
				y: yOffset,
				width: contentWidth,
				height: Constants.sectionHeaderHeight
			)

			cacheHeaderLayoutAttributes(
				indexPath: IndexPath(item: 0, section: section),
				frame: headerFrame
			)

			yOffset = headerFrame.maxY

			for item in 0 ..< dataSnapshot[section].messages.count
			{
				if item > 0
				{
					if dataSnapshot[section].messages[item - 1].direction == dataSnapshot[section].messages[item].direction
					{
						yOffset += Constants.sameDirectionInteritemSpacing
					}
					else
					{
						yOffset += Constants.differentDirectionInteritemSpacing
					}
				}

				let indexPath = IndexPath(item: item, section: section)

				let height = getHeightForItem(
					at: indexPath
				)

				let frame = CGRect(
					x: 0,
					y: yOffset,
					width: contentWidth,
					height: height
				)

				cacheCellLayoutAttributes(
					indexPath: indexPath,
					frame: frame
				)

				yOffset = frame.maxY
			}
		}

		let minTopInset = Constants.minTopInset

		let topInset = max(minTopInset, visibleAreaHeight - yOffset)

		addTopInset(topInset)

		self.contentHeight = yOffset
			+ topInset
			+ Constants.bottomInset
	}

	func cacheHeaderLayoutAttributes(
		indexPath: IndexPath,
		frame: CGRect
	)
	{
		let attributes = UICollectionViewLayoutAttributes(
			forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
			with: indexPath
		)
		attributes.frame = frame

		layoutAttributesCache.append(attributes)
	}

	func cacheCellLayoutAttributes(
		indexPath: IndexPath,
		frame: CGRect
	)
	{
		let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
		attributes.frame = frame

		layoutAttributesCache.append(attributes)
		itemsFramesByIndexPaths[indexPath] = frame
	}

	func addTopInset(_ topInset: CGFloat)
	{
		layoutAttributesCache
			.forEach { $0.frame.origin.y += topInset }
	}
}



// MARK: - Get Data

private extension ChatLayout
{
	func getDataSnapshot() -> [MessagesByDate]
	{
		return snapshotProvider?.getChatDataSnapshot() ?? []
	}
}



// MARK: - Size Calculation

private extension ChatLayout
{
	func calculateSizes()
	{
		let dataSnapshot = getDataSnapshot()

		for (section, messagesByDate) in dataSnapshot.enumerated()
		{
			for (item, message) in messagesByDate.messages.enumerated()
			{
				calculateSize(
					for: message,
					indexPath: IndexPath(item: item, section: section)
				)
			}
		}
	}

	func calculateSizes(for indexPaths: Set<IndexPath>)
	{
		let dataSnapshot = getDataSnapshot()

		for indexPath in indexPaths
		{
			if indexPath.section < dataSnapshot.count
				&& indexPath.item < dataSnapshot[indexPath.section]
				.messages.count
			{
				let message = dataSnapshot[indexPath.section]
					.messages[indexPath.item]

				calculateSize(
					for: message,
					indexPath: indexPath
				)
			}
		}
	}

	func calculateSize(for message: Message, indexPath: IndexPath)
	{
		cellSizesByMessagesIds[message.id] = getSize(
			for: message
		)
	}

	func getHeightForItem(at indexPath: IndexPath) -> CGFloat
	{
		let dataSnapshot = getDataSnapshot()
		let message = dataSnapshot[indexPath.section].messages[indexPath.item]

		return getSize(for: message).height
	}

	func getSize(
		for message: Message
	) -> CGSize
	{
		switch (message.direction, message.content)
		{
		case (.incoming, .text(let text)):
			incomingMessageSizingCell.setup(message, text: text)

			return getSize(incomingMessageSizingCell)

		case (.incoming, .media(let attachments)):
			incomingMediaMessageSizingCell.setup(
				message,
				attachments: attachments,
				expanded: isExpanded(message),
				handleAttachmentTapped: nil,
				handleMoreButton: nil
			)

			return getSize(incomingMediaMessageSizingCell)

		case (.outgoing, .text(let text)):
			outgoingMessageSizingCell.setup(
				message,
				text: text,
				status: snapshotProvider?.getOutgoingMessageStatus(messageId: message.id),
				handleRetryButton: nil
			)

			return getSize(outgoingMessageSizingCell)

		case (.outgoing, .media(let attachments)):
			outgoingMediaMessageSizingCell.setup(
				message,
				attachments: attachments,
				status: snapshotProvider?.getOutgoingMessageStatus(messageId: message.id),
				expanded: isExpanded(message),
				handleAttachmentTapped: nil,
				handleRetryButton: nil,
				handleMoreButton: nil
			)

			return getSize(outgoingMediaMessageSizingCell)
		}
	}

	func getSize(_ cell: UICollectionViewCell) -> CGSize
	{
		return cell.contentView
			.systemLayoutSizeFitting(
				.init(
					width: contentWidth,
					height: .greatestFiniteMagnitude
				)
			)
	}

	func isExpanded(_ message: Message) -> Bool
	{
		return snapshotProvider?.isExpanded(message) ?? false
	}

	var hasMoreOlderMessages: Bool
	{
		return snapshotProvider?.hasMoreOlderMessages ?? false
	}
}



// MARK: - Constants

extension ChatLayout
{
	struct Constants
	{
		static let sectionHeaderHeight = CGFloat(56)
		static let sameDirectionInteritemSpacing = CGFloat(8)
		static let differentDirectionInteritemSpacing = CGFloat(24)
		static let minTopInset = CGFloat(24)
		static let bottomInset = CGFloat(24)
		static let topLoaderInset = CGFloat(100)
	}
}
