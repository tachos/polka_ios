//
//  ChatViewController.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 02.06.2021.
//

import UIKit
import RxSwift



class ChatViewController: UIViewController, StoryboardCreatable
{
	// MARK: StoryboardCreatable
	static let storyboardName = StoryboardName.chats

	// MARK:
	var keyboardObserver: NSObjectProtocol?

	// MARK: Outlets
	@IBOutlet var collectionView: UICollectionView!
	@IBOutlet var zeroStateView: UIView!
	@IBOutlet var messageInputView: MessageInputView!
	@IBOutlet var messageInputViewBottom: NSLayoutConstraint!
	@IBOutlet var topLoader: UIActivityIndicatorView!
	
	// MARK: State
	private var isFirstWillAppear = true
	private var isFirstLayout = true
	private var expandedMessagesIds = Set<Message.Id>()
	private var keyboardFrame: CGRect?
	
	var navBarAlpha: CGFloat = 0

	// MARK: Config
	var config: Config!

	struct Config
	{
		let helper: ChatHelper
		let viewAttachmentSegue: Handler<Attachment>
		let pickMediaSegue: AsyncHandler<[LocalMedia]>
		let sellerSegue: VoidCallback?
	}

	// MARK: Data Snapshot
	private var dataSource: UICollectionViewDiffableDataSource<Date, Message>!
}



// MARK: - UIViewController

extension ChatViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()

		setup()
	}

	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)

		updateNavigationBar()
		updateState()

		if isFirstWillAppear
		{
			config.helper.loadFirstPage()

			isFirstWillAppear = false
		}
	}

	override func viewDidLayoutSubviews()
	{
		super.viewDidLayoutSubviews()

		if isFirstLayout
		{
			collectionView.contentInset.bottom = view.bounds.height
				- messageInputView.frame.minY

			dataSource.apply(
				getDiffableSnapshot(),
				completion:
			{
				[weak self] in

				self?.scrollToBottom(animated: false)
			})

			isFirstLayout = false
		}
	}
}



// MARK: - API

extension ChatViewController
{
	func setTopLoaderVisible(_ visible: Bool)
	{
		topLoader.isHidden = !visible
	}

	func handleFirstPage()
	{
		updateState()
		dataSource.apply(
			getDiffableSnapshot(),
			animatingDifferences: false,
			completion:
		{
			[weak self] in

			self?.scrollToBottom(animated: false)
		})
	}

	func handleOlderMessages()
	{
		updateState()

		let oldContentOffsetY = collectionView.contentOffset.y
		let oldContentHeight = collectionView.contentSize.height

		dataSource.apply(
			getDiffableSnapshot(),
			animatingDifferences: false,
			completion:
		{
			[weak self] in

			guard let self = self else { return }

			let contentHeightDelta = self.collectionView.contentSize.height - oldContentHeight

			self.collectionView.contentOffset = .init(
				x: 0,
				y: oldContentOffsetY + contentHeightDelta
			)
		})
	}

	func handleNewerMessage()
	{
		updateState()
		dataSource.apply(
			getDiffableSnapshot(),
			completion:
		{
			[weak self] in

			self?.adjustScrollInset(keyboardFrame: self?.keyboardFrame ?? .zero)
			self?.scrollToBottom(animated: true)
		})
	}

	func handleOutgoingMessageStatusUpdate(
		_ message: Message,
		indexPath: IndexPath
	)
	{
		var snapshot = getDiffableSnapshot()
		snapshot.reloadItems([message])
		dataSource.apply(
			snapshot,
			completion:
		{
			[weak self] in

			guard let self = self else { return }

			let isLastMessage = indexPath.section == snapshot.numberOfSections - 1
				&& indexPath.item == snapshot.numberOfItems(
					inSection: snapshot.sectionIdentifiers[indexPath.section]
				) - 1

			if isLastMessage && self.isScrollCloseToBottom
			{
				self.scrollToBottom(animated: true)
			}
		})
	}

	func handleMessageDeletion()
	{
		dataSource.apply(getDiffableSnapshot())
	}
}



// MARK: - Setup

extension ChatViewController
{
	func setup()
	{
		setupCollectionView()
		setupMessageInputView()
		setupTapGestureRecognizer()
		setupKeyboardObserver()
	}

	func setupCollectionView()
	{
		collectionView.registerNibHeader(ChatSectionHeader.self)
		collectionView.registerNibCell(IncomingMessageCell.self)
		collectionView.registerNibCell(OutgoingMessageCell.self)
		collectionView.registerNibCell(IncomingMediaMessageCell.self)
		collectionView.registerNibCell(OutgoingMediaMessageCell.self)

		collectionView.delegate = self
		chatLayout?.snapshotProvider = self

		dataSource = .init(
			collectionView: collectionView,
			cellProvider:
		{
			[weak self] _, indexPath, message in

			self?.cell(for: message, at: indexPath)
		})

		dataSource.supplementaryViewProvider =
		{
			[weak self] _, _, indexPath in

			self?.header(at: indexPath)
		}
	}

	func setupMessageInputView()
	{
		messageInputView.config = .init(
			onDidBeginEditing:
		{
			[weak self] in

			guard let self = self else { return }

			if self.isScrollCloseToBottom
			{
				self.scrollToBottom(animated: true)
			}
		 },
			onTextEntered:
		{
			[weak self] text in

			guard let self = self else { return }

			self.config.helper.sendMessage(text: text)
		},
			onAttachButton:
		{
			[weak self] in

			self?.handleAttachButton()
		})
	}

	func setupTapGestureRecognizer()
	{
		let tapGestureRecognizer = UITapGestureRecognizer(
			target: self,
			action: #selector(handleTap)
		)
		tapGestureRecognizer.cancelsTouchesInView = false
		tapGestureRecognizer.delegate = self

		collectionView.addGestureRecognizer(tapGestureRecognizer)
	}
}



// MARK: - Manage UI

extension ChatViewController
{
	var chatLayout: ChatLayout?
	{
		return collectionView.collectionViewLayout as? ChatLayout
	}

	func updateNavigationBar()
	{
		let navBarCustomizable = (self.navigationController as? NavBarCustomizable)

		switch config.helper.receiver
		{
		case .seller(let seller):
			navBarCustomizable?.setAppearance(
				navBarBackground: .clear,
				hasBackButton: true,
				titleViewType: .sellerInfo(
					seller.info.profileImageUrl,
					seller.info.caption,
					"К товарам продавца",
					{ [weak self] in self?.config.sellerSegue?() }
				),
				initiallyNotVisible: false
			)

		case .consumer(let consumer):
			let title = consumer.info.deliveryAddresses?.last?.toString() ?? ""

			navBarCustomizable?.setAppearance(
				navBarBackground: .clear,
				hasBackButton: true,
				titleViewType: .tabTitleHeader(title),
				initiallyNotVisible: false
			)
		}
	}

	func updateState()
	{
		zeroStateView.isHidden = !getChatDataSnapshot().isEmpty
	}

	var maxContentOffset: CGFloat
	{
		guard let chatLayout = chatLayout else { return 0 }

		return chatLayout.contentHeight
			- collectionView.frame.height
			+ collectionView.contentInset.top
			+ collectionView.contentInset.bottom
	}

	var isScrollCloseToBottom: Bool
	{
		return collectionView.contentOffset.y + collectionView.contentInset.bottom >= maxContentOffset - Constants.scrollTolerance
	}

	func scrollToBottom(animated: Bool)
	{
		collectionView.setContentOffset(
			.init(x: 0, y: maxContentOffset),
			animated: animated
		)
	}

	func scrollMessageToVisible(_ message: Message)
	{
		if let indexPath = getVisibleCellIndexPath(for: message)
		{
			collectionView.scrollToItem(
				at: indexPath,
				at: .centeredVertically,
				animated: true
			)
		}
	}
}



// MARK: - Handle UI

private extension ChatViewController
{
	@objc func handleTap()
	{
		messageInputView.textViewWrapper.textView.resignFirstResponder()
	}

	func handleRetry(_ message: Message)
	{
		showActionSheet(
			actions: [
				.init(
					title: "Отправить повторно",
					style: .default,
					handler:
				{
					[weak self] _ in

					guard let self = self else { return }

					self.config.helper.resendMessage(
						message: message
					)
				}),
				.init(
					title: "Отменить",
					style: .destructive,
					handler:
				{
					[weak self] _ in

					guard let self = self else { return }

					self.config.helper.deleteFailedMessage(
						message: message
					)
				})
			]
		)
	}

	func handleMoreButton(_ message: Message)
	{
		expandedMessagesIds.insert(message.id)

		var snapshot = getDiffableSnapshot()
		snapshot.reloadItems([message])

		dataSource.apply(
			snapshot,
			completion:
		{
			[weak self] in

			self?.scrollMessageToVisible(message)
		})
	}

	func handleAttachButton()
	{
		config.pickMediaSegue(
		{
			[weak self] localMedia in

			guard let self = self else { return }

			self.config.helper.sendMessage(
				localMedia: localMedia
			)
		})
	}

	func handleAttachmentTapped(_ attachment: Attachment)
	{
		config.viewAttachmentSegue(attachment)
	}
}



// MARK: - Data

extension ChatViewController: ChatDataSnapshotProvider
{
	// used for safe operations like managing views visibility
	// and caching layout attributes
	func getChatDataSnapshot() -> [MessagesByDate]
	{
		return config.helper.messagesByDate
	}

	func getOutgoingMessageStatus(messageId: Message.Id) -> ChatHelper.OutgoingMessageStatus?
	{
		return config.helper.getOutgoingMessageStatus(messageId: messageId)
	}

	func isExpanded(_ message: Message) -> Bool
	{
		return expandedMessagesIds.contains(message.id)
	}

	var hasMoreOlderMessages: Bool
	{
		return config.helper.hasMoreOlderMessages
	}

	// used for potentially unsafe operations like updating collection view
	private func getDiffableSnapshot() -> NSDiffableDataSourceSnapshot<Date, Message>
	{
		var diffableSnapshot = NSDiffableDataSourceSnapshot<Date, Message>()

		let dataSnapshot = getChatDataSnapshot()

		for section in dataSnapshot
		{
			diffableSnapshot.appendSections([section.date])
			diffableSnapshot.appendItems(section.messages, toSection: section.date)
		}

		return diffableSnapshot
	}

	private func getVisibleCellIndexPath(for message: Message) -> IndexPath?
	{
		return self.collectionView
			.indexPathsForVisibleItems
			.first(
			where:
		{
			if let messageIdentifiable = self.collectionView
				.cellForItem(at: $0) as? MessageIdentifiable,
			   messageIdentifiable.messageId == message.id
			{
				return true
			}

			return false
		})
	}
}



// MARK: - Navigationable

extension ChatViewController: Navigationable
{
	func adjustSafeAreaIfNeeded(_ navBarHeight: CGFloat)
	{
		additionalSafeAreaInsets.top = navBarHeight
	}
}



// MARK: - KeyboardFrameAware

extension ChatViewController: KeyboardFrameAware
{
	func adjustForKeyboardFrame(_ frame: CGRect)
	{
		self.keyboardFrame = frame

		adjustScrollInset(keyboardFrame: frame)

		messageInputViewBottom.constant = Constants.messageInputViewBottom
			+ extraBottomSpacing(for: frame)

		view.layout()
	}

	func adjustScrollInset(keyboardFrame: CGRect)
	{
		adjustInsets(
			collectionView,
			for: keyboardFrame,
			distanceToKeyboard: messageInputView.frame.height + Constants.messageInputViewBottom
		)
	}
}



// MARK: - UIGestureRecognizerDelegate

extension ChatViewController: UIGestureRecognizerDelegate
{
	func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool
	{
		if touch.view is UIButton
		{
			return false
		}

		return true
	}
}



// MARK: - Get Cells and Headers

extension ChatViewController
{
	func cell(for message: Message, at indexPath: IndexPath) -> UICollectionViewCell
	{
		switch (message.direction, message.content)
		{
		case (.incoming, .text(let text)):
			let cell = collectionView.dequeueReusableCell(
				ofType: IncomingMessageCell.self,
				for: indexPath
			)!

			cell.setup(message, text: text)

			return cell

		case (.incoming, .media(let attachments)):
			let cell = collectionView.dequeueReusableCell(
				ofType: IncomingMediaMessageCell.self,
				for: indexPath
			)!

			cell.setup(
				message,
				attachments: attachments,
				expanded: isExpanded(message),
				handleAttachmentTapped: { [weak self] in self?.handleAttachmentTapped($0) },
				handleMoreButton: { [weak self] in self?.handleMoreButton(message) }
			)

			return cell

		case (.outgoing, .text(let text)):
			let cell = collectionView.dequeueReusableCell(
				ofType: OutgoingMessageCell.self,
				for: indexPath
			)!

			cell.setup(
				message,
				text: text,
				status: config.helper.getOutgoingMessageStatus(messageId: message.id),
				handleRetryButton: { [weak self] in self?.handleRetry(message) }
			)

			return cell

		case (.outgoing, .media(let attachments)):
			let cell = collectionView.dequeueReusableCell(
				ofType: OutgoingMediaMessageCell.self,
				for: indexPath
			)!

			cell.setup(
				message,
				attachments: attachments,
				status: config.helper.getOutgoingMessageStatus(messageId: message.id),
				expanded: isExpanded(message),
				handleAttachmentTapped: { [weak self] in self?.handleAttachmentTapped($0) },
				handleRetryButton: { [weak self] in self?.handleRetry(message) },
				handleMoreButton: { [weak self] in self?.handleMoreButton(message) }
			)

			return cell
		}
	}

	func header(at indexPath: IndexPath) -> UICollectionReusableView
	{
		let header = collectionView.dequeueSectionHeaderView(
			ChatSectionHeader.self,
			indexPath
		)

		let date = dataSource
			.snapshot()
			.sectionIdentifiers[indexPath.section]

		header.setup(date)

		return header
	}
}



// MARK: - UICollectionViewDelegate

extension ChatViewController: UICollectionViewDelegate
{
	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
	{
		if indexPath.section == 0,
		   indexPath.item == 0
		{
			config.helper.loadMore()
		}
	}
}



// MARK: - UIScrollViewDelegate

extension ChatViewController: UIScrollViewDelegate
{
	func scrollViewDidScroll(_ scrollView: UIScrollView)
	{
		let delta = max(0, scrollView.contentOffset.y)

		topLoader.transform = .init(translationX: 0, y: -delta)
	}
}



// MARK: - Constants

private extension ChatViewController
{
	struct Constants
	{
		static let messageInputViewBottom = CGFloat(16)
		static let scrollTolerance = CGFloat(20)
	}
}
