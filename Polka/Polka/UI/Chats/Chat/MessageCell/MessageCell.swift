//
//  MessageCell.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 04.06.2021.
//

import UIKit



// MARK: Message Cell

enum MessageCell
{
	static func setupTextView(_ textView: UITextView)
	{
		textView.textContainerInset = .zero
		textView.textContainer.lineFragmentPadding = 0
		textView.textContainer.maximumNumberOfLines = 0
		textView.textContainer.lineBreakMode = .byWordWrapping

		textView.isSelectable = true
		textView.isEditable = false
		textView.isScrollEnabled = false
	}

	static func setText(
		_ text: String,
		color: UIColor,
		in textView: UITextView,
		heightConstraint: NSLayoutConstraint,
		widthConstraint: NSLayoutConstraint,
		maxWidth: CGFloat
	)
	{
		let attributedText = NSAttributedString(
			string: text,
			attributes: [
				.paragraphStyle : NSParagraphStyle.with(
					lineHeight: Constants.lineHeight
				),
				.font : Constants.font,
				.foregroundColor : color
			]
		)

		let textSize = getTextSize(
			attributedText: attributedText,
			maxWidth: maxWidth
		)

		heightConstraint.constant = textSize.height
		widthConstraint.constant = textSize.width

		textView.attributedText = attributedText
	}

	private static func getTextSize(
		attributedText: NSAttributedString,
		maxWidth: CGFloat
	) -> CGSize
	{
		let size = CGSize(
			width: maxWidth,
			height: .greatestFiniteMagnitude
		)

		let framesetter = CTFramesetterCreateWithAttributedString(
			attributedText
		)

		var textSize = CTFramesetterSuggestFrameSizeWithConstraints(
			framesetter, CFRange(), nil, size, nil
		)
		textSize.width += 1 // in some cases returned textSize is a little smaller

		return textSize
	}

	struct Constants
	{
		static let lineHeight = CGFloat(18)
		static let font = UIFont.systemFont(
			ofSize: 16,
			weight: .regular
		)
	}
}



// MARK: - MessageIdentifiable

protocol MessageIdentifiable
{
	var messageId: Message.Id { get }
}
