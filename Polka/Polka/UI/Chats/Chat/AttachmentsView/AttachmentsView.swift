//
//  AttachmentsView.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 07.06.2021.
//

import UIKit
import TinyConstraints
import SDWebImage



class AttachmentsView: UIView
{
	// MARK: Layout
	private lazy var stackView: UIStackView =
	{
		let stackView = UIStackView()

		stackView.axis = .vertical
		stackView.spacing = Constants.interrowSpacing

		addSubview(stackView)
		stackView.edgesToSuperview()

		return stackView
	}()

	private lazy var attachmentViews: [AttachmentView] =
	{
		var attachmentViews: [AttachmentView] = []

		for _ in 0..<Message.maxAttachmentsCount
		{
			let attachmentView = AttachmentView()

			attachmentView.height(Constants.rowHeight)
			stackView.addArrangedSubview(attachmentView)

			attachmentViews.append(attachmentView)
		}

		return attachmentViews
	}()

	private lazy var bottomRowView: BottomRowView =
	{
		let bottomRowView = BottomRowView()
		bottomRowView.height(Constants.rowHeight)

		stackView.addArrangedSubview(bottomRowView)

		return bottomRowView
	}()

	private lazy var stackViewGestureRecognizer: UITapGestureRecognizer =
	{
		let gesture = UITapGestureRecognizer()
		gesture.addTarget(self, action: #selector(onStackViewTapped(_:)))

		stackView.addGestureRecognizer(gesture)

		return gesture
	}()

	// MARK: Data
	private var attachments: [Attachment] = []
	private var expanded = false

	// MARK: Callbacks
	private var handleAttachmentTapped: Handler<Attachment>?
}



// MARK: - Setup

extension AttachmentsView
{
	func setup(
		_ attachments: [Attachment],
		expanded: Bool,
		handleAttachmentTapped: @escaping Handler<Attachment>,
		handleMoreButton: @escaping VoidCallback
	)
	{
		self.attachments = attachments
		self.expanded = expanded
		self.handleAttachmentTapped = handleAttachmentTapped

		if expanded || attachments.count <= 2
		{
			for (index, attachment) in attachments.enumerated()
				.prefix(Message.maxAttachmentsCount)
			{
				attachmentViews[index].setup(with: attachment)
			}

			showAttachmentViews(count: attachments.count)
			bottomRowView.isHidden = true
		}
		else
		{
			attachmentViews.first!.setup(with: attachments.first!)
			showAttachmentViews(count: 1)

			bottomRowView.setup(
				Array(attachments.dropFirst()),
				handleAttachmentTapped: { handleAttachmentTapped(attachments[1]) },
				handleMoreButton: handleMoreButton
			)
			bottomRowView.isHidden = false
		}

		_ = stackViewGestureRecognizer
	}

	private func showAttachmentViews(count: Int)
	{
		if count > attachmentViews.count
		{
			return
		}

		for index in 0..<attachmentViews.count
		{
			attachmentViews[index].isHidden = index < count ? false : true
		}
	}

	@objc func onStackViewTapped(_ sender: UITapGestureRecognizer)
	{
		let location = sender.location(in: stackView)

		if let index = attachmentViews
			.enumerated()
			.first(where:
		{
			$0.element.frame.contains(location)
		})?.offset
		{
			self.handleAttachmentTapped?(attachments[index])
		}
	}
}



// MARK: - Intrinsic Size

extension AttachmentsView
{
	override var intrinsicContentSize: CGSize
	{
		return .init(
			width: intrinsicWidth,
			height: intrinsicHeight
		)
	}

	private var intrinsicWidth: CGFloat
	{
		return 2 * Constants.narrowColumnWidth
			+ Constants.intercolumnSpacing
	}

	private var intrinsicHeight: CGFloat
	{
		switch attachments.count
		{
		case 0:
			return 0

		case 1:
			return Constants.rowHeight

		default:
			if expanded
			{
				return CGFloat(attachments.count) * Constants.rowHeight
					+ CGFloat(attachments.count - 1) * Constants.interrowSpacing
			}
			else
			{
				return 2 * Constants.rowHeight + Constants.interrowSpacing
			}
		}
	}
}



// MARK: - Bottom Row View

private extension AttachmentsView
{
	class BottomRowView: UIView
	{
		lazy var stackView: UIStackView =
		{
			let stackView = UIStackView()

			stackView.axis = .horizontal
			stackView.spacing = Constants.intercolumnSpacing

			addSubview(stackView)
			stackView.edgesToSuperview()

			return stackView
		}()

		lazy var firstAttachmentView: AttachmentView =
		{
			let attachmentView = AttachmentView()

			attachmentView.width(Constants.narrowColumnWidth)
			stackView.addArrangedSubview(attachmentView)

			return attachmentView
		}()

		lazy var moreAttachmentsButton: UIButton =
		{
			let button = UIButton()

			button.layer.cornerRadius = Constants.cornerRadius
			button.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
			button.titleLabel?.font = Fonts.ObjectSans.regular(24)
			button.setTitleColor(#colorLiteral(red: 0.5607843137, green: 0.5607843137, blue: 0.5607843137, alpha: 1), for: .normal)

			button.width(Constants.narrowColumnWidth)
			stackView.addArrangedSubview(button)

			button.addTarget(
				self,
				action: #selector(onMoreButton),
				for: .touchUpInside
			)

			return button
		}()

		lazy var firstAttachmentGesture: UITapGestureRecognizer =
		{
			let gesture = UITapGestureRecognizer()
			gesture.addTarget(self, action: #selector(onAttachmentTapped))

			stackView.addGestureRecognizer(gesture)

			return gesture
		}()

		private var handleAttachmentTapped: VoidCallback?
		private var handleMoreButton: VoidCallback?

		func setup(
			_ attachments: [Attachment],
			handleAttachmentTapped: @escaping VoidCallback,
			handleMoreButton: @escaping VoidCallback
		)
		{
			self.handleAttachmentTapped = handleAttachmentTapped
			self.handleMoreButton = handleMoreButton

			if attachments.count < 2
			{
				// wrong attachments count
				return
			}

			firstAttachmentView.setup(with: attachments.first!)
			moreAttachmentsButton.setTitle(
				"+ \(attachments.count - 1)",
				for: .normal
			)

			_ = firstAttachmentGesture
		}

		@objc func onAttachmentTapped()
		{
			handleAttachmentTapped?()
		}

		@objc func onMoreButton()
		{
			handleMoreButton?()
		}
	}
}



// MARK: - AttachmentView

extension AttachmentsView
{
	class AttachmentView: UIView
	{
		lazy var imageView: UIImageView =
		{
			let imageView = UIImageView()

			imageView.backgroundColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
			imageView.layer.cornerRadius = Constants.cornerRadius
			imageView.clipsToBounds = true
			imageView.contentMode = .scaleAspectFill

			addSubview(imageView)
			imageView.edgesToSuperview()

			return imageView
		}()

		lazy var playIcon: UIImageView =
		{
			let imageView = UIImageView()

			imageView.backgroundColor = .clear
			imageView.image = #imageLiteral(resourceName: "play")

			addSubview(imageView)
			imageView.centerInSuperview()

			return imageView
		}()

		override func didMoveToSuperview()
		{
			super.didMoveToSuperview()

			_ = imageView
			_ = playIcon
		}

		override var intrinsicContentSize: CGSize
		{
			return imageView.intrinsicContentSize
		}

		func setup(with attachment: Attachment)
		{
			switch attachment.content
			{
			case .localImage(let image):
				playIcon.isHidden = true
				self.imageView.image = image

			case .remoteImage(let remoteImage):
				playIcon.isHidden = true
				self.imageView.sd_setImage(
					with: remoteImage.preview
				)

			case .localVideo(let localVideo):
				playIcon.isHidden = false
				self.imageView.image = localVideo.previewImage

			case .remoteVideo(let remoteVideo):
				playIcon.isHidden = false
				self.imageView.sd_setImage(
					with: remoteVideo.previewUrl
				)
			}
		}
	}
}



// MARK: - Constants

extension AttachmentsView
{
	struct Constants
	{
		static let rowHeight = CGFloat(100)
		static let narrowColumnWidth = CGFloat(100)
		static let intercolumnSpacing = CGFloat(4)
		static let interrowSpacing = CGFloat(4)
		static let cornerRadius = CGFloat(16)
	}
}
