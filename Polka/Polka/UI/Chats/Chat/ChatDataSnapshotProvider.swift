//
//  ChatDataSnapshotProvider.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 24.06.2021.
//

import Foundation



protocol ChatDataSnapshotProvider: AnyObject
{
	func getChatDataSnapshot() -> [MessagesByDate]
	func getOutgoingMessageStatus(messageId: Message.Id) -> ChatHelper.OutgoingMessageStatus?
	func isExpanded(_ message: Message) -> Bool
	var hasMoreOlderMessages: Bool { get }
}
