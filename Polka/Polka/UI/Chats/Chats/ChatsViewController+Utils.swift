//
//  ChatsViewController+Utils.swift
//  Polka
//
//  Created by Michael Goremykin on 19.05.2021.
//

import UIKit

// MARK:- Utils
extension ChatsViewController
{
	func getChatCellSize(_ cv: UICollectionView) -> CGSize
	{
		if let flowLayout = cv.collectionViewLayout as? UICollectionViewFlowLayout
		{
			let width = (UIScreen.main.bounds.width - flowLayout.sectionInset.left * 2 - flowLayout.minimumInteritemSpacing)/CGFloat(2)
			
			return CGSize(width: width, height: width / ChatPreviewCollectionViewCell.Constants.aspectRatio)
		}
		
		return CGSize(width: 10, height: 10)
	}
}
