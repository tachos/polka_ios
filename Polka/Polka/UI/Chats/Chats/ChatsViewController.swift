//
//  ChatsListViewController.swift
//  Polka
//
//  Created by Michael Goremykin on 21.04.2021.
//

import UIKit

class ChatsViewController: UIViewController, StoryboardCreatable
{
    // MARK:- Outlets
    @IBOutlet weak var chatsCollectionView: UICollectionView!
	
	weak var emptyChatListView: UIView!

	// MARK:- Data
	var chats: [Chat] = []
    
    // MARK:- StoryboardCreatable
    static var storyboardName: StoryboardName = .chats
    
    // MARK:- Stateful
    var cellSize: CGSize!

	// MARK:- Config
	var config: Config!
	
	struct Config
	{
		let chatSegue: Handler<Chat>
	}
}

// MARK:- Life cycle
extension ChatsViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setupUI()
    }
	
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
		
		(self.navigationController as? NavBarCustomizable)?.setAppearance(
			navBarBackground: .clear,
			hasBackButton: false,
			titleViewType: nil,
			initiallyNotVisible: true
		)
		
		updateState()

		managers.chats.loadMore(
			self,
			forceStartFromFirstPage: true
		)
	}
}



// MARK: - API

extension ChatsViewController
{
	func insertChats(at indices: [Int])
	{
		updateState()
		
		let indexPaths = indices.map { IndexPath(item: $0, section: 0) }

		chatsCollectionView.insertItems(at: indexPaths)
	}
}
