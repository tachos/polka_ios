//
//  ChatsViewController+UI.swift
//  Polka
//
//  Created by Michael Goremykin on 19.05.2021.
//

import UIKit

// MARK:- UI setup
extension ChatsViewController
{
	func setupUI()
	{
		setupCollectionView(true)
		setupEmptyChatListView(true)
	}
	
	func setupCollectionView(_ isHidden: Bool)
	{
		chatsCollectionView.isHidden = isHidden
		
		chatsCollectionView.delegate   = self
		chatsCollectionView.dataSource = self
		
		cellSize = getChatCellSize(chatsCollectionView)
	}
	
	func setupEmptyChatListView(_ isHidden: Bool)
	{
		let emptyChatListView = EmptyChatListView.create()
		
		emptyChatListView.isHidden = isHidden
		
		emptyChatListView.translatesAutoresizingMaskIntoConstraints = false
		
		self.view.addSubview(emptyChatListView)
		
		let constraints = [emptyChatListView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
						   emptyChatListView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
						   emptyChatListView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
						   emptyChatListView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)]
		
		NSLayoutConstraint.activate(constraints)
		
		self.emptyChatListView = emptyChatListView
	}
}

// MARK:- UICollectionView
extension ChatsViewController: UICollectionViewDelegate,
								   UICollectionViewDataSource,
								   UICollectionViewDelegateFlowLayout
{
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
	{
		let chat = chats[indexPath.item]

		config.chatSegue(chat)
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		return chats.count
	}

	func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
	{
		switch kind
		{
		case UICollectionView.elementKindSectionHeader:
			return collectionView.dequeueReusableSupplementaryView(
				ofKind: kind,
				withReuseIdentifier: ChatsHeaderView.typeName,
				for: indexPath
			)
			
		default:
			dbgUnreachable("Unexpected element kind: \(kind)")
			return UICollectionReusableView()
		}
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
	{
		return .init(
			width: UIScreen.main.bounds.width,
			height: ChatsHeaderView.Constants.height
		)
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
	{
		return cellSize
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{
		guard let cell = collectionView.dequeueReusableCell(
			ofType: ChatPreviewCollectionViewCell.self,
			for: indexPath
		)
		else
		{
			return UICollectionViewCell()
		}
		
		cell.initialize(
			chats[indexPath.row]
		)
		
		return cell
	}

	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
	{
		if indexPath.item == chats.count - 1
		{
			managers.chats.loadMore(self)
		}
	}
}

// MARK:- Update UI
extension ChatsViewController
{
	func updateState()
	{
		self.chats = managers.chats.chats
		chatsCollectionView.isHidden = managers.chats.chats.isEmpty
		emptyChatListView.isHidden = !managers.chats.chats.isEmpty
	}
}
