//
//  EmptyChatListView.swift
//  Polka
//
//  Created by Michael Goremykin on 19.05.2021.
//

import UIKit

enum EmptyChatListView
{
	
}

// MARK:- Instantiation
extension EmptyChatListView
{
	static func create () -> UIView
	{
		let view = UIView()
		
		let noMessageLabel 		= "Сообщений нет".createLabel(Constants.noMessagesLabelFont, Constants.noMessagesLabelTextColor)
		let engageInDialogLabel = "Начните \nдиалог".createLabel(Constants.engageInDialogLabelFont, Constants.engageInDialogLabelTextColor)
		
		view.addSubview(noMessageLabel)
		view.addSubview(engageInDialogLabel)
		
		noMessageLabel.translatesAutoresizingMaskIntoConstraints 		= false
		engageInDialogLabel.translatesAutoresizingMaskIntoConstraints 	= false
		
		let constraints = [noMessageLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: Constants.verticalSpacingToFirstLabel),
						   noMessageLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
						   engageInDialogLabel.topAnchor.constraint(equalTo: noMessageLabel.bottomAnchor, constant: Constants.labelsVerticalSpacing),
						   engageInDialogLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)]
		
		NSLayoutConstraint.activate(constraints)
		
		return view
	}
}

// MARK:- Constants
extension EmptyChatListView
{
	enum Constants
	{
		static let noMessagesLabelFont 		= UIFont.systemFont(ofSize: 20, weight: .semibold)
		static let engageInDialogLabelFont 	= Fonts.ObjectSans.heavy(44)
		
		static let noMessagesLabelTextColor 	= #colorLiteral(red: 0.5607843137, green: 0.5607843137, blue: 0.5607843137, alpha: 1)
		static let engageInDialogLabelTextColor = #colorLiteral(red: 0.1333333333, green: 0.1333333333, blue: 0.1333333333, alpha: 1)
		
		static let verticalSpacingToFirstLabel = CGFloat(160)
		static let labelsVerticalSpacing = CGFloat(16)
	}
}

extension String
{
	func createLabel(_ font: UIFont, _ textColor: UIColor, _ numberOfLines: Int = 0, _ textAligment: NSTextAlignment = .center) -> UILabel
	{
		let label = UILabel()
		
		label.font 			= font
		label.textColor 	= textColor
		label.numberOfLines = numberOfLines
		label.text 			= self
		label.textAlignment = textAligment
		
		return label
	}
}
