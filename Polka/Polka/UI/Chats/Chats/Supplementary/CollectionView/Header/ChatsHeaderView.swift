//
//  ChatsHeaderView.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 07.06.2021.
//

import UIKit



class ChatsHeaderView: UICollectionReusableView
{
	struct Constants
	{
		static let height = CGFloat(116)
	}
}
