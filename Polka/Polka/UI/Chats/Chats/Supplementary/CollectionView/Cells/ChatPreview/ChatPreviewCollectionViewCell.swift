//
//  ChatPreviewCollectionViewCell.swift
//  Polka
//
//  Created by Michael Goremykin on 21.04.2021.
//

import UIKit
import SDWebImage
import RxSwift

class ChatPreviewCollectionViewCell: UICollectionViewCell
{
    // MARK:- Outlets
    @IBOutlet weak var title: UILabel!
	@IBOutlet var mainImageView: UIImageView!
	@IBOutlet var placeholderImageView: UIImageView!
	@IBOutlet weak var gradientView: GradientView!
    @IBOutlet weak var unreadIndicatorView: UIView!

	// MARK:- ReactiveX
	private var disposeBag = DisposeBag()
}

// MARK:- Life cycle
extension ChatPreviewCollectionViewCell
{
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        setupGradient()
    }
}

// MARK:- UI setup
extension ChatPreviewCollectionViewCell
{
    func setupGradient()
    {
        gradientView.gradientDirection = .vertical(bottomColor: #colorLiteral(red: 0.6078431373, green: 0.6078431373, blue: 0.6078431373, alpha: 1),
                                                   topColor: #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1))
    }
}

// MARK:- Initialization
extension ChatPreviewCollectionViewCell
{
    func initialize(_ chat: Chat)
    {
		switch chat.receiver
		{
		case .seller(let seller):
			self.placeholderImageView.isHidden = true
			self.mainImageView.sd_setImage(
				with: seller.info.profileImageUrl
			)
			self.title.text = seller.info.caption

		case .consumer(let consumer):
			self.placeholderImageView.isHidden = false
			self.mainImageView.sd_setImage(with: nil)

			let title = consumer.info.deliveryAddresses?.last?.toString() ?? ""
			self.title.text = title
		}

		updateUnreadState(chat.receiver.id)

		self.disposeBag = DisposeBag()

		managers.chats.unreadChatsCountObservable
			.subscribe(onNext: { [weak self] _ in self?.updateUnreadState(chat.receiver.id) })
			.disposed(by: disposeBag)
    }

	private func updateUnreadState(_ receiverId: String)
	{
		let newCount = managers.chats.unreadCount(receiverId)

		unreadIndicatorView.isHidden = newCount == 0
	}
}

// MARK:- Constants
extension ChatPreviewCollectionViewCell
{
    struct Constants
    {
        static let aspectRatio = CGFloat(167)/CGFloat(200)
    }
}
