//
//  FoundProductTableCell.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 09.09.2021.
//

import UIKit



class FoundProductTableCell: UITableViewCell
{
	@IBOutlet private var leftImageView: UIImageView!
	@IBOutlet private var nameLabel: UILabel!
	@IBOutlet private var priceLabel: UILabel!
	@IBOutlet private var goToSellerButton: UIButton!
	@IBOutlet private var priceStepper: ListCartProductQuantityControl!
	
	private var goToSellerAction: VoidCallback!
	
	
	func configure(
		foundProduct: FoundProduct,
		presentPopupIfUnauthorized: @escaping VoidCallback,
		onProductQuantityChangedIfAuthorized: @escaping OnProductQuantityChanged,
		goToSellerAction: @escaping VoidCallback
	)
	{
		let cartProduct = foundProduct.cartProduct

		leftImageView.sd_setImage(
			with: cartProduct.product.imageUrl
		)

		nameLabel.text = cartProduct.product.name
		
		priceLabel.text = PriceFormatter.catalogItemWithUnits(
			price: FormatterPrice.kopeksToRoubles(cartProduct.product.price),
			measurementUnit: cartProduct.product.unit,
			displayedOn: .sellerScreen
		)
		
		priceStepper.setup(
			cartProduct: cartProduct,
			presentPopupIfUnauthorized: presentPopupIfUnauthorized,
			onProductQuantityChangedIfAuthorized: onProductQuantityChangedIfAuthorized
		)

		//
		goToSellerButton.setTitle(
			foundProduct.seller.caption,
			for: []
		)
		
		self.goToSellerAction = goToSellerAction
	}
	
	
	
	// MARK: - Actions
	
	@IBAction func onGoToSellerButton()
	{
		goToSellerAction()
	}
}
