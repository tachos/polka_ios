//
//  SearchTagCell.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 08.09.2021.
//

import UIKit


class SearchTagCell: UICollectionViewCell
{
	private let label = UILabel()
	
	
	override init(frame: CGRect)
	{
		super.init(frame: frame)
		
		setupUI()
	}

	required init?(coder aDecoder: NSCoder)
	{
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupUI()
	{
		contentView.backgroundColor = Constants.normalBackgroundColor
		contentView.layer.cornerRadius = 12
		
		setupLabel()
	}

	private func setupLabel()
	{
		contentView.addSubview(label)

		label.leadingToSuperview(offset: 20)
		label.trailingToSuperview(offset: 20)
		label.topToSuperview(offset: 8)
		label.bottomToSuperview(offset: -8)

		label.textColor = Constants.normalTextColor
		label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
	}
	
	override var isHighlighted: Bool
	{
		didSet
		{
			label.textColor = self.isHighlighted
				? Constants.highlightedTextColor : Constants.normalTextColor
			
			contentView.backgroundColor = self.isHighlighted
				? Constants.highlightedBackgroundColor : Constants.normalBackgroundColor
		}
	}
	
	override var isSelected: Bool
	{
		didSet
		{
			label.textColor = self.isSelected
				? Constants.selectedTextColor : Constants.normalTextColor
			
			contentView.backgroundColor = self.isSelected
				? Constants.selectedBackgroundColor : Constants.normalBackgroundColor
		}
	}
	
	func configure(title: String)
	{
		label.text = title
	}
}


// MARK: - Constants

private extension SearchTagCell
{
	struct Constants
	{
		static let normalTextColor = Colors.darkMain
		static let highlightedTextColor = Colors.greyText
		static let selectedTextColor = UIColor.white
		
		static let normalBackgroundColor = Colors.greyButton
		static let highlightedBackgroundColor = Colors.greyButton
		static let selectedBackgroundColor = Colors.mainOrange
	}
}
