//
//  OurSellerTableCell.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 09.09.2021.
//

import UIKit


class OurSellerTableCell: UITableViewCell
{
	@IBOutlet private var nameLabel: UILabel!
	@IBOutlet private var descriptionLabel: UILabel!
	
	
	func configure(name: String, description: String)
	{
		nameLabel.text = name
		descriptionLabel.text = description
	}
}
