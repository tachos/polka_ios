//
//  ActivityIndicator.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 10.09.2021.
//

import UIKit



class ActivityIndicator: UIImageView
{
	func startRotating()
	{
		isHidden = false
		addRotationAnimation()
	}
	
	func stopRotating()
	{
		isHidden = true
		removeRotationAnimation()
	}
	
	private func addRotationAnimation()
	{
		let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
		rotation.toValue = NSNumber(value: Double.pi * 2)
		rotation.duration = 1
		rotation.isCumulative = true
		rotation.repeatCount = Float.greatestFiniteMagnitude
		self.layer.add(rotation, forKey: "rotationAnimation")
	}
	
	private func removeRotationAnimation()
	{
		self.layer.removeAnimation(forKey: "rotationAnimation")
	}
}
