//
//  SearchTextField.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 10.09.2021.
//

import UIKit
import TinyConstraints



class SearchTextField: UITextField
{
	// MARK: - Instantiation
	
	required init?(coder: NSCoder)
	{
		super.init(coder: coder)

		setupUI()
	}
	
	
	// MARK: - TouchTrackingTextField
	
	override var intrinsicContentSize: CGSize
	{
		return .init(
			width: 90,
			height: 56
		)
	}
}



// MARK: - Setup

private extension SearchTextField
{
	func setupUI()
	{
		self.backgroundColor = Colors.input
		self.textColor = Colors.darkMain
		self.tintColor = Colors.green
		self.font = UIFont.systemFont(ofSize: 18, weight: .regular)

		setupSideViews()
		
		//
		self.delegate = self
		setupTextHandling()
		
		updateClearButtonVisibility()
		
		setupEditingHandling()
		setupPlaceholder()
	}
	
	private func setupSideViews()
	{
		func createButton(
			icon: UIImage,
			tint: UIColor,
			interactive: Bool = true
		) -> UIButton
		{
			let button = UIButton(type: .system)
			button.setImage(icon, for: [])
			button.tintColor = tint
			button.width(56)
			button.isUserInteractionEnabled = interactive
			return button
		}

		//
		self.leftView = createButton(
			icon: #imageLiteral(resourceName: "search-icon"),
			tint: Colors.darkMain,
			interactive: false
		)
		self.leftViewMode = .always
		
		//
		let clearTextButton = createButton(icon: #imageLiteral(resourceName: "clear-textfield"), tint: Colors.greyText)
		self.rightView = clearTextButton
		self.rightViewMode = .whileEditing
		
		clearTextButton.addTarget(
			self,
			action: #selector(onClearTextButton),
			for: .touchUpInside
		)
	}
	
	private func setupTextHandling()
	{
		autocapitalizationType = .sentences
		autocorrectionType = .no
		spellCheckingType = .no
	}
	
	private func setupEditingHandling()
	{
		addTarget(
			self,
			action: #selector(onEditingDidBegin),
			for: .editingDidBegin
		)
		addTarget(
			self,
			action: #selector(onEditingChanged),
			for: .editingChanged
		)
		addTarget(
			self,
			action: #selector(onEditingDidEnd),
			for: .editingDidEnd
		)
	}

	private func setupPlaceholder()
	{
		self.placeholder = Constants.placeholderText
	}
}



// MARK: - Actions

private extension SearchTextField
{
	@objc func onEditingDidBegin()
	{
	}
	
	@objc func onEditingChanged()
	{
		updateClearButtonVisibility()
	}
	
	@objc func onEditingDidEnd()
	{
	}
	
	@objc func onClearTextButton()
	{
		clearSearchTextField()
	}
}



// MARK: - Managing UI state

private extension SearchTextField
{
	func clearSearchTextField()
	{
		text = nil
		sendActions(for: .editingChanged)
	}

	func updateClearButtonVisibility()
	{
		rightView?.isHidden = (text ?? "").count == 0
	}
}



// MARK: - UITextFieldDelegate

extension SearchTextField: UITextFieldDelegate
{
}



// MARK: - Constants

private extension SearchTextField
{
	struct Constants
	{
		static let placeholderText = "Ищу на Полке"
	}
}
