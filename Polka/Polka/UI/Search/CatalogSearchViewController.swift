//
//  CatalogSearchViewController.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 08.09.2021.
//

import UIKit



class CatalogSearchViewController: UIViewController, StoryboardCreatable
{
	// MARK:- StoryboardCreatable
	static var storyboardName: StoryboardName = .search
	
	
	// MARK:- Outlets
	@IBOutlet var searchTextField: SearchTextField!

	//
	@IBOutlet var tagsCollectionView: UICollectionView!
	@IBOutlet var tagsCollectionViewHeight: NSLayoutConstraint!
	
	@IBOutlet var ourSellersTableView: UITableView!
	@IBOutlet var ourSellersTableViewHeight: NSLayoutConstraint!
	
	private var ourSellersTableCtrl: OurSellersTableController!
	
	//
	@IBOutlet var searchInProgressView: UIView!
	@IBOutlet var activityIndicator: ActivityIndicator!
	
	//
	@IBOutlet var foundProductsTableView: UITableView!
	
	//
	@IBOutlet var nothingFoundView: UIView!
	
	
	
	enum State
	{
		case searchNotStarted
		case searchInProgress
		case foundSomething
		case nothingFound
		case searchFailed(Error)
	}
		
	
	struct Config
	{
		let helper: CatalogSearchHelper
		
		let goToSellerAction: Handler<SellerShortInfo>
		
		// "nothing was found" state:
		let goToChatAction: VoidCallback
		let goToCatalogAction: VoidCallback
	}
	var config: Config!
}



// MARK:- Life cycle

extension CatalogSearchViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		setupUI()
		displayState(.searchNotStarted)
	}
	
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
		
		(navigationController as? NavBarCustomizable)?.setAppearance(
			navBarBackground: .clear,
			hasBackButton: true,
			titleViewType: .category("Поиск"),
			initiallyNotVisible: false
		)
		
		config.helper.getCatalogSearchData(on: self)
		
		setSearchTextFieldActive(true)
	}
	
	override func viewDidLayoutSubviews()
	{
		super.viewDidLayoutSubviews()
		
		tagsCollectionViewHeight.constant = tagsCollectionView.contentSize.height
		ourSellersTableViewHeight.constant = ourSellersTableView.contentSize.height
	}
}



// MARK:- UIResponder

extension CatalogSearchViewController
{
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		super.touchesBegan(touches, with: event)
		
		setSearchTextFieldActive(false)
	}

	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		super.touchesMoved(touches, with: event)
		
		setSearchTextFieldActive(false)
	}
}



// MARK:- UIResponder

extension CatalogSearchViewController: UIScrollViewDelegate
{
	func scrollViewDidScroll(_ scrollView: UIScrollView)
	{
		setSearchTextFieldActive(false)
	}
}



// MARK:- UI Setup

private extension CatalogSearchViewController
{
	func setupUI()
	{
		activityIndicator.startRotating()
		
		setupTagsCollectionView()
		setupOurSellersTableView()

		setupOurFoundProductsTableView()
	}
	
	private func setupTagsCollectionView()
	{
		tagsCollectionView.register(
			SearchTagCell.self,
			forCellWithReuseIdentifier: SearchTagCell.typeName
		)
		
		tagsCollectionView.dataSource = self
		tagsCollectionView.delegate = self
		
		tagsCollectionView.isScrollEnabled = false
		
		tagsCollectionView.allowsSelection = true
		tagsCollectionView.allowsMultipleSelection = true
		
		//
		class LeftAlignedCollectionViewFlowLayout: UICollectionViewFlowLayout
		{
			// https://medium.com/@balzsvincze/left-aligned-uicollectionview-layout-1ff9a56562d0
			override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]?
			{
				let superAttributesArray = super.layoutAttributesForElements(in: rect)!
				var newAttributesArray = [UICollectionViewLayoutAttributes]()

				for (index, attributes) in superAttributesArray.enumerated()
				{
					if index == 0 || superAttributesArray[index - 1].frame.origin.y != attributes.frame.origin.y
					{
						attributes.frame.origin.x = sectionInset.left
					}
					else
					{
						let previousAttributes = superAttributesArray[index - 1]
						let previousFrameRight = previousAttributes.frame.origin.x + previousAttributes.frame.width
						attributes.frame.origin.x = previousFrameRight + minimumInteritemSpacing
					}
					newAttributesArray.append(attributes)
				}
				return newAttributesArray
			}
		}
		
		//
		let collectionViewLayout = LeftAlignedCollectionViewFlowLayout()
		collectionViewLayout.sectionInset = .init(
			top: 0, left: 16,
			bottom: 0, right: 16
		)
		collectionViewLayout.minimumLineSpacing = 10
		collectionViewLayout.minimumInteritemSpacing = 5

		collectionViewLayout.itemSize = .init(
			width: UICollectionViewFlowLayout.automaticSize.width,
			height: Constants.itemHeight
		)
		collectionViewLayout.estimatedItemSize = .init(
			width: Constants.estimatedItemWidth,
			height: Constants.itemHeight
		)
		
		tagsCollectionView.setCollectionViewLayout(
			collectionViewLayout,
			animated: false
		)
	}
	
	private func setupOurSellersTableView()
	{
		ourSellersTableCtrl = OurSellersTableController(
			goToSellerAction: config.goToSellerAction
		)
		
		ourSellersTableView.dataSource = ourSellersTableCtrl
		ourSellersTableView.delegate = ourSellersTableCtrl
	}
	
	private func setupOurFoundProductsTableView()
	{
		foundProductsTableView.dataSource = self
		foundProductsTableView.delegate = self
	}
}



// MARK: - Search

extension CatalogSearchViewController
{
	func onSearchDataLoaded()
	{
		tagsCollectionView.reloadData()
		ourSellersTableView.reloadData()
		
		// resize scroll view
		DispatchQueue.main.async { [weak self] in
			self?.view.layout()
		}
	}
	
	func displayState(_ state: State)
	{
		switch state
		{
		case .searchNotStarted:
			searchInProgressView.isHidden = true
			foundProductsTableView.isHidden = true
			nothingFoundView.isHidden = true
		
		case .searchInProgress:
			searchInProgressView.isHidden = false
			foundProductsTableView.isHidden = true
			nothingFoundView.isHidden = true
			
			foundProductsTableView.reloadData()
			
		case .foundSomething:
			searchInProgressView.isHidden = true
			foundProductsTableView.isHidden = false
			nothingFoundView.isHidden = true
			
			foundProductsTableView.reloadData()
			foundProductsTableView.layoutSubviews()	// recalculate content size
			
		case .nothingFound, .searchFailed:
			searchInProgressView.isHidden = true
			foundProductsTableView.isHidden = true
			nothingFoundView.isHidden = false
		}
	}
	
	func setSearchTextFieldActive(_ activateSearchField: Bool)
	{
		_ = activateSearchField
			? searchTextField.becomeFirstResponder()
			: searchTextField.resignFirstResponder()
	}
}



// MARK: - Actions

extension CatalogSearchViewController
{
	@IBAction func onSearchFieldEditingChanged()
	{
		config.helper.handleSearchInput(
			searchQuery: searchTextField.text,
			on: self
		)
	}

	@IBAction func onGoToChatButton()
	{
		config.goToChatAction()
	}
	
	@IBAction func onGoToCatalogButton()
	{
		config.goToCatalogAction()
	}
}



// MARK: - UICollectionViewDataSource

extension CatalogSearchViewController: UICollectionViewDataSource
{
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		return CatalogSearchHelper.searchData.filters.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{
		let cell = collectionView.dequeueReusableCell(
			ofType: SearchTagCell.self,
			for: indexPath
		)!

		let searchCategory = CatalogSearchHelper.searchData.filters[indexPath.item]
		
		cell.configure(
			title: searchCategory.caption
		)

		return cell
	}
}



// MARK: - UICollectionViewDelegate

extension CatalogSearchViewController: UICollectionViewDelegate
{
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
	{
		config.helper.activateSearchFilter(at: indexPath.item)
	}
	
	func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
	{
		config.helper.deactivateSearchFilter(at: indexPath.item)
	}
}




private class OurSellersTableController: NSObject, UITableViewDataSource, UITableViewDelegate
{
	let goToSellerAction: Handler<SellerShortInfo>
	
	init(goToSellerAction: @escaping Handler<SellerShortInfo>)
	{
		self.goToSellerAction = goToSellerAction
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return CatalogSearchHelper.searchData.sellers.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		let cell = tableView.dequeueReusableCell(
			ofType: OurSellerTableCell.self
		)!

		let seller = CatalogSearchHelper.searchData.sellers[indexPath.item]

		cell.configure(
			name: seller.caption,
			description: seller.description
		)

		return cell
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		tableView.deselectRow(at: indexPath, animated: true)
		
		let seller = CatalogSearchHelper.searchData.sellers[indexPath.item]
		
		goToSellerAction(seller)
	}
}





// MARK: - UITableViewDataSource, UITableViewDelegate

extension CatalogSearchViewController: UITableViewDataSource, UITableViewDelegate
{
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return config.helper.foundProducts.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		let cell = tableView.dequeueReusableCell(
			ofType: FoundProductTableCell.self
		)!

		let foundProduct = config.helper.foundProducts[indexPath.item]

		cell.configure(
			foundProduct: foundProduct,
			presentPopupIfUnauthorized: { [unowned self] in
				//
				PopUps.promptUserToAuthorize(on: self)
			},
			onProductQuantityChangedIfAuthorized: { newQuantity in
				//
				CartController.shared.onProductQuantityChanged(
					newQuantity: newQuantity,
					cartProduct: foundProduct.cartProduct,
					//SVD: TODO:
					seller: Seller(
						id: foundProduct.seller.id,
						info: SellerInfo(
							caption: foundProduct.seller.caption,
							description: foundProduct.seller.description,
							profileImageUrl: nil,
							sellerGallery: .empty,
							isOpen: foundProduct.seller.isOpen,
							isPriceActual: true
						)
					)
				)
			},
			goToSellerAction: { [unowned self] in
				//
				self.config.goToSellerAction(
					foundProduct.seller
				)
			}
		)

		return cell
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		tableView.deselectRow(at: indexPath, animated: true)
		
		let foundProduct = config.helper.foundProducts[indexPath.item]

		print("Selected \(foundProduct)")
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
	{
		if isLastIndexPath(indexPath)
		{
			config.helper.onFoundProductsTableScrolledToBottom(on: self)
		}
	}
	
	private func isLastIndexPath(_ indexPath: IndexPath) -> Bool
	{
		let lastIndexPath = IndexPath(
			row: config.helper.foundProducts.count - 1,
			section: 0
		)
		
		return indexPath == lastIndexPath
	}
}



// MARK: - Constants

private extension CatalogSearchViewController
{
	struct Constants
	{
		static let itemHeight: CGFloat = 32
		static let estimatedItemWidth: CGFloat = 90
	}
}
