//
//  CatalogSearchHelper.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 10.09.2021.
//

import UIKit
import PromiseKit



class CatalogSearchHelper
{
	// cached data:
	private(set) static var searchData = CatalogSearchData(
		filters: [],
		sellers: []
	)
	private static var isSearchDataLoaded = false
	
	//
	private let currentMarketAlias: MarketAlias?
	
	//
	private var activeSearchFilterIndices = Set<Int>()

	private(set) var foundProducts: [FoundProduct] = []
	private var canLoadMoreProducts = true
	
	private var currentSearchQuery: String?
	private var searchInputTimer: Timer?
	private var cancelSearch: AlamofireUtils.Cancel? = nil
	
	
	init()
	{
		currentMarketAlias = managers.localStore.currentMarket.warnIfNil()?.alias
	}
}



// MARK: - API / Search Filters/Categories

extension CatalogSearchHelper
{
	func getCatalogSearchData(on vc: CatalogSearchViewController)
	{
		if Self.isSearchDataLoaded {
			vc.onSearchDataLoaded()
			return
		}
		
		guard let currentMarketAlias = currentMarketAlias.warnIfNil()
		else {
			return
		}
		
		vc.displayState(.searchInProgress)
		
		firstly {
			managers.webService.getCatalogSearchData(
				marketAlias: currentMarketAlias
			)
		}
		.done { [weak vc] in
			//
			Self.searchData = $0
			Self.isSearchDataLoaded = true
			
			vc?.onSearchDataLoaded()
		}
		.cauterize()
		.finally { [weak vc] in
			//
			vc?.displayState(.searchNotStarted)
		}
	}
	
	func activateSearchFilter(at index: Int)
	{
		activeSearchFilterIndices.insert(index)
	}
	
	func deactivateSearchFilter(at index: Int)
	{
		activeSearchFilterIndices.remove(index)
	}
	
	func onFoundProductsTableScrolledToBottom(on vc: CatalogSearchViewController)
	{
		if let currentSearchQuery = currentSearchQuery,
		   canLoadMoreProducts
		{
			loadMoreProducts(
				currentSearchQuery,
				on: vc
			)
		}
	}
}



// MARK: - API / Search

extension CatalogSearchHelper
{
	func handleSearchInput(
		searchQuery: String?,
		on vc: CatalogSearchViewController
	)
	{
		searchInputTimer?.invalidate()
		cancelSearch?()

		let text = searchQuery ?? ""

		if let searchQuery = searchQuery,
		   searchQuery.count >= Constants.minSearchQueryLength
		{
			currentSearchQuery = searchQuery
			searchAfterDelay(text, on: vc)
		}
		else
		{
			currentSearchQuery = nil
			vc.displayState(.searchNotStarted)
		}
	}

	private func searchAfterDelay(
		_ query: String,
		on vc: CatalogSearchViewController
	)
	{
		searchInputTimer = Timer.scheduledTimer(
			withTimeInterval: Constants.searchRequestDelay,
			repeats: false,
			block: { [weak self, weak vc] _ in
				if let vc = vc {
					self?.searchProducts(query, on: vc)
				}
			}
		)
	}
	
	private func searchProducts(
		_ query: String,
		on vc: CatalogSearchViewController
	)
	{
		resetSearchResults()
		vc.displayState(.searchInProgress)
		
		loadMoreProducts(
			query,
			on: vc
		)
	}
	
	private func loadMoreProducts(
		_ query: String,
		on vc: CatalogSearchViewController
	)
	{
		guard let currentMarketAlias = currentMarketAlias.warnIfNil()
		else {
			return
		}

		let nextPage = WebServiceManager.Page(offset: foundProducts.count)
		
		let activeSearchCategories = activeSearchFilterIndices
			.map { Self.searchData.filters[$0] }
		
		firstly {
			managers.webService.searchProducts(
				marketAlias: currentMarketAlias,
				query: query,
				filters: activeSearchCategories,
				page: nextPage,
				cancel: &cancelSearch
			)
		}
		.done { [weak self, weak vc] newFoundProducts in
			//
			let hasLoadedMore = !newFoundProducts.isEmpty
			
			self?.canLoadMoreProducts = hasLoadedMore
			
			if let vc = vc
			{
				if hasLoadedMore
				{
					self?.appendFoundProducts(
						newFoundProducts,
						on: vc
					)
				}
				else
				{
					self?.updateState(on: vc)
				}
			}
		}
		.catch { [weak vc] error in
			//
			vc?.displayState(.searchFailed(error))
		}
	}
	
	private func appendFoundProducts(
		_ newFoundProducts: [FoundProduct],
		on vc: CatalogSearchViewController
	)
	{
		let oldCount = foundProducts.count
		let newCount = oldCount + newFoundProducts.count

		foundProducts.append(contentsOf: newFoundProducts)

		//
		let newIndexPaths: [IndexPath] = Array(oldCount...(newCount - 1))
			.map { IndexPath(row: $0, section: 0) }
		
		//
		let tableView = vc.foundProductsTableView!
		
		let contentOffset = tableView.contentOffset
		
		tableView.performBatchUpdates(
			{
				tableView.insertRows(
					at: newIndexPaths,
					with: .fade
				)

				tableView.setContentOffset(contentOffset, animated: false)

				updateState(on: vc)
			},
			completion: nil
		)
	}
	
	private func updateState(on vc: CatalogSearchViewController)
	{
		if foundProducts.isEmpty && !canLoadMoreProducts
		{
			vc.displayState(.nothingFound)
		}
		else
		{
			vc.displayState(.foundSomething)
		}
	}
	
	private func resetSearchResults()
	{
		foundProducts.removeAll()
	}
}



// MARK: - Constants

private extension CatalogSearchHelper
{
	struct Constants
	{
		static let minSearchQueryLength = 3
		static let searchRequestDelay: TimeInterval = 0.3
	}
}
