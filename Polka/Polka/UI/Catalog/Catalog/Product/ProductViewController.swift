//
//  DetailsSellerCardViewController.swift
//  Polka
//
//  Created by Makson on 26.04.2021.
//

import UIKit
import TinyConstraints

class ProductViewController: UIViewController,StoryboardCreatable
{
	//MARK: - StoryboardCreatable
	static let storyboardName: StoryboardName = .product
    
    //MARK: - Outlet
    @IBOutlet private var gallerySliderView: UIView!
	weak var gallerySliderContainer: BackgroundGallery!
    @IBOutlet private var nameLbl: UILabel!
    @IBOutlet private var descriptionLbl: UILabel!
    @IBOutlet private var priceLbl: UILabel!
    @IBOutlet private var quantityControl: ProductScreenCartProductQuantityControl!
    
    @IBOutlet private var containerStepperView: UIView!

	
	//MARK: - Variable
	private var minStep:Float = 0.0

	
	struct Config
	{
		let cartProduct: CartProduct
		let isSellerOpen: Bool
		
		let back: VoidCallback
	}
	
	var config: Config!
}


//MARK: - Lifecycle
extension ProductViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		setup()
	}
	
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
		
		(self.navigationController as? NavBarCustomizable)?.setAppearance(navBarBackground: .clear,
																		 hasBackButton: true,
																		 titleViewType: nil,
																		 initiallyNotVisible: false)
	}
}
extension ProductViewController
{
	func setup()
	{
		setupNameLbl(title: self.config.cartProduct.product.name)
		setupDescription(description: self.config.cartProduct.product.description)
		setupPriceLbl(
			price: self.config.cartProduct.product.price,
			unit: self.config.cartProduct.product.unit
		)
		setupGallery()
		setupQuantityControl()
	}
}

//MARK: - Initialization
extension ProductViewController
{
	func setupQuantityControl()
	{
        quantityControl.setup(editingEnabled: true,
							  cartProduct: self.config.cartProduct,
							  onQuantityChanged: { (val: Float) in
							  },
							  onGoToCart: {[weak self] in self?.getTabBarController()?.switchToTab(.cart)
							  })
	}
	
	func setupGallery()
	{
		if gallerySliderContainer == nil
		{
			let viewToEmbed = BackgroundGallery.create(
				imageUrls: [],
				isSellerOpen: self.config.isSellerOpen
			)
			
			self.gallerySliderView.addSubview(viewToEmbed)
			
			viewToEmbed.edgesToSuperview()
			
			self.gallerySliderContainer = viewToEmbed
			print(gallerySliderContainer.frame.height)
		}
		else
		{
			gallerySliderContainer.setupAppearance(
				imageUrls: [],
				isSellerOpen: self.config.isSellerOpen
			)
		}
	}
	
	func setupNameLbl(title: String)
	{
		if title != ""
		{
			nameLbl.text = title
		}
		else
		{
			nameLbl.isHidden = true
		}
	}
	
	func setupDescription(description:String)
	{
		if description != ""
		{
			descriptionLbl.text = description
		}
		else
		{
			descriptionLbl.isHidden = true
		}
	}
	
	func setupPriceLbl(price:Int, unit: MeasurementUnit)
	{
		if price != 0
		{
			priceLbl.text = PriceFormatter.catalogItemWithUnits(price: FormatterPrice.kopeksToRoubles(price),
																measurementUnit: unit,
																displayedOn: .aboutProductScreen)
		}
		else
		{
			priceLbl.isHidden = true 
		}
	}
}
