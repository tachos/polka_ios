//
//  CatalogViewController.swift
//  Polka
//
//  Created by Michael Goremykin on 20.04.2021.
//

import UIKit
import RxSwift
import MBProgressHUD

class CatalogViewController: UIViewController, StoryboardCreatable
{
    // MARK:- StoryboardCreatable
    static let storyboardName: StoryboardName = .catalog
    
    // MARK:- Outlets
    @IBOutlet var categoriesCollectionView: UICollectionView!
    @IBOutlet var informationAddressView: UIView!
	

	// for dynamic height calculation
	var catalogHeaderHeight: CGFloat?
	

    // MARK:- Stateful
	var categories: [Category] = []
	var cancelCategoriesLoad: AlamofireUtils.Cancel?
	
	var lastContentOffset: CGFloat = 0
	var navBarVisibilityToggled = false

    
	// MARK:- Rx
	let disposeBag = DisposeBag()
	
	
	struct Config
	{
		let onViewWillAppear: VoidCallback
		
		let showSellersByCategoryScreen: Handler<Category>

		let searchButtonAction: VoidCallback
		
		let showMarketsSegue: VoidCallback
		
		let deliveryAddressSegue: VoidCallback
	}
	var config: Config!
}

// MARK:- Life cycle
extension CatalogViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
		
        setupUI()
		
		subscribeToAppEvents()
		subscribeToUserEvents()
		subscribeToCatalogEvents()
    }
	
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
		
		(self.navigationController as? NavBarCustomizable)?.setAppearance(
			navBarBackground: .blurred,
			hasBackButton: false,
			titleViewType: .tabTitleHeader("Каталог"),
			initiallyNotVisible: lastContentOffset < (getNavBarHeight() ?? 0)
		)
		displayCachedCategoriesIfGotAny()
		getCategoriesIfNeeded()
		updateStateInformationAddressViewAction()
		
		config.onViewWillAppear()
	}
}



// MARK: - API

extension CatalogViewController
{
	func onSelectedMarketChanged(_ market: Market)
	{
		// accomodate market descriptions of varying lengths
		recomputeHeaderHeight()
		
		loadProductCategories()
	}
}



// MARK: - Action
extension CatalogViewController
{
	@IBAction func closeInformationAddressViewAction()
	{
		informationAddressView.isHidden = true
	}
	
	@IBAction func searchButtonAction()
	{
		config.searchButtonAction()
	}
}


//
extension CatalogViewController
{
	func recomputeHeaderHeight()
	{
		catalogHeaderHeight = nil
		categoriesCollectionView.collectionViewLayout.invalidateLayout()
	}
	
	func updateStateInformationAddressViewAction()
	{
		informationAddressView.isHidden
			= managers.webService.deliveryAddress != nil
			|| !managers.webService.isAuthorized
	}
}
