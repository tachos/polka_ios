//
//  CatalogViewController+Utils.swift
//  Polka
//
//  Created by Michael Goremykin on 05.05.2021.
//

import UIKit
import MBProgressHUD


// MARK:- WebService

extension CatalogViewController
{
	func loadProductCategories()
	{
		dbgtrace()
		
		showProgressIndicator()
		
		cancelCatalogLoadingRequest()
		
		CatalogController.shared.loadProductCategories(
			onFailure: { [weak self] in
				//
				if let `self` = self {
					PopUps.showRetryQuery(
						from: self,
						queryRetryCallback: {[weak self] in self?.loadProductCategories() },
						onCancelCallback: {[weak self] in self?.setupCells(categories: []) }
					)
				}
			},
			completion: { [weak self] in
				//
				self?.hideProgressIndicator()
			},
			cancel: &cancelCategoriesLoad
		)
	}
	
	func subscribeToAppEvents()
	{
		managers.appEvents.appWillMoveToBackgroundObservable
			.subscribe(onNext: {
				[weak self] in self?.cancelCatalogLoadingRequest()
		})
			.disposed(by: disposeBag)
		
		managers.appEvents.appMovedToForegroundObservable
			.subscribe(onNext: {
				[weak self] in self?.startCatalogLoadingRequestIfNeeded()
		})
			.disposed(by: disposeBag)
	}
	
	private func cancelCatalogLoadingRequest()
	{
		cancelCategoriesLoad?()
	}
	
	private func startCatalogLoadingRequestIfNeeded()
	{
		if categories.isEmpty {
			loadProductCategories()
		}
	}
	
	
	func subscribeToCatalogEvents()
	{
		CatalogController.shared.categoriesStateObservable.subscribe(onNext: {[weak self] categoriesState in
			self?.onCatalogStateChanged(categories: categoriesState.value)
		}).disposed(by: disposeBag)
	}
	
	private func onCatalogStateChanged(categories: [Category])
	{
		setupCells(categories: categories)
		categoriesCollectionView.reloadData()
	}
	
	
	
	func subscribeToUserEvents()
	{
		UserController.shared.userLocalEventsObservable
			.subscribe(
				onNext:
		{
			[weak self] userEvent in

			switch userEvent
			{
			case .newDeliveryAddressHasBeenSet:
				self?.recomputeHeaderHeight()
			}
		}).disposed(by: disposeBag)
	}
}



// MARK:- Loading Progress Indicator

extension CatalogViewController
{
	func showProgressIndicator()
	{
		_ = self.view.addProgressIndicator()
	}
	
	func hideProgressIndicator()
	{
		MBProgressHUD.hide(for: self.view, animated: true)
	}
}



// MARK:- Utils

extension CatalogViewController
{
	func setupCells(categories: [Category])
	{
		self.categories = categories
		
		self.categoriesCollectionView.reloadData()
	}
	
	func displayCachedCategoriesIfGotAny()
	{
		//SVD: wtf?
		if CacheController.shared.getCachedCategories().count > 0
		{
			setupCells(categories: CacheController.shared.getCachedCategories())
		}
	}
	
	func getCategoriesIfNeeded()
	{
		if !cellsContainCategoryItems() || CatalogController.shared.categoriesState.isNotSynced()
		{
			loadProductCategories()
		}
	}
	
	func cellsContainCategoryItems() -> Bool
	{
		return !categories.isEmpty
	}
	
	func getCachedCategories()
	{
		let cachedCategories = CacheController.shared.getCachedCategories()
		
		if cachedCategories.count > 0
		{
			setupCells(categories: cachedCategories)
		}
	}
}
