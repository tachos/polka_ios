//
//  CatalogFooterView.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 22.10.2021.
//

import UIKit



class CatalogFooterView: UICollectionViewCell
{
	// MARK:- Parameters
	private var onActionTapped: (() -> Void)!
}



// MARK:- Initialization

extension CatalogFooterView
{
	func initialize(_ onActionTapped: @escaping () -> Void)
	{
		self.onActionTapped = onActionTapped
	}
}



// MARK:- UI handlers

extension CatalogFooterView
{
	@IBAction func onTapped(_ sender: Any)
	{
		onActionTapped()
	}
}



extension CatalogFooterView
{
	struct Constants
	{
		static let height = CGFloat(124+104)
	}
}
