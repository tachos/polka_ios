//
//  CatalogHeaderView.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 22.10.2021.
//

import UIKit
import RxSwift


class CatalogHeaderView: UICollectionViewCell
{
	// MARK:- Outlets
	@IBOutlet private var marketNameLabel: UILabel!
	@IBOutlet private var showMarketsButton: UIButton!
	@IBOutlet private var marketWorkHoursLabel: UILabel!
	@IBOutlet private var marketDescriptionLabel: UILabel!
	
	@IBOutlet private var addressPickerContainer: UIView!
	@IBOutlet private var addressPickerHeight: NSLayoutConstraint!

	private var addressView: AddressView!
	
	private var showMarketsSegue: VoidCallback?
	private var goBackToCatalogSegue: VoidCallback?
	
	// MARK:- RX
	private let disposeBag = DisposeBag()
}


// MARK:- Constants
extension CatalogHeaderView
{
	func initialize(
		tipTitle: String,
		resourceImageName: String?,
		title:String?,
		market: Market?,
		deliveryAddressSegue: @escaping VoidCallback,
		showMarketsSegue: @escaping VoidCallback
	)
	{
		self.showMarketsSegue = showMarketsSegue
		
		showMarketsButton.isHidden = market == nil
		
		//
		marketNameLabel.text = market?.caption
		marketWorkHoursLabel.text = market?.workHoursString
		marketDescriptionLabel.text = market?.description
		
		//
		setAddressPickerVisible(managers.webService.isAuthorized)
		
		addressView.initialize(
			nameLbl: tipTitle,
			image: resourceImageName,
			text: title,
			type: .availableTapGesture,
			onPress: deliveryAddressSegue
		)
	}
	
	func setAddressPickerVisible(_ showAddressPicker: Bool)
	{
		addressPickerHeight.constant = showAddressPicker
									? Constants.addressPickerHeight
									: 0
	}
}



// MARK:- Life cycle

extension CatalogHeaderView
{
	override func awakeFromNib()
	{
		super.awakeFromNib()
		
		embedAddressPicker()
		subscribeToUserEvents()
	}
	
	private func embedAddressPicker()
	{
		addressView = AddressView.setInContainer(
			addressPickerContainer,
			"",
			nil,
			nil,
			.availableTapGesture,
			onPress: nil
		)
	}
}



// MARK:- Utils

private extension CatalogHeaderView
{
	func subscribeToUserEvents()
	{
		UserController.shared.userLocalEventsObservable.subscribe(onNext: {[weak self] userEvent in
			switch userEvent
			{
			case .newDeliveryAddressHasBeenSet(_):
				self?.setAddressPickerVisible(true)
			}
		}).disposed(by: disposeBag)
	}
}



// MARK: - Action

private extension CatalogHeaderView
{
	@IBAction func onShowMarketsButton()
	{
		showMarketsSegue.warnIfNil()?()
	}
}



// MARK: - Constants

private extension CatalogHeaderView
{
	struct Constants
	{
		static let addressPickerHeight: CGFloat = 54
	}
}
