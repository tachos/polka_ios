//
//  CatalogItemCollectionViewCell.swift
//  Polka
//
//  Created by Michael Goremykin on 20.04.2021.
//

import UIKit
import SDWebImage



class CatalogItemCollectionViewCell: UICollectionViewCell
{
	@IBOutlet private var catalogItemImageView: UIImageView!
	@IBOutlet private var title: UILabel!
}



// MARK:- Initialization

extension CatalogItemCollectionViewCell
{
	func initialize(_ imageUrl: URL?,  _ title: String)
    {
        self.title.text = title
		
		catalogItemImageView.sd_setImage(
			with: imageUrl,
			placeholderImage: UIImage(named: "smile"),
			options: []
		)
    }
}
