//
//  CatalogViewController+UI.swift
//  Polka
//
//  Created by Michael Goremykin on 05.05.2021.
//

import UIKit



// MARK:- UI setup

extension CatalogViewController
{
	func setupUI()
	{
		setupCollectionView()
		setupInformationAddressView()
	}

	private func setupInformationAddressView()
	{
		informationAddressView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
		informationAddressView.layer.shadowOpacity = 1
		informationAddressView.layer.shadowOffset = .zero
		informationAddressView.layer.shadowRadius = 40
		informationAddressView.layer.masksToBounds = false
	}

	private func setupCollectionView()
	{
		categoriesCollectionView.delegate   = self
		categoriesCollectionView.dataSource = self
	}
}



// MARK:- UICollectionViewDataSource

extension CatalogViewController: UICollectionViewDataSource
{
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		return categories.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{
		guard let cell = categoriesCollectionView.dequeueReusableCell(
			ofType: CatalogItemCollectionViewCell.self, for: indexPath
		).warnIfNil()
		else
		{
			return  UICollectionViewCell()
		}

		//
		let category = categories[indexPath.row]
		cell.initialize(category.imageUrl, category.caption)
		
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
	{
		switch kind
		{
		case UICollectionView.elementKindSectionHeader:
			let headerView = collectionView.dequeueReusableSupplementaryView(
				ofKind: kind,
				withReuseIdentifier: CatalogHeaderView.typeName,
				for: indexPath
			) as! CatalogHeaderView
						
			headerView.initialize(
				tipTitle: Address.addressLbl(),
				resourceImageName: "edit",
				title: Address.getAddressConsumer(),
				market: managers.localStore.currentMarket,
				deliveryAddressSegue: config.deliveryAddressSegue,
				showMarketsSegue: config.showMarketsSegue
			)
			
			if catalogHeaderHeight == nil
			{
				recomputeHeaderHeight()
				
				headerView.layoutIfNeeded()
				
				var targetSize = CGSize()
				targetSize.height = CGFloat.greatestFiniteMagnitude
				targetSize.width = collectionView.frame.width
				
				catalogHeaderHeight = ceil(headerView.systemLayoutSizeFitting(
					targetSize,
					withHorizontalFittingPriority: .required,
					verticalFittingPriority: .fittingSizeLevel
				).height)
			}
			
			return headerView
			
		case UICollectionView.elementKindSectionFooter:
			let footerView = collectionView.dequeueReusableSupplementaryView(
				ofKind: kind,
				withReuseIdentifier: CatalogFooterView.typeName,
				for: indexPath
			) as! CatalogFooterView

			footerView.initialize({[weak self] in
				//
				self?.getTabBarController()?.showContactsActionSheet()
			})

			return footerView
			
		default:
			dbgUnreachable("Unexpected element kind: \(kind)")
			return UICollectionReusableView()
		}
	}
}



// MARK:- UICollectionViewDelegate

extension CatalogViewController: UICollectionViewDelegate
{
	func scrollViewDidScroll(_ scrollView: UIScrollView)
	{
		if scrollView.contentOffset.y > (getNavBarHeight() ?? 0) && !navBarVisibilityToggled
		{
			(self.navigationController as? NavigationController)?.toggleVisibility()
			
			navBarVisibilityToggled = true
		}
		else if lastContentOffset > scrollView.contentOffset.y &&
					scrollView.contentOffset.y < (getNavBarHeight() ?? 0)  && navBarVisibilityToggled
		{
			(self.navigationController as? NavigationController)?.toggleVisibility()
			
			navBarVisibilityToggled = false
		}

		lastContentOffset = scrollView.contentOffset.y
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
	{
		let category = categories[indexPath.item]
		config.showSellersByCategoryScreen(category)
	}
}



// MARK:- UICollectionViewDelegateFlowLayout

extension CatalogViewController: UICollectionViewDelegateFlowLayout
{
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
	{
		// You need this because this delegate method will run at least
		// once before the header is available for sizing.
		// Returning zero will stop the delegate from trying to get a supplementary view
		if catalogHeaderHeight == nil
		{
			recomputeHeaderHeight()
		}
		
		return CGSize(
			width: 1,
			height: catalogHeaderHeight ?? 300//big enough to avoid autolayout warnings
		)
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
	{
		let cellWidth = ceil(
			(collectionView.frame.width
			- Constants.collectionViewSideMargin*2
			- Constants.collectionViewintercellSpacing
			) * 0.5
		)

		let cellHeight = ceil(
			cellWidth * Constants.categoryCellHeightToWidthRatio
		)
		
		return CGSize(
			width: cellWidth,
			height: cellHeight
		)
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize
	{
		return .init(
			width: UIScreen.main.bounds.width,
			height: CatalogFooterView.Constants.height
		)
	}
}


private extension CatalogViewController
{
	struct Constants
	{
		static let collectionViewSideMargin: CGFloat = 16
		static let collectionViewintercellSpacing: CGFloat = 10
		static let categoryCellHeightToWidthRatio = CGFloat(232)/CGFloat(167)
	}
}



#warning("MOVE!")
extension UIViewController
{
	func isVisible() -> Bool
	{
		//SVD
		self.viewIfLoaded?.window != nil
	}
}
