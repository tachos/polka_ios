//
//  SellerCardViewController+UI.swift
//  Polka
//
//  Created by Michael Goremykin on 29.04.2021.
//

import UIKit


// MARK:- Setup UI
extension SellerViewController
{
	func setupUI()
	{
		setupSellersProductsCollectionView()
	}
	
	func setupSellersProductsCollectionView()
	{
		collectionView.registerNibCell(SellerInfoCollectionViewCell.self)
		collectionView.registerNibCell(SellerProductCollectionViewCell.self)
		collectionView.registerNibCell(ProductsObtainingProgressStatusCollectionViewCell.self)
	}
}

// MARK:- Navbar
extension SellerViewController
{
	func setupNavBarAppearance()
	{
		(self.navigationController as? NavBarCustomizable)?.setAppearance(navBarBackground: .clear,
																		 hasBackButton: true,
																		 titleViewType: nil,
																		 initiallyNotVisible: false)
	}
}

// MARK: - UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension SellerViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
	func numberOfSections(in collectionView: UICollectionView) -> Int
	{
		return Constants.countSection
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
	{
		if section == 0
		{
		   return 0
		}
		else
		{
			if portionOfCatalogWasFetched
			{
				return 24
			}
			else
			{
				return 0
			}
		}
	}
	
	func collectionView(_ collectionView: UICollectionView,
						  layout collectionViewLayout: UICollectionViewLayout,
						  insetForSectionAt section: Int) -> UIEdgeInsets
	{
		if section == 0 || !portionOfCatalogWasFetched
		{
			return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		}
		else
		{
			return UIEdgeInsets(top: 16, left: 19, bottom: 24, right: 19)
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
	{
		if indexPath.section == 0
		{
			return CGSize(width: self.view.frame.width, height: SellerInfoCollectionViewCell.Constants.height)
		}
		else
		{
			if portionOfCatalogWasFetched
			{
				return SellerProductCollectionViewCell.Constants.size
			}
			else
			{
				return CGSize(width: self.view.frame.width,
							  height: UIScreen.main.bounds.height - SellerInfoCollectionViewCell.Constants.height)
			}
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		if section == 0
		{
			return 1
		}
		else
		{
			if portionOfCatalogWasFetched
			{
				return storedCartProducts.count
			}
			else
			{
				return 1
			}
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{
		if indexPath.section == 0
		{
			return getSellerInfoCell(collectionView, indexPath)
		}
		else
		{
			if portionOfCatalogWasFetched
			{
				let cell = collectionView.dequeueReusableCell(ofType: SellerProductCollectionViewCell.self, for: indexPath)!
				
				cell.initialize(
					self,
					storedCartProducts[indexPath.item],
					getOnProductQuantityChangedCallback(storedCartProducts[indexPath.item]),
					isSellerOpen: seller.info.isOpen
				)
				
				return cell
			}
			else
			{
				guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsObtainingProgressStatusCollectionViewCell", for: indexPath) as? ProductsObtainingProgressStatusCollectionViewCell else { return UICollectionViewCell() }
				
				return cell
			}
		}
	}
	
	func getSellerInfoCell(_ collectionView: UICollectionView, _ indexPath: IndexPath) -> UICollectionViewCell
	{
		let cell = collectionView.dequeueReusableCell(ofType: SellerInfoCollectionViewCell.self, for: indexPath)!
		
		cell.initialize(
			title: seller.info.caption,
			description: seller.info.description,
			profileImagesUrls: seller.info.sellerGallery.getProfileImagesUrls(),
			handleChatButton: getChatSegueIfAny(),
			isSellerOpen: seller.info.isOpen
		)
		
		return cell
	}
	
	func getChatSegueIfAny() -> VoidCallback?
	{
		// AAAAA TOREMOVE
		if managers.webService.currentUserProfile != nil
		{
			return {[weak self] in
				guard let self = self else { return }
				self.config.chatSegue?(.init(id: self.seller.id,info: self.seller.info))
			}
		}
		
		return nil
	}
	
	func getOnProductQuantityChangedCallback(_ cartProduct: CartProduct) -> (Float) -> Void
	{
		return {[weak self] (value: Float) in
			guard let self = self else { return }
			
			self.config.cartController.set(
				quantity: value,
				forProduct: cartProduct.product,
				stockQuantity: cartProduct.stockQuantity,
				fromSeller: .init(
					id: self.seller.id,
					info: self.seller.info
				)
			)
		}
	}
}

// MARK:- Pagination
extension SellerViewController
{
	func scrollViewDidScroll(_ scrollView: UIScrollView)
	{
		fetchDataIfNeeded(scrollView)
	}
}
