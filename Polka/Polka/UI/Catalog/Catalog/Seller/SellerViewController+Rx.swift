//
//  SellerViewController+Rx.swift
//  Polka
//
//  Created by Michael Goremykin on 03.08.2021.
//

import RxSwift

// MARK:- Rx
extension SellerViewController
{
	func subscribeToCartEvents()
	{
		_ = config.cartController.cartStateObservable.subscribe(onNext: {[weak self] cartState in
			self?.onCartStateChanged(cartState)
		}).disposed(by: disposeBag)
	}
	
	private func onCartStateChanged(_ cartState: CartState)
	{
		if !cartState.isInCorrectedState()
		{
			self.cells = [[getSellerCellInfoIfAny()].compactMap{ $0 },
						  updateCartProducts(storedCartProducts).map{ .product($0) }].reduce([], +)

			collectionView.reloadData()
		}
		else if self.isVisible() && cartState.isInCorrectedState()
		{
			switch cartState
			{
			case .corrected(_, let error):
				showErrorPopUp(error: error)
			
			case .local, .synced:
				break
			}
		}
	}
}
