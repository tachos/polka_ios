//
//  SellerCardViewController.swift
//  Polka
//
//  Created by Makson on 20.04.2021.
//

import RxSwift

class SellerViewController: UIViewController,StoryboardCreatable
{
    //MARK: - StoryboardName
    static let storyboardName = StoryboardName.seller
	
	
	enum Mode
	{
		case withFullInfo(Seller)
		case fromSearch(SellerShortInfo)
		case fromDeepLink(SellerId)
	}
	
	struct Config
	{
		let mode: Mode

		let cartController: CartControllerProtocol
		
		let categoryPartialInfo: CategoryPartialInfo?

		// nil if the user is not authorized
		let chatSegue: Handler<Seller>?
	}
	var config: Config!
	
	
	//MARK: - Outlets
	@IBOutlet var collectionView: UICollectionView!
	
	// MARK:- Stateful
	var cells: [CellKind] = []
	var portionOfCatalogWasFetched = false
	var backendProductsCount = 0
	var isAlreadyFetching = false
	
	// non-nil to avoid crashes
	var sellerInfo: SellerInfo!

	var sellerId: SellerId
	{
		switch config.mode
		{
		case .withFullInfo(let seller):
			return seller.id
			
		case .fromSearch(let sellerShortInfo):
			return sellerShortInfo.id
			
		case .fromDeepLink(let sellerId):
			return sellerId
		}
	}
	
	var seller: Seller
	{
		switch config.mode
		{
		case .withFullInfo(let seller):
			return seller
			
		case .fromSearch:
			return Seller(id: sellerId, info: sellerInfo)
			
		case .fromDeepLink(let sellerId):
			return Seller(id: sellerId, info: sellerInfo)
		}
	}
	
	// MARK:- Rx
	let disposeBag = DisposeBag()
}


//MARK: - Lifecycle
extension SellerViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
		
		//
		switch config.mode
		{
		case .withFullInfo(let seller):
			self.sellerInfo = seller.info
			
		case .fromSearch(let sellerShortInfo):
			self.sellerInfo = SellerInfo(
				caption: sellerShortInfo.caption,
				description: sellerShortInfo.description,
				profileImageUrl: nil,
				sellerGallery: .empty,
				isOpen: sellerShortInfo.isOpen,
				isPriceActual: false
			)
			
		case .fromDeepLink:
			self.sellerInfo = SellerInfo(
				caption: "",
				description: "",
				profileImageUrl: nil,
				sellerGallery: .empty,
				isOpen: true,
				isPriceActual: false
			)
		}

		//
		self.cells = [.sellerInfo(sellerInfo), .obtainingData]

		//
		setupUI()
		
		subscribeToCartEvents()
		
		getSellerCatalogFirstBatch()
    }
	
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
		
		setupNavBarAppearance()
	}
}
