//
//  SellerViewController+Pagination.swift
//  Polka
//
//  Created by Michael Goremykin on 03.08.2021.
//

import UIKit

extension SellerViewController
{
	func fetchDataIfNeeded(_ scrollView: UIScrollView)
	{
		if isSomeDataNotObtainedFromBackend() &&
		   (hasBeenScrolledToAdditionalDataFetchState(scrollView) || isNotObtainedDataSizeLessThenPageSize()) &&
		   !isAlreadyFetching
		{
			isAlreadyFetching = true
			getSellerCatalogNextBatch()
		}
	}
	
	private func isSomeDataNotObtainedFromBackend() -> Bool
	{
		return storedCartProducts.count < backendProductsCount
	}
	
	private func hasBeenScrolledToAdditionalDataFetchState(_ scrollView: UIScrollView) -> Bool
	{
		let browsedRowsCount = (UIScreen.main.bounds.height - (SellerInfoCollectionViewCell.Constants.height - scrollView.contentOffset.y))/(CGFloat(storedCartProducts.count)/CGFloat(getItemsCountPerRow()))
		
		let fetchedRowsCount = CGFloat(storedCartProducts.count)/CGFloat(getItemsCountPerRow())
		
		return fetchedRowsCount - browsedRowsCount <= Constants.fetchThreshold
	}
	
	private func isNotObtainedDataSizeLessThenPageSize() -> Bool
	{
		return backendProductsCount - storedCartProducts.count <= Constants.itemsPerBatch
	}
}

// MARK:- Next batch
extension SellerViewController
{
	func getSellerCatalogNextBatch()
	{
		getSellerCatalogBatch(storedCartProducts.count,
							  {[weak self] response in self?.onNextBatchReceived(response)},
							  {[weak self] error in self?.showErrorPopUp(error: error)})
	}
	
	func onNextBatchReceived(_ sellerProductsPaginationResponse: SellerCatalogPaginationResponse)
	{
		let receivedProducts = (managers.webService.currentUserProfile == nil) ? toCartProducts(sellerProductsPaginationResponse.sellerProducts) : mergeSellerProductsWithCart(sellerProductsPaginationResponse.sellerProducts)

		self.cells = [[getSellerCellInfoIfAny()].compactMap{ $0 },
					  updateCartProducts(storedCartProducts).map{ .product($0) },
					  receivedProducts.map{ .product($0) }].reduce([], +)
		
		self.backendProductsCount = sellerProductsPaginationResponse.count
		
		self.collectionView.reloadData()
	}
}

// MARK:- First batch
extension SellerViewController
{
	func getSellerCatalogFirstBatch()
	{
		getSellerCatalogBatch(0,
							  {[weak self] response in self?.onFirstBatchReceived(response)},
							  {[weak self] error in self?.showErrorPopUp(error: error)})
	}
	
	func onFirstBatchReceived(_ sellerProductsPaginationResponse: SellerCatalogPaginationResponse)
	{
		switch config.mode
		{
		case .withFullInfo:
			break
			
		case .fromSearch, .fromDeepLink:
			self.sellerInfo = sellerProductsPaginationResponse.sellerInfo
		}
		
		let cartProducts = (managers.webService.currentUserProfile == nil) ? toCartProducts(sellerProductsPaginationResponse.sellerProducts) : mergeSellerProductsWithCart(sellerProductsPaginationResponse.sellerProducts)

		self.cells = [[getSellerCellInfoIfAny()].compactMap{ $0 },
					  cartProducts.map{ .product($0) }].reduce([], +)
		
		self.backendProductsCount = sellerProductsPaginationResponse.count
		
		self.collectionView.setContentOffset(.zero, animated: false)
		
		self.collectionView.reloadData()
		
		self.portionOfCatalogWasFetched = true
	}
}

// MARK:- Utils
extension SellerViewController
{
	
	private func getSellerCatalogBatch(_ offsetInItems: Int,
									   _ onSuccess: @escaping (SellerCatalogPaginationResponse) -> Void,
									   _ onFailure: @escaping(Error) -> Void)
	{
		managers.webService.getSellerCatalogWithPagination(
			sellerId,
			self.config.categoryPartialInfo?.id,
			Constants.itemsPerBatch,
			offsetInItems
		)
		.ensure
		{
			self.isAlreadyFetching = false
		}.done
		{ paginationResponse  in
			onSuccess(paginationResponse)
		}
		.catch
		{ error in
			onFailure(error)
		}
	}
	
	func updateCartProducts(_ cartProducts: [CartProduct]) -> [CartProduct]
	{
		if let sellerProductsInCart = self.config.cartController.cart.sellers.first(where: { $0.seller.id == self.sellerId })?.cartProducts
		{
			return cartProducts.map { cartProduct in
				return .init(product: cartProduct.product,
							 quantity: sellerProductsInCart.first { $0.product.id == cartProduct.product.id}?.quantity ?? 0,
							 stockQuantity: cartProduct.stockQuantity)
			}
		}
		
		return cartProducts.map{ .init(product: $0.product, quantity: 0, stockQuantity: $0.stockQuantity) }
	}
	
	func mergeSellerProductsWithCart(_ sellerProducts: [SellerCatalogProduct]) -> [CartProduct]
	{
		if let sellerProductsInCart = self.config.cartController.cart.sellers.first(where: { $0.seller.id == self.sellerId })?.cartProducts
		{
			return sellerProducts.map { sellerProduct in
				return .init(product: sellerProduct.product,
							 quantity: sellerProductsInCart.first { $0.product.id == sellerProduct.product.id}?.quantity ?? 0,
							 stockQuantity: sellerProduct.stockQuantity)
				
			}
		}
		
		return toCartProducts(sellerProducts)
	}
	
	func toCartProducts(_ sellerProducts: [SellerCatalogProduct]) -> [CartProduct]
	{
		return sellerProducts.map { .init(product: $0.product, quantity: 0, stockQuantity: $0.stockQuantity) }
	}
	
	func getSellerCellInfoIfAny() -> SellerViewController.CellKind?
	{
		cells.compactMap{
			switch $0
			{
			case .sellerInfo:
				return $0
				
			default:
				return nil
			}
		}.first
	}
	
}

// MARK:- On failure
extension SellerViewController
{
	func showErrorPopUp(error: Error)
	{
		if let cartUpdateError = error as? WebServiceManager.CartUpdateError
		{
			switch cartUpdateError
			{
			case .cannotPutProductsFromDifferentMarkets:
				PopUps.showCannotAddToCartProductsFromDifferentMarketsError(
					from: self,
					clearCartAndAddProduct: { [weak self] in
						//
						CartController.shared.removeAll()
						self?.leaveSellerScreen()
					}
				)
			}
		}
		else
		{
			PopUps.showQueryError(from: self,
								  title: "Ошибка при получении данных",
								  message: "Попробуйте повторить попытку через некоторое время.",
								  onOkCallback: {[weak self] in self?.leaveSellerScreen() })
		}
	}
	
	func leaveSellerScreen()
	{
		self.navigationController?.popViewController(animated: true)
	}
}

