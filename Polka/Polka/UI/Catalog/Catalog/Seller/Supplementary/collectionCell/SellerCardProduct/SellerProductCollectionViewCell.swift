//
//  SellerProductCollectionViewCell.swift
//  Polka
//
//  Created by Makson on 21.04.2021.
//

import UIKit

class SellerProductCollectionViewCell: UICollectionViewCell
{
    // MARK:- Outlets
    @IBOutlet private var productImage: CustomImage!
    @IBOutlet private var quantityControl: ListCartProductQuantityControl!
    @IBOutlet private var nameProductLbl: UILabel!
    @IBOutlet private var descriptionLbl: UILabel!
    @IBOutlet private var priceLbl: UILabel!
	
	@IBOutlet private var darkeningView: UIView!
	@IBOutlet private var numItemsInCartLabel: UILabel!
	
    @IBOutlet private var heightNameLbl: NSLayoutConstraint!
    // MARK:- Parameters
	var cartProduct: CartProduct!
	weak var parentViewController: UIViewController!
	var onProductQuantityChanged: ((Float) -> Void)!
}

// MARK:- Initialize
extension SellerProductCollectionViewCell
{
	func initialize(
		_ parentViewController: UIViewController,
		_ cartProduct: CartProduct,
		_ onProductQuantityChanged: @escaping (Float) -> Void,
		isSellerOpen: Bool
	)
	{
		self.onProductQuantityChanged = onProductQuantityChanged
		self.cartProduct = cartProduct
		self.parentViewController = parentViewController

		setupUI(isSellerOpen: isSellerOpen)
	}
}

// MARK:- Setup UI
private extension SellerProductCollectionViewCell
{
	func setupUI(isSellerOpen: Bool)
	{
		productImage.sd_setImage(with: cartProduct.product.imageUrl, placeholderImage: nil, options: [])
		
		nameProductLbl.text = cartProduct.product.name
		descriptionLbl.text = cartProduct.product.description
		
		priceLbl.text = PriceFormatter.catalogItemWithUnits(price: FormatterPrice.kopeksToRoubles(cartProduct.product.price),
															measurementUnit: cartProduct.product.unit,
															displayedOn: .sellerScreen)
		
		quantityControl.setup(
			forUserState: getQuantityControlState(isSellerOpen: isSellerOpen)
		)
		
		//
		let numItemsInCart = cartProduct.toStepperCount()
		
		darkeningView.isHidden = numItemsInCart <= 0
		numItemsInCartLabel.text = "\(Int(numItemsInCart))"
		
		hiddenDescription()
	}
	
	private func hiddenDescription()
	{
		if cartProduct.product.description == ""
		{
			self.descriptionLbl.isHidden = true
			self.heightNameLbl.constant = Constants.heightNameLblWithHiddenDescription
		}
		else
		{
			self.descriptionLbl.isHidden = false
			self.heightNameLbl.constant = Constants.heightNameLbl
		}
	}
	private func getQuantityControlState(isSellerOpen: Bool) -> ListCartProductQuantityControl.UserState
	{
		if !isSellerOpen
		{
			return .sellerIsResting
		}
		
		if managers.webService.currentUserProfile == nil
		{
			return .notAuthorised(product: cartProduct.product,
								  onAddToCartTapped: {[weak self] in self?.onAddToCartTappedForNotAuthorisedUser() })
		}
		else
		{
			return .authorised(stepperStyle: .orange,
							   cartProduct: cartProduct,
							   onQuantityChanged: onProductQuantityChanged)
		}
	}
	
	private func onAddToCartTappedForNotAuthorisedUser()
	{
		if let parentViewController = parentViewController
		{
			PopUps.promptUserToAuthorize(on: parentViewController)
		}
	}
}

extension SellerProductCollectionViewCell
{
	enum Constants
	{
		static let heightNameLblWithHiddenDescription: CGFloat = 40
		static let heightNameLbl                     : CGFloat = 18
		static let size = CGSize(width: 162, height: 222)
	}
}
