//
//  SellerCardInfoCollectionViewCell.swift
//  Polka
//
//  Created by Makson on 20.04.2021.
//

import SDWebImage

class SellerCardInfoCollectionViewCell: UICollectionViewCell
{
   //MARK: - Outlet and Variable
    @IBOutlet weak var infoImageView: UIImageView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
}

extension SellerCardInfoCollectionViewCell
{
	func initialize(imageUrl: URL, isSellerOpen: Bool)
	{
		infoImageView.sd_setImage(
			with: imageUrl,
			placeholderImage: nil,
			options: [.progressiveLoad],
			completed: { [weak infoImageView] (image, error, cacheType, url) in
				//
				if !isSellerOpen
				{
					infoImageView?.image = image?.grayed
				}
			}
		)
	}
}
