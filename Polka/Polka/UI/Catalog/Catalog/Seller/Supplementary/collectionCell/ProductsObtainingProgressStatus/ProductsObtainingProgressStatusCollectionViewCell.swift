//
//  SellerProductsObtainingProgressStatus.swift
//  Polka
//
//  Created by Michael Goremykin on 23.06.2021.
//

import UIKit
import MBProgressHUD

class ProductsObtainingProgressStatusCollectionViewCell: UICollectionViewCell
{
	// MARK:- Outlets
	weak var progressIndicator:MBProgressHUD!
}

// MARK:- Life cycle
extension ProductsObtainingProgressStatusCollectionViewCell
{
	override func awakeFromNib()
	{
		if progressIndicator == nil
		{
			setupProgressIndicator()
		}
		else
		{
			self.progressIndicator.show(animated: true)
		}
	}
}

// MARK:- Life cycle
extension ProductsObtainingProgressStatusCollectionViewCell
{
	func setupProgressIndicator()
	{
		self.progressIndicator = self.addProgressIndicator()
		
		//self.progressIndicator.show(animated: true)
	}
}
