//
//  SellerCardOneCollectionViewCell.swift
//  Polka
//
//  Created by Makson on 21.04.2021.
//

import UIKit

class SellerInfoCollectionViewCell: UICollectionViewCell
{
	// MARK:- Outlets
    @IBOutlet private var gallerySliderContainer: UIView!
    @IBOutlet private var nameLbl: UILabel!
    @IBOutlet private var nameShop: UILabel!
    @IBOutlet private var descriptionLbl: UILabel!
    @IBOutlet private var chatBtn: CustomButton!
	
	@IBOutlet private var isClosedLabel: UIView!
	
    weak var gallerySliderView: BackgroundGallery!
	
    @IBOutlet private var topConstraintDescriptionLbl: NSLayoutConstraint!
	
    // MARK:- Parameters
	private var profileImagesUrls: [URL] = []
	private var handleChatButton: VoidCallback?
	private var isSellerOpen = true
}

// MARK:- Intialize
extension SellerInfoCollectionViewCell
{
	func initialize(
		title: String,
		description: String,
		profileImagesUrls: [URL],
		handleChatButton: VoidCallback?,
		isSellerOpen: Bool
	)
	{
		self.nameLbl.text 			= title
		self.nameShop.text			= ""
		self.profileImagesUrls 		= profileImagesUrls
		self.handleChatButton 		= handleChatButton
		
		self.isSellerOpen = isSellerOpen
		
		isClosedLabel.isHidden = isSellerOpen
		
		setupDescription(description: description)
		setupGallery()
		setupOpenChatButton()
		showOrHideShopNameLabel()
	}
}

// MARK:- Life cycle
extension SellerInfoCollectionViewCell
{
    override func awakeFromNib()
    {
        super.awakeFromNib()
		
		setupGallery()
    }
	
}

// MARK:- Setup UI
private extension SellerInfoCollectionViewCell
{
	func setupGallery()
	{
		if gallerySliderView == nil
		{
			let viewToEmbed = BackgroundGallery.create(
				imageUrls: profileImagesUrls,
				isSellerOpen: isSellerOpen
			)
			self.gallerySliderContainer.addSubview(viewToEmbed)
			self.gallerySliderView = viewToEmbed
		}
		else
		{
			gallerySliderView.setupAppearance(
				imageUrls: profileImagesUrls,
				isSellerOpen: isSellerOpen
			)
		}
	}
	
	func setupOpenChatButton()
	{
		chatBtn.isHidden = handleChatButton == nil
		
		chatBtn.setEnabled(isSellerOpen)
	}
	
	func setupDescription(description:String)
	{
		if description.contains("\\n")
		{
			self.descriptionLbl.text = description.replacingOccurrences(of: "\\n", with: "\n" )
		}
		else
		{
			self.descriptionLbl.text = description
		}
	}
	
	func showOrHideShopNameLabel()
	{
		if self.nameShop.text == "" && self.descriptionLbl.text != ""
		{
			self.nameShop.isHidden = true
			self.topConstraintDescriptionLbl.constant = Constants.topConstraintWithHiddenNameShop
		}
		else
		{
			self.nameShop.isHidden = false
			self.topConstraintDescriptionLbl.constant = Constants.topConstraintWithNameShop
		}
	}
	
}

// MARK:- UI handlers
extension SellerInfoCollectionViewCell
{
    @IBAction func chatAction()
    {
		self.handleChatButton?()
    }
}

// MARK:- Constants
extension SellerInfoCollectionViewCell
{
    enum Constants
    {
        static let oneElement:Int = 1
		static let topConstraintWithHiddenNameShop:CGFloat = 11
		static let topConstraintWithNameShop:CGFloat = 36
		static let height = CGFloat(380)
    }
}
