//
//  SellerViewController+Utils.swift
//  Polka
//
//  Created by Michael Goremykin on 24.05.2021.
//

import UIKit

// MARK:- Cell kind
extension SellerViewController
{
	var storedCartProducts: [CartProduct]
	{
		cells.compactMap
		{
			switch $0
			{
			case .product(let cartProduct):
				return cartProduct
			
			default:
				return nil
			}
		}
	}
	
	func getItemsCountPerRow() -> Int
	{
		UIScreen.main.bounds.width < 375 ? 1 : 2
	}
}
// MARK:- Cell kind
extension SellerViewController
{
	enum CellKind
	{
		case sellerInfo(SellerInfo)
		case obtainingData
		case product(CartProduct)
	}
}

// MARK: - Constant
extension SellerViewController
{
	enum Constants
	{
		static let fetchThreshold = CGFloat(2)
		static let itemsPerBatch: Int = 10
		static let countSection: Int = 2
	}
}

typealias CategoryPartialInfo = (id: String, description: String)
