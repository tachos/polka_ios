//
//  SellerCollectionViewCell.swift
//  Polka
//
//  Created by Michael Goremykin on 22.04.2021.
//

import SDWebImage

class SellerCollectionViewCell: UICollectionViewCell
{
	// MARK:- Outletss
	@IBOutlet private var isClosedLabel: UIView!
	@IBOutlet weak var sellerImageView: UIImageView!
	@IBOutlet weak var sellerLabel: UILabel!
	@IBOutlet weak var gradientView: GradientView!
}

// MARK:- Life cycle
extension SellerCollectionViewCell
{
	override func awakeFromNib()
	{
		super.awakeFromNib()
		
		setupGradient()
	}
}

// MARK:- UI setup
extension SellerCollectionViewCell
{
	func setupGradient()
	{
		gradientView.gradientDirection = .vertical(bottomColor: #colorLiteral(red: 0.6078431373, green: 0.6078431373, blue: 0.6078431373, alpha: 1),
												   topColor: #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1))
	}
}

// MARK:- Initialization
extension SellerCollectionViewCell
{
	func initialize(
		title: String,
		imageUrl: URL?,
		isSellerOpen: Bool
	)
	{
		isClosedLabel.isHidden = isSellerOpen
		sellerLabel.text = title
		sellerImageView.sd_setImage(
			with: imageUrl,
			placeholderImage: nil,
			options: [],
			completed: { [weak sellerImageView] (image, error, cacheType, url) in
				//
				if !isSellerOpen
				{
					sellerImageView?.image = image?.grayed
				}
			}
		)
	}
}

// MARK:- Constants
extension SellerCollectionViewCell
{
	struct Constants
	{
		static let aspectRatio = CGFloat(167)/CGFloat(200)
	}
}
