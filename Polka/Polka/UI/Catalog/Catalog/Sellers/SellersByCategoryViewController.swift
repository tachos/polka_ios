//
//  VendorsByCategoryViewController.swift
//  Polka
//
//  Created by Michael Goremykin on 22.04.2021.
//

import UIKit
import RxSwift

class SellersByCategoryViewController: UIViewController, StoryboardCreatable
{
	// MARK:- StoryboardCreatable
	static let storyboardName: StoryboardName = .catalog
    
    // MARK:- Outlets
    @IBOutlet private var sellersCollectionView: UICollectionView!
	
	// MARK:- Delegates
	var navBarCustomizable: NavBarCustomizable!
    
    // MARK:- Stateful
	var categoryId:		     String!
	var categoryTitle: 	     String!
	var sellers: 		     [Seller] = []//generateMockData()
	var cellSize: 		     CGSize!
	var categoryDescription: String!
	
	var navBarAlpha: CGFloat = 0
	
	// MARK:- RX
	let disposeBag = DisposeBag()
}

extension SellersByCategoryViewController
{
	static func create(_ navBarCustomizable: NavBarCustomizable,
					   _ categoryId: String,
					   _ categoryTitle: String,
					   _ sellers: [Seller],
					   _ categoryDescription: String) -> SellersByCategoryViewController
	{
		let vc = createViewController(.catalog, SellersByCategoryViewController.self)
		
		vc.initialize(navBarCustomizable, categoryId, categoryTitle, sellers,categoryDescription)
		
		return vc
	}
}

extension SellersByCategoryViewController: Navigationable
{
	func adjustSafeAreaIfNeeded(_ navBarHeight: CGFloat)
	{
		self.additionalSafeAreaInsets = UIEdgeInsets(
			top: navBarHeight,
			left: 0,
			bottom: 0,
			right: 0
		)
	}
}

// MARK:- Initialization
extension SellersByCategoryViewController
{
	func initialize(_ navBarCustomizable: NavBarCustomizable,
					_ categoryId: String,
					_ categoryTitle: String,
					_ sellers: [Seller],
					_ categoryDescription: String)
	{
		self.navBarCustomizable  = navBarCustomizable
		self.categoryId 		 = categoryId
		self.categoryTitle		 = categoryTitle
		self.sellers			 = sellers
		self.categoryDescription = categoryDescription
	}
}

// MARK:- Life cycle
extension SellersByCategoryViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		setupUI()
		
		subscribeToCatalogEvents()
	}
	
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
		
		navBarCustomizable.setAppearance(navBarBackground: .clear,
										hasBackButton: true,
										titleViewType: .category(categoryTitle),
										initiallyNotVisible: false)
	}
	
	override func viewWillDisappear(_ animated: Bool)
	{
		super.viewWillDisappear(animated)
		
		if let alpha = getNavigationController()?.navBarView.alpha, alpha > 0
		{
			navBarAlpha = alpha
			
			getNavigationController()?.navBarView.alpha = 0
		}
	}
}

// MARK:- UI setup
extension SellersByCategoryViewController
{
	func setupUI()
	{
		setupCollectionView()
	}
	
	func setupNavigationBar()
	{
		self.navigationController?.isNavigationBarHidden = false
	}
    
    func setupCollectionView()
    {
		sellersCollectionView.delegate   = self
		sellersCollectionView.dataSource = self
        
        cellSize = getCellSize(sellersCollectionView)
    }
}

// MARK:- UICollectionView
extension SellersByCategoryViewController: UICollectionViewDelegate,
								 UICollectionViewDataSource,
								 UICollectionViewDelegateFlowLayout
{
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
	{
		showSellerScreen(indexPath.item)
	}
	
	func numberOfSections(in collectionView: UICollectionView) -> Int
	{
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		return sellers.count
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
	{
		return cellSize
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: SellerCollectionViewCell.self),
															for: indexPath) as? SellerCollectionViewCell else
		{
			return UICollectionViewCell()
		}
		
		let seller = sellers[indexPath.item]
		
		cell.initialize(
			title: seller.info.caption,
			imageUrl: seller.info.profileImageUrl,
			isSellerOpen: seller.info.isOpen
		)
		
		return cell
	}
}

// MARK:- Utils
extension SellersByCategoryViewController
{
	func showSellerScreen(_ sellerIndex: Int)
	{
		// TODO: refactor. Perform segue passed from parent Story
		
		guard let navigationController = navigationController else { return }

		SellerStory.start(
			in: navigationController,
			seller: sellers[sellerIndex],
			categoryId: self.categoryId,
			categoryDescription: self.categoryDescription
		)
	}
	
	func getCellSize(_ cv: UICollectionView) -> CGSize
	{
		if let flowLayout = cv.collectionViewLayout as? UICollectionViewFlowLayout
		{
			let width = (UIScreen.main.bounds.width - flowLayout.sectionInset.left * 2 - flowLayout.minimumInteritemSpacing)/CGFloat(2)
			
			return CGSize(width: width, height: width / SellerCollectionViewCell.Constants.aspectRatio)
		}
		
		return CGSize(width: 10, height: 10)
	}
	
	func subscribeToCatalogEvents()
	{
		CatalogController.shared.categoriesStateObservable.subscribe(onNext: {[weak self] categoriesState in
			self?.onCategoriesStateChanged(categoriesState)
		}).disposed(by: disposeBag)
	}
	
	func onCategoriesStateChanged(_ categoriesState: CategoriesState)
	{
		sellers = categoriesState.value.first{ $0.id == categoryId}?.sellers ?? []
		
		sellersCollectionView.reloadData()
	}
}
