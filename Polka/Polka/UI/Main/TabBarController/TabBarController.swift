//
//  TabBarController.swift
//  Polka
//
//  Created by Michael Goremykin on 23.04.2021.
//

import UIKit
import RxSwift


enum Tab: CaseIterable
{
	case catalog
	case cart
	case chats
	case contacts
	case orders
	case statysWork
	case sellerOrder
}


class TabBarController: UIViewController
{
	// MARK:- Outlets
	var tabBarView: TabBarView!
	
	var tabBarHeight: CGFloat
	{
		TabBarView.Constants.tabBarHeight
	}
	
	// MARK:- Stateful
	var selectedTabIndex: Int = 0
	var hasAlreadyAppeared: Bool = false
	
	private var tabs: [Tab]!
	private var tabViewControllers: [UIViewController]!

	// MARK:- ReactiveX
	private let disposeBag = DisposeBag()
}

// MARK:- API
extension TabBarController
{
	func modifyTabScreenHierarchy(_ tabToUpdate: Tab, _ tabFirstChildController: UIViewController)
	{
		tabViewControllers = tabs.map { (tab: Tab) in
			if tab == tabToUpdate
			{
				return tabFirstChildController
			}
			else
			{
				if let index = getTabIndexIfAny(tab)
				{
					return tabViewControllers[index]
				}
			}
			
			return UIViewController()
		}
	}
	
	func getTabViewController(_ tab: Tab) -> UIViewController?
	{
		if let tabIndex = getTabIndexIfAny(tab)
		{
			return tabViewControllers[tabIndex]
		}
		
		return nil
	}
	
	@discardableResult
	func switchToTab(_ tab: Tab) -> UIViewController?
	{
		if let currentViewController = self.children.first,
		   let tabIndexToSwitch = tabs.firstIndex(of: tab)
		{
			hideContentController(currentViewController)
		 	displayContentController(tabViewControllers[tabIndexToSwitch])
			tabBarView.selectTab(tabIndexToSwitch)
			selectedTabIndex = tabIndexToSwitch
			return tabViewControllers[tabIndexToSwitch]
		}

		return nil
	}
	
	func getTabIndexIfAny(_ tab: Tab) -> Int?
	{
		return tabs.firstIndex(of: tab)
	}
}

// MARK:- API for UIViewController
extension  UIViewController
{
	func getTabBarController() -> TabBarController?
	{
		return UIApplication.shared.windows.first?.rootViewController as? TabBarController
	}

	func getTabBarHeight() -> CGFloat?
	{
		getTabBarController()?.tabBarHeight
	}
}

// MARK:- Life cycle
extension TabBarController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		setupUI()
		
		if !hasAlreadyAppeared
		{
			displayContentController(tabViewControllers[selectedTabIndex])
			
			hasAlreadyAppeared = true
		}
	}
	
	override func viewDidAppear(_ animated: Bool)
	{
		super.viewDidAppear(animated)
		
		// NOTE: navigation bar isn't visible unless we wrap it in async()
		DispatchQueue.main.async {
			RootStory.handlePendingRedirect(from: self)
		}
	}
	
	override func viewSafeAreaInsetsDidChange()
	{
		super.viewSafeAreaInsetsDidChange()
	}
}

// MARK:- Setup UI
extension TabBarController
{
	func setupUI()
	{
		setupTabBarView()
	}
	
	func setupTabBarView()
	{
		let tabBarView = TabBarView.create(self, tabs, selectedTabIndex)
		
		self.view.addSubview(tabBarView)
		
		let constraints = [tabBarView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
						   tabBarView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
						   tabBarView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0),
						   tabBarView.heightAnchor.constraint(equalToConstant: TabBarView.Constants.tabBarHeight + (getBottomSafeAreaHeight() ?? 0))]
		
		NSLayoutConstraint.activate(constraints)
		
		self.tabBarView = tabBarView
	}
}

// MARK:- UI handlers
extension TabBarController: TabBarItemViewDelegate
{
	func handleTap(_ tab: Tab)
	{
		switch tab
		{
		case .catalog, .cart, .chats,.statysWork, .sellerOrder:
			selectTab(tab)
		case .orders:
			// AAAAA TOREMOVE
			if managers.webService.currentUserProfile == nil
			{
				AuthorizationStory.start(window: (UIApplication.shared.delegate?.window!)!)
			}
			else
			{
				selectTab(tab)
			}
		case .contacts:
			showContactsActionSheet()
		}
	}
	
	func selectTab(_ tab: Tab)
	{
		let previousSelectedIndex = selectedTabIndex
		
		selectedTabIndex = tabs.firstIndex(of: tab) ?? 0
		
		if previousSelectedIndex != selectedTabIndex
		{
			hideContentController(tabViewControllers[previousSelectedIndex])
			
			displayContentController(tabViewControllers[selectedTabIndex])
			
			tabBarView.selectTab(selectedTabIndex)
		}
	}
	
	func showContactsActionSheet()
	{
		ActionSheet.showContacts(from: self,
								 options: [("+7 916 927-11-95", { [weak self] in self?.onPhoneActionTapped() }),
										   ("WhatsApp", { [weak self] in self?.onWhatsAppTapped() })])
	}
	
	func onPhoneActionTapped()
	{
		if let url = URL(string: "tel://\("79169271195")"), UIApplication.shared.canOpenURL(url) {
			if #available(iOS 10, *) {
				UIApplication.shared.open(url)
			} else {
				UIApplication.shared.openURL(url)
			}
		}
	}
	
	func onWhatsAppTapped()
	{
		if let url = URL(string: "https://wa.me/79169271195"), UIApplication.shared.canOpenURL(url) {
			if #available(iOS 10, *) {
				UIApplication.shared.open(url)
			} else {
				UIApplication.shared.openURL(url)
			}
		}
	}
}

// MARK:- Initialization
extension TabBarController
{
	func initialize(_ tabs: [Tab])
	{
		self.tabs = tabs
		
		tabViewControllers = tabs.enumerated().map(createTabViewController)
		
		tabViewControllers.forEach{ $0.updateInsetsDueToCustomTabBar() }
		tabViewControllers
			.compactMap { $0 as? NavigationController }
			.forEach { self.subscribeToNavigationController($0) }
	}
	
	func createTabViewController(for tuple: (index: Int, tab: Tab)) -> UIViewController
	{
		switch tuple.tab
		{
		case .catalog:
			return CatalogStory.start(tabBarController: self)
			
		case .orders:
			return NavigationController.create([OrdersViewController()])
			//return createProfileController()
			
		case .cart:
			return createCartScreen()
		case .chats:
			return createChatsScreen()
		case .statysWork:
			return createSellerScreen()
		case .sellerOrder:
			return createSellerOder()
		default:
			return createFakeTabRootViewController("VC#\(tuple.index)")
		}
	}
}

// MARK:- Navigation Controller
extension TabBarController
{
	func subscribeToNavigationController(_ navigationController: NavigationController)
	{
		navigationController.willShowViewControllerObservable
			.subscribe(onNext: { [weak self] in self?.onNavigationControllerWillShowViewController($0) })
			.disposed(by: disposeBag)

		navigationController.didShowViewControllerObservable
			.subscribe(onNext: { [weak self] in self?.onNavigationControllerDidShowViewController($0) })
			.disposed(by: disposeBag)
	}

	private func onNavigationControllerWillShowViewController(
		_ parameters: ViewControllerVisibilityChangedParameters
	)
	{
		if parameters.viewController.hidesBottomBarWhenPushed
		{
			if let firstVC = parameters.navigationController
				.viewControllers
				.first(where: { !$0.hidesBottomBarWhenPushed }),
			   firstVC != parameters.viewController
			{
				attachTabBarToView(firstVC.view)
			}

			parameters.navigationController.additionalSafeAreaInsets.bottom = 0
		}
		else
		{
			attachTabBarToView(parameters.viewController.view)

			parameters.navigationController.additionalSafeAreaInsets.bottom = tabBarHeight
		}
	}

	private func onNavigationControllerDidShowViewController(
		_ parameters: ViewControllerVisibilityChangedParameters
	)
	{
		let viewControllers = parameters.navigationController.viewControllers

		if let viewControllerIndex = viewControllers.firstIndex(
			of: parameters.viewController
		),
			viewControllerIndex == 0
		{
			attachTabBarToView(self.view)
		}
	}

	private func attachTabBarToView(_ view: UIView)
	{
		if tabBarView.superview != view
		{
			tabBarView.removeFromSuperview()

			view.addSubview(tabBarView)
			view.bringSubviewToFront(tabBarView)

			tabBarView.leadingToSuperview()
			tabBarView.trailingToSuperview()
			tabBarView.bottomToSuperview()
			tabBarView.height(tabBarHeight)
		}
	}
}

// MARK:- Instantiation
extension TabBarController
{
	static func create(_ tabs: [Tab]) -> TabBarController
	{
		let tabBarController = TabBarController()
		
		tabBarController.initialize(tabs)
		
		return tabBarController
	}
}


extension Tab: Equatable
{
	static func ==(lhs: Tab, rhs: Tab) -> Bool
	{
		switch (lhs, rhs)
		{
		case (cart, cart), (catalog, catalog), (chats, chats), (contacts, contacts),
			 (orders, orders), (statysWork, statysWork), (sellerOrder, sellerOrder):
			return true
			
		default:
			return false
		}
	}
}

extension Tab
{
	func getIcon() -> UIImage
	{
		switch self
		{
		case .cart:
			return UIImage(named: "tabbar-cart") ?? UIImage()
			
		case .catalog:
			return UIImage(named: "tabbar-catalog") ?? UIImage()
			
		case .chats:
			return UIImage(named: "tabbar-chats") ?? UIImage()
			
		case .contacts:
			return UIImage(named: "tabbar-contacts") ?? UIImage()
			
		case .orders:
			return UIImage(named: "tabbar-orders") ?? UIImage()
			
		case .statysWork:
			return UIImage(named: "tabbar-catalog") ?? UIImage()
			
		case .sellerOrder:
			return UIImage(named: "tabbar-cart") ?? UIImage()
		}
	}
}

// MARK:- TabBar mock methods
extension TabBarController
{
	func createFakeTabRootViewController(_ title: String) -> UIViewController
	{
		let vc = UIViewController()
		
		let backgroundColors = [#colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1), #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1), #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1), #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)]
		
		vc.view.backgroundColor =  backgroundColors[Int.random(in: 0..<5)]
		
		let label 			= UILabel()
		label.text 			= title
		label.textColor 	= .white
		label.numberOfLines = 0
		label.sizeToFit()
		
		vc.view.addSubview(label)
		
		label.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [label.centerYAnchor.constraint(equalTo: vc.view.centerYAnchor),
						   label.centerXAnchor.constraint(equalTo: vc.view.centerXAnchor)]
		
		NSLayoutConstraint.activate(constraints)
		
		vc.view.setNeedsLayout()
		
		return vc
	}
	
	func createProfileController() -> UIViewController
	{
		return ProfileStory.start()
	}
	func createCartScreen() -> UIViewController
	{
		return CartStory.start(tabBarController: self)
	}
	func createChatsScreen() -> UIViewController
	{
		return ChatsStory.start()
	}
	func createSellerScreen() -> UIViewController
	{
		return SellerScreenStory.start()
	}
	func createSellerOder() -> UIViewController
	{
		return SellerOrderStory.start()
	}
	
}

// MARK:- UIViewcontroller utils for TabBar
extension UIViewController
{
	func updateInsetsDueToCustomTabBar()
	{
		self.additionalSafeAreaInsets = UIEdgeInsets(top: 0,
													 left: 0,
													 bottom: TabBarView.Constants.tabBarHeight,
													 right: 0)
	}
	
	func getBottomSafeAreaHeight() -> CGFloat?
	{
		if #available(iOS 11.0, *)
		{
		  return UIApplication.shared.windows[0].safeAreaInsets.bottom
		}
		
		return nil
	}
	
	func displayContentController(_ contentController: UIViewController)
	{
		addChild(contentController)
		self.view.addSubview(contentController.view)
		
		contentController.didMove(toParent: self)
		
		contentController.updateInsetsDueToCustomTabBar()
		
		if let tabBarController = self as? TabBarController
		{
			self.view.bringSubviewToFront(tabBarController.tabBarView)
		}
	}
	
	func hideContentController(_ contentController: UIViewController)
	{
		contentController.willMove(toParent: nil)
		contentController.view.removeFromSuperview()
		contentController.removeFromParent()
	}
	
}

