//
//  TabBarView.swift
//  Polka
//
//  Created by Michael Goremykin on 26.04.2021.
//

import UIKit
import RxSwift

class TabBarView: UIView
{
	// MARK:- Outlets
	var tabsStack: UIStackView!
	
	// MARK:- Stateful
	var selectedTabIndex: Int!
	
	// MARK:- RxSwift
	let disposeBag = DisposeBag()
	
	init()
	{
		super.init(frame: .zero)
		
		setRoundedCorners()
		setShadow()
		
		subscribeToCartEvents()
		subscribeToChatEvents()
	}
	
	required init?(coder: NSCoder)
	{
		fatalError("init(coder:) has not been implemented")
	}
}

// MARK:- Instantiation
extension TabBarView
{
	static func create(_ tabBarItemViewDelegate: TabBarItemViewDelegate ,
					   _ tabs: [Tab],
					   _ selectedTabIndex: Int? = nil) -> TabBarView
	{
		let view = TabBarView()
		
		view.translatesAutoresizingMaskIntoConstraints = false
		
		view.setupUI(tabBarItemViewDelegate,
					 tabs,
					 selectedTabIndex)
		
		return view
	}
}

// MARK:- API
extension TabBarView
{
	func selectTab(_ itemIndexToSelect: Int)
	{
		tabsStack.arrangedSubviews.forEach { ($0 as? TabBarItemView)?.setIconTintColor(Constants.unselectedTintColor)}
		
		(tabsStack.arrangedSubviews.enumerated().first { $0.offset == itemIndexToSelect }?.element as? TabBarItemView)?.setIconTintColor(Constants.selectedTintColor)
	}
	
	func setBadge(forTab tab: Tab, withValue value: Int)
	{
		if let tabToSetBadge = tabsStack?.arrangedSubviews
			.compactMap({ $0 as? TabBarItemView })
			.first(where: { $0.tabItem == tab })
		{
			tabToSetBadge.setBadge(withValue: value)
		}
	}
	
	func setBadge(forTab tab: Tab, isHidden: Bool)
	{
		if let tabToSetBadge = tabsStack?.arrangedSubviews
			.compactMap({ $0 as? TabBarItemView })
			.first(where: { $0.tabItem == tab })
		{
			tabToSetBadge.setBadge(isHidden: isHidden)
		}
	}
}

// MARK:- Life cycle
extension TabBarView
{
	override func layoutSubviews()
	{
		super.layoutSubviews()
	}
}

// MARK:- Setup UI
extension TabBarView
{
	func setRoundedCorners()
	{
		self.clipsToBounds 			= true
		self.layer.cornerRadius 	= Constants.cornderRadius
		self.layer.maskedCorners	= [.layerMinXMinYCorner, .layerMaxXMinYCorner]
	}
	
	func setShadow()
	{
		self.clipsToBounds = false
		self.layer.shadowColor = UIColor.gray.cgColor
		self.layer.shadowOffset = CGSize(width: 0, height: -40)
		self.layer.shadowOpacity = 0.1
		self.layer.shadowRadius = 40.0
	}
	
	func setupUI(_ tabBarItemViewDelegate: TabBarItemViewDelegate ,
				 _ tabs: [Tab],
				 _ selectedIndex: Int? = nil)
	{
		setTabsStack(tabBarItemViewDelegate, tabs, selectedIndex)
		
		self.backgroundColor = .white
	}
	
	func setTabsStack(_ tabBarItemViewDelegate: TabBarItemViewDelegate,
					  _ tabs: [Tab],
					  _ selectedTabIndex: Int? = nil)
	{
		let tabsStack = createHorizontalStack()

		tabsStack.translatesAutoresizingMaskIntoConstraints = false
		placeTabStack(tabsStack)
		
		populateTabsViews(tabBarItemViewDelegate, tabs, selectedTabIndex).forEach {
			tabsStack.addArrangedSubview($0)
		}
		
		self.tabsStack = tabsStack
	}
	
	func createHorizontalStack() -> UIStackView
	{
		let tabsStack 			= UIStackView()
		
		tabsStack.axis 			= .horizontal
		tabsStack.alignment 	= .center
		tabsStack.distribution 	= .fillEqually
		
		return tabsStack
	}
	
	func placeTabStack(_ stack: UIStackView)
	{
		self.addSubview(stack)
		
		let constraints = [stack.leadingAnchor.constraint(equalTo: self.leadingAnchor),
						   stack.trailingAnchor.constraint(equalTo: self.trailingAnchor),
						   stack.topAnchor.constraint(equalTo: self.topAnchor),
						   stack.heightAnchor.constraint(equalToConstant: Constants.tabBarHeight)]
		
		NSLayoutConstraint.activate(constraints)
	}
	
	func populateTabsViews(_ tabBarItemViewDelegate: TabBarItemViewDelegate,
						   _ tabs: [Tab],
						   _ selectedTabIndex: Int? = nil) -> [TabBarItemView]
	{
		return tabs.enumerated().map { TabBarItemView.create(tabBarItemViewDelegate,
															 $0.element.getIcon(),
															 $0.offset == selectedTabIndex ? Constants.selectedTintColor: Constants.unselectedTintColor,
															 $0.element) }
	}
}

// MARK:- RX
extension TabBarView
{
	func subscribeToCartEvents()
	{
		CartController.shared.cartStateObservable.subscribe(onNext: {[weak self] cartState in
			self?.onCartChanged(cartState.cart)
		}).disposed(by: disposeBag)
	}
	
	private func onCartChanged(_ cart: Cart)
	{
		setBadge(forTab: .cart, withValue: cart.getCartProductsCountInCart())
	}
	
	func subscribeToChatEvents()
	{
		managers.chats.unreadChatsCountObservable
			.subscribe(onNext: { [weak self] in self?.onUnreadChatsCountChanged($0) })
			.disposed(by: disposeBag)
	}
	
	private func onUnreadChatsCountChanged(_ unreadChatsCount: Int)
	{
		setBadge(forTab: .chats, isHidden: unreadChatsCount == 0)
	}
}

// MARK:- Constnats
extension TabBarView
{
	struct Constants
	{
		static let tabBarHeight 		= CGFloat(49)
		static let cornderRadius		= CGFloat(20)
		static let selectedTintColor 	= #colorLiteral(red: 0.9843137255, green: 0.4745098039, blue: 0.003921568627, alpha: 1)
		static let unselectedTintColor 	= #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
	}
}


