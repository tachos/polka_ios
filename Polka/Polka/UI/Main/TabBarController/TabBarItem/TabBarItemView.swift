//
//  TabBarItemView.swift
//  Polka
//
//  Created by Michael Goremykin on 26.04.2021.
//

import UIKit

protocol TabBarItemViewDelegate: AnyObject
{
	func handleTap(_ tab: Tab)
}

class TabBarItemView: UIView
{
	// MARK:- Outlets
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var badgeContainer: UIView!
    @IBOutlet weak var badgeLabel: UILabel!
	
	var tabItem: Tab!
	
	weak var delegate: TabBarItemViewDelegate?
}

// MARK:- API
extension TabBarItemView
{
	func setIconTintColor(_ color: UIColor)
	{
		iconImageView.tintColor = color
	}
	
	func setBadge(withValue value: Int)
	{
		badgeContainer.isHidden = value == 0
		
		if value > 0
		{
			badgeLabel.text = String(value)
		}
	}
	
	func setBadge(isHidden: Bool)
	{
		badgeContainer.isHidden = isHidden
		
		badgeLabel.text = "  "
	}
}

// MARK:- Initialization
extension TabBarItemView
{
	func initialize(_ tabBarItemViewDelegate: TabBarItemViewDelegate,
					_ icon: UIImage,
					_ tintColor: UIColor,
					_ tabItem: Tab)
	{
		self.iconImageView.image 		= icon
		self.iconImageView.tintColor	= tintColor
		self.tabItem 					= tabItem
		self.delegate					= tabBarItemViewDelegate
	}
}

// MARK:- Instantiation
extension TabBarItemView
{
	static func create(_ tabBarItemViewDelegate: TabBarItemViewDelegate,
					   _ icon: UIImage,
					   _ tintColor: UIColor,
					   _ tabItem: Tab) -> TabBarItemView
	{
		let view = createView(TabBarItemView.self)
		
		view.initialize(tabBarItemViewDelegate,
						icon,
						tintColor,
						tabItem)
		
		return view
	}
}

// MARK:- Life cycle
extension TabBarItemView
{
	override func awakeFromNib()
	{
		super.awakeFromNib()
		
		addTapGesture()
	}
}

// MARK:- UI handling
extension TabBarItemView
{
	func addTapGesture()
	{
		let tapGesture = UITapGestureRecognizer(target: self,
												action: #selector(handleGesture(_:)))
		self.addGestureRecognizer(tapGesture)
	}
	
	@objc
	func handleGesture(_ sender: UITapGestureRecognizer)
	{
		self.delegate?.handleTap(self.tabItem)
	}
}

// MARK:- Constants
extension TabBarItemView
{
	struct Constants
	{
		static let badgeSide = CGFloat(18)
		static let badgeExtendedSide = CGFloat(25)
	}
}

