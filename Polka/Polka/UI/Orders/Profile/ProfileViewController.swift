//
//  ProfileViewController.swift
//  Polka
//
//  Created by Makson on 29.04.2021.
//

import UIKit
import AnyFormatKit

class ProfileViewController: UIViewController,StoryboardCreatable
{
	//MARK: - StoryboardCreatable
	static var storyboardName: StoryboardName = .profile
	
	//MARK: Outlet and variable
   
    @IBOutlet weak var collectionView: UICollectionView!
    
    var sendNumber = DefaultTextInputFormatter(textPattern: "############")
	
	// MARK: Config
	var config: Config!
	var phone:String!
	
	struct Config
	{
		let helper: ProfileHelper
		var phone:String
	}
	var fakeOrder:[FakeOrder] = []/*[FakeOrder(state: "Ожидает",
										   number: 100, countOrDate:
											"10 товаров",
										   dateOrPrice: "Заказ от 14:00",
										   count: 1),
								 FakeOrder(state: "В доставке",
										   number: 1000,
										   countOrDate: "20 товаров",
										   dateOrPrice: "Заказ от 14:00",
										   count: 2),
								 FakeOrder(state: "Доставлен ",
										   number: 1000,
										   countOrDate: "7 товаров",
										   dateOrPrice: "Заказ от 14:00",
										   count:3),
								 FakeOrder(state: "Доставлен ",
										   number: 1000,
										   countOrDate: "15 товаров",
										   dateOrPrice: "Заказ от 14:00",
										   count:15)]*/
}

//MARK: - Lifecycle
extension ProfileViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		setupUI()
	}
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
		collectionView.reloadItems(at: [IndexPath(item: 0, section: 0)])
	}
	override func viewDidDisappear(_ animated: Bool)
	{
		super.viewDidDisappear(animated)
		
	}
}

extension ProfileViewController
{
	func setupUI()
	{
		registrationCell()
		phone = setupPhoneLbl(phone: config.phone)
	}
	
	func setupPhoneLbl(phone:String) -> String
	{
		sendNumber = DefaultTextInputFormatter(textPattern: Formatter.phoneProfile.rawValue)
		config.phone = sendNumber.format(config.phone)!
		return config.phone
	}
	
	func registrationCell()
	{
		collectionView.register(UINib(nibName: "ProfileNameCollectionViewCell", bundle: nil),
								forCellWithReuseIdentifier: "ProfileNameCollectionViewCell")
		collectionView.register(UINib(nibName: "OrderCollectionViewCell", bundle: nil),
								forCellWithReuseIdentifier: "OrderCollectionViewCell")
		collectionView.contentInset = UIEdgeInsets(top: 48, left: 0, bottom: 0, right: 0)
	}
}


//MARK: - UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension ProfileViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
	func numberOfSections(in collectionView: UICollectionView) -> Int
	{
		return Constant.countSection
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		if section == 0
		{
			return Constant.defaultCellCount
		}
		else
		{
			return fakeOrder.count
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
	{
		
		return CGSize(width: UIScreen.main.bounds.width, height: 0)
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
	{
		if indexPath.section == 0
		{
			return CGSize(width: UIScreen.main.bounds.width, height: Constant.heightCell)
		}
		else
		{
			return CGSize(width: UIScreen.main.bounds.width - 32, height: Constant.heightCell)
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{
		if indexPath.section == 0
		{
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileNameCollectionViewCell", for: indexPath) as! ProfileNameCollectionViewCell
			cell.setup(phone: self.phone,
					   tipTitle: Address.addressLbl(),
					   resourceImageName: "location",
					   title: Address.getAddressConsumer(),
					   onWhereBring:
						{
							WhereBringStory.start(window: nil, viewController: self, titleBtn: "Вернуться в профиль")
						},
					   onExit:
						{
							self.config.helper.logout()
						})
			return cell
		}
		else if indexPath.section == 1
		{
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderCollectionViewCell", for: indexPath) as! OrderCollectionViewCell
			cell.setup(order: fakeOrder[indexPath.item], type: OrderType.customerOrder )
			cell.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
			cell.layer.shadowOpacity = 1
			cell.layer.shadowOffset = .zero
			cell.layer.shadowRadius = 40
			cell.layer.masksToBounds = false
			return cell
		}
		return UICollectionViewCell()
	}
}

extension ProfileViewController
{
	struct Constant
	{
		static let countSection:Int = 2
		static let defaultCellCount:Int = 1
		static let heightCell:CGFloat = 174
		
	}
}
extension ProfileViewController
{}

struct FakeOrder
{
	var state:String?
	var number:Int?
	var countOrDate:String?
	var dateOrPrice:String?
	var count:Int?
}
