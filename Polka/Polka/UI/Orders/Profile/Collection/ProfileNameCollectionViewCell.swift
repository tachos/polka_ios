//
//  ProfileNameCollectionViewCell.swift
//  Polka
//
//  Created by Makson on 21.05.2021.
//

import UIKit

class ProfileNameCollectionViewCell: UICollectionViewCell
{
	
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var addressPickerContainer: UIView!
    
	weak var addressView: AddressView!
	
	//MARK: - variable
	var tipTitle: String!
	var resourceImageName: String?  = nil
	var title: String? = nil
	var onWhereBring:VoidCallback!
	var onExit:VoidCallback?
}

extension ProfileNameCollectionViewCell
{
	func setup(phone:String,
			   tipTitle:String,
			   resourceImageName:String,
			   title:String,
			   onWhereBring: @escaping VoidCallback,
			   onExit: @escaping VoidCallback)
	{
		self.onExit = onExit
		self.onWhereBring = onWhereBring
		self.phoneLbl.text = phone
		self.tipTitle = tipTitle
		self.resourceImageName = resourceImageName
		self.title = title
		setupAddressPicker()
	}
}

extension ProfileNameCollectionViewCell
{
	func setupAddressPicker()
	{
		if let addressView = addressView
		{
			addressView.setupAppearance(nameLbl: self.tipTitle,
										image: self.resourceImageName,
										text: self.title,
										type: .availableTapGesture,
										onPress:{
											self.onWhereBring?()
										} )
		}
		else
		{
			addressView = AddressView.setInContainer(addressPickerContainer,
													 self.tipTitle,
													 self.resourceImageName,
													 self.title,
													 .availableTapGesture,
													 onPress: {
														self.onWhereBring?()
													 })
		}
	}
}
	
extension ProfileNameCollectionViewCell
{
	@IBAction func exitAction()
	{
		onExit?()
	}
}
