//
//  OrderCollectionViewCell.swift
//  Polka
//
//  Created by Makson on 21.05.2021.
//

import UIKit


enum OrderType
{
	case sellerOrder
	case customerOrder
}
class OrderCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var numberLbl: UILabel!
    @IBOutlet weak var countOrDateLbl: UILabel!
    @IBOutlet weak var dateOrPriceLbl: UILabel!
    
    @IBOutlet weak var threeView: UIView!
    @IBOutlet weak var twoView: UIView!
    @IBOutlet weak var oneView: UIView!
    @IBOutlet weak var imageThree: UIImageView!
    @IBOutlet weak var imageTwo: UIImageView!
    @IBOutlet weak var imageOne: UIImageView!
    @IBOutlet weak var countField: UILabel!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var widthStack: NSLayoutConstraint!
    @IBOutlet weak var contentsView: UIView!
    
    var orange = UIColor(red: 255/255, green: 122/255, blue: 0/255, alpha: 1)
	var gray = UIColor(red: 143/255, green: 143/255, blue: 143/255, alpha: 1)
	var count = 0
}
extension OrderCollectionViewCell
{
	override func awakeFromNib()
	{
		super.awakeFromNib()
		setupBorder()
	}
}

extension OrderCollectionViewCell
{
	func setupBorder()
	{
		oneView.layer.borderWidth = Constant.borderWidth
		twoView.layer.borderWidth = Constant.borderWidth
		threeView.layer.borderWidth = Constant.borderWidth
		countView.layer.borderWidth = Constant.borderWidth
		oneView.layer.borderColor = UIColor.white.cgColor
		twoView.layer.borderColor = UIColor.white.cgColor
		threeView.layer.borderColor = UIColor.white.cgColor
		countView.layer.borderColor = UIColor.white.cgColor
	}
	func setup(order: FakeOrder?,
			   type:OrderType)
	{
		guard let order = order else
		{
			return
		}
		setupColor(status: order.state!, type: type)
		numberLbl.text = "№ \(order.number ?? 0)"
		countOrDateLbl.text = order.countOrDate
		dateOrPriceLbl.text = order.dateOrPrice
		updateStackView(count: order.count ?? 0)
	}
	func updateStackView(count:Int)
	{
		if count == 1
		{
			widthStack.constant = Constant.defaultWidth
			twoView.isHidden = true
			threeView.isHidden = true
			countView.isHidden = true
		}
		else if count == 2
		{
			widthStack.constant =  Constant.twoItem
			threeView.isHidden = true
			countView.isHidden = true
		}
		else if count == 3
		{
			widthStack.constant = Constant.threeItem
			countView.isHidden = true
		}
		else if count > 3
		{
			countField.text = "+\(count - Constant.countImage)"
		}
	}
	func setupColor(status:String, type:OrderType)
	{
		statusLbl.text = status
		
		switch type
		{
		case .customerOrder:
			statusLbl.textColor = colorCustomer(status: status)
		case .sellerOrder:
			statusLbl.textColor = colorSeller(status: status)
		}
	}
	
	func colorCustomer(status:String) -> UIColor
	{
		switch status
		{
		case "В доставке":
			return orange
		case "Доставлен":
			return gray
		case "Ожидает":
			return .black
		default:
			break
		}
		return .black
	}
	
	func colorSeller(status:String) -> UIColor
	{
		switch status
		{
		case "В доставке":
			return .black
		case "Доставлен":
			return gray
		case "Ожидает":
			return orange
		default:
			break
		}
		return .black
	}
}

extension OrderCollectionViewCell
{
	struct Constant
	{
		static let defaultCount = 4
		static let defaultWidth:CGFloat = 48
		static let twoItem:CGFloat = 96
		static let threeItem:CGFloat = 144
		static let countImage = 3
		static let borderWidth:CGFloat = 2
	}
}
