//
//  OrdersViewController+SetupUI.swift
//  Polka
//
//  Created by Michael Goremykin on 09.06.2021.
//

import TinyConstraints

// MARK:- UI utils
extension OrdersViewController
{
	func setupUI()
	{
		self.view.backgroundColor = .white
		
		setupOrdersTableView()
		createAmEmbedEmptyOrdersView()
		
		self.view.bringSubviewToFront(ordersTableView)
	}
	
	func setupOrdersTableView()
	{
		let ordersTableView = createOrdersTabelView()
		
		embedOrdersTabelView(ordersTableView)
		
		self.ordersTableView =  ordersTableView
	}
	
	func createOrdersTabelView() -> UITableView
	{
		let tv = UITableView()
		
		tv.backgroundColor = .clear
		tv.separatorStyle = .none
		tv.showsVerticalScrollIndicator = false
		
		tv.contentInset = UIEdgeInsets(top: -(getStatusBarHeight() ?? 0),
									   left: 0,
									   bottom: 0,
									   right: 0)
		
		tv.registerNibCell(OrderInfoTableViewCell.self)
		tv.registerNibCell(OrdersObtainingTableViewCell.self)
		
		setupTableHeader(tv)
		
		tv.delegate = self
		tv.dataSource = self
		
		return tv
	}
	
	func setupTableHeader(_ tableView: UITableView)
	{
		let headerView = OrdersHeaderView.create(parentViewController: self,
												 phoneNumber: managers.webService.currentUserProfile?.profile?.phone ?? "",
													 onLogOutTapped: {[weak self] in self?.onLogoutTapped() })
		
		let headerContainerView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: self.view.frame.width,
																				   height: OrdersHeaderView.Constants.height)))
		
		headerContainerView.addSubview(headerView)
		
		headerView.translatesAutoresizingMaskIntoConstraints = false
		
		NSLayoutConstraint.activate ([headerView.centerXAnchor.constraint(equalTo: headerContainerView.centerXAnchor),
		 headerView.centerYAnchor.constraint(equalTo: headerContainerView.centerYAnchor),
		 headerView.widthAnchor.constraint(equalToConstant: self.view.frame.width),
		 headerView.heightAnchor.constraint(equalToConstant: OrdersHeaderView.Constants.height)])
		
		tableView.tableHeaderView = headerContainerView
	}
	
	func embedOrdersTabelView(_ tableViewToEmbed: UITableView)
	{
		self.view.addSubview(tableViewToEmbed)
		
		tableViewToEmbed.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [tableViewToEmbed.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
						   tableViewToEmbed.topAnchor.constraint(equalTo: self.view.topAnchor),
						   tableViewToEmbed.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
						   tableViewToEmbed.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
		
		NSLayoutConstraint.activate(constraints)
	}
	
	func showWebPaymentForm(
		url: URL
	)
	{
		let helper = PaymentHelper()
		
		let webViewController = WebViewController()
		
		webViewController.config = .init(
			url: url,
			delegate: helper,
			dismissSegue:
		{
			[weak self] in

			self?.dismiss(animated: true)
			
			self?.getOrdersList()
		})
		
		helper.hidePaymentFormSegue =
		{
			[weak self] in

			self?.dismiss(animated: true)
			self?.getOrdersList()
			managers.appsFlyer.sendLog(logText: "payment_success", params: nil)
		}
		
		self.present(
			webViewController.embeddedInNavigation(),
			animated: true,
			completion: nil
		)
	}
	
	func showCancelAlert(onCancelButton: @escaping VoidCallback)
	{
		let alert = UIAlertController(
			title: "Отмена заказа",
			message: "Уверены, что хотите отменить заказ?",
			preferredStyle: UIAlertController.Style.alert
		)
		alert.addAction(
			UIAlertAction(
				title: "Нет",
				style: UIAlertAction.Style.cancel,
				handler: nil
			)
		)
		
		alert.addAction(
			UIAlertAction(
				title: "Да",
				style: UIAlertAction.Style.default,
				handler:
			{
				_ in
				onCancelButton()
			}
			)
		)
		
		self.present(alert, animated: true, completion: nil)
	}
	
	func createAmEmbedEmptyOrdersView()
	{
		let emptyListView = createView(EmptyOrdersListView.self)
		
		self.view.addSubview(emptyListView)
		
		emptyListView.translatesAutoresizingMaskIntoConstraints = false
		
		NSLayoutConstraint.activate([emptyListView.topAnchor.constraint(equalTo: self.view.topAnchor),
				 emptyListView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
				 emptyListView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
				 emptyListView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)])
		
		self.emptyOrdersListView = emptyListView
	}
	
	func setupNavBarAppearance()
	{
		(self.navigationController as? NavBarCustomizable)?.setAppearance(navBarBackground: .blurred,
																		hasBackButton: false,
																		titleViewType: .tabTitleHeader("Заказы"),
																		initiallyNotVisible: lastContentOffset < (getNavBarHeight() ?? 0))
	}
}

// MARK:- UITableViewDelegate, UITableViewDataSource
extension OrdersViewController: UITableViewDelegate, UITableViewDataSource
{
	func scrollViewDidScroll(_ scrollView: UIScrollView)
	{
		setNavBarAppearance(scrollView)
		
		fetchDataIfNeeded(scrollView)
	}
	
	func setNavBarAppearance(_ scrollView: UIScrollView)
	{
		if scrollView.contentOffset.y > (getNavBarHeight() ?? 0) && !navBarVisibilityToggled
		{
			(self.navigationController as? NavigationController)?.toggleVisibility()
			
			navBarVisibilityToggled = true
		}
		else if lastContentOffset > scrollView.contentOffset.y &&
					scrollView.contentOffset.y < (getNavBarHeight() ?? 0)  && navBarVisibilityToggled
		{
			(self.navigationController as? NavigationController)?.toggleVisibility()
			
			navBarVisibilityToggled = false
		}

		lastContentOffset = scrollView.contentOffset.y
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		if cells[indexPath.row].isOrderInfoCell(),
		   let orderInfo = cells[indexPath.row].getOrderInfoIfAny()
		{
			onOrderTapped(orderInfo)
		}
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return cells.count
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
	{
		switch cells[indexPath.row]
		{
		case .obtainingOrders:
			return UIScreen.main.bounds.height - OrdersHeaderView.Constants.height - (getTabBarHeight() ?? 0)
			
		case .orderInfo:
			return getCellHeight(indexPath.row)
		}
	}
	
	func getCellHeight(_ currentIndex: Int) -> CGFloat
	{
		return getOrderInfoCellHeight(currentIndex)
	}
	
	private func getOrderInfoCellHeight(_ currentIndex: Int) -> CGFloat
	{
		if let locationKind = getOrderCellLocationKind(currentIndex)
		{
			switch locationKind
			{
			case .top, .bottom:
				return OrderInfoTableViewCell.Constants.Dimensions.cornerCellHeight
				
			case .middle:
				return OrderInfoTableViewCell.Constants.Dimensions.cellHeight
			}
		}
		
		return 0
	}
	
	private func getOrderCellLocationKind(_ currentIndex: Int) -> OrderInfoTableViewCell.CellLocationKind?
	{
		return OrderInfoTableViewCell.CellLocationKind.get(0, currentIndex, cells.count - 1)
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		switch cells[indexPath.row]
		{
		case .obtainingOrders:
			return createOrdersObtainingTableViewCell(tableView)
		
		case .orderInfo(let orderInfo):
			return createOrderInfoTableViewCell(tableView, indexPath, orderInfo)
		}
	}
	
	private func onLogoutTapped()
	{
		// AAAAA TOREMOVE
		LogoutStory.start(on: self) // trigger segue passed in Config from parent story instead
	}
	
	private func createOrderInfoTableViewCell(_ tableView: UITableView, _ indexPath: IndexPath, _ orderInfo: OrderInfo) -> UITableViewCell
	{
		if let cell = tableView.dequeueReusableCell(ofType: OrderInfoTableViewCell.self),
		   let cellLocationKind = getOrderCellLocationKind(indexPath.row)
		{
			cell.initialize(
				parentContainerWidth: self.view.frame.width,
				cellLocationKind: cellLocationKind,
				kind: .customer(orderInfo),
				config: .init(
					onPayButton:
						{
							[weak self] url in

							self?.showWebPaymentForm(
								url: url
							)

						},
					onCancelButton:
						{
							[weak self] in

							self?.showCancelAlert(
								onCancelButton: { [weak self] in
									self?.cancelOrder(orderInfo)
								}
							)
						}
				)
			)
			
			cell.selectionStyle = .none
			cell.clipsToBounds = false
			
			return cell
		}
		
		return UITableViewCell()
	}
	
	private func createOrdersObtainingTableViewCell(_ tableView: UITableView) -> UITableViewCell
	{
		if let cell = tableView.dequeueReusableCell(ofType: OrdersObtainingTableViewCell.self)
		{
			return cell
		}
		
		return UITableViewCell()
	}
}
