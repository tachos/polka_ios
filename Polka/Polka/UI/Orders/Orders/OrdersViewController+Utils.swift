//
//  OrdersViewController+Utils.swift
//  Polka
//
//  Created by Michael Goremykin on 09.06.2021.
//

import UIKit

// MARK:- UI handlers
extension OrdersViewController
{
	func getOrdersList()
	{
		ordersTableView.setContentOffset(.zero, animated: false)
		
		managers.webService.getOrdersWithPagination(ordersPerPageCount, 0)//.indicateProgress(on: self.view)
		.done {[weak self] response  in
			self?.handleSuccess(response)
		}
		.catch
		{ error in
			PopUps.showRetryQuery(from: self,
								  queryRetryCallback: {[weak self] in self?.getOrdersList() },
								  onCancelCallback: {[weak self] in self?.handleFailure() })
		}
	}
	
	func handleSuccess(_ response: OrdersPaginationResponse)
	{
		self.cells = response.items.map { .orderInfo($0) }
		
		self.backendOrdersCount = response.count
		
		ordersTableView.reloadData()
		
		showEmptyOrdersViewIfNeeded()
		
		setInitialOffset()
		
		setOrdersBadge()
	}
	
	func handleFailure()
	{
	}
	
	func onOrderTapped(_ orderInfo: OrderInfo)
	{
		let orderViewController = OrderViewController()
		
		orderViewController.initialize(orderInfo)
		
		getTabBarController()?.present(orderViewController, animated: true, completion: nil)
	}

	func cancelOrder(_ orderInfo: OrderInfo)
	{
		managers.webService.cancelOrder(orderId: orderInfo.id)
		.indicateProgress(on: self.view)
		.done {[weak self]  in
			self?.onOrderCancelled()
		}
		.catch
		{ error in
			PopUps.showRetryQuery(
				from: self,
				queryRetryCallback: {[weak self] in self?.cancelOrder(orderInfo) },
				onCancelCallback: {[weak self] in self?.handleFailure() }
			)
		}
	}

	private func onOrderCancelled()
	{
		getOrdersList()
	}

	func showEmptyOrdersViewIfNeeded()
	{
		emptyOrdersListView.isHidden =  !cells.contains{ $0.isObtainingOrdersCell() } && !(cells.count <= 1)
	}
	
	func setInitialOffset()
	{
		if initialOffset == 0
		{
			initialOffset = getVisibleCellsIndexes().map { getCellHeight($0) }.reduce(0, +)
		}
	}
	
	func getVisibleCellsIndexes() -> [Int]
	{
		if ordersTableView.visibleCells.count == 0 { return [] }
		
		return ordersTableView.visibleCells.prefix(ordersTableView.visibleCells.count-1).map{ ordersTableView.indexPath(for: $0)?.row }.compactMap{ $0 }
	}
	
	func fetchDataIfNeeded(_ scrollView: UIScrollView)
	{
		if isSomeDataNotObtainedFromBackend() &&
		   (hasBeenScrolledToAdditionalDataFetchState(scrollView) || isNotObtainedDataSizeLessThenPageSize()) &&
		   !isAlreadyFetching
		{
			//print("Should fetch!")
			
			isAlreadyFetching = true
			getNextBatchOfOrders(cells.count - 1)
		}
		else
		{
			//print("No fetch!")
		}
	}
	
	private func getNextBatchOfOrders(_ offsetInElements: Int)
	{
		//print("Fetching!")
		managers.webService.getOrdersWithPagination(ordersPerPageCount, offsetInElements)
		.done {[weak self] response  in
			self?.handleOrdersBatchSuccess(response)
		}
		.catch
		{ error in
			PopUps.showRetryQuery(from: self,
								  queryRetryCallback: {[weak self] in self?.getOrdersList() },
								  onCancelCallback: {[weak self] in self?.handleFailure() })
		}
	}
	
	private func handleOrdersBatchSuccess(_ response: OrdersPaginationResponse)
	{
		isAlreadyFetching = false
		
		self.cells = Array(Set(cells.filter{ $0.isOrderInfoCell() }.compactMap{ $0.getOrderInfoIfAny() }).union( response.items)).sorted{ $0.orderNumber > $1.orderNumber }.map{ .orderInfo($0) }
		
		ordersTableView.reloadData()
		
		setOrdersBadge()
		//print("Stop fetching!")
	}
	
	private func handleOrdersBatchFailure()
	{
		
	}
	
	private func isSomeDataNotObtainedFromBackend() -> Bool
	{
		return cells.count < backendOrdersCount
	}
	
	private func hasBeenScrolledToAdditionalDataFetchState(_ scrollView: UIScrollView) -> Bool
	{
		return getNumberOffCellsFromCurrentScrollPositionToBottom(scrollView) <= fetchThreshold
	}
	
	private func isNotObtainedDataSizeLessThenPageSize() -> Bool
	{
		return backendOrdersCount - cells.count <= ordersPerPageCount
	}
	
	private func getNumberOffCellsFromCurrentScrollPositionToBottom(_ scrollView: UIScrollView) -> Double
	{
		Double((getObtainedContentHeight() - (initialOffset + scrollView.contentOffset.y))/OrderInfoTableViewCell.Constants.Dimensions.cellHeight)
	}
	
	private func getObtainedContentHeight() -> CGFloat
	{
		return cells.filter{ $0.isOrderInfoCell() }.enumerated().map{ getCellHeight($0.offset) }.reduce(0, +)
	}
	
	private func getNotFinalisedOrders() ->  Bool
	{
		cells.compactMap{ $0.getOrderInfoIfAny() }.filter{ !$0.isInFinalisedState() }.count > 0
	}
	
	private func setOrdersBadge()
	{
		getTabBarController()?.tabBarView.setBadge(forTab: .orders, isHidden: !getNotFinalisedOrders())
	}
	
}

extension OrdersViewController
{
	enum CellKind
	{
		case obtainingOrders
		case orderInfo(OrderInfo)
	}
}

extension OrdersViewController.CellKind
{
	func isOrderInfoCell() -> Bool
	{
		switch self
		{
		case .obtainingOrders:
			return false
		case .orderInfo:
			return true
		}
	}
	
	func isObtainingOrdersCell() -> Bool
	{
		switch self
		{
		case .obtainingOrders:
			return true
		case .orderInfo:
			return false
		}
	}
}

extension OrdersViewController.CellKind
{
	func getOrderInfoIfAny() -> OrderInfo?
	{
		switch self
		{
		case .orderInfo(let orderInfo):
			return orderInfo
			
		default:
			return nil
		}
	}
}


extension OrdersViewController
{
	static func getMockData() -> [CellKind]
	{
		var ret: [CellKind] = []
		
		ret.append(contentsOf: Array(1...6).map{ .orderInfo(createMockOrder($0-1)) })
		
		return ret
	}
	
	static func createMockOrder(_ val: Int) -> OrderInfo
	{
		return .init(id: String(val),
					 orderNumber: val,
					 state: .completed,
					 created: Date(),
					 modified: Date(),
					 subOrders: createMokSubOrders(),
					 deliveryInfo: .init(type: .free, cost: 0),
					 is_paid: true,
					 paymentTransactionInfo: .init(state: .new,
												   paymentFormUrl: URL(string: "https://www.google.com/")!))
	}
	
	static func createMokSubOrders() -> [SubOrderInfo]
	{
		return  Array(1...3).map{
			return SubOrderInfo(id: String($0),
								orderId: $0,
								state: .assembled,
								created: Date(),
								modified: Date(),
								seller: .init(id: "0", info: .init(caption: String($0),
																   description:  String($0),
																   profileImageUrl: nil,
																   sellerGallery: .empty,
																   isOpen: true,
																   isPriceActual: true)),
								products: createMockProducts())
		}
	}
	
	static func createMockProducts() -> [OrderProduct]
	{
		return  Array(1...Int.random(in: 2..<15)).map{
			return .init(product: .init(id: String($0),
										categoryId: "",
										name: "Product \($0)",
										imageUrl: nil,
										description: "",
										price: Int.random(in: 10...1000),
										nominalPrice: Int.random(in: 10...1000),
										unitWeight: 1.0, unit: .unit,
										minCountProductInCart: 1.0,
										stepChangeInCart: 1.0),
						 quantityPlacedInOrder: 10,
						 finalQuantityInAssembledOrder: 10)
		}
	}
}
