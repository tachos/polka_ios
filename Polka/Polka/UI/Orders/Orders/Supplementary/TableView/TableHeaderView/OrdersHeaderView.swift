//
//  OrdersHeaderView.swift
//  Polka
//
//  Created by Michael Goremykin on 13.06.2021.
//

import AnyFormatKit
import RxSwift

class OrdersHeaderView: UIView
{
    // MARK:- Outlets
    @IBOutlet weak var phoneNumberLabel: UILabel!
	weak var addressView: AddressView!

    // MARK:- Parameters
	var onLogOutTappedCallback: (() -> Void)!
	// AAAAA TOREMOVE
	weak var parentViewController: UIViewController!
	
	// MARK:- Rx
	let disposeBag = DisposeBag()
}

// MARK:- UI handlers
extension OrdersHeaderView
{
    @IBAction func onLogOutTapped(_ sender: Any)
    {
        onLogOutTappedCallback?()
    }
}

// MARK:- Life cycle
extension OrdersHeaderView
{
	override func awakeFromNib()
	{
		super.awakeFromNib()
		
		createAndEmbedAddressView()
		
		subscribeToUserEvents()
	}
}

// MARK:- Initialization
extension OrdersHeaderView
{
	func initialize(_ parentViewController: UIViewController,
					_ phoneNumber: String,
					_ onLogOutTapped: @escaping () -> Void)
	{
		self.parentViewController = parentViewController
		self.onLogOutTappedCallback = onLogOutTapped
		
		setupPhoneLabel(phoneNumber)
		
		setupAddressView()
	}
	
	func setupPhoneLabel(_ phoneNumber: String)
	{
		phoneNumberLabel.text = DefaultTextInputFormatter(textPattern: Formatter.phoneProfile.rawValue).format(phoneNumber) ?? ""
	}
}

// MARK:- Instantiation
extension OrdersHeaderView
{
	static func create(parentViewController: UIViewController,
					   phoneNumber: String,
					   onLogOutTapped: @escaping () -> Void) -> OrdersHeaderView
	{
		let ordersHeaderView = createView(OrdersHeaderView.self)
		
		ordersHeaderView.initialize(parentViewController, phoneNumber, onLogOutTapped)
		
		return ordersHeaderView
	}
}

// MARK:- Utils
extension OrdersHeaderView
{
	// AAAAA TOREMOVE
	func setupAddressView()
	{
		addressView?.initialize(nameLbl: Address.addressLbl(),
							   image: "edit",
							   text: Address.getAddressConsumer(),
							   type: .availableTapGesture,
							   onPress: {[weak self] in
								WhereBringStory.start(window: nil, viewController: self?.parentViewController, titleBtn: "Вернуться на экран заказов")
							   })
	}
	
	func subscribeToUserEvents()
	{
		UserController.shared.userLocalEventsObservable.subscribe(onNext: {[weak self] userEvent in
			guard let self = self else { return }
			switch userEvent
			{
			case .newDeliveryAddressHasBeenSet(_):
				self.setupAddressView()
			}
		}).disposed(by: disposeBag)
	}
	
	func createAndEmbedAddressView()
	{
		let viewToEmbed = createView(AddressView.self)
		
		self.addSubview(viewToEmbed)
		
		viewToEmbed.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [viewToEmbed.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Constants.horizontalSpacing),
						   viewToEmbed.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Constants.horizontalSpacing),
						   viewToEmbed.bottomAnchor.constraint(equalTo: self.bottomAnchor),
						   viewToEmbed.heightAnchor.constraint(equalToConstant: Constants.addressViewHeight)]
		
		NSLayoutConstraint.activate(constraints)
		
		self.addressView = viewToEmbed
	}
}

// MARK:- Constants
extension OrdersHeaderView
{
    enum Constants
    {
		static let height = CGFloat(216)
		static let horizontalSpacing = CGFloat(16)
		static let addressViewHeight = CGFloat(56)
    }
}
