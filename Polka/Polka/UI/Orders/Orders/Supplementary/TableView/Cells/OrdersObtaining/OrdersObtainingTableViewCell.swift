//
//  OrdersObtainingTableViewCell.swift
//  Polka
//
//  Created by Michael Goremykin on 05.07.2021.
//

import UIKit
import MBProgressHUD

class OrdersObtainingTableViewCell: UITableViewCell
{
	// MARK:- Outlets
	weak var progressIndicator:MBProgressHUD!
}

// MARK:- Life cycle
extension OrdersObtainingTableViewCell
{
	override func awakeFromNib()
	{
		if progressIndicator == nil
		{
			setupProgressIndicator()
		}
		else
		{
			self.progressIndicator.show(animated: true)
		}
	}
}

// MARK:- Life cycle
extension OrdersObtainingTableViewCell
{
	func setupProgressIndicator()
	{
		self.progressIndicator = self.addProgressIndicator()
	}
}
