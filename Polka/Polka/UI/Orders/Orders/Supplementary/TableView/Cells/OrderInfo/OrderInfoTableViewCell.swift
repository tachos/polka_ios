//
//  OrderInfoTableViewCell.swift
//  Polka
//
//  Created by Michael Goremykin on 09.06.2021.
//

import UIKit
import RxSwift

class OrderInfoTableViewCell: UITableViewCell
{
	// MARK:- Outlets
    @IBOutlet weak var roundedBackgroundView: UIView!
    @IBOutlet weak var roundedBackgroundViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var roundedBackgroundViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var orderStateLabel: UILabel!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var orderInfoLabel1: UILabel!
    @IBOutlet weak var orderInfoLabel2: UILabel!
    @IBOutlet weak var horizontalStackView: UIStackView!
    @IBOutlet weak var payButton: CustomButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    weak var productsImagesStack: OverlappingImagesHorizontalStackView!
	weak var productsImagesStackWidthConstraint: NSLayoutConstraint!
	
    @IBOutlet weak var paymentButton: CustomButton!
	
	struct Config
	{
		let onPayButton: Handler<URL>
		let onCancelButton: VoidCallback
		
		init(
			onPayButton: @escaping Handler<URL> = {_ in },
			onCancelButton: @escaping VoidCallback = {}
		)
		{
			self.onPayButton = onPayButton
			self.onCancelButton = onCancelButton
		}
	}
	
	
	var config: Config!
	
    // MARK:- Rx
	let disposeBag = DisposeBag()
	
	// MARK:- Parameters
	var paymentFormUrl: URL!
	var parentContainerWidth: CGFloat = 0
	var cellLocationKind: CellLocationKind!
	var kind: Kind!
}

// MARK:- Life cycle
extension OrderInfoTableViewCell
{
	override func awakeFromNib()
	{
		super.awakeFromNib()
		
		setupShadows()
		setupProductsImagesStackView()
	}
}

// MARK:- Initialize
extension OrderInfoTableViewCell
{
	func initialize(parentContainerWidth: CGFloat, cellLocationKind: CellLocationKind, kind: Kind, config: Config)
	{
		self.parentContainerWidth = parentContainerWidth
		self.cellLocationKind = cellLocationKind
		self.kind = kind
		self.config = config
		
		setupUI()
		
		//createPaymentAvailabilityTimer()
	}
}
