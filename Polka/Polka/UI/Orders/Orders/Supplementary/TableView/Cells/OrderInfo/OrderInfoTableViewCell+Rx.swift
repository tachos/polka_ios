//
//  OrderInfoTableViewCell+Rx.swift
//  Polka
//
//  Created by Michael Goremykin on 06.07.2021.
//

import RxSwift

extension OrderInfoTableViewCell
{
	func createPaymentAvailabilityTimer()
	{
		let countdown = 10
		Observable<Int>.interval(.seconds(1), scheduler: MainScheduler.instance)
			.map { countdown - $0 }
			.takeUntil(.inclusive, predicate: { $0 == 0 })
			.subscribe(onNext: {[weak self] value in
				self?.onTick(value)
			}, onCompleted: {[weak self] in
				self?.onTimerCompleted()
			}).disposed(by: disposeBag)
	}
	
	func onTick(_ tick: Int)
	{
		paymentButton.setTitle("\(tick) seconds left", for: .normal)
	}
	
	func onTimerCompleted()
	{
		paymentButton.setTitle("That's it, that's it, that's all!", for: [])
	}
}
