//
//  OrderInfoTableViewCell+UI.swift
//  Polka
//
//  Created by Michael Goremykin on 06.07.2021.
//

import UIKit

// MARK:- Setup UI
extension OrderInfoTableViewCell
{
	func setupUI()
	{
		setupCancelButton()
		setupConstraints()
		setupLabels()
		setupProductsPreview()
		setHorizontalStackViewVisible()
	}
	
	func setupCancelButton()
	{
		cancelButton.layer.cornerRadius = 18
		cancelButton.layer.borderWidth = 1
		cancelButton.layer.borderColor = #colorLiteral(red: 1, green: 0.5572302341, blue: 0, alpha: 1).cgColor
	}
	
	func setupProductsPreview()
	{
		productsImagesStack.setup(kind.getProductsUrls(), 3)
	}
	
	func setupConstraints()
	{
		let settings = getConstraintsSettings()
		
		roundedBackgroundViewTopConstraint.constant = settings.top
		roundedBackgroundViewBottomConstraint.constant = settings.bottom
	}
	
	func getConstraintsSettings() -> (top: CGFloat, bottom: CGFloat)
	{
		if let cellLocationKind = cellLocationKind
		{
			switch cellLocationKind
			{
			case .top:
				return Constants.Dimensions.topCellPaddings
				
			case .middle:
				return Constants.Dimensions.cellPaddings
				
			case .bottom:
				return Constants.Dimensions.bottomCellPaddings
			}
		}
		
		return (0,0)
	}
	
	func setupLabels()
	{
		if let kind = kind
		{
			switch kind
			{
			case .customer(let orderInfo):
				setupLabelsForCustomer(orderInfo)
				
			case .seller(let subOrderInfo):
				setupLabelsForSeller(subOrderInfo)
			}
		}
	}
	
	private func setupLabelsForCustomer(_ orderInfo: OrderInfo)
	{
		orderStateLabel.textColor = orderInfo.state.getLabelColor()
		orderStateLabel.text = orderInfo.state.toString()
		orderIdLabel.text = "№ \(orderInfo.orderNumber)"
		orderInfoLabel1.text = "Заказ от \(DatesFormatter.forOrder(orderInfo.created))"
		orderInfoLabel2.text = "\(orderInfo.getProductsCount()) \(orderInfo.getProductsCount().productString()), \(PriceFormatter.catalogItem(orderInfo.getTotal()))"
	}
	
	private func setupLabelsForSeller(_ subOrderInfo: SubOrderInfo)
	{
		orderStateLabel.textColor = subOrderInfo.state.getLabelColor()
		orderStateLabel.text = subOrderInfo.state.toString()
		orderIdLabel.text = "№ \(subOrderInfo.orderId)"
		orderInfoLabel1.text = "\(subOrderInfo.getTotalInUnits()) \(subOrderInfo.getTotalInUnits().productString())"
		orderInfoLabel2.text = "Заказ от \(DatesFormatter.toSellerOrder(subOrderInfo.created))"
	}
	
	func setupProductsImagesStackView()
	{
		let productsStack = createView(OverlappingImagesHorizontalStackView.self)
		
		self.addSubview(productsStack)
		
		productsStack.translatesAutoresizingMaskIntoConstraints = false
		
		NSLayoutConstraint.activate(
		[productsStack.heightAnchor.constraint(equalToConstant: OverlappingImagesHorizontalStackView.Constants.Dimensions.dimension),
		 productsStack.widthAnchor.constraint(equalToConstant: CGFloat(168)),
		 productsStack.leadingAnchor.constraint(equalTo: roundedBackgroundView.leadingAnchor, constant: CGFloat(16)),
		 productsStack.bottomAnchor.constraint(equalTo: roundedBackgroundView.bottomAnchor, constant: -CGFloat(16))])
		
		self.productsImagesStack = productsStack
	}
	
	func setupShadows()
	{
		roundedBackgroundView.clipsToBounds = false
		roundedBackgroundView.layer.shadowColor = UIColor.gray.cgColor
		roundedBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 0)
		roundedBackgroundView.layer.shadowOpacity = 0.2
		roundedBackgroundView.layer.shadowRadius = 7.0
	}
}

private extension OrderInfoTableViewCell
{
	private func setHorizontalStackViewVisible()
	{
		switch kind
		{
		case .customer(let orderInfo):
			setCancelButtonVisible(
				state: orderInfo.state,
				paymentState: orderInfo.paymentTransactionInfo.state,
				is_paid: orderInfo.is_paid
			)
			paymentFormUrl = orderInfo.paymentTransactionInfo.paymentFormUrl
		case .seller:
			horizontalStackView.isHidden = true
			productsImagesStack.isHidden = false 
		case .none:
			break
		}
	}
	
	private func setCancelButtonVisible(state: OrderState, paymentState: PaymentState, is_paid: Bool)
	{
		switch state
		{
		case .verifyingDelivery,.waitingForPayment:
			cancelButton.isHidden = false
			setPayButtonVisible(
				state: paymentState,
				is_paid: is_paid
			)
		case .assembling,.waitingForCourier,.delivering,.completed,.canceled:
			cancelButton.isHidden = true
			horizontalStackView.isHidden = true
			productsImagesStack.isHidden  = false
		}
	}
	
	private func setPayButtonVisible(state: PaymentState, is_paid: Bool)
	{
		switch state
		{
		case .new:
			payButton.isHidden = is_paid
			horizontalStackView.isHidden = is_paid
			productsImagesStack.isHidden  = !is_paid
		case .authorized,.rejected,.canceled,.reversed,.refunded,.partialRefunded,.confirmed:
			payButton.isHidden = true
			horizontalStackView.isHidden = true
			productsImagesStack.isHidden  = false
		}
	}
}

private extension OrderInfoTableViewCell
{
	@IBAction func onPayButton()
	{
		config.onPayButton(paymentFormUrl)
	}
	
	@IBAction func onCancelButton()
	{
		config.onCancelButton()
	}
}
