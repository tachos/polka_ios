//
//  OrderInfoTableViewCell+utils.swift
//  Polka
//
//  Created by Michael Goremykin on 06.07.2021.
//

import UIKit

// MARK:- Constants
extension OrderInfoTableViewCell
{
	enum Constants
	{
		enum Dimensions
		{
			static let cornerCellHeight = CGFloat(183)
			static let cellHeight = CGFloat(175)
			
			static let topCellPaddings = (top: CGFloat(16), bottom: CGFloat(8))
			static let cellPaddings = (top: CGFloat(8), bottom: CGFloat(8))
			static let bottomCellPaddings = (top: CGFloat(8), bottom: CGFloat(16))
		}
	}
}

extension OrderInfoTableViewCell
{
	enum Kind
	{
		case customer(OrderInfo)
		
		case seller(SubOrderInfo)
	}
}

extension OrderInfoTableViewCell.Kind
{
	func getProductsUrls() -> [URL?]
	{
		switch self
		{
		case .customer(let orderInfo):
			return orderInfo.getProductsImagesUrls()
			
		case .seller(let subOrderInfo):
			return subOrderInfo.getProductsImagesUrls()
		}
	}
}

extension OrderInfoTableViewCell
{
	enum CellLocationKind
	{
		case top
		case middle
		case bottom
	}
}

extension OrderInfoTableViewCell.CellLocationKind
{
	static func get(_ firstIndex: Int, _ currentIndex: Int, _ lastIndex: Int) -> OrderInfoTableViewCell.CellLocationKind
	{
		if firstIndex == currentIndex
		{
			return .top
		}
		else if lastIndex == currentIndex
		{
			return .bottom
		}
		else
		{
			return .middle
		}
	}
}
