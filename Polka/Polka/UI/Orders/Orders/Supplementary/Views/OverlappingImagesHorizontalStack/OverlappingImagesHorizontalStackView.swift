//
//  OverlappingImagesHorizontalStackView.swift
//  Polka
//
//  Created by Michael Goremykin on 09.06.2021.
//

import SDWebImage

class OverlappingImagesHorizontalStackView: UIView
{
	// MARK:- Outlets
    @IBOutlet weak var productsStackView: UIStackView!
}

// MARK:- API
extension OverlappingImagesHorizontalStackView
{
	func setup(_ imageUrls: [URL?], _ maxElementDisplay: Int)
	{
		clearPreviousContents()
		setupConstraints(imageUrls, maxElementDisplay)
		embedViews(imageUrls, maxElementDisplay)
	}
}

// MARK:- Life cycle
extension OverlappingImagesHorizontalStackView
{
	override func awakeFromNib()
	{
		super.awakeFromNib()
		
		setupStackView()
	}
}

// MARK:- Setup UI
extension OverlappingImagesHorizontalStackView
{
	func clearPreviousContents()
	{
		productsStackView.arrangedSubviews.forEach{
			productsStackView.removeArrangedSubview($0)
			$0.subviews.forEach{ subView in subView.removeFromSuperview()}
			$0.removeFromSuperview()
		}
	}
	
	func embedViews(_ imageUrls: [URL?], _ maxElementDisplay: Int)
	{
		let productPreviewViews: [UIView] = imageUrls.prefix(maxElementDisplay).map { toProductPreview($0) }
		
		let collapsedImagesCountView: UIView? = getCollapsedImagesCountViewIfAny(imageUrls.count, maxElementDisplay)

		var tmpViews: [UIView] = productPreviewViews
		
		if let collapsedImagesCountView = collapsedImagesCountView {
			tmpViews.append(collapsedImagesCountView)
		}
		
		//
		tmpViews.forEach{ productsStackView.addArrangedSubview($0) }
		tmpViews.forEach{
			$0.translatesAutoresizingMaskIntoConstraints = false
		}
		
		NSLayoutConstraint.activate(tmpViews.map { $0.widthAnchor.constraint(equalToConstant: Constants.Dimensions.dimension) })
	}
	
	func setupConstraints(_ imageUrls: [URL?], _ maxElementDisplay: Int)
	{
		if let widthConstraint = widthConstraint
		{
			widthConstraint.constant = getWidth(imageUrls, maxElementDisplay)
		}
		else
		{
			NSLayoutConstraint.activate([self.heightAnchor.constraint(equalToConstant: getWidth(imageUrls, maxElementDisplay))])
		}
	}
	
	func getWidth(_ imageUrls: [URL?], _ maxElementDisplay: Int) -> CGFloat
	{
		var itemsCount = imageUrls.prefix(maxElementDisplay).count
		
		if imageUrls.count > maxElementDisplay
		{
			itemsCount += 1
		}
		
		return CGFloat(itemsCount)*Constants.Dimensions.dimension + CGFloat(itemsCount-1)*Constants.Dimensions.interItemOffset
	}
}

// MARK:- Utils
extension OverlappingImagesHorizontalStackView
{
	func setupStackView()
	{
        self.productsStackView.spacing = Constants.Dimensions.interItemOffset
		self.productsStackView.distribution = .fill
		self.productsStackView.alignment = .fill
	}
	
	func toProductPreview(_ imageUrl: URL?) -> UIView
	{
		let imageView = UIImageView()
		
		imageView.contentMode = .scaleAspectFill
		
		imageView.backgroundColor = .white
		
		imageView.layer.borderColor = UIColor.white.cgColor
		imageView.layer.borderWidth = Constants.Dimensions.borderWidth
		imageView.layer.cornerRadius = Constants.Dimensions.radius
		imageView.clipsToBounds = true
		
		imageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "smile"), options: [])
		
		return imageView
	}
	
	func getCollapsedImagesCountViewIfAny(_ productsCount: Int, _ maxElementCount: Int) -> UIView?
	{
		if productsCount - maxElementCount <= 0 { return nil }
		
		let ret = UIView()
		
		ret.backgroundColor = Constants.Colors.orange
		ret.layer.borderWidth = Constants.Dimensions.borderWidth
		ret.layer.borderColor = UIColor.white.cgColor
		ret.layer.cornerRadius = Constants.Dimensions.radius
		
		embedCounterLabel(createLabelForCounter(productsCount - maxElementCount), in: ret)
		
		return ret
	}
	
	func createLabelForCounter(_ count: Int) -> UILabel
	{
		let counterLabel = UILabel()
		
		counterLabel.textAlignment = .center
		counterLabel.textColor = .white
		counterLabel.numberOfLines = 0
		counterLabel.font = Fonts.ObjectSans.regular(20)
		counterLabel.text = "+\(count)"
		
		return counterLabel
	}
	
	func embedCounterLabel(_ label: UILabel, in container: UIView)
	{
		container.addSubview(label)
		
		label.translatesAutoresizingMaskIntoConstraints = false
		
		NSLayoutConstraint.activate([label.centerXAnchor.constraint(equalTo: container.centerXAnchor),
									 label.centerYAnchor.constraint(equalTo: container.centerYAnchor)])
		
		container.bringSubviewToFront(label)
	}
}

extension UIView
{
	var widthConstraint: NSLayoutConstraint?
	{
		get
		{
			return constraints.first(where: {
				$0.firstAttribute == .width && $0.relation == .equal
			})
		}
		set { setNeedsLayout() }
	}
	
	var heightConstraint: NSLayoutConstraint?
	{
		get
		{
			return constraints.first(where: {
				$0.firstAttribute == .height && $0.relation == .equal
			})
		}
		set { setNeedsLayout() }
	}
}

// MARK:- Constants
extension OverlappingImagesHorizontalStackView
{
	enum Constants
	{
		static let maxElementDisplayCount = 3
		
		enum Dimensions
		{
			static let interItemOffset = -CGFloat(8)
			static let radius = CGFloat(24)
			static let dimension = CGFloat(48)
			static let innerRadius = CGFloat(22)
			static let borderWidth = CGFloat(2)
		}
		enum Colors
		{
			static let orange = #colorLiteral(red: 1, green: 0.4784313725, blue: 0, alpha: 1)
		}
	}
}
