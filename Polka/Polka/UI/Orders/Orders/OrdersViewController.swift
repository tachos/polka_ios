//
//  OrdersViewController.swift
//  Polka
//
//  Created by Michael Goremykin on 09.06.2021.
//

import UIKit

class OrdersViewController: UIViewController
{
	// MARK:- Outlets
	weak var ordersTableView: UITableView!
	weak var emptyOrdersListView: EmptyOrdersListView!
	
	// MARK:- Parameters
	let ordersPerPageCount: Int = 6
	let fetchThreshold: Double = 2.0
	
	// MARK:- Stateful
	var cells: [CellKind] = [.obtainingOrders]
	var backendOrdersCount = 0
	var navBarVisibilityToggled = false
	var initialOffset: CGFloat = 0
	var lastContentOffset = CGFloat(0)
	var isAlreadyFetching = false
}

// MARK:- Life Cycle
extension OrdersViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		setupUI()
	}
	
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
		
		setupNavBarAppearance()
		
		showEmptyOrdersViewIfNeeded()
		
		getOrdersList()
	}
}
