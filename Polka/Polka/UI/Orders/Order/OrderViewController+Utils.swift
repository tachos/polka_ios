//
//  OrderViewController+Utils.swift
//  Polka
//
//  Created by Michael Goremykin on 11.06.2021.
//

import UIKit


// MARK:- Close button handler
extension OrderViewController
{
	@objc func onCloseTapped()
	{
		self.dismiss(animated: true, completion: nil)
	}
}

// MARK:- Utils
extension OrderInfo
{
	func toCells() -> [OrderViewController.CellKind]
	{
		[self.subOrders.map { subOrder in
			return OrderViewController.CellKind.seller(subOrder.seller, subOrder.state, subOrder.products)
		}, [OrderViewController.CellKind.check(self)]].reduce([], +)
	}
}

// MARK:- CellKind
extension OrderViewController
{
	enum CellKind
	{
		case seller(Seller, SubOrderState, [OrderProduct])
		case check(OrderInfo)
	}
}

// MARK:- Constants
extension OrderViewController
{
	enum Constants
	{
		static let tableViewTopPadding = CGFloat(32)
		static let closeButtonPadding = CGFloat(11)
		static let closeButtonDimension = CGFloat(24)
	}
}
