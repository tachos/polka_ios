//
//  CheckTableViewCell.swift
//  Polka
//
//  Created by Michael Goremykin on 11.06.2021.
//

import UIKit

class CheckTableViewCell: UITableViewCell
{
    @IBOutlet weak var checkItemsStack: UIStackView!
}

// MARK:- Life cycle
extension CheckTableViewCell
{
	override func awakeFromNib()
	{
		super.awakeFromNib()
		
		setupStack()
	}
}

// MARK:- Setup UI
extension CheckTableViewCell
{
	func setup(_ viewsToEmbed: [UIView])
	{
		clearStack()
		
		viewsToEmbed.forEach{ checkItemsStack.addArrangedSubview($0) }
	}
}

// MARK:- Utils
extension CheckTableViewCell
{
	func clearStack()
	{
		checkItemsStack?.arrangedSubviews.forEach{ viewToRemove in
			checkItemsStack.removeArrangedSubview(viewToRemove)
			viewToRemove.removeFromSuperview()
		}
	}
	
	func setupStack()
	{
		checkItemsStack.spacing = Constants.checkInterItemSpacing
	}
}

// MARK:- Constants
extension CheckTableViewCell
{
	enum Constants
	{
		static let checkInterItemSpacing = CGFloat(8)
		// Delivery info + Total
		static let additionalElementsCount = 2
	}
}
