//
//  OrderProductTableViewCell.swift
//  Polka
//
//  Created by Michael Goremykin on 11.06.2021.
//

import SDWebImage

class OrderProductTableViewCell: UITableViewCell
{
    @IBOutlet weak var productImageView: CustomImage!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var pricePerUnitLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
}

// MARK:- Life cycle
extension OrderProductTableViewCell
{
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        productImageView.layer.cornerRadius = CGFloat(16)
    }
}

// MARK:- Initialization
extension OrderProductTableViewCell
{
	func initialize(productImageUrl: URL?,
					productTitle: String,
					pricePerUnitString: NSAttributedString,
					quantity: String)
	{
		self.productImageView.sd_setImage(with: productImageUrl)
		self.productTitleLabel.text = productTitle
		self.pricePerUnitLabel.attributedText = pricePerUnitString
		self.quantityLabel.text = quantity
	}
}

// MARK:- Constants
extension OrderProductTableViewCell
{
	enum Constants
	{
		static let height = CGFloat(48)
	}
}
