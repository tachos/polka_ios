//
//  OrderHeadeView.swift
//  Polka
//
//  Created by Michael Goremykin on 11.06.2021.
//

import UIKit

class OrderHeaderView: UIView
{
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var orderStateLabel: UILabel!
}

// MARK:- Initialization
extension OrderHeaderView
{
    func initialize(_ orderId: Int, _ orderState: OrderState)
    {
		orderIdLabel.text = "№ \(orderId)"
        
        orderStateLabel.textColor = orderState.getLabelColor()
        orderStateLabel.text = orderState.toString()
    }
}

// MARK:- Instantiation
extension OrderHeaderView
{
    static func create(size: CGSize, orderId: Int, orderState: OrderState) -> OrderHeaderView
	{
		let orderHeaderView = createView(OrderHeaderView.self)
		
		orderHeaderView.frame = CGRect(origin: .zero, size: size)
        
        orderHeaderView.initialize(orderId, orderState)
		
		return  orderHeaderView
	}
}

// MARK:- Constants
extension OrderHeaderView
{
	enum Constants
	{
		static let height = CGFloat(94)
	}
}
