//
//  OrderSectionHeaderView.swift
//  Polka
//
//  Created by Michael Goremykin on 11.06.2021.
//

import UIKit

class OrderSectionHeaderView: UITableViewHeaderFooterView
{
    @IBOutlet weak var fakeRoundedCornersHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabelTopPaddingConstraint: NSLayoutConstraint!
    
}

// MARK:- Initialization
extension OrderSectionHeaderView
{
	func initialize(_ hideFakeRoundedCorners: Bool, _ headerKind: HeaderKind)
	{
        setupLabels(headerKind)
        setupFakeRoundedCornersView(hideFakeRoundedCorners)
        setupConstraints(headerKind)
	}
}

// MARK:- Utils
extension OrderSectionHeaderView
{
    func setupConstraints(_ headerKind: HeaderKind)
    {
        var topPadding: CGFloat = 0
        
        switch headerKind
        {
        case .seller:
            topPadding = Constants.sellerTopPadding
        case .check:
            topPadding = Constants.checkTopPadding
        }
        
        titleLabelTopPaddingConstraint.constant = topPadding
    }
    
    func setupFakeRoundedCornersView(_ hideFakeRoundedCorners: Bool)
    {
        fakeRoundedCornersHeightConstraint.constant = hideFakeRoundedCorners ? 0 : Constants.fakeRoundedCornersViewHeight
    }
    
    
    func setupLabels(_ headerKind: HeaderKind)
    {
        let strings = getStrings(headerKind)
        
        titleLabel.text         = strings.0
        descriptionLabel.text   = strings.1
    }
    
    func getStrings(_ headerKind: HeaderKind) -> (String, String)
    {
        switch headerKind
        {
        
        case .seller(let sellerName, let sellerDescription):
            return (sellerName, sellerDescription)
            
        case .check:
            return ("Чек", "")
        }
    }
}

// MARK:- HeaderKind
extension OrderSectionHeaderView
{
	enum HeaderKind
	{
		case seller(sellerName: String, sellerDescription: String)
		case check
	}
}

//  MARK:- Constants
extension OrderSectionHeaderView
{
	enum Constants
	{
		static let sellerTopPadding = CGFloat(0)
		static let checkTopPadding = CGFloat(8)
		static let heightWithRoundedCorners = CGFloat(108)
		static let heightWithoutRoundedCorners = CGFloat(60)
        static let fakeRoundedCornersViewHeight = CGFloat(50)
	}
}
