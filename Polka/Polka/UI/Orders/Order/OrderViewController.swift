//
//  OrderViewController.swift
//  Polka
//
//  Created by Michael Goremykin on 11.06.2021.
//

import UIKit

class OrderViewController: UIViewController
{
	// MARK:- Outlets
	weak var orderContentsTableView: UITableView!
	
	// MARK:- Stateful
	var cells: [CellKind] = []
	
	// MARK:- Parameters
	var orderInfo: OrderInfo!
}

// MARK:- Initialization
extension OrderViewController
{
	func initialize(_ orderInfo: OrderInfo)
	{
		self.orderInfo = orderInfo
		self.cells = orderInfo.toCells()
	}
}

// MARK:- Instantiation
extension OrderViewController
{
	static func create(_ orderInfo: OrderInfo) -> OrderViewController
	{
		let vc = OrderViewController()
		
		vc.initialize(orderInfo)
		
		vc.view.backgroundColor = .red
		
		return vc
	}
}

// MARK:- Life cycle
extension OrderViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		setupUI()
	}
}
