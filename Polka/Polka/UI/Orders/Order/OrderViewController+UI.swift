//
//  OrderViewController+UI.swift
//  Polka
//
//  Created by Michael Goremykin on 11.06.2021.
//

import UIKit

//
extension OrderViewController
{
	func setupUI()
	{
		setupBackgroundColor()
		setupCloseButton()
		setupOrderContentsTabelView()
	}
}

// MARK:- UI utils
extension OrderViewController
{
	func setupBackgroundColor()
	{
		self.view.backgroundColor = .white
	}
}

// MARK:- UI close button utils
extension OrderViewController
{
	func setupCloseButton()
	{
		embedCloseButton(createCloseButton())
	}
	
	func createCloseButton() -> UIButton
	{
		let closeButton = UIButton()
		
		closeButton.addTarget(self, action: #selector(onCloseTapped), for: .touchUpInside)
		
		closeButton.setImage(UIImage(named: "close") ?? UIImage(), for: [])
		closeButton.imageView?.contentMode = .center
		
		return closeButton
	}
	
	func embedCloseButton(_ buttonToEmbed: UIButton)
	{
		self.view.addSubview(buttonToEmbed)
		
		buttonToEmbed.translatesAutoresizingMaskIntoConstraints = false
		
		NSLayoutConstraint.activate([buttonToEmbed.heightAnchor.constraint(equalToConstant: Constants.closeButtonDimension),
									 buttonToEmbed.widthAnchor.constraint(equalToConstant: Constants.closeButtonDimension),
									 buttonToEmbed.topAnchor.constraint(equalTo: self.view.topAnchor,
																		constant: Constants.closeButtonPadding),
									 buttonToEmbed.trailingAnchor.constraint(equalTo: self.view.trailingAnchor,
																			 constant: -Constants.closeButtonPadding)])
	}
}

// MARK:- UI tableView utils
extension OrderViewController
{
	func setupOrderContentsTabelView()
	{
		let orderContentsTableView = createOrdersContentsTableView()
		
		embedOrdersContentsTableView(orderContentsTableView)
		
		self.orderContentsTableView = orderContentsTableView
	}
	
	func createOrdersContentsTableView() -> UITableView
	{
		let tv = UITableView(frame: CGRect(origin: .zero, size: CGSize(width: self.view.frame.width,
																	   height: self.view.frame.height - Constants.tableViewTopPadding)), style: .grouped)
		
		tv.backgroundColor = .clear
		tv.separatorStyle = .none
		tv.showsVerticalScrollIndicator = false
		
		tv.register(UINib(nibName: String(describing: OrderSectionHeaderView.self), bundle: nil),
					forHeaderFooterViewReuseIdentifier: String(describing: OrderSectionHeaderView.self))
		
		tv.registerNibCell(OrderProductTableViewCell.self)
		tv.registerNibCell(CheckTableViewCell.self)
		
		tv.delegate = self
		tv.dataSource = self
		
		tv.tableHeaderView = OrderHeaderView.create(size: CGSize(width: self.view.frame.width,
														   height: OrderHeaderView.Constants.height),
													orderId: orderInfo.orderNumber,
													orderState: orderInfo.state)
		
		tv.sectionFooterHeight = 0
		
		return tv
	}
	
	func embedOrdersContentsTableView(_ tableView: UITableView)
	{
		self.view.addSubview(tableView)
		
		tableView.translatesAutoresizingMaskIntoConstraints = false
		
		NSLayoutConstraint.activate([tableView.topAnchor.constraint(equalTo: self.view.topAnchor,
																	constant: Constants.tableViewTopPadding),
				 tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
				 tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
				 tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)])
	}
}

// MARK:- UITableViewDelegate, UITableViewDataSource
extension OrderViewController: UITableViewDelegate, UITableViewDataSource
{
	func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat
	{
		return .leastNormalMagnitude
	}
	
	func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
	{
		return UIView(frame: CGRect(origin: .zero, size: CGSize(width: 0, height: 0)))
	}
	
	func numberOfSections(in tableView: UITableView) -> Int
	{
		return cells.count
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return getItemsCountPerSection(section)
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
	{
		return getSectionHeaderHeight(section)
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
	{
		guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: OrderSectionHeaderView.self)) as? OrderSectionHeaderView else {
			return UIView()
		}
		
		headerView.initialize(section == 0,
							  getHeaderKind(section))
		
		headerView.contentView.backgroundColor = .white
		
		return headerView
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
	{
		switch cells[indexPath.section]
		{
		case .seller:
			return OrderProductTableViewCell.Constants.height
			
		case .check(_):
			return getCheckCellHeight()
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		switch cells[indexPath.section]
		{
		case .seller(_, let state, let orderProducts):
			return getOrderProductCell(tableView, state, orderProducts[indexPath.row])
			
		case .check(_):
			return getCheckCell(tableView)
		}
	}
}

// MARK:- UI utils
extension OrderViewController
{
	func getItemsCountPerSection(_ sectionIndex: Int) -> Int
	{
		switch cells[sectionIndex]
		{
		case .seller(_, _, let orderProducts):
			return orderProducts.count
			
		case .check:
			return 1
		}
	}
	
	func getSectionHeaderHeight(_ sectionIndex: Int) -> CGFloat
	{
		switch sectionIndex
		{
		case 0:
			return OrderSectionHeaderView.Constants.heightWithoutRoundedCorners
			
		default:
			return OrderSectionHeaderView.Constants.heightWithRoundedCorners
		}
	}
	
	func getHeaderKind(_ section: Int) -> OrderSectionHeaderView.HeaderKind
	{
		switch cells[section]
		{
		case .seller(let seller, _, _):
			return .seller(sellerName: seller.info.caption,
						   sellerDescription: seller.info.description)
			
		case .check:
			return .check
		}
	}
	
	func getOrderProductCell(_ tableView: UITableView, _ state: SubOrderState, _ orderProduct: OrderProduct) -> UITableViewCell
	{
		guard let orderProductCell = tableView.dequeueReusableCell(ofType: OrderProductTableViewCell.self) else {
			return UITableViewCell()
		}
		
		let pricePerUnit = PriceFormatter.catalogItemWithUnits(price: FormatterPrice.kopeksToRoubles(orderProduct.product.price),
															   measurementUnit: orderProduct.product.unit)
		
		orderProductCell.initialize(productImageUrl: orderProduct.product.imageUrl,
									productTitle: orderProduct.product.name,
									pricePerUnitString: pricePerUnit,
									quantity: QuantityFormatter.cartItemQuantity(getProductQuantity(state, orderProduct),
																				 orderProduct.product.unit) )
		
		return orderProductCell
	}
	
	func getProductQuantity(_ state: SubOrderState, _ orderProduct: OrderProduct) -> Float
	{
		switch state
		{
		case .created, .assembling, .canceled:
			return orderProduct.quantityPlacedInOrder
			
		case .assembled:
			return orderProduct.finalQuantityInAssembledOrder ?? 0
		}
	}
	
	func getCheckCellHeight() -> CGFloat
	{
		return CGFloat(orderInfo.subOrders.count + CheckTableViewCell.Constants.additionalElementsCount) * OrderItemView.Constants.height +
			   CGFloat(orderInfo.subOrders.count + CheckTableViewCell.Constants.additionalElementsCount - 1) * CheckTableViewCell.Constants.checkInterItemSpacing
	}
	
	func getCheckCell(_ tableView: UITableView) -> UITableViewCell
	{
		guard let checkCell = tableView.dequeueReusableCell(ofType: CheckTableViewCell.self) else {
			return UITableViewCell()
		}
		
		var viewsToEmbed = orderInfo.subOrders.map{ OrderItemView.create(.SellerInfo(sellerName: $0.seller.info.caption,
																					  positionsCount: $0.getTotalInUnits(),
																					  total: $0.getTotal()))}
		
		viewsToEmbed.append(contentsOf: [OrderItemView.create(.DeliveryInfo(time: "",
																			price: FormatterPrice.kopeksToRoubles(orderInfo.deliveryInfo.cost ?? 0))),
							 OrderItemView.create(.TotalInfo(total: orderInfo.getTotal()))])
		
		checkCell.setup(viewsToEmbed)
		
		return checkCell
	}
	
}
