//
//  MarketTableCell.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 13.10.2021.
//

import UIKit



class MarketTableCell: UITableViewCell
{
	@IBOutlet private var statusLabel: UILabel!
	@IBOutlet private var captionLabel: UILabel!
	@IBOutlet private var addressLabel: UILabel!
	@IBOutlet private var workHoursLabel: UILabel!
		
	
	func configure(
		market: Market
	)
	{
		setStatusLabel(isMarketOpenNow: market.isOpenNow)
		
		captionLabel.text = market.caption
		addressLabel.text = market.address
		workHoursLabel.text = market.workHoursString
	}
	
	
	private func setStatusLabel(isMarketOpenNow: Bool)
	{
		statusLabel.text = isMarketOpenNow ? "открыто" : "закрыто"
		statusLabel.backgroundColor = isMarketOpenNow ? Colors.green : Colors.pink
	}
}
