//
//  LoadMarketsHelper.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 13.10.2021.
//

import UIKit
import PromiseKit



class LoadMarketsHelper
{
	private(set) var loadedMarkets: [Market] = []
	
	private let onMarketSelected: Handler2<Market, Bool>
	private let previousMarketAlias: MarketAlias?
	

	var onLoadingFailed: VoidCallback?
	
	
	init(
		onMarketSelected: @escaping Handler2<Market, Bool>
	)
	{
		self.onMarketSelected = onMarketSelected
		
		self.previousMarketAlias = managers.localStore.currentMarket?.alias
	}
}



// MARK: - API

extension LoadMarketsHelper
{
	func loadMarkets(on vc: SelectMarketViewController)
	{
		vc.displayState(.loadingInProgress)
		
		firstly {
			managers.webService.getMarkets()
		}
		.done { [weak self, weak vc] in
			//
			self?.loadedMarkets = $0
			
			vc?.displayState(.loaded)
		}
		.catch { [weak self, weak vc] error in
			//
			vc?.displayState(.loadingFailed(error))
			self?.onLoadingFailed?()
		}
	}
	
	func selectMarket(_ market: Market)
	{
		let selectedMarketChanged = self.previousMarketAlias != market.alias

		if selectedMarketChanged
		{
			managers.localStore.currentMarket = market
		}

		onMarketSelected(market, selectedMarketChanged)
	}
}
