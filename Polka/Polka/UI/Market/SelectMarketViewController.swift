//
//  SelectMarketViewController.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 12.10.2021.
//

import UIKit
import FloatingPanel



class SelectMarketViewController: UIViewController, StoryboardCreatable
{
	// MARK:- StoryboardCreatable
	static var storyboardName: StoryboardName = .market
	
	@IBOutlet private var selectMarketLabel: UILabel!
	
	//
	@IBOutlet var marketsTableView: UITableView!
	
	//
	@IBOutlet private var loadInProgressView: UIView!
	@IBOutlet private var activityIndicator: ActivityIndicator!
	
	
	enum State
	{
		case loadingInProgress
		case loaded
		case loadingFailed(Error)
	}
	
	
	struct Config
	{
		let helper: LoadMarketsHelper
	}
	var config: Config!
}



// MARK:- Life cycle

extension SelectMarketViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		setupUI()
	}
	
	private func setupUI()
	{
		selectMarketLabel.attributedText = NSMutableAttributedString(
			string: "Выберите рынок ",
			attributes: [NSAttributedString.Key.kern: -1.8]
		)
	}
	
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
		//
		config.helper.loadMarkets(on: self)
	}
}



// MARK:- API

extension SelectMarketViewController
{
	func displayState(_ state: State)
	{
		switch state
		{
		case .loadingInProgress:
			loadInProgressView.isHidden = false
			activityIndicator.startRotating()
			
		case .loaded:
			loadInProgressView.isHidden = true
			activityIndicator.stopRotating()
			marketsTableView.reloadData()
		
		case .loadingFailed:
			loadInProgressView.isHidden = false
			activityIndicator.stopRotating()
		}
	}
}



// MARK: - UITableViewDataSource

extension SelectMarketViewController: UITableViewDataSource
{
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return config.helper.loadedMarkets.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		let cell = tableView.dequeueReusableCell(
			ofType: MarketTableCell.self
		)!

		let market = config.helper.loadedMarkets[indexPath.item]

		cell.configure(market: market)
		
		return cell
	}
}



// MARK: - UITableViewDelegate

extension SelectMarketViewController: UITableViewDelegate
{
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		tableView.deselectRow(at: indexPath, animated: true)
		
		let market = config.helper.loadedMarkets[indexPath.item]

		config.helper.selectMarket(market)
	}
}



// MARK: - FloatingPanelControllerDelegate

extension SelectMarketViewController: FloatingPanelControllerDelegate
{
}


// MARK: - UIGestureRecognizerDelegate

extension SelectMarketViewController: UIGestureRecognizerDelegate
{
	func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool
	{
		return true
	}
}
