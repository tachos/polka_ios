//
//  Colors.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 09.09.2021.
//

import UIKit


struct Colors
{
	static let darkMain = #colorLiteral(red: 0.1333333333, green: 0.1333333333, blue: 0.1333333333, alpha: 1)
	static let greyText = #colorLiteral(red: 0.5607843137, green: 0.5607843137, blue: 0.5607843137, alpha: 1)
	static let greyButton = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
	static let input = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
	
	static let green = #colorLiteral(red: 0.2705882353, green: 0.6980392157, blue: 0.4196078431, alpha: 1)
	
	static let pink = #colorLiteral(red: 0.9294117647, green: 0.2745098039, blue: 0.4352941176, alpha: 1)		// #ED466F
	
	static let mainOrange = #colorLiteral(red: 1, green: 0.4784313725, blue: 0, alpha: 1)
}
