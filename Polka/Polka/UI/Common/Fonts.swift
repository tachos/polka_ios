//
//  Fonts.swift
//  Polka
//
//  Created by Michael Goremykin on 21.04.2021.
//

import UIKit

struct Fonts
{
    struct JejuGothic
    {
        static func regular(_ size: CGFloat) -> UIFont
        {
            return UIFont(name:"JejuGothic",size:size)!
        }
    }
    
    struct ObjectSans
    {
        static func heavy(_ size: CGFloat) -> UIFont
        {
            return UIFont(name:"ObjectSans-Heavy",size:size)!
        }
        
        static func regular(_ size: CGFloat) -> UIFont
        {
            return UIFont(name:"ObjectSans-Regular",size:size)!
        }
    }
}
