//
//  StorkView.swift
//  Polka
//
//  Created by Michael Goremykin on 14.05.2021.
//

import UIKit

protocol BiDirectionalScrollable
{
	func scrollToTop()
	func scrollToBottom()
}

extension StorkView: BiDirectionalScrollable
{
	func scrollToTop()
	{
		tableView.setContentOffset(.zero, animated: true)
	}
	
	func scrollToBottom()
	{
		let indexPath = IndexPath(row: tableView.numberOfRows(inSection: 0)-1, section: 0)
		tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
	}
}

enum StorkDisplayState
{
	case collapsed(Bool)
	case expanded(CGFloat)
}

class StorkView: UIView
{
	// MARK:- Outlets
	weak var tableView: UITableView!
	
	// MARK:- Stateful
	var storkCollapsedStateToggled = false
	var lastContentOffset: CGFloat = 0
	
	// MARK:- Parameters
	var availableSpaceForStork		: CGSize!
	
	var headerViewForCollapsedStateWithHeight: (UIView, CGFloat)!
	var headerViewForUncollapsedStateWithHeight: (UIView,CGFloat)!
	var viewsToDisplayWithHeights: [(UIView, CGFloat)] = []
}

extension StorkView
{
	override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView?
	{
		let view = super.hitTest(point, with: event)
		return view !== tableView ? view : nil
	}
}

// MARK:- API
extension StorkView
{
	static func getAvailableStorkHeight(_ screenHeight: CGFloat,
									  _ navBarHeight: CGFloat,
									  _ tabBarHeight: CGFloat) -> CGFloat
	{
		return screenHeight - navBarHeight - tabBarHeight
	}
}

// MARK:- Initialization
extension StorkView
{
	func initialize(_ availableSpaceForStork: CGSize,
					_ headerViewForCollapsedState: (UIView, CGFloat),
					_ headerViewForUncollapsedState: (UIView, CGFloat),
					_ viewsToDisplayWithHeights: [(UIView, CGFloat)])
	{
		self.availableSpaceForStork 	= availableSpaceForStork
		self.headerViewForCollapsedStateWithHeight = headerViewForCollapsedState
		self.headerViewForUncollapsedStateWithHeight = headerViewForUncollapsedState
		self.viewsToDisplayWithHeights = viewsToDisplayWithHeights
	}
}

// MARK:- Instantiation
extension StorkView
{
	static func create(availableSpaceForStork: CGSize,
					   headerViewForCollapsedState: (UIView, CGFloat),
					   headerViewForUncollapsedState: (UIView, CGFloat),
					   viewsToDisplayWithHeights: [(UIView, CGFloat)]) -> StorkView
	{
		let view = StorkView()
		
		view.initialize(availableSpaceForStork,
						headerViewForCollapsedState,
						headerViewForUncollapsedState,
						viewsToDisplayWithHeights)
		
		view.setupUI()
		
		return view
	}
}
