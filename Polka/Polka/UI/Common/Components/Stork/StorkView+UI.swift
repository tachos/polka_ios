//
//  StorkView+UI.swift
//  Polka
//
//  Created by Michael Goremykin on 14.05.2021.
//

import UIKit

// MARK:- Setup UI
extension StorkView
{
	func setupUI()
	{
		self.tableView = createAndEmbedTableView(availableSpaceForStork,
												 headerViewForCollapsedStateWithHeight.1)
	}
}

// MARK:- UITableView
extension StorkView: UITableViewDelegate, UITableViewDataSource
{
	func scrollViewDidScroll(_ scrollView: UIScrollView)
	{
		if scrollView.contentOffset.y > 0 && !storkCollapsedStateToggled
		{
			UIView.animate(withDuration: 0.2)
			{
//				self.headerViewForUncollapsedStateWithHeight.0.alpha = 1
//				self.headerViewForCollapsedStateWithHeight.0.alpha = 0
				self.headerViewForUncollapsedStateWithHeight.0.isHidden = false
				self.headerViewForCollapsedStateWithHeight.0.isHidden = true
			}
			
			storkCollapsedStateToggled = true
		}
		else if scrollView.contentOffset.y == 0 && storkCollapsedStateToggled
		{
			UIView.animate(withDuration: 0.2)
			{
//				self.headerViewForUncollapsedStateWithHeight.0.alpha = 1
//				self.headerViewForCollapsedStateWithHeight.0.alpha = 0
				self.headerViewForUncollapsedStateWithHeight.0.isHidden = true
				self.headerViewForCollapsedStateWithHeight.0.isHidden = false
			}
			
			storkCollapsedStateToggled = false
		}

		lastContentOffset = scrollView.contentOffset.y
	}
	
	func createAndEmbedTableView(_ availableSpace: CGSize,
								 _ storkCollapsedViewHeight: CGFloat?) -> UITableView
	{
		let tv = createTableView(availableSpace, storkCollapsedViewHeight)
		
		embedTableView(tv)
		
		return tv
	}
	
	func embedTableView(_ tableView: UITableView)
	{
		self.addSubview(tableView)
		
		tableView.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [tableView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
						   tableView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
						   tableView.topAnchor.constraint(equalTo: self.topAnchor),
						   tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor)]
		
		NSLayoutConstraint.activate(constraints)
	}
	
	func createTableView(_ availableSpace: CGSize, _ storkCollapsedViewHeight: CGFloat?) -> UITableView
	{
		let tv = UITableView()
		
		tv.delegate		= self
		tv.dataSource	= self
		
		tv.backgroundColor = .clear
		tv.bounces = false
		
		tv.separatorStyle 				= .none
		tv.showsVerticalScrollIndicator = false
		
		tv.backgroundColor = .clear
		
		tv.tableHeaderView = createTransparentHeaderView(CGSize(width: availableSpace.width,
																height: availableSpace.height - (storkCollapsedViewHeight ?? 0)))
		
		tv.registerNibCell(StorkViewContainerTableViewCell.self)
		
		return tv
	}
	
	func createTransparentHeaderView(_ size: CGSize) -> UIView
	{
		let view = UIView(frame: CGRect(origin: .zero, size: size))
		
		view.isUserInteractionEnabled = false
		
		view.backgroundColor = .clear
		
		return view
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return viewsToDisplayWithHeights.count + 1
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
	{
		return indexPath.row == 0 ? headerViewForCollapsedStateWithHeight.1 : viewsToDisplayWithHeights[indexPath.row - 1].1
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		guard let cell = tableView.dequeueReusableCell(ofType: StorkViewContainerTableViewCell.self) else
		{
			return UITableViewCell()
		}
		
		cell.selectionStyle = .none
		
		cell.intialize(getContentToEmbed(at: indexPath))
		
		return cell
	}
	
	func getContentToEmbed(at indexPath: IndexPath) -> StorkViewContainerContentType
	{
		if indexPath.row == 0
		{
			return .multiple(.init(first: headerViewForCollapsedStateWithHeight.0,
								   second: headerViewForUncollapsedStateWithHeight.0))
		}
		else
		{
			return .single(.init(view: viewsToDisplayWithHeights[indexPath.row - 1].0))
		}
	}
	
	func getCellViewToEmbed(at indexPath: IndexPath) -> UIView
	{
		if indexPath.row == 0
		{
			return headerViewForCollapsedStateWithHeight.0
		}
		else
		{
			return viewsToDisplayWithHeights[indexPath.row - 1].0
		}
	}
}
