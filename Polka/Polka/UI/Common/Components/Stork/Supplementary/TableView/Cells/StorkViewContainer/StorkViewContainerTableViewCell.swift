//
//  StorkViewContainerTableViewCell.swift
//  Polka
//
//  Created by Michael Goremykin on 26.05.2021.
//

import UIKit

enum StorkViewContainerContentType
{
	case single(WeakView)
	case multiple(SwitchableViews)
}

extension StorkViewContainerContentType
{
	func getContents() -> [UIView?]
	{
		switch self
		{
		case .single(let view):
			return [view.view]
			
		case .multiple(let switchableViews):
			return [switchableViews.first, switchableViews.second]
		}
	}
}

struct WeakView
{
	weak var view: UIView?
}

struct SwitchableViews
{
	weak var first: UIView?
	weak var second: UIView?
}

class  StorkViewContainerTableViewCell: UITableViewCell
{
	var storkViewContainerContentType: StorkViewContainerContentType? = nil
}

extension StorkViewContainerTableViewCell
{
	func intialize(_ contentType: StorkViewContainerContentType)
	{
		removePreviousContent()
		
		embedContent(contentType)
		
		self.storkViewContainerContentType = contentType
	}
}

// MARK:- Utils
extension StorkViewContainerTableViewCell
{
	func embedContent(_ contentType: StorkViewContainerContentType)
	{
		switch contentType
		{
		case .single(let viewToEmbed):
			embedSingleView(viewToEmbed.view)
		
		case .multiple(let switchableViews):
			embedMultipleViews(first: switchableViews.first, second: switchableViews.second)
		}
	}
	
	func embedSingleView(_ viewToEmbed: UIView?)
	{
		if let viewToEmbed = viewToEmbed
		{
			stickToSuperView(viewToEmbed)
		}
	}
	
	func embedMultipleViews(first: UIView?, second: UIView?)
	{
		if let first = first, let second = second
		{
			stickToSuperView(first)
			stickToSuperView(second)
			
			//second.alpha = 0
			second.isHidden = true
		}
	}
	
	func removePreviousContent()
	{
		if storkViewContainerContentType != nil
		{
			storkViewContainerContentType?.getContents().forEach{ $0?.removeFromSuperview() }
		}
	}
	
	func stickToSuperView(_ viewToEmbed: UIView)
	{
		self.addSubview(viewToEmbed)
		
		viewToEmbed.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [viewToEmbed.topAnchor.constraint(equalTo: self.topAnchor),
						   viewToEmbed.bottomAnchor.constraint(equalTo: self.bottomAnchor),
						   viewToEmbed.leadingAnchor.constraint(equalTo: self.leadingAnchor),
						   viewToEmbed.trailingAnchor.constraint(equalTo: self.trailingAnchor)]
		
		NSLayoutConstraint.activate(constraints)
	}
}
