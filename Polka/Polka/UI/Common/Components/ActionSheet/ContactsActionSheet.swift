//
//  ContactsActionSheet.swift
//  Polka
//
//  Created by Michael Goremykin on 19.05.2021.
//

import UIKit

enum ActionSheet
{
	static func showContacts(from vc: UIViewController, options: [(title: String, action: () -> Void)])
	{
		let actionSheet = UIAlertController(title: "По любым вопросам, звоните, пишите:",
											message: nil,
											preferredStyle: .actionSheet)
		
		options.forEach { option in actionSheet.addAction(UIAlertAction(title: option.title,
																		style: .default,
																		handler: { _ in option.action() })) }
		
		let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in }
		
		actionSheet.addAction(cancel)
		
		vc.present(actionSheet, animated: true, completion: nil)
	}
}
