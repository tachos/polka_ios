//
//  ActionSheet.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 25.06.2021.
//

import UIKit



extension UIViewController
{
	func showActionSheet(actions: [UIAlertAction])
	{
		let actionSheet = UIAlertController(
			title: nil,
			message: nil,
			preferredStyle: .actionSheet
		)

		actions.forEach { actionSheet.addAction($0) }

		actionSheet.addAction(
			.init(title: "Cancel", style: .cancel)
		)

		self.present(actionSheet, animated: true, completion: nil)
	}
}
