//
//  QuantityFormatter.swift
//  Polka
//
//  Created by Michael Goremykin on 04.06.2021.
//

import Foundation

protocol QuantityFormatable
{
	func cartItemQuantity(_ quantity: Float, _ measurementUnit: MeasurementUnit) -> String
}

//_ quantityFormatable: QuantityFormatable = QuantityFormatter.shared
class QuantityFormatter: QuantityFormatable
{
	static let shared = QuantityFormatter()
	
	func cartItemQuantity(_ quantity: Float, _ measurementUnit: MeasurementUnit) -> String {
		fatalError()
	}
	
	private static var cartQuantityFormatter:NumberFormatter = {
		let formatter = NumberFormatter()
		formatter.minimumFractionDigits = 0
		formatter.maximumFractionDigits = 2
		
		formatter.groupingSeparator = ""
		formatter.numberStyle = .decimal
		formatter.locale = .init(identifier: "ru_RU")
		
		return formatter
	}()
	
	private static var orderQuantityFormatter:NumberFormatter = {
		let formatter = NumberFormatter()
		formatter.minimumFractionDigits = 0
		formatter.maximumFractionDigits = 3
		
		formatter.groupingSeparator = ""
		formatter.numberStyle = .decimal
		formatter.locale = .init(identifier: "ru_RU")
		
		return formatter
	}()
}

// MARK:- Formats
extension QuantityFormatter
{
	// 1 шт
	// 0,5 кг
	// 0,01 л
	static func cartItemQuantity(_ quantity: Float, _ measurementUnit: MeasurementUnit) -> String
	{
		switch measurementUnit
		{
		case .unit:
			return "\(String(format: "%.0f", quantity.rounded(.towardZero))) \(measurementUnit.toString())"
			
		case .kg, .l:
			return "\(cartQuantityFormatter.string(from: NSNumber(value: quantity)) ?? "") \(measurementUnit.toString())"
		}
	}
	
	static func cartItemQuantityWithoutUnit(_ quantity: Float, _ measurementUnit: MeasurementUnit) -> String
	{
		switch measurementUnit
		{
		case .unit:
			return String(format: "%.0f", quantity.rounded(.towardZero))
			
		case .kg, .l:
			return "\(cartQuantityFormatter.string(from: NSNumber(value: quantity)) ?? "")"
		}
	}
	
	static func orderItemQuantity(_ quantity: Float, _ measurementUnit: MeasurementUnit) -> String
	{
		switch measurementUnit
		{
		case .unit:
			return String(format: "%.0f", quantity.rounded(.towardZero))
			
		case .kg, .l:
			return "\(orderQuantityFormatter.string(from: NSNumber(value: quantity)) ?? "")"
		}
	}
	
	static func forOrderQuantityConfirmation(_ rawQuantity: String) -> String
	{
		let splitted = rawQuantity.split(separator: ",")

		if splitted.count > 1
		{
			return [splitted[0], splitted[1].prefix(3)].joined(separator: ",")
		}
		else
		{
			return "\(NSString(string: rawQuantity).integerValue),"
		}
	}
}
