//
//  Formatter.swift
//  Polka
//
//  Created by Makson on 22.04.2021.
//

import UIKit

enum PriceFormatter
{
	static var priceFormatter:NumberFormatter = {
		let formatter = NumberFormatter()
		
		formatter.minimumFractionDigits = 2
		formatter.maximumFractionDigits = 2
		
		formatter.numberStyle = .decimal
		
		formatter.groupingSeparator = ""
		
		formatter.locale = .init(identifier: "ru_RU")
		
		return formatter
	}()
	
	static var priceSplitByThousandsFormatter:NumberFormatter = {
		let formatter = NumberFormatter()
		
		formatter.minimumFractionDigits = 2
		formatter.maximumFractionDigits = 2
		
		formatter.numberStyle = .decimal
		
		formatter.groupingSeparator = " "
		formatter.locale = .init(identifier: "ru_RU")
		
		return formatter
	}()
}

// MARK:- Formats
extension PriceFormatter
{
	// 1 000,00 ₽
	static func cartTotal(_ price: Float) -> String
	{
		return "\(priceSplitByThousandsFormatter.string(from: NSNumber(value: price)) ?? "") ₽"
	}
	
	// 1000,00₽
	static func orderItem(_ price: Float) -> String
	{
		return "\(priceFormatter.string(from: NSNumber(value: price)) ?? "")₽"
	}
	
	// 1000,00 ₽
	static func catalogItem(_ price: Float) -> String
	{
		return "\(priceFormatter.string(from: NSNumber(value: price)) ?? "") ₽"
	}
	
	// 1000,00 ₽ за кг  sellerScreen
	// 1000,00 ₽/кг     aboutProductScreen
	static func catalogItemWithUnits(price: Float,
									 measurementUnit : MeasurementUnit,
									 displayedOn: DisplayedOn) -> String
	{
		return "\(catalogItem(price))\(catalogItemSeparator(displayedOn))\(measurementUnit.toString())"
	}
	
	// 1000,00 ₽ · за 1 кг
	static func catalogItemWithUnits(price: Float, measurementUnit : MeasurementUnit) -> NSAttributedString
	{
		let result = NSMutableAttributedString()
		
		
		result.append(createAttributedString(Constants.CartItem.priceFont,
											 Constants.CartItem.priceTextColor,
											 catalogItem(price)))
		
		result.append(createAttributedString(Constants.CartItem.measurementUnitFont,
											 Constants.CartItem.measurementUnitTextColor,
											 " · за 1 \(measurementUnit.toString())"))
		
		return result
	}
}



func formatPrice(cartProduct: CartProduct, count: Int) -> String
{
	let quantity = cartProduct.getProductQuantity(stepCount: count)
	
	return PriceFormatter.catalogItem(
		FormatterPrice.kopeksToRoubles(cartProduct.product.price) * quantity
	)
}




// MARK:- Utils
extension PriceFormatter
{
	private static func createAttributedString(_ font: UIFont, _ textColor: UIColor, _ val: String) -> NSAttributedString
	{
		var multipleAttributes = [NSAttributedString.Key : Any]()
		
		multipleAttributes[NSAttributedString.Key.font] 			= font
		multipleAttributes[NSAttributedString.Key.foregroundColor]	= textColor
		
		return NSAttributedString(string: val, attributes: multipleAttributes)
	}
	
	private static func catalogItemSeparator(_ displayedOn: DisplayedOn) -> String
	{
		switch displayedOn
		{
		case .sellerScreen:
			return " за "
			
		case .aboutProductScreen:
			return "/"
		}
	}
}

enum DisplayedOn
{
	case sellerScreen
	case aboutProductScreen
}

// MARK:- Utils
extension PriceFormatter
{
	enum Constants
	{
		enum CartItem
		{
			static let priceTextColor: UIColor						= #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
			static let measurementUnitTextColor: UIColor			= #colorLiteral(red: 0.5607843137, green: 0.5607843137, blue: 0.5607843137, alpha: 1)
			static let priceFont 									= UIFont.systemFont(ofSize: 13, weight: .semibold)
			static let measurementUnitFont							= UIFont.systemFont(ofSize: 13, weight: .thin)
		}
	}
}

extension Float
{
	func roundToFourDecimal() -> Float
	{
		return (self * 1000.0).rounded()/1000.0
	}
	
	func roundToThreeDecimal() -> Float
	{
		return (self * 1000.0).rounded()/1000.0
	}
	
	func round(decimalCount: Int) -> Float
	{
		let base = pow(10.0, Float(decimalCount))
		
		return (self * base).rounded()/base
	}
}

















// MARK:- Legacy
struct FormatterPrice
{
	static func kopeksToRoubles(_ kopeks: Int) -> Float
	{
		return Float(kopeks)/100.0
	}
	static func RoublesToKopeks(_ kopeks: Float) -> Int
	{
		return Int(kopeks * 100)
	}
	
	static func toStringWithTwoDecimal(_ val: Float) -> String
	{
		return String(format: "%.2f", val)
	}
}
