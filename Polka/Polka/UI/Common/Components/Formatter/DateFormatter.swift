//
//  DateFormatter.swift
//  Polka
//
//  Created by Michael Goremykin on 11.06.2021.
//

import Foundation

enum DatesFormatter
{
	private static let orderDateFormatter: DateFormatter = {
		let formatter = DateFormatter()
		
		formatter.dateFormat = "dd MMMM yyyy"
		formatter.locale = .init(identifier: "ru_RU")
		
		return formatter
	}()
	
	private static let sellerOrderDateFormatter: DateFormatter = {
		let formatter = DateFormatter()
		
		formatter.dateFormat = "d MMM, HH:mm"
		formatter.locale = .init(identifier: "ru_RU")
		
		return formatter
	}()

	private static let dayMonthFormatter: DateFormatter = {
		let formatter = DateFormatter()

		formatter.dateFormat = "d MMMM"
		formatter.locale = .init(identifier: "ru_RU")

		return formatter
	}()

	private static let dayMonthYearFormatter: DateFormatter = {
		let formatter = DateFormatter()

		formatter.dateFormat = "d MMMM yyyy"
		formatter.locale = .init(identifier: "ru_RU")

		return formatter
	}()

	private static let messageFormatter: DateFormatter = {
		let formatter = DateFormatter()

		formatter.dateFormat = "HH:mm"
		formatter.locale = .init(identifier: "ru_RU")

		return formatter
	}()
	
	private static let dateResponseFormatter: DateFormatter = {
		let formatter = DateFormatter()
		
		formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"
		formatter.locale = .init(identifier: "en_US")
		
		return formatter
	}()
}

extension DatesFormatter
{
	// 14 июня 2021
	static func forOrder(_ date: Date) -> String
	{
		return orderDateFormatter.string(from: date)
	}
	
	// 14:11
	static func toSellerOrder(_ date: Date) -> String
	{
		return sellerOrderDateFormatter.string(from: date)
	}

	// Сегодня / Вчера / 1 июня / 1 июня 2020
	static func toMessageGroup(_ date: Date) -> String
	{
		if date.isToday
		{
			return "Сегодня"
		}

		if date.isYesterday
		{
			return "Вчера"
		}

		if date.isThisYear
		{
			return dayMonthFormatter.string(from: date)
		}

		return dayMonthYearFormatter.string(from: date)
	}

	// 16:00
	static func toMessage(_ date: Date) -> String
	{
		return messageFormatter.string(from: date)
	}
}

extension DatesFormatter
{
	static func fromResponse(_ rawDate: String?) -> Date
	{
		return dateResponseFormatter.date(from: rawDate ?? "") ?? Date()
	}

	static func toRequestString(_ date: Date) -> String
	{
		return dateResponseFormatter.string(from: date)
	}
}



// MARK: - Date

extension Date
{
	var startOfDay: Date
	{
		return Calendar.current.startOfDay(for: self)
	}

	var isToday: Bool
	{
		return Calendar.current.isDateInToday(self)
	}

	var isYesterday: Bool
	{
		return Calendar.current.isDateInYesterday(self)
	}

	var isThisYear: Bool
	{
		return Calendar.current.component(.year, from: self)
			== Calendar.current.component(.year, from: Date())
	}
}
