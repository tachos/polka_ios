//
//  CustomButton.swift
//  Polka
//
//  Created by Makson on 21.04.2021.
//

import UIKit
import Foundation


class CustomButton: UIButton
{
	@IBInspectable var cornerRadius:CGFloat = 18.0
	{
		didSet
		{
			self.setup()
		}
	}
	
	var btnFont: UIFont { .systemFont(ofSize: fontSize, weight: .regular) }
	
	@IBInspectable var style:String?
	{
		didSet
		{
			self.setup()
		}
	}
	
	@IBInspectable var fontSize: CGFloat = 20.0
	{
		didSet
		{
			self.setup()
		}
	}
	
    override init(frame: CGRect)
	{
		super.init(frame: frame)
		setup()
	}
	
	required init?(coder: NSCoder)
	{
		super.init(coder: coder)
		setup()
	}
	
	override func awakeFromNib()
	{
		super.awakeFromNib()
		setup()
	}
	
	func setup()
	{
		guard let style = style else { return }
		setupBackgroundImage(colorForImage: getColorBackground(name: style) , colorTitle: getTitleColor(name: style))
		self.layer.cornerRadius = cornerRadius
		
		self.titleLabel?.font = btnFont
		self.titleLabel?.textAlignment = .center
	}
    func getColorBackground(name:String) -> UIColor
    {
        switch name
        {
        case "orange":
            return UIColor(red: 255/255, green: 122/255, blue: 0/255, alpha: 1)
        case "green":
            return UIColor(red: 69/255, green: 178/255, blue: 107/255, alpha: 1)
        case "gray", "grayWithDarkText":
            return UIColor(red: 236/255, green: 236/255, blue: 236/255, alpha: 1)
		case "white":
			return UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        default:
            break
        }
        
        return UIColor()
    }
    
    func getTitleColor(name:String) -> UIColor
    {
        switch name
        {
        case "orange","green":
            return UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        case "gray":
            return UIColor(red: 143/255, green: 143/255, blue: 143/255, alpha: 1)
		case "white":
			return UIColor(red: 255/255, green: 122/255, blue: 0/255, alpha: 1)
		case "grayWithDarkText":
			return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        default:
            break
        }
        
        return UIColor()
    }
    
    func setupBackgroundImage(colorForImage:UIColor,colorTitle:UIColor)
    {
		self.backgroundColor = colorForImage
        setTitleColor(colorTitle, for: .normal)
    }
    
	
	func setEnabled(_ enable: Bool)
	{
		self.backgroundColor = enable ? Colors.mainOrange : Colors.greyButton
		
		let newTitleColor = enable ? UIColor.white : Colors.greyText
		self.setTitleColor(newTitleColor, for: [])
		
		self.isUserInteractionEnabled = enable
	}
}
