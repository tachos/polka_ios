//
//  BackgroundGallery.swift
//  Polka
//
//  Created by Makson on 22.04.2021.
//

import UIKit

class BackgroundGallery: UIView
{
    //MARK: - Outlet and variable
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var gradientView: GradientView!
	
    @IBOutlet weak var bottomPaddtingPageControll: NSLayoutConstraint!
    @IBOutlet weak var topPaddingGradientView: NSLayoutConstraint!
    var imageItem:[String] = []
	var imageUrls: [URL] = []
	private var isSellerOpen = true
}

extension BackgroundGallery
{
	private func initialize(
		imageUrls:[URL],
		isSellerOpen: Bool
	)
	{
		setupCollectionView()
		setupAppearance(
			imageUrls: imageUrls,
			isSellerOpen: isSellerOpen
		)
	}
	
	static func create(
		imageUrls: [URL],
		isSellerOpen: Bool
	) -> BackgroundGallery
	{
		let view = createView(BackgroundGallery.self)
			   
		view.initialize(
			imageUrls: imageUrls,
			isSellerOpen: isSellerOpen
		)
		
		return view
	}
	
	
	override func draw(_ rect: CGRect)
	{
		paddingBottomPageControll()
	}
}

extension BackgroundGallery
{
	func setupAppearance(
		imageUrls:[URL],
		isSellerOpen: Bool
	)
	{
		self.imageUrls = imageUrls
		self.isSellerOpen = isSellerOpen
		
		chekPageCount(count: imageUrls.count)
		setupGradient()
		paddingTopGradient()
		self.collectionView.reloadData()
	}
	
	func setupGradient()
	{
		gradientView.gradientDirection = .vertical(bottomColor: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1), topColor: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0))
	}
	func chekPageCount(count: Int)
	{
		if count == 1
		{
			self.pageController.isHidden = true
		}
		else
		{
			self.pageController.numberOfPages = count
		}
	}
}

extension BackgroundGallery
{
	func setupCollectionView()
	{
		collectionView.delegate   = self
		collectionView.dataSource = self
		
		collectionView.backgroundColor = .clear
		
	
		collectionView.showsHorizontalScrollIndicator = false
		collectionView.isPagingEnabled = true
		collectionView.register(UINib(nibName: "SellerCardInfoCollectionViewCell", bundle: nil),
								forCellWithReuseIdentifier: "SellerCardInfoCollectionViewCell")
		setNeedsLayout()
	}
	
	func paddingTopGradient()
	{
		//topPaddingGradientView.constant = self.frame.height * Constant.rate
	}
	
	func paddingBottomPageControll()
	{
		if self.frame.height > Constant.defaultHeightView
		{
			bottomPaddtingPageControll.constant = Constant.bottomPaddingPageControll
		}
	}
}

//MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension BackgroundGallery: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
	func scrollToClosest(_ scrollView: UIScrollView) -> IndexPath?
	{
		if collectionView.visibleCells.count > 0
		{
			var closestCell : UICollectionViewCell = collectionView.visibleCells[0]
			   
			for cell in collectionView!.visibleCells as [UICollectionViewCell]
			{
				let closestCellDelta = abs(closestCell.center.x - collectionView.bounds.size.width/2.0 - collectionView.contentOffset.x)
				let cellDelta = abs(cell.center.x - collectionView.bounds.size.width/2.0 - collectionView.contentOffset.x)
				if (cellDelta < closestCellDelta)
				{
					closestCell = cell
				}
			}
			
			let indexPath = collectionView.indexPath(for: closestCell)
		
			return indexPath
		}
		
		return nil
	}
	
	func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
	{
		let indexPath = scrollToClosest(scrollView)
		
		pageController.currentPage = indexPath?.item ?? 0
	}
	
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
	{
		let indexPath  = scrollToClosest(scrollView)
		
		pageController.currentPage = indexPath?.item ?? 0
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
	{
		
		return CGSize(width: self.frame.width, height: self.frame.height + Constant.padding)
	}
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		return imageUrls.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{
		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SellerCardInfoCollectionViewCell", for: indexPath) as! SellerCardInfoCollectionViewCell
		
		cell.initialize(
			imageUrl: imageUrls[indexPath.item],
			isSellerOpen: isSellerOpen
		)
		
		return cell
	}
	
	
}
extension BackgroundGallery
{
	struct Constant
	{
		static let padding:CGFloat = 10
		static let rate: CGFloat = 0.5
		static let bottomPaddingPageControll: CGFloat = 48
		static let defaultHeightView:CGFloat = 380
	}
}
