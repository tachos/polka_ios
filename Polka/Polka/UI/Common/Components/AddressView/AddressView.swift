//
//  AddressView.swift
//  Polka
//
//  Created by Makson on 11.05.2021.
//

import UIKit


enum AddressViewLogic
{
	case availableTextField
	case availableTapGesture
}


class AddressView: UIView
{
    //MARK: - Outlet and variable
    @IBOutlet var streetAndHouseLbl: UILabel!
    
    @IBOutlet var streetAndHouseField: UITextField!
    @IBOutlet var picture: UIImageView!
	
	private var onPress: VoidCallback!
}
extension AddressView
{
	func initialize(nameLbl: String,
					image: String?,
					text:String?,
					type:AddressViewLogic,
					onPress: VoidCallback?)
	{
		setupAppearance(nameLbl: nameLbl, image: image, text: text,type: type,onPress: onPress)
	}
	
	static func create(nameLbl: String,
					   image:String?,
					   text:String?,
					   type:AddressViewLogic,
					   onPress: VoidCallback?) -> AddressView
	{
		let view = createView(AddressView.self)
			   
		view.initialize(nameLbl: nameLbl,
						image: image,
						text: text,
						type: type,
						onPress: onPress)
		
		return view
	}
	
	func setupAppearance(nameLbl:String,
						 image:String?,
						 text:String?,
						 type:AddressViewLogic,
						 onPress: VoidCallback?)
	{
		setupStreetAndHouseLbl(text: nameLbl)
		setupPicture(image: image)
		self.onPress = onPress
		setupStreetandHouseField(text: text)
		updateLogic(type: type)
	}
	
}

extension AddressView
{
	func updateLogic(type:AddressViewLogic)
	{
		switch type
		{
		case .availableTapGesture:
			streetAndHouseField.isEnabled = false
			tapView()
		case .availableTextField:
			streetAndHouseField.isEnabled = true
		}
	}
	func tapView()
	{
		let tap = UITapGestureRecognizer(target: self, action: #selector(pressView))
		tap.cancelsTouchesInView = false
		self.addGestureRecognizer(tap)
	}
	
	@objc func pressView()
	{
		onPress?()
	}
	func setupStreetandHouseField(text:String?)
	{
		if text != ""
		{
			streetAndHouseField.text = text
		}
		else
		{
			streetAndHouseField.text = ""
		}
	}
	
	func setupStreetAndHouseLbl(text:String)
	{
		streetAndHouseLbl.text = text
	}
	func setupPicture(image: String?)
	{
		guard let image = image else
		{
			picture.isHidden = true
			return
		}
		
		picture.image = UIImage(named: image)
	}
}

// MARK:- Instantiation API
extension AddressView
{
	static func setInContainer(_ container: UIView,
							   _ nameLbl: String,
							   _ image:String?,
							   _ text:String?,
							   _ type:AddressViewLogic,
							   onPress:VoidCallback?) -> AddressView
	{
		let ret: AddressView = createView(AddressView.self)
		
		ret.initialize(nameLbl: nameLbl, image: image, text: text , type: type, onPress: onPress)
		
		container.addSubview(ret)
		
		ret.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [ret.topAnchor.constraint(equalTo: container.topAnchor),
						   ret.bottomAnchor.constraint(equalTo: container.bottomAnchor),
						   ret.leadingAnchor.constraint(equalTo: container.leadingAnchor),
						   ret.trailingAnchor.constraint(equalTo: container.trailingAnchor)]
		
		NSLayoutConstraint.activate(constraints)
		
		return ret
	}
}
