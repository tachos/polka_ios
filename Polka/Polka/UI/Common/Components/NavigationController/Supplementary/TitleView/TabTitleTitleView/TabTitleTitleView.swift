//
//  TabTitleTitleView.swift
//  Polka
//
//  Created by Michael Goremykin on 12.05.2021.
//

import UIKit

// MARK:- Instantiation
class TabTitleTitleView
{
	static func create(_ tabTitle: String) -> UILabel
	{
		let label = UILabel()
		
		label.font 			= UIFont.systemFont(ofSize: 20, weight: .light)
		label.textColor 	= #colorLiteral(red: 0.1333333333, green: 0.1333333333, blue: 0.1333333333, alpha: 1)
		label.textAlignment = .center
		label.numberOfLines = 0
		
		label.text = tabTitle
		
		return label
	}
}

// MARK:- Constants
extension TabTitleTitleView
{
	static let bottomSpacing = -CGFloat(14)
}
