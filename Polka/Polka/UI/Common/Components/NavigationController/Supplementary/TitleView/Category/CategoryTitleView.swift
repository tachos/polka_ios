//
//  CategoryTitleView.swift
//  Polka
//
//  Created by Michael Goremykin on 05.05.2021.
//

import UIKit

// MARK:- Instantiation
class CategoryTitleView
{
	static func create(_ categoryTitle: String) -> UILabel
	{
		let label = UILabel()
		
		label.font 			= Fonts.ObjectSans.heavy(32)
		label.textColor 	= #colorLiteral(red: 0.1333333333, green: 0.1333333333, blue: 0.1333333333, alpha: 1)
		label.textAlignment = .left
		label.numberOfLines = 1
		
		label.text = categoryTitle
		
		return label
	}
}

// MARK:- Constants
extension CategoryTitleView
{
	struct Constants
	{
		static let horizontalSpacing = CGFloat(16)
	}
}
