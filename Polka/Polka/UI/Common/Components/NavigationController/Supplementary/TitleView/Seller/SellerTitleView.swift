//
//  SellerTitleView.swift
//  Polka
//
//  Created by Michael Goremykin on 05.05.2021.
//

import SDWebImage

class SellerTitleView: UIView
{
	@IBOutlet weak var sellerProfileImageView: CustomImage!
	@IBOutlet weak var sellerNameLabel: UILabel!
	@IBOutlet weak var additionalInfoLabel: UILabel!

	private var handleInfoButton: VoidCallback?
}

// MARK:- Initialization
extension SellerTitleView
{
	func initialize(_ profileImageURL: URL?,
					_ sellerName: String,
					_ additionalInfo: String?,
					_ handleInfoButton: @escaping VoidCallback)
	{
		sellerProfileImageView.sd_setImage(
			with: profileImageURL,
			placeholderImage: UIImage(named: "smile")
		)
		
		sellerNameLabel.text			= sellerName
		additionalInfoLabel.text		= additionalInfo

		self.handleInfoButton = handleInfoButton
	}
}

// MARK:- Instantiation
extension SellerTitleView
{
	static func create(_ profileImageURL: URL?,
					   _ sellerName: String,
					   _ additionalInfo: String?,
					   _ handleInfoButton: @escaping VoidCallback) -> SellerTitleView
	{
		let sellerTitleView = createView(SellerTitleView.self)
		
		sellerTitleView.initialize(profileImageURL,
								   sellerName,
								   additionalInfo,
								   handleInfoButton)
		
		return sellerTitleView
	}
}

// MARK:- Handle UI
extension SellerTitleView
{
	@IBAction func onInfoButton()
	{
		handleInfoButton?()
	}
}

// MARK:- Constants
extension SellerTitleView
{
	struct Constants
	{
		static let leadingSpacing = CGFloat(20)
		static let size = CGSize(width: 214, height: 40)
	}
}
