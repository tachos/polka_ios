//
//  NavigationBarView.swift
//  Polka
//
//  Created by Michael Goremykin on 30.04.2021.
//

import UIKit
import TinyConstraints

class NavigationBarView: UIView
{
	// MARK:- Outlets
	var backButton: UIButton!
	var backButtonBackgroundView: UIView!
	var blurView: UIView!
	var titleView: UIView!

	private(set) var rightButton: UIButton?
	private(set) var rightButtonZeroWidth: NSLayoutConstraint?
	
	// MARK:- Stateful
	var onBackButtonCallback: (() -> Void)!
	var onRightButtonCallback: VoidCallback?
}

// MARK:- Initialization
extension NavigationBarView
{
	func initialize(_ size: CGSize, _ onBackButtonCallback: @escaping () -> Void)
	{
		self.onBackButtonCallback = onBackButtonCallback
		
		setupUI(size)
	}
}

// MARK:- Setup UI
extension NavigationBarView
{
	func setupUI(_ size: CGSize)
	{
		setupBlurView(size)
		setupBackButton()
		addRightButton()
	}
	
	func setupBackButton()
	{
		let buttonBackgroundView = UIView()
		buttonBackgroundView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.6)
		buttonBackgroundView.layer.cornerRadius = 0.5 * Constants.backButtonDimension
		buttonBackgroundView.clipsToBounds = true
		
		let button = UIButton()
		button.setImage(UIImage(named: "left-arrow"), for: [])
		button.imageView?.contentMode = .center
		button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0)
		button.addTarget(self, action: #selector(NavigationBarView.onBackButtonTapped), for: .touchUpInside)
		
		self.addSubview(buttonBackgroundView)
		self.addSubview(button)
		
		buttonBackgroundView.translatesAutoresizingMaskIntoConstraints = false
		button.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [buttonBackgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor,
														  constant: -Constants.backButtonBottomSpacing),
						   buttonBackgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
						   buttonBackgroundView.heightAnchor.constraint(equalToConstant: Constants.backButtonDimension),
						   buttonBackgroundView.widthAnchor.constraint(equalToConstant: Constants.backButtonDimension),
						   button.bottomAnchor.constraint(equalTo: self.bottomAnchor,
														  constant:0),
						   button.leadingAnchor.constraint(equalTo: self.leadingAnchor),
						   button.heightAnchor.constraint(equalToConstant: Constants.barHeight),
						   button.widthAnchor.constraint(equalToConstant: Constants.barHeight)]
		
		NSLayoutConstraint.activate(constraints)
		
		self.backButtonBackgroundView = buttonBackgroundView
		self.backButton = button
	}

	private func addRightButton()
	{
		let newRightButton = UIButton()
		newRightButton.contentMode = .right
		newRightButton.setTitleColor(Colors.greyText, for: .normal)
		newRightButton.setTitleColor(Colors.darkMain, for: .highlighted)
		newRightButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .regular)

		newRightButton.addTarget(
			self,
			action: #selector(NavigationBarView.onRightButtonTapped),
			for: .touchUpInside
		)
		self.addSubview(newRightButton)

		newRightButton.trailingToSuperview(offset: 16)
		newRightButton.centerY(to: backButton)

		self.rightButton = newRightButton

		self.rightButtonZeroWidth = newRightButton.width(0)

		newRightButton.isHidden = true
	}
	
	func setupBlurView(_ size: CGSize)
	{
		let blurEffect = UIBlurEffect(style: .extraLight)
		let blurView = UIVisualEffectView(effect: blurEffect)
		blurView.frame = CGRect(origin: .zero, size: size)

		self.addSubview(blurView)
		
		self.blurView = blurView
	}
}

// MARK:- UI handlers
extension NavigationBarView
{
	@objc func onBackButtonTapped()
	{
		onBackButtonCallback()
	}

	@objc func onRightButtonTapped()
	{
		onRightButtonCallback.warnIfNil()?()
	}
}

// MARK:- Instantiation
extension NavigationBarView
{
	static func create(_ size: CGSize, _ onBackButtonCallBack: @escaping () -> Void) -> NavigationBarView
	{
		let navBarView = NavigationBarView()
		
		navBarView.initialize(size, onBackButtonCallBack)
		
		return navBarView
	}
}

// MARK:- Constnats
extension NavigationBarView
{
	struct Constants
	{
		static let barHeight 				= CGFloat(60)
		static let backButtonDimension		= CGFloat(32)
		static let backButtonBottomSpacing	= CGFloat(14)
	}
}
