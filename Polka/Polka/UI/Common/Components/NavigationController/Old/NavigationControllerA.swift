//
//  NavigationController.swift
//  Polka
//
//  Created by Makson on 19.04.2021.
//

import Foundation
import UIKit

class NavigationControllerA: UINavigationController
{
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        guard let topViewController = topViewController
        else
        {
            return super.preferredStatusBarStyle
        }
        
        return topViewController.preferredStatusBarStyle
    }
}

extension NavigationControllerA
{
    func setupTransparentNavigationBar(
        titleColor: UIColor = .black,
        barTintColor: UIColor = .black,
        animated: Bool
        )
    {
        setNavigationBarHidden(false, animated: animated)

        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()

        navigationBar.titleTextAttributes = [.foregroundColor: titleColor]
        navigationBar.tintColor = barTintColor
        navigationBar.barTintColor = barTintColor

        navigationBar.backgroundColor = .clear
        navigationBar.isTranslucent = true
    }
}
extension NavigationControllerA
{
    static func transparent(
        rootViewController: UIViewController? = nil
    ) -> NavigationControllerA
    {
        let navigationController = NavigationControllerA()

        navigationController.setupTransparentNavigationBar(
            animated: false
        )
        
        navigationController.modalPresentationStyle = .fullScreen

        if let rootViewController = rootViewController
        {
            navigationController.pushViewController(
                rootViewController,
                animated: false
            )
        }

        return navigationController
    }
    
    static func withHiddenBar(
        rootViewController: UIViewController? = nil
    ) -> NavigationControllerA
    {
        let navigationController = NavigationControllerA()
        
        navigationController.modalPresentationStyle = .fullScreen

        navigationController.setNavigationBarHidden(
            true,
            animated: false
        )

        if let rootViewController = rootViewController
        {
            navigationController.pushViewController(
                rootViewController,
                animated: false
            )
        }

        return navigationController
    }
}
