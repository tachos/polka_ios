//
//  NavigationController.swift
//  Polka
//
//  Created by Michael Goremykin on 30.04.2021.
//

import UIKit
import RxSwift
import TinyConstraints

enum NavBarBackground
{
	case clear
	case blurred
}

enum TitleViewType
{
	case category(String)
	case tabTitleHeader(String)
	case sellerInfo(URL?, String, String, VoidCallback)
}

struct ViewControllerVisibilityChangedParameters
{
	let navigationController: UINavigationController
	let viewController: UIViewController
	let animated: Bool
}


protocol NavBarCustomizable
{
	func setAppearance(navBarBackground: NavBarBackground,
					  hasBackButton: Bool,
					  titleViewType: TitleViewType?,
					  initiallyNotVisible: Bool)
	
	func toggleVisibility()

	func setRightButtonTitle(_ title: String?)
}

protocol Navigationable
{
	func adjustSafeAreaIfNeeded(_ navBarHeight: CGFloat)
	
	var navBarAlpha: CGFloat { get }
}

class NavigationController: UINavigationController
{
	// MARK:- Outlets
	weak var navBarView: NavigationBarView!
	
	var navBarHeight: CGFloat
	{
		NavigationBarView.Constants.barHeight
	}

	// MARK:- ReactiveX
	private let willShowViewControllerSubject = PublishSubject<ViewControllerVisibilityChangedParameters>()
	private let didShowViewControllerSubject = PublishSubject<ViewControllerVisibilityChangedParameters>()
}

extension NavigationController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		interactivePopGestureRecognizer?.delegate = self
		
		interactivePopGestureRecognizer?.addTarget(self, action: #selector(NavigationController.handlePopGesture))
	}
}

extension NavigationController
{
	@objc func handlePopGesture()
	{
		if let vc =  self.topViewController as? Navigationable,
		   vc.navBarAlpha > 0,
		   let percentComplete = self.transitionCoordinator?.percentComplete
		{
			self.navBarView.alpha = percentComplete > 0.3 ? 1 : percentComplete
		}
	}
}

extension NavigationController: UIGestureRecognizerDelegate
{
	func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
		return viewControllers.count > 1
	}
}

// MARK: ReactiveX / Observables
extension NavigationController
{
	var willShowViewControllerObservable: Observable<ViewControllerVisibilityChangedParameters>
	{
		return willShowViewControllerSubject
	}

	var didShowViewControllerObservable: Observable<ViewControllerVisibilityChangedParameters>
	{
		return didShowViewControllerSubject
	}
}

// MARK:- API
extension NavigationController: NavBarCustomizable
{
	func setNavBarVisibleAnimated(_ showNavBar: Bool)
	{
		UIView.animate(withDuration: 0.5)
		{
			self.navBarView.alpha = showNavBar ? 1 : 0
		}
	}

	func toggleVisibility()
	{
		UIView.animate(withDuration: 0.5)
		{
			self.navBarView.alpha = self.navBarView.alpha == 1.0 ? 0 : 1
		}
	}
	
	func setAppearance(navBarBackground: NavBarBackground,
					  hasBackButton: Bool,
					  titleViewType: TitleViewType?,
					  initiallyNotVisible: Bool)
	{
		setNavBarBackground(navBarBackground)
		setNavBarBackButton(hasBackButton)
		setTitleView(titleViewType)
		
		navBarView.alpha = initiallyNotVisible ? 0 : 1
	}

	func setRightButtonTitle(_ title: String?)
	{
		navBarView.rightButton?.setTitle(title, for: .normal)

		navBarView.rightButton?.isHidden = title == nil

		// make space for the title
		navBarView.rightButtonZeroWidth?.isActive = title == nil
	}
	
	func setNavBarBackground(_ navBarBackground: NavBarBackground)
	{
		navBarView.backgroundColor = .clear
		
		switch navBarBackground
		{
		case .clear:
			navBarView.blurView.isHidden = true
			
		case .blurred:
			navBarView.blurView.isHidden = false
		}
	}
	
	func setNavBarBackButton(_ hasBackButton: Bool)
	{
		self.navBarView.backButtonBackgroundView.isHidden = !hasBackButton
		self.navBarView.backButton.isHidden = !hasBackButton
	}
	
	func setTitleView(_ titleViewType: TitleViewType?)
	{
		self.navBarView.titleView?.removeFromSuperview()
		
		if let titleViewType = titleViewType
		{
			switch titleViewType
			{
			case .category(let title):
				setupAndEmbedCategoryTitleView(title)
				
			case .sellerInfo(let url, let title, let info, let handler):
				setupAndEmbedSellerTitleView(url, title, info, handler)
				
			case .tabTitleHeader(let title):
				setupAndEmbedTabTitleView(title)
			}
		}
	}
	
	func setupAndEmbedCategoryTitleView(_ title: String)
	{
		let categoryLabel = CategoryTitleView.create(title)
		
		self.navBarView.addSubview(categoryLabel)
		
		categoryLabel.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [
			categoryLabel.bottomAnchor.constraint(equalTo: self.navBarView.bottomAnchor, constant: -11),
			categoryLabel.leadingAnchor.constraint(equalTo: self.navBarView.backButton.trailingAnchor,
												   constant: 0),
			categoryLabel.trailingAnchor.constraint(
				lessThanOrEqualTo: self.navBarView.trailingAnchor,
				constant: -CategoryTitleView.Constants.horizontalSpacing
			)
		]

		if let rightButton = navBarView.rightButton {
			categoryLabel.trailingToLeading(of: rightButton, relation: .equalOrLess)
		}

		categoryLabel.sizeToFit()
		
		NSLayoutConstraint.activate(constraints)
		
		self.navBarView.setNeedsLayout()
		
		navBarView.titleView = categoryLabel
	}
	
	func setupAndEmbedTabTitleView(_ title: String)
	{
		let tabTitleView = TabTitleTitleView.create(title)

		self.navBarView.addSubview(tabTitleView)
		
		tabTitleView.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [tabTitleView.bottomAnchor.constraint(equalTo: self.navBarView.bottomAnchor,
																constant: TabTitleTitleView.bottomSpacing),
						   tabTitleView.centerXAnchor.constraint(equalTo: self.navBarView.centerXAnchor)]
		
		tabTitleView.sizeToFit()
		
		NSLayoutConstraint.activate(constraints)
		
		self.navBarView.setNeedsLayout()
		
		navBarView.titleView = tabTitleView
	}
	
	func setupAndEmbedSellerTitleView(_ profileImageUrl: URL?, _ title: String, _ info: String, _ handleInfoButton: @escaping VoidCallback)
	{
		let sellerTitleView = SellerTitleView.create(profileImageUrl,
											   title,
											   info,
											   handleInfoButton)
		
		self.navBarView.addSubview(sellerTitleView)
		
		sellerTitleView.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [sellerTitleView.centerYAnchor.constraint(equalTo: self.navBarView.backButton.centerYAnchor),
						   sellerTitleView.leadingAnchor.constraint(equalTo: self.navBarView.backButton.trailingAnchor,
																	constant: SellerTitleView.Constants.leadingSpacing),
						   sellerTitleView.heightAnchor.constraint(equalToConstant: SellerTitleView.Constants.size.height),
						   sellerTitleView.widthAnchor.constraint(equalToConstant: SellerTitleView.Constants.size.width)]
		
		
		NSLayoutConstraint.activate(constraints)
		
		navBarView.titleView = sellerTitleView
	}
}

// MARK:- NavBarCustomizable
extension NavigationController
{
	
}

// MARK:- Initalization
extension NavigationController
{
	func initialize(_ contentControllers: [UIViewController])
	{
		self.delegate = self
		
		self.viewControllers = contentControllers
		
		setupUI()
	}
}

// MARK:- Setup UI
extension NavigationController: UINavigationControllerDelegate
{
	func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool)
	{
		(viewController as? Navigationable)?.adjustSafeAreaIfNeeded(NavigationBarView.Constants.barHeight)

		willShowViewControllerSubject.onNext(
			.init(
				navigationController: self,
				viewController: viewController,
				animated: animated
			)
		)
	}
	func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool)
	{
		didShowViewControllerSubject.onNext(
			.init(
				navigationController: self,
				viewController: viewController,
				animated: animated
			)
		)
	}
}

// MARK:- Setup UI
extension NavigationController
{
	func setupUI()
	{
		setNavigationBarHidden(true, animated: false)
		
		createAndEmbedNavbar()
	}
	
	func createAndEmbedNavbar()
	{
		let navBarSize = CGSize(width: view.frame.width,
								height: navBarHeight + (getStatusBarHeight() ?? 0))
		
		let navBarView = NavigationBarView.create(navBarSize,
												  {[weak self]() in
													self?.popViewController(animated: false)
												  })
		
		let containerVC = self as UIViewController
		
		containerVC.view.addSubview(navBarView)
		
		navBarView.translatesAutoresizingMaskIntoConstraints = false
		
		let constraints = [
			navBarView.topAnchor.constraint(equalTo: containerVC.view.topAnchor),
			navBarView.leadingAnchor.constraint(equalTo: containerVC.view.leadingAnchor),
			navBarView.trailingAnchor.constraint(equalTo: containerVC.view.trailingAnchor),
			navBarView.heightAnchor.constraint(equalToConstant: navBarSize.height)
		]
		
		NSLayoutConstraint.activate(constraints)
		
		self.navBarView = navBarView
	}
}


// MARK:- Instatiation
extension NavigationController
{
	static func create(_ contentControllers: [UIViewController]? = nil) -> NavigationController
	{
		let nc = NavigationController()
		
		nc.initialize(contentControllers ?? [])

		return nc
	}
	
	@objc func onTapped()
	{
		let vc = UIViewController()
		
		vc.view.backgroundColor = .red
		
		self.pushViewController(vc, animated: false)
	}
}

// MARK:- UIViewController utils for NavigationController
extension UIViewController
{
	func getNavigationController() -> NavigationController?
	{
		return  (self.navigationController as? NavigationController)
	}
	
	func getStatusBarManager() -> UIStatusBarManager?
	{
		return UIApplication.shared.windows.first?.windowScene?.statusBarManager
	}
	
	func getStatusBarHeight() -> CGFloat?
	{
		return UIApplication.shared.windows.first?.windowScene?.statusBarManager?.statusBarFrame.height
	}
	
	func getNavBarHeight() -> CGFloat?
	{
		return (self.navigationController as? NavigationController)?.navBarHeight
	}
	
	func getTopSafeAnchor() -> NSLayoutYAxisAnchor
	{
		if #available(iOS 11.0, *)
		{
			return self.view.safeAreaLayoutGuide.topAnchor
		}
		
		return self.view.topAnchor
	}
}
