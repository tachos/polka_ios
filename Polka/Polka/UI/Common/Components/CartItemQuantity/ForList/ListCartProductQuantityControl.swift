//
//  SingleButtonCartItemQuantityControlView.swift
//  Polka
//
//  Created by Michael Goremykin on 02.06.2021.
//

import UIKit


//SVD: REFACTOR: must be Int, right?
typealias OnProductQuantityChanged = (Float) -> Void


extension ListCartProductQuantityControl
{
	enum UserState
	{
		case authorised(stepperStyle: StepperStyle,
						cartProduct: CartProduct,
						onQuantityChanged: OnProductQuantityChanged,
						onSetToZeroAdditionalAction: (() -> Void)? = nil)
		
		case notAuthorised(product: Product, onAddToCartTapped: () -> Void)
		
		case sellerIsResting
	}
}

class ListCartProductQuantityControl: UIView
{
    // MARK:- Outlets
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var addToCartButton: CustomButton!
    @IBOutlet weak var stepperView: StepperView!
	
	// MARK:- Stateful
	var onAddToCartButtonTapped: (() -> Void)!
    
    // MARK:- Parameters
    var cartProduct: CartProduct!
	var onQuantityChangedCallback: ((Float) -> Void)!
	var onSetToZeroAdditionalAction: (() -> Void)?
    
    // MARK:- Initialization
    required init?(coder aDecoder: NSCoder)
	{
        super.init(coder: aDecoder)

        guard let view = loadViewFromNib() else { return }
		
        view.frame = self.bounds
		view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		
        self.addSubview(view)
		
        contentView = view
    }

    func loadViewFromNib() -> UIView?
	{
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: ListCartProductQuantityControl.self),
						bundle: bundle)
		
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
}

// MARK:- Initialize
extension ListCartProductQuantityControl
{
	func setup(forUserState userState: UserState)
	{
		switch userState
		{
		case .authorised(let stepperStyle, let  cartProduct, let onQuantityChanged, let onSetToZeroAdditionalAction):
			setupForAuthorisedUser(stepperStyle, cartProduct, onQuantityChanged, onSetToZeroAdditionalAction)
			
		case .notAuthorised(let product, let onAddToCartTapped):
			setupForNotAuthorisedUser(product, onAddToCartTapped)
			
		case .sellerIsResting:
			setupUiForSellerIsResting()
		}
	}
	
	private func setupForNotAuthorisedUser(_ product: Product, _ onAddToCartButtonTapped: @escaping () -> Void)
	{
		setupAddToCartButtonForNotAuthorisedUser(product, onAddToCartButtonTapped)
	}
	
	private func setupAddToCartButtonForNotAuthorisedUser(_ product: Product, _ onAddToCartButtonTapped: @escaping () -> Void)
	{
		stepperView.isHidden = true
		addToCartButton.isHidden = false
		addToCartButton.isEnabled = true
		addToCartButton.setTitle(PriceFormatter.catalogItem(FormatterPrice.kopeksToRoubles(product.price)), for: [])
		
		self.onAddToCartButtonTapped = onAddToCartButtonTapped
	}
	
	private func setupForAuthorisedUser(_ stepperStyle: StepperStyle,
										_ cartProduct: CartProduct,
										_ onQuantityChanged: @escaping (Float) -> Void,
										_ onSetToZeroAdditionalAction: (() -> Void)? = nil)
	{
		
		self.cartProduct = cartProduct
		self.onQuantityChangedCallback = onQuantityChanged
		self.onSetToZeroAdditionalAction = onSetToZeroAdditionalAction
		self.onAddToCartButtonTapped = self.onAddToCartLocalTapped
		
		let currentStepCount = cartProduct.toStepperCount()
		
		if currentStepCount > 0
		{
			setupStepper(stepperStyle, currentStepCount)
		}
		else
		{
			setupAddToCartButton()
		}
	}
	
	private func setupUiForSellerIsResting()
	{
		stepperView.isHidden = true

		addToCartButton.isHidden = false
		addToCartButton.setTitle("Продавец отдыхает", for: [])
		addToCartButton.isEnabled = false
		addToCartButton.alpha = Constants.transparency
	}
}

// MARK:- UI handlers
extension ListCartProductQuantityControl
{
	@IBAction func onAddToCartTapped(_ sender: Any)
	{
		self.onAddToCartButtonTapped()
	}
}

// MARK:- Utils
extension ListCartProductQuantityControl
{
	private func onAddToCartLocalTapped()
	{
		onQuantityChangedCallback(cartProduct.product.minCountProductInCart)

		addToCartButton.isHidden = true
		stepperView.isHidden = false

		setupStepper(StepperStyle.orange, 1)
	}
	
	private func setupAddToCartButton()
	{
		let settings = getAddToCartButtonSettings()
		addToCartButton.isHidden = false
		stepperView.isHidden = true
		addToCartButton.setTitle(settings.title, for: [])
		addToCartButton.isEnabled = settings.isEnabled
		addToCartButton.alpha = addToCartButton.isEnabled ? 1 : Constants.transparency
	}
	
	private func getAddToCartButtonSettings() -> (title: String, isEnabled: Bool)
	{
		if cartProduct.stockAvailability.isAvailable()
		{
			return (PriceFormatter.catalogItem(FormatterPrice.kopeksToRoubles(cartProduct.product.price)), true)
		}
		else
		{
			return ("Разобрали", false)
		}
	}
	
	private func setupStepper(_ stepperStyle: StepperStyle, _ currentStepCount: Int)
	{
		stepperView.setup(
			stepperStyle,
			currentStepCount,
			cartProduct.toAvailableStepCount(),
			weightFormatter: {[weak self] (val: Int) -> String in self?.stepperLabelFormatter(val) ?? "" },
			priceFormatter: { [unowned self] in
				//
				formatPrice(cartProduct: self.cartProduct, count: $0)
			},
			{[weak self] (val: Int) in self?.onQuantityChanged(val) },
			getAdditionalAction()
		)
		
		stepperView.isHidden = false
		addToCartButton.isHidden = true
	}
	
	private func getAdditionalAction() -> () -> Void
	{
		if let onSetToZeroAdditionalAction = onSetToZeroAdditionalAction
		{
			return onSetToZeroAdditionalAction
		}
		else
		{
			return {[weak self] in self?.showAddToCartButton() }
		}
	}
	
	private func onQuantityChanged(_ val: Int)
	{
		val == 0
			? self.onQuantityChangedCallback(0)
			: self.onQuantityChangedCallback(cartProduct.getProductQuantity(stepCount: val))
	}
	
	private func stepperLabelFormatter(_ stepCount: Int) -> String
	{
		return QuantityFormatter.cartItemQuantity(cartProduct.getProductQuantity(stepCount: stepCount),
												  cartProduct.product.unit)
	}
	
	private func showAddToCartButton()
	{
		setupAddToCartButton()
		
		addToCartButton.isHidden = false
		stepperView.isHidden = true
	}
}

// MARK:- Constants
extension ListCartProductQuantityControl
{
	enum Constants
	{
		static let size = CGSize(width: 112, height: 32)
		static let transparency = CGFloat(0.7)
	}
}


// MARK:- Cart utils for quantity control
extension CartProduct
{
	func toStepperCount() -> Int
	{
		if self.quantity >= self.product.minCountProductInCart
		{
			return getStepperCount(quantity)
		}
		else
		{
			return 0
		}
	}
	
	func toAvailableStepCount() -> Int
	{
		switch stockAvailability
		{
		case .available(let stockQuantity):
			return getStepCountForStockAvailableState(stockQuantity)

		case .notAvailable:
			return toStepperCount() == 0 ? 0 : toStepperCount() - 1
		}
	}

	private func getStepCountForStockAvailableState(_ stockQuantity: Float) -> Int
	{
		let availableStepCount = getStepperCount(stockQuantity)
		
		if stockQuantity == 0 || availableStepCount == 0
		{
			return 0
		}
		else
		{
			return availableStepCount
		}
	}

	// NOTE: this function must be the inverse of CartProduct.getProductQuantity(stepCount: Int)!
	private func getStepperCount(_ quantity: Float) -> Int
	{
		if quantity < product.minCountProductInCart {
			return 0
		}

		let remainder: Float = quantity - product.minCountProductInCart
		let numStepsRaw: Float = remainder / product.stepChangeInCart
		let numStepsClamped: Int = Int(numStepsRaw.rounded(.up)) + 1
		return numStepsClamped
	}
}



extension ListCartProductQuantityControl
{
	func setup(
		cartProduct: CartProduct,
		presentPopupIfUnauthorized: @escaping VoidCallback,
		onProductQuantityChangedIfAuthorized: @escaping OnProductQuantityChanged
	)
	{
		self.setup(
			forUserState: getQuantityControlState(
				cartProduct: cartProduct,
				presentPopupIfUnauthorized: presentPopupIfUnauthorized,
				onProductQuantityChangedIfAuthorized: onProductQuantityChangedIfAuthorized
			)
		)
	}
}



private func getQuantityControlState(
	cartProduct: CartProduct,
	presentPopupIfUnauthorized: @escaping VoidCallback,
	onProductQuantityChangedIfAuthorized: @escaping OnProductQuantityChanged
) -> ListCartProductQuantityControl.UserState
{
	if managers.webService.currentUserProfile == nil
	{
		return .notAuthorised(
			product: cartProduct.product,
			onAddToCartTapped: presentPopupIfUnauthorized
		)
	}
	else
	{
		return .authorised(
			stepperStyle: .orange,
			cartProduct: cartProduct,
			onQuantityChanged: onProductQuantityChangedIfAuthorized
		)
	}
}
