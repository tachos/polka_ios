//
//  StepperWithGoToCartButtonCartItemQuantityControlView.swift
//  Polka
//
//  Created by Michael Goremykin on 02.06.2021.
//

import UIKit

class ProductScreenCartProductQuantityControl: UIView
{
	// MARK:- Outlets
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var goToCartButton: CustomButton!
    @IBOutlet weak var stepperContainer: UIView!
    @IBOutlet weak var stepperView: StepperView!
    @IBOutlet weak var addToCartButton: CustomButton!
    
	// MARK:- Parameters
	var cartProduct: CartProduct!
	var onQuantityChangedCallback: ((Float) -> Void)!
    var onGoToCartCallback: (() -> Void)!
	var onSetToZeroAdditionalActionCallback: (() -> Void)?
    
	required init?(coder aDecoder: NSCoder)
	{
		super.init(coder: aDecoder)

		guard let view = loadViewFromNib() else { return }
		
		view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		
		self.addSubview(view)
		
		contentView = view
	}

	func loadViewFromNib() -> UIView?
	{
		let bundle = Bundle(for: type(of: self))
		let nib = UINib(nibName: String(describing: ProductScreenCartProductQuantityControl.self),
						bundle: bundle)
		
		return nib.instantiate(withOwner: self, options: nil).first as? UIView
	}
}

// MARK:- UI handlers
extension ProductScreenCartProductQuantityControl
{
    @IBAction func onGoToCartTapped(_ sender: Any)
    {
        onGoToCartCallback?()
    }
    
    @IBAction func onAddToCartTapped(_ sender: Any)
    {
        self.onQuantityChangedCallback(cartProduct.product.minCountProductInCart)
    }
}

// MARK:- Setup
extension ProductScreenCartProductQuantityControl
{
    func setup(editingEnabled: Bool,
			   cartProduct: CartProduct,
			   onQuantityChanged: @escaping (Float) -> Void,
               onGoToCart: @escaping () -> Void,
			   onSetToZeroAdditionalAction: (() -> Void)? = nil)
    {
		self.cartProduct = cartProduct
		self.onQuantityChangedCallback = onQuantityChanged
		self.onSetToZeroAdditionalActionCallback = onSetToZeroAdditionalAction
        self.onGoToCartCallback = onGoToCart
        
        setupAddToCartButton()
        
        setupAppearance(editingEnabled)
    }
    
    func setupAddToCartButton()
    {
        addToCartButton.setTitle(PriceFormatter.catalogItem(FormatterPrice.kopeksToRoubles(cartProduct.product.price)),
                                 for: [])
    }
}

// MARK:- Utils
extension ProductScreenCartProductQuantityControl
{
    private func stepperLabelFormatter(_ stepCount: Int) -> String
    {
		return QuantityFormatter.cartItemQuantity(cartProduct.getProductQuantity(stepCount: stepCount),
                                                  cartProduct.product.unit)
    }
    
    private func onQuantityChanged(_ val: Int)
    {
        val == 0 ? self.onQuantityChangedCallback(0) :
			self.onQuantityChangedCallback(cartProduct.getProductQuantity(stepCount: val))
    }
    
	private func setupAppearance(_ editingEnabled: Bool)
	{
		var addToCartButtonEnabled = false
		
		if editingEnabled
		{
            setupAppearanceDueToProductQuantity(cartProduct.toStepperCount())
			addToCartButtonEnabled = true
		}
		else
		{
			addToCartButton.isHidden 	= false
            [stepperContainer, goToCartButton].forEach{ $0.isHidden = true }
			addToCartButtonEnabled 		= false
		}
		
		addToCartButton.isEnabled = addToCartButtonEnabled
		addToCartButton.alpha = addToCartButtonEnabled ? 1.0 : 0.7
	}
	
	private func setupAppearanceDueToProductQuantity(_ currentStepCount: Int)
	{
		addToCartButton.isHidden 	= currentStepCount != 0
        [stepperContainer, goToCartButton].forEach{ $0.isHidden = currentStepCount == 0 }
	}
}
