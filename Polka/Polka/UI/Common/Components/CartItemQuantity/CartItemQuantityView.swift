//
//  File.swift
//  Polka
//
//  Created by Michael Goremykin on 02.06.2021.
//

import UIKit

enum CartItemQuantityViewStyle
{
	case stepper(StepperStyle)
	case steppertWithGoToCartButton
}

class CartItemQuantityView: UIView
{
	// MARK:- Outlets
	var stepperView: StepperView!
	var addToCartButton: UIButton!
	var showCart: UIButton!
	
	// MARK:- Parameters
	var cartItemQuantityViewStyle: CartItemQuantityViewStyle!
	var size: CGSize!
	
	init()
	{
		super.init(frame: .zero)
	}
	
	required init?(coder: NSCoder)
	{
		fatalError("init(coder:) has not been implemented")
	}
}

// MARK:- API
extension CartItemQuantityView
{
	func setup(_ cartProduct: CartProduct)
	{
		
	}
	
	static func getSize(for style: CartItemQuantityViewStyle, parentViewWidth parentWidth: CGFloat) -> CGSize
	{
		switch style
		{
		case .stepper:
			return Constants.sizeForStepper
			
		case .steppertWithGoToCartButton:
			let width = parentWidth * Constants.stepperWithGoToCartToParentWidthRatio
			return CGSize(width: width,
						  height: width/Constants.stepperWithGoToCartAspectRatio)
		}
	}
}



// MARK:- Initialization
extension CartItemQuantityView
{
	func initialize(_ cartItemQuantityViewStyle: CartItemQuantityViewStyle)
	{
		
	}
}

// MARK:- Instantiation
extension CartItemQuantityView
{
	static func create(cartItemQuantityViewStyle: CartItemQuantityViewStyle,
					   size: CGSize,
					   cartProduct: CartProduct) -> CartItemQuantityView
	{
		fatalError()
	}
}

// MARK:- Instantiation
extension CartItemQuantityView
{
	func embedStatelessStepperView()
	{
		
	}
	
	func embedAddToCartButton()
	{
		
	}
	
	func embedGoToCartButton()
	{
		
	}
}

// MARK:- Constants
extension CartItemQuantityView
{
	enum Constants
	{
		static let sizeForStepper = CGSize(width: CGFloat(112), height: CGFloat(32))
		
		static let stepperWithGoToCartAspectRatio = CGFloat(311)/CGFloat(56)
		static let stepperWithGoToCartToParentWidthRatio = CGFloat(311)/CGFloat(375)
		
		static let cornerRadiusRatio = CGFloat(18)/CGFloat(56)
	}
}
