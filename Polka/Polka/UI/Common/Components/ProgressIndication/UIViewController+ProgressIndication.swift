//
//  UIViewController+ProgressIndication.swift
//  RSchip
//
//  Created by Vitaly Trofimov on 31/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation
import MBProgressHUD
import PromiseKit




extension Promise
{
	func indicateProgress(
		on view: UIView = (UIApplication.shared.delegate as! AppDelegate).window!
		) -> Promise
	{
		let hud = view.addProgressIndicator()
		
		return ensure { [weak hud] in
			hud?.hide(animated: true)
		}
	}
}

extension Guarantee
{
	func indicateProgress(
		on view: UIView = (UIApplication.shared.delegate as! AppDelegate).window!
		) -> Guarantee
	{
		let hud = view.addProgressIndicator()
		
		return get { [weak hud] _ in
			hud?.hide(animated: true)
		}
	}
}

extension AnyPromise
{
	@objc
	var indicateProgress: (_ view: UIView?) -> AnyPromise
	{
		return { view in
			//
			let view = view ?? (UIApplication.shared.delegate as! AppDelegate).window!
			let hud = view.addProgressIndicator()
			
			return AnyPromise(
				self.get { [weak hud] _ in
					hud?.hide(animated: true)
				}
			)
		}
	}
}

extension UIView
{
	func addProgressIndicator() -> MBProgressHUD
	{
		let hud = MBProgressHUD.showAdded(
			to: self,
			animated: true
		)
		hud.backgroundView.color = UIColor(white: 0, alpha: 0.5)
		
		return hud
	}
}
