//
//  CustomImage.swift
//  Polka
//
//  Created by Makson on 22.04.2021.
//

import UIKit

class CustomImage: UIImageView
{
	
	override init(frame: CGRect)
	{
		super.init(frame: frame)
		setup()
	}
	
	required init?(coder: NSCoder)
	{
		super.init(coder: coder)
		setup()
	}
	
	func setup()
	{
		self.layer.cornerRadius = 20
	}
}
