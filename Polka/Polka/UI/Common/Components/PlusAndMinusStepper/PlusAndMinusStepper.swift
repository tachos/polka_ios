//
//  PlusAndMinusStepper.swift
//  Polka
//
//  Created by Makson on 22.04.2021.
//

import UIKit

typealias ValueChangedCallback = Handler2<Float,Float>

enum TypeButton
{
	case orange
	case lightOrange
	case gray
}

class PlusAndMinusStepper: UIView
{
    //MARK: - Callback
	private var _onValueChanged: ValueChangedCallback!
	private var _onPlusButtonTapped: VoidCallback!
	
	// MARK:- State
	private var _currentValue:Float = 0.0
	private var _step:Float = 0.0
	private var _typeBtn:TypeButton = .orange
	private var _minCount:Float = 0.0
	private var _unit = ""
	private var backgroundBtn: UIColor?
	
    //MARK: - Outlet
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var countLbl: UILabel!
    
}

extension PlusAndMinusStepper
{
	func initialize(type:TypeButton,
					minCount:Float,
					step:Float,
					unit:String,
					onValueChanged: @escaping ValueChangedCallback)
	{
		setupAppearance(type: type,
						minCount: minCount,
						step: step,
						unit:unit,
						onValueChanged: onValueChanged)
	}
	
	static func create(type:TypeButton,
					   minCount:Float,
					   step:Float,
					   unit:String,
					   onValueChanged: @escaping ValueChangedCallback) -> PlusAndMinusStepper
	{
		let view = createView(PlusAndMinusStepper.self)
			   
		view.initialize(type: type,
						minCount: minCount,
						step: step,
						unit:unit,
						onValueChanged: onValueChanged)
		
		return view
	}
	
	func setupAppearance(type:TypeButton,
						 minCount:Float,
						 step:Float,
						 unit:String,
						 onValueChanged: @escaping ValueChangedCallback)
	{
		_onValueChanged = onValueChanged
		_typeBtn = type
		_step = step
		_minCount = minCount
		_currentValue = minCount
		_unit = unit
		setupCount(count: minCount, unit:unit)
		setupBtn(type: _typeBtn)
	}
	
	func setupBtn(type:TypeButton)
	{
		/*
		getButtonsColors(PlusAndMinusStepper) -> (leftButtonColor: UIColor, UIColor)
		setButtonAppearance(UIButton, UIColor)
		setButtonAppearance(UIButton, UIColor)
		setButtonAppearance(UIButton, UIColor)
		*/
		switch type
		{
		case .lightOrange:
			backgroundBtn = UIColor(red: 255/255, green: 216/255, blue: 179/255, alpha: 1)
			plusBtn.backgroundColor = backgroundBtn
			minusBtn.backgroundColor = backgroundBtn
			plusBtn.setImage(UIImage(named: "Plus")?.tinted(with: UIColor(red: 255/255, green: 122/255, blue: 0/255, alpha: 1)), for: .normal)
			plusBtn.setImage(UIImage(named: "Plus")?.tinted(with: UIColor(red: 255/255, green: 122/255, blue: 0/255, alpha: 0.3)), for: .disabled)
			minusBtn.setImage(UIImage(named: "Minus")?.tinted(with: UIColor(red: 255/255, green: 122/255, blue: 0/255, alpha: 1)), for: .normal)
		case .orange:
			backgroundBtn = UIColor(red: 255/255, green: 122/255, blue: 0/255, alpha: 1)
			plusBtn.backgroundColor = backgroundBtn
			minusBtn.backgroundColor = backgroundBtn
			plusBtn.setImage(UIImage(named: "Plus")?.tinted(with: UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)), for: .normal)
			plusBtn.setImage(UIImage(named: "Plus")?.tinted(with: UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.3)), for: .disabled)
			minusBtn.setImage(UIImage(named: "Minus")?.tinted(with: UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)), for: .normal)
		case .gray:
			backgroundBtn = UIColor(red: 236/255, green: 236/255, blue: 236/255, alpha: 1)
			plusBtn.backgroundColor = backgroundBtn
			minusBtn.backgroundColor = backgroundBtn
			plusBtn.setImage(UIImage(named: "Plus")?.tinted(with: UIColor(red: 33/255, green: 32/255, blue: 31/255, alpha: 1)), for: .normal)
			plusBtn.setImage(UIImage(named: "Plus")?.tinted(with: UIColor(red: 33/255, green: 32/255, blue: 31/255, alpha: 0.3)), for: .disabled)
			minusBtn.setImage(UIImage(named: "Minus")?.tinted(with: UIColor(red: 33/255, green: 32/255, blue: 31/255, alpha: 1)), for: .normal)
		}
	}
	
	func setupCount(count:Float,unit:String)
	{
		countLbl.text = QuantityFormatter.cartItemQuantity(count, .unit)
	}
	
	func updateUI(value: Float)
	{
		transformationCount(value: value)
		disablePlusBtn(value: value)
	}
	
	func transformationCount(value:Float)
	{
		countLbl.text = QuantityFormatter.cartItemQuantity(value, .unit)
//		if "\(value)".contains(".0")
//		{
//			countLbl.text = "\(FormatterPrice.countForUnit(count: round(value * 10)/10, unit:_unit))".replacingOccurrences(of: ".0", with: "")
//		}
//		else
//		{
//			countLbl.text = "\(FormatterPrice.countForUnit(count: round(value * 10)/10, unit:_unit))"
//		}
	}
	
	func disablePlusBtn(value:Float)
	{
		if Int(value) == Constants.maxValue
		{
			plusBtn.isEnabled = false
			plusBtn.backgroundColor = backgroundBtn?.withAlphaComponent(0.3)
		}
		else
		{
			plusBtn.isEnabled = true
			plusBtn.backgroundColor = backgroundBtn?.withAlphaComponent(1)
		}
		setupBtn(type: _typeBtn)
	}
	
	
}
extension PlusAndMinusStepper
{
	@IBAction func minusAction()
	{
		let previousValue = _currentValue
		
		if _currentValue > _minCount
		{
			_currentValue -= _step
		}
		
		if _currentValue == 0
		{
			 
			_onValueChanged(previousValue,_currentValue)
			_currentValue = _minCount
		}
		else if previousValue != _currentValue
		{
			updateUI(value: _currentValue)
			
			_onValueChanged(previousValue, _currentValue)
		}
		else
		{
			_onValueChanged(previousValue, _currentValue - _step)
		}
		print("press minus")
	}
	
	@IBAction func plusAction()
	{
		let previousValue = _currentValue

		if _currentValue < Float(Constants.maxValue)
		{
			_currentValue += _step
		}
		
		updateUI(value: _currentValue)
		if previousValue != _currentValue
		{
			_onValueChanged(previousValue, _currentValue)
		}
		print("press plus")
	}
}

extension PlusAndMinusStepper
{
	struct Constants
	{
		static let maxValue:Int = 15
	}
}
