//
//  KeyboardFrameAware.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 24.06.2021.
//

import UIKit



@objc protocol KeyboardFrameAware
{
	func adjustForKeyboardFrame(_ frame: CGRect)

	var keyboardObserver: NSObjectProtocol? { get set }
}



extension KeyboardFrameAware
{
	func setupKeyboardObserver()
	{
		keyboardObserver = NotificationCenter.default.addObserver(forName: UIWindow.keyboardWillChangeFrameNotification, object: nil, queue: nil) { [weak self] notification in
			//
			self?.keyboardWillChange(notification: notification)
		}
	}

	func resetKeyboardObserver()
	{
		keyboardObserver = nil
	}
}



extension KeyboardFrameAware where Self: UIViewController
{
	func extraBottomSpacing(for keyboardFrame: CGRect) -> CGFloat
	{
		if keyboardFrame.size == .zero
		{
			return 0
		}

		let extra = view.bounds.height
			- keyboardFrame.minY
			- view.safeAreaInsets.bottom

		return max(0, extra)
	}

	func adjustInsets(
		_ scrollView: UIScrollView,
		for keyboardFrame: CGRect,
		distanceToKeyboard: CGFloat
	)
	{
		let inset = extraBottomSpacing(for: keyboardFrame)
			+ distanceToKeyboard

		scrollView.contentInset.bottom = inset
		scrollView.verticalScrollIndicatorInsets.bottom = inset
	}
}



fileprivate extension KeyboardFrameAware
{
	func keyboardWillChange(notification: Notification)
	{
		let frameValue = notification.userInfo![UIWindow.keyboardFrameEndUserInfoKey] as! NSValue
		let frame = frameValue.cgRectValue
		let curveValue = notification.userInfo![UIWindow.keyboardAnimationCurveUserInfoKey] as! NSNumber
		let curve = UIView.AnimationCurve(rawValue: curveValue.intValue)!
		let durationValue = notification.userInfo![UIWindow.keyboardAnimationDurationUserInfoKey] as! NSNumber
		let duration = durationValue.doubleValue
		adjustForKeyboardFrame(frame, withAnimation: curve, duration: duration)
	}

	func adjustForKeyboardFrame(_ frame: CGRect, withAnimation curve: UIView.AnimationCurve, duration: TimeInterval)
	{
		UIView.animate(withDuration: duration, delay: 0, options: curve.toOption(), animations: { [weak self] in
			//
			self?.adjustForKeyboardFrame(frame)
		})
	}
}



fileprivate extension UIView.AnimationCurve
{
	func toOption() -> UIView.AnimationOptions
	{
		switch self
		{
		case .easeIn:
			return .curveEaseIn
		case .easeInOut:
			return .curveEaseInOut
		case .easeOut:
			return .curveEaseOut
		case .linear:
			return .curveLinear
		default:
			return .curveLinear
		}
	}
}
