//
//  TwoButtonPopup.swift
//  Polka
//
//  Created by Michael Goremykin on 30.04.2021.
//

import UIKit

enum PopUps
{
	
}

extension PopUps
{
	static func showTwoButtonPopUp(
		from vc: UIViewController,
		title: String,
		message: String,
		firstButtonTitle: String,
		isFirstButtonDestructive: Bool = false,
		onFirstButton: @escaping () -> Void,
		secondButtonTitle: String = "Отмена",
		onSecondButton: (() -> Void)? = nil
	)
	{
		let popup = UIAlertController(title: title,
									  message: message,
									  preferredStyle: UIAlertController.Style.alert)
		
		popup.addAction(
			UIAlertAction(
				title: firstButtonTitle,
				style: isFirstButtonDestructive ? .destructive : .default,
				handler: { (action: UIAlertAction!) in onFirstButton() }
			)
		)
		
		popup.addAction(UIAlertAction(title: secondButtonTitle,
									  style: .cancel, handler: { (action: UIAlertAction!) in onSecondButton?() }))
		
		vc.present(popup, animated: true, completion: nil)
	}
	
	static func showRetryQuery(
		from vc: UIViewController,
		queryRetryCallback: @escaping () -> Void,
		cancelButtonTitle: String = "Отмена",
		onCancelCallback: (() -> Void)? = nil
	)
	{
		showTwoButtonPopUp(
			from: vc,
			title: "Ошибка",
			message: "Возникли проблемы при получении данных. Повторите попытку позже.",
			firstButtonTitle: "Повторить",
			onFirstButton: queryRetryCallback,
			secondButtonTitle: cancelButtonTitle,
			onSecondButton: onCancelCallback
		)
	}
	
	
	static func showCannotAddToCartProductsFromDifferentMarketsError(
		from vc: UIViewController,
		clearCartAndAddProduct: @escaping () -> Void
	)
	{
		showTwoButtonPopUp(
			from: vc,
			title: "Ошибка",
			message: "Вы пытаетесь добавить в корзину товары из разных рынков.\nВ корзину можно добавлять товары только с одного рынка.",
			//firstButtonTitle: "Очистить корзину и добавить товар",
			firstButtonTitle: "Очистить корзину",
			isFirstButtonDestructive: true,
			onFirstButton: clearCartAndAddProduct,
			secondButtonTitle: "Отмена"
		)
	}
}




extension UIViewController
{
	func showRetryAlert(onRetry: @escaping VoidCallback)
	{
		PopUps.showRetryQuery(
			from: self,
			queryRetryCallback: onRetry,
			onCancelCallback: {}
		)
	}
}
