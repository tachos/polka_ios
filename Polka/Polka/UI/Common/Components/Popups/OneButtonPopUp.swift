//
//  OneButtonPopUp.swift
//  Polka
//
//  Created by Michael Goremykin on 31.05.2021.
//

import UIKit

extension PopUps
{
	static func showOneButtonPopUp(from vc: UIViewController,
					 title: String,
					 message: String,
					 onOk: @escaping () -> Void)
	{
		let popup = UIAlertController(title: title,
									  message: message,
									  preferredStyle: UIAlertController.Style.alert)

		popup.addAction(UIAlertAction(title: "Ок",
											 style: .default,
											 handler: { (action: UIAlertAction!) in onOk() }))

		vc.present(popup, animated: true, completion: nil)
	}
	
	static func showQueryError(from vc: UIViewController,
							   title: String,
							   message: String,
							   onOkCallback: @escaping () -> Void)
	{
		showOneButtonPopUp(from: vc,
						   title: title,
						   message: message,
						   onOk: onOkCallback)
	}
}


extension PopUps
{
	static func promptUserToAuthorize(on presentingViewController: UIViewController)
	{
		PopUps.showOneButtonPopUp(
			from: presentingViewController,
			title: "",
			message: "Для того, чтобы стал доступен функционал корзины, необходимо авторизоваться при помощи телефона.",
			onOk: {}
		)
	}
}
