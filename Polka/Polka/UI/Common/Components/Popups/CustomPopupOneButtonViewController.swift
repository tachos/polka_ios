//
//  CustomPopupOneButtonViewController.swift
//  Polka
//
//  Created by Makson on 26.06.2021.
//

import UIKit

class CustomPopupOneButtonViewController: UIViewController
{
	//MARK: - Outlet and variable
	private var yesActionCallback:VoidCallback!
	
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var yesBtn: CustomButton!
	private var titleMain:String!
	private var titleYesBtn:String!
	
}

//MARK: - Lifecycle
extension CustomPopupOneButtonViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		setup()
	}
}

//MARK: - create viewController
extension CustomPopupOneButtonViewController
{
	static func create(
		text: String,
		textYesButton:String,
		yesAction: @escaping VoidCallback
		) -> CustomPopupOneButtonViewController
		{
			let viewController = createViewController(
			.CustomPopup,
				CustomPopupOneButtonViewController.self
		)
		viewController.titleMain = text
		viewController.titleYesBtn = textYesButton
		viewController.yesActionCallback = yesAction

		return viewController
	}
}

extension CustomPopupOneButtonViewController
{
	func setup()
	{
		titleLbl.text = titleMain
		yesBtn.setTitle(titleYesBtn, for: .normal)
	}
}
//MARK: - Action
extension CustomPopupOneButtonViewController
{
	@IBAction func yesAction()
	{
		yesActionCallback?()
	}
}
