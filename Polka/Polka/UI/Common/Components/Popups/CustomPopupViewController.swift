//
//  CustomPopupViewController.swift
//  Polka
//
//  Created by Makson on 20.05.2021.
//

import UIKit

class CustomPopupViewController: UIViewController
{
	//MARK: - Outlet
	
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var yesBtn: CustomButton!
    @IBOutlet weak var noBtn: UIButton!
	
	private var yesActionCallback:VoidCallback!
	private var noActionCallback:VoidCallback!
	
    @IBOutlet weak var heightViewConstraint: NSLayoutConstraint!
    private var titleMain:String!
	private var titleYesBtn:String!
	private var titleNoBtn:String!
	
}

//MARK: - Lifecycle
extension CustomPopupViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		setup()
	}
}

extension CustomPopupViewController
{
	static func create(
		text: String,
		titleYesBtn:String,
		titleNoBtn:String,
		yesAction: @escaping VoidCallback,
		noAction: @escaping VoidCallback
		) -> CustomPopupViewController
		{
			let viewController = createViewController(
			.CustomPopup,
			CustomPopupViewController.self
		)

		viewController.titleMain = text
		viewController.titleYesBtn = titleYesBtn
		viewController.titleNoBtn = titleNoBtn
		viewController.yesActionCallback = yesAction
		viewController.noActionCallback = noAction

		return viewController
	}
}
extension CustomPopupViewController
{
	func setup()
	{
		titleLbl.text = titleMain
		yesBtn.setTitle(titleYesBtn, for: .normal)
		noBtn.setTitle(titleNoBtn, for: .normal)
		updateHeightView(height: titleLbl.bounds.size.height)
		
	}
	
	func updateHeightView(height: CGFloat)
	{
		if height > Constant.defaultHeightTitleLbl
		{
			heightViewConstraint.constant += (height - Constant.defaultHeightTitleLbl)
		}
		else if height < Constant.defaultHeightTitleLbl
		{
			heightViewConstraint.constant -= (Constant.defaultHeightTitleLbl - height)
			heightViewConstraint.constant += Constant.bottomPaddingTitleLbl
		}
	}
}
//MARK: - Action
extension CustomPopupViewController
{
	@IBAction func yesAction()
	{
		yesActionCallback?()
	}
	
	@IBAction func noAction()
	{
		noActionCallback?()
	}
}

extension CustomPopupViewController
{
	struct Constant
	{
		static let defaultHeightTitleLbl:CGFloat = 72
		static let bottomPaddingTitleLbl:CGFloat = 24
	}
}
