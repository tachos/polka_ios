//
//  +View.swift
//  Polka
//
//  Created by Makson on 19.05.2021.
//

import Foundation
import UIKit

extension UIView
{
	func animateLayoutChanges(with duration: TimeInterval = 0.2, animations: VoidCallback? = nil)
	{
		UIView.animate(withDuration: duration,
					   delay: 0,
					   options: [.curveEaseInOut],
					   animations:
			{
				[weak self] in
				
				animations?()
				
				self?.updateConstraints()
				self?.layoutIfNeeded()
		})
	}

	func layout()
	{
		setNeedsLayout()
		layoutIfNeeded()
	}
}
