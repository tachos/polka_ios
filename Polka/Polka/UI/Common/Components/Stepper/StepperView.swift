//
//  StepperAView.swift
//  Polka
//
//  Created by Michael Goremykin on 01.06.2021.
//

import UIKit

enum StepperStyle
{
	case orange
	case paleOrange
	case grey
}


final class StepperView: UIView
{
	typealias WeightFormatter = (_ quantity: Int) -> String
	typealias PriceFormatter = (_ quantity: Int) -> String

	
    // MARK:- Outlets
	@IBOutlet private var minusButton: UIButton!
    @IBOutlet private var plusButton: UIButton!
    @IBOutlet private var weightLabel: UILabel!
	@IBOutlet private var priceLabel: UILabel!
    
	// MARK:- Parameters
	private var stepCount = 1
	private var availableStepCount: Int = 1
	{
		didSet
		{
			maxStepCount = availableStepCount + 1
		}
	}
    private var style: StepperStyle!
	private var isSellerOrder:Bool = false
    
	private var weightFormatter: WeightFormatter!
	private var priceFormatter: PriceFormatter!
	
    private var onStepChanged: ((Int) -> Void)!
    private var additionalActionOnSetToZero: (() -> Void)?
	
	// MARK:- Stateful
	private var maxStepCount = 0
	private var zeroStep = 0
	
	required init?(coder aDecoder: NSCoder)
	{
		super.init(coder: aDecoder)

		guard let nibView = loadViewFromNib() else { return }
		self.addSubview(nibView)
		nibView.edgesToSuperview()
	}

	func loadViewFromNib() -> UIView?
	{
		let bundle = Bundle(for: type(of: self))
		let nib = UINib(nibName: String(describing: StepperView.self), bundle: bundle)
		return nib.instantiate(withOwner: self, options: nil).first as? UIView
	}
	
}

// MARK:- UI handlers
extension StepperView
{
	// onDecrementButtonTapped
    @IBAction func onMinusButtonTapped(_ sender: Any)
    {
        let previousStepCount = stepCount
		
		stepCount -= 1
        
		weightLabel.text = weightFormatter(stepCount)
		
        if stepCount == 0
		{
			setMinusButtonState(didReachZeroValue: true)
			additionalActionOnSetToZero?()
		}
		else if previousStepCount == maxStepCount
		{
			setPlusButtonState(didReachMaxValue: false)
		}
		
		onStepChanged(stepCount)
    }
	
    @IBAction func onPlusButtonTapped(_ sender: Any)
    {
        stepCount += 1
		
		
		setMinusButtonState(didReachZeroValue: false )
		
		weightLabel.text = weightFormatter(stepCount)
        
		if stepCount == maxStepCount
		{
			setPlusButtonState(didReachMaxValue: true)
		}
		
		onStepChanged(stepCount)
    }
	
	private func setMinusButtonState(didReachZeroValue: Bool)
	{
		minusButton.isEnabled = !didReachZeroValue
		minusButton.alpha = minusButton.isEnabled ? 1.0 : 0.7
	}
	
	private func setPlusButtonState(didReachMaxValue: Bool)
	{
		if didReachMaxValue {
			dbgwarn("didReachMaxValue!")
		}
		plusButton.isEnabled = !didReachMaxValue
		plusButton.alpha = plusButton.isEnabled ? 1.0 : 0.7
	}
}

// MARK:- Life cycle
extension StepperView
{
    override func awakeFromNib()
    {
        super.awakeFromNib()
		
        roundButtonsCorners()
    }
}

// MARK:- API
extension StepperView
{
	func setup(_ stepperStyle: StepperStyle,
			   _ stepCount: Int,
			   _ availableStepCount: Int,
			   weightFormatter: @escaping WeightFormatter,
			   priceFormatter: @escaping PriceFormatter,
			   _ onStepChanged: @escaping (Int) -> Void,
			   _ additionalActionOnSetToZero: (() -> Void)? = nil)
	{
		self.style = stepperStyle
		self.stepCount = stepCount
		self.availableStepCount = availableStepCount
		
		self.weightFormatter = weightFormatter
		self.priceFormatter = priceFormatter
		
		self.onStepChanged = onStepChanged
		self.additionalActionOnSetToZero = additionalActionOnSetToZero

		dbgout("StepperView: stepCount=\(stepCount), maxStepCount=\(maxStepCount)")

		setupUI()
	}
}

// MARK:- Utils
extension StepperView
{
	func setupUI()
	{
		setupButtons()
		
		weightLabel.text = weightFormatter(stepCount)
		priceLabel.text = priceFormatter(stepCount)
	}
}

// MARK:- Utils
extension StepperView
{
    func roundButtonsCorners()
    {
        [minusButton, plusButton].forEach{ button in
            button?.backgroundColor = .clear
            button?.layer.cornerRadius = Constants.buttonCornerRadius
        }
    }
    
    func setupButtons()
    {
        setupButtonsAppearance()
		setPlusButtonEnabled()
		setMinusButtonEnabled()
    }
    
    func setPlusButtonEnabled()
    {
		setPlusButtonState(didReachMaxValue: stepCount == maxStepCount)
    }
	
	func setMinusButtonEnabled()
	{
		setMinusButtonState(didReachZeroValue: stepCount == zeroStep)
	}
	
    func setupButtonsAppearance()
    {
        [minusButton, plusButton].forEach{ button in
            let settings = getButtonColorSettings(for: style)
            
            button?.backgroundColor = settings.backgroundColor
            button?.setTitleColor(settings.textColor, for: [])
        }
    }
    
    func getButtonColorSettings(for style: StepperStyle) -> (backgroundColor: UIColor,
                                                             textColor: UIColor)
    {
        switch style
        {
        case .orange:
            return (backgroundColor: Constants.orange,
                    textColor: Constants.textOnOrange)
            
        case .paleOrange:
            return (backgroundColor: Constants.paleOrange,
                    textColor: Constants.textOnPaleOrange)
            
        case .grey:
            return (backgroundColor: Constants.grey,
                    textColor: Constants.textOnGrey)
        }
    }
}

// MARK:- Constants
extension StepperView
{
    enum Constants
    {
        static let orange = #colorLiteral(red: 1, green: 0.4784313725, blue: 0, alpha: 1)
        static let textOnOrange = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        static let paleOrange = #colorLiteral(red: 1, green: 0.8470588235, blue: 0.7019607843, alpha: 1)
        static let textOnPaleOrange = #colorLiteral(red: 1, green: 0.4784313725, blue: 0, alpha: 1)
        static let grey = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
        static let textOnGrey = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        static let buttonCornerRadius = CGFloat(12)
    }
}
