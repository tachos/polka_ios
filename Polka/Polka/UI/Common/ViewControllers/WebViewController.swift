//
//  WebViewController.swift
//  Polka
//
//  Created by Dmitry Cherednikov on 23.06.2021.
//

import UIKit
import WebKit
import TinyConstraints



class WebViewController: UIViewController
{
	// MARK: WebView
	private var webView: WKWebView?

	// MARK: Config
	var config: Config!

	struct Config
	{
		let url: URL
		let delegate: WKNavigationDelegate?
		let dismissSegue: VoidCallback
	}
}



// MARK:- Utils

extension WebViewController
{
	func embeddedInNavigation() -> UINavigationController
	{
		let navigationController = UINavigationController(rootViewController: self)

		navigationController.navigationBar.isTranslucent = true
		navigationController.navigationBar.backgroundColor = .white
		
		navigationController.modalPresentationStyle = .fullScreen

		return navigationController
	}
}



// MARK: - UIViewController

extension WebViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		self.view.backgroundColor = .white
		setup()
	}

	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)

		webView?.load(
			.init(url: config.url)
		)
	}
}



// MARK: - Setup

extension WebViewController
{
	func setup()
	{
		setupWebView()
		setupNavigationBar()
	}

	func setupWebView()
	{
		let webView = WKWebView()
		webView.navigationDelegate = config.delegate
		view.addSubview(webView)
		webView.topToSuperview(usingSafeArea: true)
		webView.edgesToSuperview(excluding: .top)

		self.webView = webView
	}

	func setupNavigationBar()
	{
		navigationItem.leftBarButtonItem = .init(
			title: "Закрыть",
			style: .done,
			target: self,
			action: #selector(onDoneButton)
		)
	}
}



// MARK: - Handle UI

extension WebViewController
{
	@objc func onDoneButton()
	{
		config.dismissSegue()
	}
}
