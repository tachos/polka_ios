//
//  Storyboard.swift
//  Polka
//
//  Created by Makson on 19.04.2021.
//

enum StoryboardName: String
{
	case onboarding
    case authorization
    case seller
    case chats
    case catalog
	case market
	case product
	case profile
	case whereBring
	case search
	case sellerScreen
	case sellerOrderScreen
	case CustomPopup
	case MoreDetailsOrder
	case Splash
}
