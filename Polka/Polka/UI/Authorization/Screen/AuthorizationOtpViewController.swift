//
//  AuthorizationOtpViewController.swift
//  Polka
//
//  Created by Makson on 16.04.2021.
//

import UIKit
import AnyFormatKit
import PromiseKit

enum FormatterOTP: String
{
    case code = "#       #       #       #"
}
 
class AuthorizationOtpViewController: UIViewController,StoryboardCreatable
{
    //MARK: - StoryboardCreatable
    static var storyboardName: StoryboardName = .authorization
    
    @IBOutlet weak var errorLbl: UILabel!
    //MARK: - Outlets and variable
    @IBOutlet weak var otpField: UITextField!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var repeatBtn: CustomButton!
    @IBOutlet weak var phoneLbl: UILabel!
    
    var phone: String = ""
	var isError: Bool = false
	var timer: Timer?
    var formatterOtp = DefaultTextInputFormatter(textPattern: "####")
    var sendNumber = DefaultTextInputFormatter(textPattern: "############")
    // MARK: Config
    var config: Config!
	
    
    struct Config
    {
		let helper: AuthorizationHelper
    }
}

//MARK: - Lifecycle
extension AuthorizationOtpViewController
{
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        otpField.becomeFirstResponder()
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    }
   
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
		(self.navigationController as? NavBarCustomizable)?.setAppearance(navBarBackground: .clear,
																		 hasBackButton: false,
																		 titleViewType: nil,
																		 initiallyNotVisible: false)
        setup()
        
    }
    
    func setup()
    {
        inizializationNumberField()
        inizializationPhoneLbl()
		startTimer()
    }
	
	func startTimer()
	{
		guard timer == nil else { return }

		  timer =  Timer.scheduledTimer(
			  timeInterval: TimeInterval(1.0),
			  target      : self,
			  selector    : #selector(timerActionTest),
			  userInfo    : nil,
			  repeats     : true)
	}
	func sendOTP(phone:String)
	{
		config.helper.sendOTP(phone,false)
		.get
		{ _ in
			self.startTimer()
		}
		.catch
		{ error in
				
			self.setupErrorLbl(error: true, string: error.localizedDescription)
		}
	}
	
	@objc func timerActionTest()
	{
		if config.helper.timeTimer != 0
		{
			repeatBtn.setTitle("Повторить(\(config.helper.timeTimer) сек)", for: .normal)
			config.helper.timeTimer -= 1
		}
		else
		{
			repeatBtn.isEnabled = true
			repeatBtn.style = "orange"
			repeatBtn.setTitle("Повторить", for: .normal)
			stopTimerTest()
		}
	}
	
	func stopTimerTest()
	{
	  timer?.invalidate()
	  config.helper.timeTimer = 5
	  timer = nil
	}
    func inizializationPhoneLbl()
    {
        sendNumber = DefaultTextInputFormatter(textPattern: Formatter.send.rawValue)
		phone = sendNumber.format(config.helper.phone)!
        
        phoneLbl.text = "Отправили код на номер \n\(phone)"
    }
}

extension AuthorizationOtpViewController
{
    @IBAction func backAction()
    {
		config.helper.back()
    }
	
	@IBAction func repeatBtnAction()
	{
		repeatBtn.isEnabled = false
		repeatBtn.style = "gray"
		sendOTP(phone: sendNumber.unformat(phone)!)
	}
}
// MARK: - UITextFieldDelegate
extension AuthorizationOtpViewController: UITextFieldDelegate
{
    func inizializationNumberField()
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 20))
        otpField.delegate = self
        otpField.layer.borderWidth = 1
        otpField.layer.borderColor = UIColor.clear.cgColor
        otpField.leftView = paddingView
        otpField.leftViewMode = .always
		otpField.textContentType = .oneTimeCode
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        var position = 0
		setupErrorLbl(error: isError, string: string)
		isError = false
		errorLbl.isHidden = true
		
        let currentText = textField.text ?? ""
        
        if let selectedRange = textField.selectedTextRange
        {
            
            let cursorPosition = textField.offset(from: textField.beginningOfDocument, to: selectedRange.start)
            let endPosition = textField.position(from: textField.beginningOfDocument, offset: textField.text!.count)
            textField.selectedTextRange = textField.textRange(from: endPosition!, to: endPosition!)
            position = cursorPosition
        }
        
        if position == textField.text?.count
        {
            formatterOtp = DefaultTextInputFormatter(textPattern: FormatterOTP.code.rawValue)

            if let range = Range(range, in: currentText)
            {
                let resultFormatter = formatterOtp.formatInput(currentText: currentText , range: NSRange(range,in:currentText), replacementString: string)
                let resultUnformatter = formatterOtp.unformat(resultFormatter.formattedText) ?? ""
                colorBorder(text: resultUnformatter)
                if isValidInput(resultUnformatter)
                {
                    textField.text = resultFormatter.formattedText
					sendOtp(otp: resultUnformatter)
                }
            }
        }
        
        return false
    }
	
	func setupErrorLbl(error:Bool, string:String)
	{
		if !error
		{
			self.errorLbl.isHidden = true
		}
		else
		{
			self.errorLbl.isHidden = false
			self.otpField.text = string
		}
	}
	func sendOtp(otp:String)
	{
		if otp.count == 4
		{
			config.helper.authorization(otp)
				.catch
				{ error in
					self.isError = true
					self.errorLbl.isHidden = false
					self.errorLbl.text = error.localizedDescription
				}
		}
	}
    func setCursorToTextfieldEnd(_ textfield: UITextField)
    {
            let end = textfield.endOfDocument
            textfield.selectedTextRange = textfield.textRange(from: end, to: end)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        let newPosition = textField.endOfDocument
        textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
    }
    
    func colorBorder(text:String)
    {
        if text.count > 0
        {
            otpField.layer.borderColor = UIColor(red: 255/255, green: 122/255, blue: 0/255, alpha: 1).cgColor
        }
        else
        {
            otpField.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    func isValidInput(_ string: String) -> Bool
    {
        let otpLength = string.count - 1
        
        return otpLength <= Constant.countNumbers
    }
}

//MARK: - Contants
extension AuthorizationOtpViewController
{
    struct Constant
    {
        static let countNumbers = 4
    }
}
