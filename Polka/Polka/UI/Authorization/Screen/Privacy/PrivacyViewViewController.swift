//
//  PrivacyViewViewController.swift
//  Polka
//
//  Created by Makson on 01.06.2021.
//

import UIKit
import WebKit

class PrivacyViewViewController: UIViewController,StoryboardCreatable
{
	//MARK: - StoryboardCreatable
	static var storyboardName: StoryboardName = .authorization
	
    @IBOutlet weak var webView: WKWebView!
	
    @IBOutlet weak var topConstraintWebView: NSLayoutConstraint!
    var url = ""
}
extension PrivacyViewViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		webView.navigationDelegate = self
		setupWebView(string: url)
		setupNavBarAppearance()
	}
	func setupNavBarAppearance()
	{
		(self.navigationController as? NavBarCustomizable)?.setAppearance(navBarBackground: .blurred,
																		 hasBackButton: true,
																		 titleViewType: nil,
																		 initiallyNotVisible: false)
		topConstraintWebView.constant = getNavBarHeight()! + getStatusBarHeight()!
	}
	
}
extension PrivacyViewViewController: WKNavigationDelegate
{
	func setupWebView(string:String)
	{
		let url = URL(string:string)!
		webView.load(URLRequest(url: url))
		webView.allowsBackForwardNavigationGestures = true
		webView.scrollView.contentInsetAdjustmentBehavior = .never
	}
}
