//
//  AuthorizationViewController.swift
//  Polka
//
//  Created by Makson on 16.04.2021.
//

import UIKit
import AnyFormatKit


enum Formatter: String
{
    case russia = "## ### ### ####"
    case send = "## (###) ###-##-##"
	case phoneProfile = "## ### ### ## ##"
}

class AuthorizationViewController: UIViewController,StoryboardCreatable
{
    //MARK: - StoryboardCreatable
    static var storyboardName: StoryboardName = .authorization
    
    @IBOutlet weak var errorLbl: UILabel!
    @IBOutlet weak var confidentialityLbl: UILabel!
    @IBOutlet weak var nextBtn: CustomButton!
    //MARK: - Outlets and variable
    @IBOutlet weak var numberField: UITextField!
	let tapRec = UITapGestureRecognizer()
    // MARK: Config
    var config: Config!
    
    struct Config
    {
        
		let helper: AuthorizationHelper
    }
    
    var formatterNumber = DefaultTextInputFormatter(textPattern: "############")
    var phone = ""
	var textString = "Нажимая Далее, я соглашаюсь с политикой конфиденциальности"
}


extension AuthorizationViewController
{
    @IBAction func sendPhone()
    {
		config.helper.sendOTP(phone, true)
			.catch
			{ error in
				self.nextBtn.isUserInteractionEnabled = true
				self.nextBtn.style = "gray"
				self.errorLbl.text = error.localizedDescription 
				self.errorLbl.isHidden = false
			}
		nextBtn.isUserInteractionEnabled = false 
    }
	
	@IBAction func onCatalogButton()
	{
		config.helper.skipAuthorization()
	}
}
//MARK: - Lifecycle
extension AuthorizationViewController
{
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        numberField.becomeFirstResponder()
		nextBtn.isUserInteractionEnabled = true
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        setup()
		(self.navigationController as? NavBarCustomizable)?.setAppearance(navBarBackground: .clear,
																		 hasBackButton: false,
																		 titleViewType: nil,
																		 initiallyNotVisible: false)
    }
	
	override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {

		if numberField.isFirstResponder {
			DispatchQueue.main.async(execute: {
				if #available(iOS 13.0, *) {
					(sender as? UIMenuController)?.hideMenu()
				} else {
					(sender as? UIMenuController)?.setMenuVisible(false, animated: false)
				}
			})
			return false
		}

		return super.canPerformAction(action, withSender: sender)
	}
	
    func setup()
    {
        inizializationNumberField()
		setupConfidentialityField()
		//setupNotificationForPasteBoard()
    }
//	func setupNotificationForPasteBoard()
//	{
//		NotificationCenter.default.addObserver(self, selector: #selector(clipboardChanged),
//											   name: UIPasteboard.changedNotification, object: nil)
//	}
//	@objc func clipboardChanged(){
//		   let pasteboardString: String? = UIPasteboard.general.string
//		   if let theString = pasteboardString
//		{
//			   print("String is \(theString)")
//			   // Do cool things with the string
//		   }
//	   }
	func isEnableSendPhoneBtn(phoneNumber:String)
	{
		errorLbl.isHidden = true 
		if phoneNumber.count < 12
		{
			nextBtn.isEnabled = false
			nextBtn.style = "gray"
		}
		else
		{
			nextBtn.isEnabled = true
			nextBtn.style = "orange"
		}
	}
	func setupConfidentialityField()
	{
		let range = (textString as NSString).range(of: "политикой конфиденциальности")
		let attributedString = NSMutableAttributedString(string: textString)
		attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 255/255, green: 122/255, blue: 0/255, alpha: 1), range: range)
		tapRec.addTarget(self, action: #selector(tapConfidentiality(gesture:)))
		confidentialityLbl.isUserInteractionEnabled = true
		confidentialityLbl.addGestureRecognizer(tapRec)
		self.confidentialityLbl.attributedText = attributedString
	}
	@objc func tapConfidentiality(gesture: UITapGestureRecognizer)
	{
		let privacyRange = (textString as NSString).range(of: "политикой конфиденциальности")

		if gesture.didTapAttributedTextInLabel(label: confidentialityLbl, inRange: privacyRange)
		{
			guard let url = URL(string: "https://drive.google.com/file/d/19oC8urmIWuvOMBUoxJ-SBZ8rPVfKkpLS/view") else { return }
			UIApplication.shared.open(url)
		} else
		{
			print("Tapped none")
		}
	}
}

// MARK: - Inizialization and UITextFieldDelegate
extension AuthorizationViewController: UITextFieldDelegate
{
    func inizializationNumberField()
    {
        numberField.delegate = self
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let currentText = textField.text ?? ""
        
        formatterNumber = DefaultTextInputFormatter(textPattern: Formatter.russia.rawValue)
        
        if let range = Range(range, in: currentText)
        {
            let resultFormatter = formatterNumber.formatInput(currentText: currentText , range: NSRange(range,in:currentText), replacementString: string)
            let resultUnformatter = formatterNumber.unformat(resultFormatter.formattedText)
			
            if isValidPhoneInput(resultUnformatter!)
            {
                textField.text = resultFormatter.formattedText
                phone = resultUnformatter!
            }
			
         }
        
         return false
     }
	
    func isValidPhoneInput(_ string: String) -> Bool
    {
		isEnableSendPhoneBtn(phoneNumber: string)
        if string.prefix(2) != "+7"
        {
            return false
        }
        let charsInRestOfString = CharacterSet(
            charactersIn: String(string.suffix(string.count-1))
        )

        if !charsInRestOfString.isSubset(of: .decimalDigits)
        {
            return false
        }

        let phoneNumberLength = string.count - 1
        
        return phoneNumberLength <= Constant.countNumbers
    }
}

//MARK: - Contants
extension AuthorizationViewController
{
    struct Constant
    {
        static let countNumbers = 11
		
    }
}

protocol StoryboardCreatable: UIViewController
{
    static var storyboardName: StoryboardName { get }

    static func create() -> Self
}

extension StoryboardCreatable
{
    static func create() -> Self
    {
        return createViewController(
            storyboardName,
            self
        )
    }
}
