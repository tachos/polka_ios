//
//  WhereBringViewController.swift
//  Polka
//
//  Created by Makson on 29.04.2021.
//

import UIKit
import CoreLocation
import RxSwift
import PromiseKit

class WhereBringViewController: UIViewController,StoryboardCreatable
{
	//MARK: - StoryboardCreatable
	static var storyboardName: StoryboardName = .whereBring
	
	//MARK: - Outlet
    @IBOutlet weak var entranceField: UITextField!
    @IBOutlet weak var intercomField: UITextField!
    @IBOutlet weak var floorField: UITextField!
    @IBOutlet weak var apartmentField: UITextField!
	weak var addressViewContainer: AddressView!
    @IBOutlet weak var continerAddressView: UIView!
    @IBOutlet weak var generalVerticalStack: UIStackView!
    @IBOutlet weak var heightMoreDetailsViewContraint: NSLayoutConstraint!
    @IBOutlet weak var horizontalStackView: UIStackView!
    @IBOutlet weak var heightGeneralViewContraint: NSLayoutConstraint!
    
    @IBOutlet weak var saveBtn: CustomButton!
    @IBOutlet weak var commentField: UITextField!
    @IBOutlet weak var partOne: UIView!
    @IBOutlet weak var widthEntranceContraint: NSLayoutConstraint!
    @IBOutlet weak var widthIntercomContraint: NSLayoutConstraint!
    @IBOutlet weak var widthFloorContraint: NSLayoutConstraint!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var entranceView: UIView!
	@IBOutlet weak var intercomView: UIView!
	@IBOutlet weak var floorView: UIView!
	@IBOutlet weak var apartmentView: UIView!
    @IBOutlet weak var searchAddressTable: UITableView!
    @IBOutlet weak var showCatalogBtn: UIButton!
	
    let disposeBag = DisposeBag()
	
	let tapRec = UITapGestureRecognizer()
	var locationCoordinate: CLLocationCoordinate2D!
	var isEnablelocation = false
	
	// MARK: Config
	var config: Config!
	
	struct Config
	{
		
		let helper: WhereBringHelper
	}
	private var places:[PlaceAutocompleteResult] = []
	private var cancelRequest: VoidCallback?
	private var consumer:ConsumerInfo?
	
}

//MARK: - Lifecycle
extension WhereBringViewController
{
	override func viewDidAppear(_ animated: Bool)
	{
		super.viewDidAppear(animated)
		uploadConstraint()
		saveBtn.isUserInteractionEnabled = true
		showCatalogBtn.isUserInteractionEnabled = true
		checkStatusGeo()
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		managers.webService.updateBring(isBring: true)
		setupUI()
		
	}
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
		(self.navigationController as? NavBarCustomizable)?.setAppearance(navBarBackground: .clear,
																		 hasBackButton: false,
																		 titleViewType: nil,
																		 initiallyNotVisible: false)
	}
}

//MARK: - Action
extension WhereBringViewController
{
	@IBAction func saveAddress()
	{
		saveBtn.isUserInteractionEnabled = false
		managers.webService.updateBring(isBring: false)
		updatingOptionalFieldsDevileryAddress()
		if config.helper.address.id != nil
		{
			updateAddress(id: config.helper.address.id ?? "" ,
						  street: config.helper.address.street ?? "",
						  house:  config.helper.address.house ?? "",
						  apartment: config.helper.address.apartment,
						  floor: config.helper.address.floor,
						  entrance: config.helper.address.entrance,
							  door_code: config.helper.address.door_code ,
							  location_point: config.helper.address.location_point! ,
							  comment: config.helper.address.comment )
		}
		else
		{
			saveAddress(street: config.helper.address.street ??  "",
					house: config.helper.address.house  ?? "-",
					apartment: (apartmentField.text!.count > 0 ? apartmentField.text : nil),
					floor: (floorField.text!.count > 0 ? floorField.text : nil),
					entrance: (entranceField.text!.count > 0 ? entranceField.text : nil),
					door_code: (intercomField.text!.count > 0 ? intercomField.text : nil),
					location_point: LocationPoint(
						latitude: config.helper.address.location_point?.latitude,
						longitude: config.helper.address.location_point?.longitude),
					comment:  (commentField.text!.count > 0 ? commentField.text : nil) )
		}
	}
	
	@IBAction func showCatalog()
	{
		config.helper.navigationToScreen()
		managers.webService.updateBring(isBring: false)
		showCatalogBtn.isUserInteractionEnabled = false
	}
}

//MARK: - Setting values
extension WhereBringViewController
{
	
	func uploadConstraint()
	{
		if UIScreen.main.bounds.width == Constant.heightScreen
		{
			heightMoreDetailsViewContraint.constant = Constant.heightMoreDetailsView
			horizontalStackView.axis = .vertical
			heightGeneralViewContraint.constant += Constant.defaultHeightView
			widthEntranceContraint.constant = Constant.widthView
			widthIntercomContraint.constant = Constant.widthView
			widthFloorContraint.constant = Constant.widthView
		}
		else
		{
		}
	}
	
	func setupUI()
	{
		config.helper.address = config.helper.setupAddress()
		setupAddressView()
		setupBorderColorAllView(active: false)
		setupTextFieldDelegate()
		startTrackingLocation()
		registredCell()
		showCatalogBtn.setTitle(config.helper.titleBtn, for: .normal)
		loadingConsumer()
		setupDataField()
	}
	
	func checkStatusGeo()
	{
		if CLLocationManager.locationServicesEnabled()
		{
			switch CLLocationManager.authorizationStatus()
			{
			case .authorizedAlways, .authorizedWhenInUse,.notDetermined:
				print("Access")
			case .denied,.restricted:
				fallthrough
			@unknown default:
				print("No access")
				config.helper.presentPopupOverScreen()
			}
		}
		else
		{
			print("Location services are not enabled")
		}
	}

	
	func updatingOptionalFieldsDevileryAddress()
	{
		config.helper.address.apartment = apartmentField.text!.count > 0 ? apartmentField.text : nil
		config.helper.address.floor = floorField.text!.count > 0 ? floorField.text : nil
		config.helper.address.entrance = entranceField.text!.count > 0 ? entranceField.text : nil
		config.helper.address.door_code = intercomField.text!.count > 0  ? intercomField.text : nil
		config.helper.address.comment = commentField.text!.count > 0  ? commentField.text : nil
	}
	
	func loadingConsumer()
	{
		switch managers.webService.currentUserProfile?.profile?.role
		{
		case .consumer(let consumer):
			self.consumer = consumer
		case .seller(_):
			print("")
		default:
			print("")
		}
	}
	
	func registredCell()
	{
		searchAddressTable.registerNibCell(WhereBringTableViewCell.self)
	}
	
	func stateSaveBtn()
	{
		if addressViewContainer.streetAndHouseField.text != ""
		{
			saveBtn.style = "orange"
			saveBtn.isEnabled = true 
		}
		else
		{
			saveBtn.style = "gray"
			saveBtn.isEnabled = false
		}
	}
	
	func startTrackingLocation()
	{
			managers.locationManager.obtainLocation().done { [weak self] location in
				self?.locationCoordinate = location.coordinate
				self?.onCoordinateUpdated(location.coordinate)
			}.cauterize()
	}
	
	func onCoordinateUpdated(_ coordinate: CLLocationCoordinate2D)
	{
		if !isEnablelocation
		{
			getAddress(of: coordinate)
		}
	}
	
	func updateAddress(id:String,
					   street:String,
					   house:String,
					   apartment:String?,
					   floor:String?,
					   entrance:String?,
					   door_code:String?,
					   location_point:LocationPoint,
					   comment:String?)
	{
		config.helper.updateDeliveryAddress(id: id,
											street: street,
											house: house,
											apartment: apartment,
											floor: floor,
											entrance: entrance,
											door_code: door_code,
											location_point: location_point,
											comment: comment)
			.get
			{ _ in
				managers.webService.updateBring(isBring: false)
			
			}
			.catch
			{ _ in
				self.saveBtn.isUserInteractionEnabled = true
			}
	}
	
	func saveAddress(street:String,
					 house:String,
					 apartment:String?,
					 floor:String?,
					 entrance:String?,
					 door_code:String?,
					 location_point:LocationPoint,
					 comment:String?)
	{
		config.helper.createDeliveryAddress(
			street: street,
			house: house,
			apartment: apartment,
			floor: floor,
			entrance: entrance,
			door_code: door_code,
			location_point: location_point,
			comment: comment)
			.get
			{ _ in
				managers.webService.updateBring(isBring: false)
			}
			.catch
			{ _ in
				self.saveBtn.isUserInteractionEnabled = true
			}
	}
	
	func getAddress(of coordinate: CLLocationCoordinate2D)
	{
			
		_ = managers.geoCodingManager.getAddress(at: coordinate)
			.done
			{ address in
				self.config.helper.updateAddress(street: address.street ?? "", house: address.house ?? "", lat: address.lat ?? 0.0, lng: address.lng ?? 0.0)
				self.addressViewContainer.streetAndHouseField.text = self.getString(for: address)
				self.stateSaveBtn()
			}
			.catch
		{
			_ in

		}
	}
	
	private func getString(for address: Address) -> String
	{
		guard let addressHouse = address.house, let addressStreet = address.street
		else
		{
			return ""
		}
		return "\(addressStreet),\(addressHouse)"
	}
	
	func setupTextFieldDelegate()
	{
		entranceField.delegate  = self
		intercomField.delegate  = self
		floorField.delegate     = self
		entranceField.delegate  = self
		apartmentField.delegate = self
		commentField.delegate   = self
		addressViewContainer.streetAndHouseField.delegate = self
		addressViewContainer.streetAndHouseField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
	}
	
	func setupAddressView()
	{
		if addressViewContainer == nil
		{
			let viewToEmbed = AddressView.create(
				nameLbl: "Улица и дом",
				image: "location",
				text: getAddressConsumer(),
				type: .availableTextField,
				onPress: nil)
			self.continerAddressView.addSubview(viewToEmbed)
			
			viewToEmbed.edgesToSuperview()
			
			self.addressViewContainer = viewToEmbed
		}
		else
		{
			addressViewContainer.setupAppearance(
				nameLbl: "Улица и дом",
				image: "location",
				text: getAddressConsumer(),
				type: .availableTextField,
				onPress: nil)
		}
		checkstreetAndHouseField()
		tapRec.addTarget(self, action: #selector(tappedLocationImage))
		addressViewContainer.picture.addGestureRecognizer(tapRec)
		addressViewContainer.picture.isUserInteractionEnabled = true
		self.stateSaveBtn()
	}
	func checkstreetAndHouseField()
	{
		if addressViewContainer.streetAndHouseField.text != ""
		{
			isEnablelocation = true
		}
		else
		{
			isEnablelocation = false
		}
	}
	@objc func tappedLocationImage()
	{
		isEnablelocation = false
		searchAddressTable.isHidden = true
		managers.locationManager.startUpdatingLocation()
		if managers.locationManager.currentLocation != nil
		{

			onCoordinateUpdated(managers.locationManager.currentLocation!)
		}
		else if managers.locationManager.lastUpdatedLocation?.coordinate != nil
		{
			onCoordinateUpdated(locationCoordinate)
		}
		else if CLLocationManager.authorizationStatus() == .notDetermined
		{
			managers.locationManager.startUpdatingLocation()
		}
		else
		{
			let alertController = UIAlertController(title: "Требуется разрешить получения гео-данных", message: "Пожалуйста разрешите работу с гео-данными в настройках.", preferredStyle: UIAlertController.Style.alert)
			let okAction = UIAlertAction(title: "Настройки",
										 style: .default,
										 handler: {
											(cAlertAction) in
							//Redirect to Settings app
											UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!, completionHandler:{ (success) in
												if success
												{
													managers.locationManager.startUpdatingLocation()
													//self.startTrackingLocation()
													//self.locationCoordinate = managers.locationManager.lastUpdatedLocation?.coordinate
												}
											})
										 })
						
			let cancelAction = UIAlertAction(title: "Отмена", style: UIAlertAction.Style.cancel)
			alertController.addAction(cancelAction)
			alertController.addAction(okAction)
			self.present(alertController, animated: true, completion: nil)
			
		}
	}
	func getAddressConsumer() -> String
	{
		if managers.webService.deliveryAddress != nil
		{
			let street = managers.webService.deliveryAddress?.street
			let house = managers.webService.deliveryAddress?.house
			return "\(street ?? ""), \(house ?? "")"
		}
		else if managers.webService.currentUserProfile?.profile?.role != nil
		{
			switch managers.webService.currentUserProfile?.profile?.role
			{
			case .consumer(let consumer):
				if consumer?.deliveryAddresses?.last?.street != nil && consumer?.deliveryAddresses?.last?.house != nil
				{
					let street = consumer?.deliveryAddresses?.last?.street
					let house = consumer?.deliveryAddresses?.last?.house
					return "\(street ?? ""), \(house ?? "")"
				}
				else
				{
					return ""
				}
			case .seller(_):
				return ""
			default:
				return ""
			}
		}
		return ""
	}
	
	func setupDataField()
	{
		if config.helper.address.id != nil
		{
			apartmentField.text 	= config.helper.address.apartment
			floorField.text 		= config.helper.address.floor
			entranceField.text 		= config.helper.address.entrance
			intercomField.text 		= config.helper.address.door_code
			commentField.text 		= config.helper.address.comment
		}
		else
		{
			apartmentField.text 	= ""
			floorField.text 		= ""
			entranceField.text 		= ""
			intercomField.text 		= ""
			commentField.text 		= ""
		}
	}
}

extension WhereBringViewController
{
	func setupBorderColorAllView(active:Bool)
	{
		commentView.layer.borderColor = getBorderColor(active: active).cgColor
		entranceView.layer.borderColor = getBorderColor(active: active).cgColor
		intercomView.layer.borderColor = getBorderColor(active: active).cgColor
		floorView.layer.borderColor = getBorderColor(active: active).cgColor
		entranceView.layer.borderColor = getBorderColor(active: active).cgColor
	    apartmentView.layer.borderColor = getBorderColor(active: active).cgColor
		continerAddressView.layer.borderColor = getBorderColor(active: active).cgColor
	}
	
	func getBorderColor(active:Bool) -> UIColor
	{
		if active
		{
			return UIColor(red: 255/255, green: 122/255, blue: 0/255, alpha: 1)
		}
		else
		{
			return UIColor.clear
		}
	}
	
	func setupBorderView(view:UIView,active:Bool)
	{
		view.layer.borderColor = getBorderColor(active: active).cgColor
	}
	
	func getPlace(place:PlaceAutocompleteResult, language:String)
	{
		managers.geoCodingManager.getPlace(with: place.googlePlaceId!, language: language)
			.ensure
			{
				print("")
			}
			.done
			{
				self.config.helper.updateAddress(street: $0.name ?? "", house: $0.house ?? "", lat: $0.coordinates.latitude ?? 0.0, lng: $0.coordinates.longitude ?? 0.0)
				self.stateSaveBtn()
			}
			.catch
			{
				print($0)
			}
	}
}

//MARK: - UITextFieldDelegate
extension WhereBringViewController:UITextFieldDelegate
{
	func textFieldDidBeginEditing(_ textField: UITextField)
	{
		if textField == addressViewContainer.streetAndHouseField
		{
			setupBorderView(view: continerAddressView, active: true)
		}
		else if textField == entranceField
		{
			setupBorderView(view: entranceView, active: true)
		}
		else if textField == intercomField
		{
			setupBorderView(view: intercomView, active: true)
		}
		else if textField == floorField
		{
			setupBorderView(view: floorView, active: true)
		}
		else if textField == apartmentField
		{
			setupBorderView(view: apartmentView, active: true)
		}
		else if textField == commentField
		{
			setupBorderView(view: commentView, active: true)
		}
	}
	
	func textFieldDidEndEditing(_ textField: UITextField)
	{
		if textField == addressViewContainer.streetAndHouseField
		{
			setupBorderView(view: continerAddressView, active: false)
		}
		else if textField == entranceField
		{
			setupBorderView(view: entranceView, active: false)
		}
		else if textField == intercomField
		{
			setupBorderView(view: intercomView, active: false)
		}
		else if textField == floorField
		{
			setupBorderView(view: floorView, active: false)
		}
		else if textField == apartmentField
		{
			setupBorderView(view: apartmentView, active: false)
		}
		else if textField == commentField
		{
			setupBorderView(view: commentView, active: false)
		}
		stateSaveBtn()
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool
	{
		if textField == addressViewContainer.streetAndHouseField
		{
			setupBorderView(view: continerAddressView, active: false)
			entranceField.becomeFirstResponder()
		}
		else if textField == entranceField
		{
			setupBorderView(view: entranceView, active: false)
			intercomField.becomeFirstResponder()
		}
		else if textField == intercomField
		{
			setupBorderView(view: intercomView, active: false)
			floorField.becomeFirstResponder()
		}
		else if textField == floorField
		{
			setupBorderView(view: floorView, active: false)
			apartmentField.becomeFirstResponder()
		}
		else if textField == apartmentField
		{
			setupBorderView(view: apartmentView, active: false)
			commentField.becomeFirstResponder()
		}
		else if textField == commentField
		{
			setupBorderView(view: commentView, active: false)
			commentField.resignFirstResponder()
		}
		stateSaveBtn()
		return true
	}
	
	@objc func textFieldDidChange()
	{
		let  currentText = addressViewContainer.streetAndHouseField.text!
		
		if currentText.count > 2
		{
			searchAddressTable.isHidden = false
			managers.geoCodingManager.getPlaces(
				for: currentText,
				nearestTo: City.defaultCity.coordinates,
				language: Constant.language)
				.ensure
				{
					print("")
				}
				.done
				{
					self.places = $0.address
					self.searchAddressTable.reloadData()
				}
				.catch
				{
					print($0)
				}
		}
		else if currentText.count == 0
		{
			searchAddressTable.isHidden = true
			stateSaveBtn()
		}
	}
}

extension WhereBringViewController: UITableViewDelegate, UITableViewDataSource
{
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return places.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		let cell = searchAddressTable.dequeueReusableCell(withIdentifier: String(describing: WhereBringTableViewCell.self)) as! WhereBringTableViewCell
		cell.setup(address: places[indexPath.row].name)
		return cell
	}
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		addressViewContainer.streetAndHouseField.text = places[indexPath.row].name!
		getPlace(place: places[indexPath.row], language: Constant.language)
		searchAddressTable.isHidden = true
	}
}
extension WhereBringViewController
{
	struct Constant
	{
		static let heightScreen:          CGFloat = 320.0
		static let heightMoreDetailsView: CGFloat = 112.0
		static let widthMoreDetailsView:  CGFloat = 343.0
		static let defaultHeightView:     CGFloat = 64.0
		static let paddingView:           CGFloat = 8.0
		static let widthView:             CGFloat = (widthMoreDetailsView - paddingView) / 2
		static let language:              String  = "ru"
	}
}
