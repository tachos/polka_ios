//
//  WhereBringTableViewCell.swift
//  Polka
//
//  Created by Makson on 13.05.2021.
//

import UIKit

class WhereBringTableViewCell: UITableViewCell
{

    @IBOutlet weak var nameAddressLbl: UILabel!
	
}
extension WhereBringTableViewCell
{
	func setup(address:String?)
	{
		guard let address = address
		else
		{
			return
		}
		nameAddressLbl.text = address
	}
}
