//
//  SplashViewController.swift
//  Polka
//
//  Created by Makson on 21.06.2021.
//

import UIKit


class SplashViewController: UIViewController, StoryboardCreatable
{
	static var storyboardName: StoryboardName = .Splash
	
	// MARK: Config
	var config: Config!
	
	struct Config
	{
		
		let helper: SplashHelper
	}
}

extension SplashViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		config.helper.getProfile()
	}
}

