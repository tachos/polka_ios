//
//  OnboardingPage0ViewController.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 04.06.2021.
//

import UIKit


class OnboardingPage0ViewController
	: UIViewController
	, StoryboardCreatable
{
	// MARK: StoryboardCreatable
	static let storyboardName: StoryboardName = .onboarding
}


// MARK: - OnboardingPage

extension OnboardingPage0ViewController: OnboardingPage
{}
