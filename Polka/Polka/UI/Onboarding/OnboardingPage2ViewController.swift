//
//  OnboardingPage2ViewController.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 04.06.2021.
//

import UIKit


class OnboardingPage2ViewController
	: UIViewController
	, StoryboardCreatable
{
	// MARK: StoryboardCreatable
	static let storyboardName: StoryboardName = .onboarding
}


// MARK: - OnboardingPage

extension OnboardingPage2ViewController: OnboardingPage
{}
