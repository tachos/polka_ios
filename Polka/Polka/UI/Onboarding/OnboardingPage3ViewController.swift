//
//  OnboardingPage3ViewController.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 04.06.2021.
//

import UIKit


class OnboardingPage3ViewController
	: UIViewController
	, StoryboardCreatable
{
	// MARK: StoryboardCreatable
	static let storyboardName: StoryboardName = .onboarding
	
	// MARK: Outlets
	@IBOutlet private var leftFadingImageView: UIImageView!
}


// MARK: - OnboardingPage

extension OnboardingPage3ViewController: OnboardingPage
{
	func setTransitionAlpha(_ alpha01: CGFloat)
	{
		leftFadingImageView.alpha = alpha01
	}
	
	func fadeIn()
	{
		fadeInAnimated(views: [leftFadingImageView])
	}

	func fadeOut()
	{
		fadeOutAnimated(views: [leftFadingImageView])
	}
}
