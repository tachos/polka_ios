//
//  OnboardingViewController.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 04.06.2021.
//

import UIKit


protocol OnboardingPage: UIViewController
{
	func setTransitionAlpha(_ alpha01: CGFloat)
	
	func fadeIn()
	func fadeOut()
}

extension OnboardingPage
{
	func setTransitionAlpha(_ alpha01: CGFloat) {}
	
	func fadeIn() {}
	func fadeOut() {}
}


class OnboardingViewController
	: UIViewController
	, StoryboardCreatable
{
	// MARK: StoryboardCreatable
	static let storyboardName: StoryboardName = .onboarding
	
	// MARK: Outlets
	@IBOutlet private var pagesScrollView: UIScrollView!
	@IBOutlet private var contentStackView: UIStackView!
	@IBOutlet private var pageControl: UIPageControl!
	@IBOutlet private var goToNextPageButton: UIButton!
	@IBOutlet private var bottomButton: UIButton!
	
	private var pageViewControllers: [OnboardingPage]!
	
	// MARK: Config
	struct Config
	{
		let pageViewControllers: [OnboardingPage]
		let startButtonSegue: VoidCallback
	}
	var config: Config!
	
	
	override func viewDidLoad()
	{
		super.viewDidLoad()

		setupUI()
	}
}


// MARK: - Setup

private extension OnboardingViewController
{
	func setupUI()
	{
		pageViewControllers = config.pageViewControllers
		
		for viewController in pageViewControllers
		{
			addChild(viewController)
			
			contentStackView.addArrangedSubview(viewController.view)
			viewController.view.width(to: self.view)
			viewController.view.clipsToBounds = false
			
			viewController.setTransitionAlpha(0)
		}
		
		pageControl.numberOfPages = pageViewControllers.count
	}
}


// MARK: - UIScrollViewDelegate

extension OnboardingViewController: UIScrollViewDelegate
{
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
	{
		let currPageIndex = currentScrollPageIndex()
		
		let currPageVC = pageViewControllers[currPageIndex]
		currPageVC.fadeIn()
	}

	func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
	{
		let currPageIndex = currentScrollPageIndex()
		
		let currPageVC = pageViewControllers[currPageIndex]
		currPageVC.fadeIn()
	}
	
	func scrollViewDidScroll(_ scrollView: UIScrollView)
	{
		let currPageIndex = currentScrollPageIndex()
		
		pageControl.currentPage = currPageIndex

		//
		let isLastPage = currPageIndex >= pageViewControllers.count - 1
		goToNextPageButton.setTitle(
			isLastPage ? "Начать" : "Далее",
			for: .normal
		)
		
		let currPageVC = pageViewControllers[currPageIndex]
		currPageVC.fadeOut()
	}
	
	private func updatePageProgress(pageIndex: Int)
	{
		let viewportWidth = self.view.frame.width
		
		let pageVC = pageViewControllers[pageIndex]
		
		let pageCenterX = (pageVC.view.frame.minX + pageVC.view.frame.maxX)*0.5
		
		let scrollCenter = pagesScrollView.contentOffset.x + viewportWidth*0.5
		let diff = abs(pageCenterX - scrollCenter)

		// diff = 0 => progress = 1
		let alpha = 1 - (diff / viewportWidth*0.5)*2
		pageVC.setTransitionAlpha(alpha.clamped(to: 0...1))
	}
}


// MARK: - Handling scroll progress

private extension OnboardingViewController
{
	func currentScrollPageIndex() -> Int
	{
		let normalizedProgress = normalizeScrollProgress(
			currentScrollProgress()
		)
		return Int(round(normalizedProgress))
			.clamped(
				to: 0...(pageViewControllers.count - 1)
			)
	}
	
	func currentScrollProgress() -> CGFloat
	{
		let contentWidth = pagesScrollView.contentSize.width
		
		// scroll content may be not layed out yet
		guard contentWidth > 0
		else
		{
			return 0
		}
		
		guard pageViewControllers.count > 1
		else
		{
			return 0
		}
		
		let pageWidth = contentWidth / CGFloat(pageViewControllers.count)
		return pagesScrollView.contentOffset.x
			/ (pageWidth * CGFloat(pageViewControllers.count - 1))
	}
	
	func normalizeScrollProgress(_ progress: CGFloat) -> CGFloat
	{
		return progress * CGFloat(pageViewControllers.count - 1)
	}
}


// MARK: - Handle UI

private extension OnboardingViewController
{
	@IBAction func onGoToNextPageButton()
	{
		let currPageIndex = currentScrollPageIndex()

		if currPageIndex < pageViewControllers.count - 1
		{
			let nextPageVC = pageViewControllers[currPageIndex + 1]
			pagesScrollView.scrollRectToVisible(
				nextPageVC.view.frame,
				animated: true
			)
		}
		else
		{
			config.startButtonSegue()
		}
	}
}


private extension Comparable
{
	func clamped(to range: ClosedRange<Self>) -> Self
	{
		let min = range.lowerBound, max = range.upperBound

		return self < min ? min : (max < self ? max : self)
	}
}



func fadeInAnimated(views: [UIView])
{
	setAlphaAnimated(
		views: views,
		alpha: 1,
		duration: 0.5
	)
}

func fadeOutAnimated(views: [UIView])
{
	views.forEach { $0.alpha = 0 }
}

private func setAlphaAnimated(
	views: [UIView],
	alpha: CGFloat,
	duration: TimeInterval = 0.5
)
{
	UIView.animate(
		withDuration: 0.5,
		animations: {
			views.forEach { $0.alpha = alpha }
		},
		completion: nil
	)
}
