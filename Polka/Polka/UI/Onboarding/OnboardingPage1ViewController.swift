//
//  OnboardingPage1ViewController.swift
//  Polka
//
//  Created by Vyacheslav Shakaev on 04.06.2021.
//

import UIKit


class OnboardingPage1ViewController
	: UIViewController
	, StoryboardCreatable
{
	// MARK: StoryboardCreatable
	static let storyboardName: StoryboardName = .onboarding
	
	// MARK: Outlets
	@IBOutlet private var leftFadingImageView: UIImageView!
	@IBOutlet private var rightFadingImageView: UIImageView!
}


// MARK: - OnboardingPage

extension OnboardingPage1ViewController: OnboardingPage
{
	func setTransitionAlpha(_ alpha01: CGFloat)
	{
		leftFadingImageView.alpha = alpha01
		rightFadingImageView.alpha = alpha01
	}
	
	func fadeIn()
	{
		fadeInAnimated(views: [leftFadingImageView, rightFadingImageView])
	}

	func fadeOut()
	{
		fadeOutAnimated(views: [leftFadingImageView, rightFadingImageView])
	}
}
